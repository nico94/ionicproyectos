webpackJsonp([0],{

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuPrincipalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuPrincipalComponent = /** @class */ (function () {
    function MenuPrincipalComponent(platform, viewControl, nav, app, alertCtrl, eventos, toast) {
        this.platform = platform;
        this.viewControl = viewControl;
        this.nav = nav;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.eventos = eventos;
        this.toast = toast;
        if (this.platform.is('cordova')) {
            this.esDispositivo = true;
        }
        else {
            this.esDispositivo = false;
        }
        this.items = [
            { id: 1, titulo: 'Cerrar sesión' }
        ];
    }
    MenuPrincipalComponent.prototype.clickItem = function (item) {
        console.log(item.id);
        switch (item.id) {
            case 1: {
                this.mostrarAlerta();
                break;
            }
            default: {
                break;
            }
        }
        this.viewControl.dismiss(item);
    };
    MenuPrincipalComponent.prototype.mostrarAlerta = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Cerrar sesión',
            message: '¿Está seguro que quiere cerrar la sesión?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancela');
                    }
                }, {
                    text: 'Si',
                    handler: function () {
                        _this.cerrarSesion();
                    }
                }
            ]
        });
        alert.present();
    };
    MenuPrincipalComponent.prototype.cerrarSesion = function () {
        if (this.esDispositivo) {
            localStorage.removeItem('login');
            localStorage.removeItem('cod_cargo');
            localStorage.removeItem('ejecutivo');
            this.eventos.publish('verifica_ruta');
        }
        else {
            localStorage.removeItem('login');
            localStorage.removeItem('cod_cargo');
            localStorage.removeItem('ejecutivo');
            this.eventos.publish('verifica_ruta');
        }
    };
    MenuPrincipalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'menu-principal',template:/*ion-inline-start:"C:\ionicproyectos\parqueCopiapo\src\components\menu-principal\menu-principal.html"*/'<ion-list no-lines>\n\n  <ion-item *ngFor="let item of items" (click)="clickItem(item)">\n\n    {{item.titulo}}\n\n  </ion-item>\n\n</ion-list>'/*ion-inline-end:"C:\ionicproyectos\parqueCopiapo\src\components\menu-principal\menu-principal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["A" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* ToastController */]])
    ], MenuPrincipalComponent);
    return MenuPrincipalComponent;
}());

//# sourceMappingURL=menu-principal.js.map

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModNotaIngresoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__modelos_girosEmpleadorRequest__ = __webpack_require__(515);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modelos_actividadRequest__ = __webpack_require__(516);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modelos_parentescoRequest__ = __webpack_require__(517);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modelos_necesidadRequest__ = __webpack_require__(518);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modelos_presionRequest__ = __webpack_require__(519);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modelos_clienteRequest__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modelos_ciudadRequest__ = __webpack_require__(520);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modelos_sexoRequest__ = __webpack_require__(521);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__modelos_civilRequest__ = __webpack_require__(522);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__modelos_sectorRequest__ = __webpack_require__(523);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__modelos_comunaRequest__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_webservice_webservice__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__mod_nota_ingreso_empleador_mod_nota_ingreso_empleador__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_chilean_rut__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_chilean_rut___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_chilean_rut__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

//MODELOS











//MODULOS





var ModNotaIngresoPage = /** @class */ (function () {
    function ModNotaIngresoPage(toast, navCtrl, navParams, viewCtrl, alertCtrl, modalCtrl, webService, eventos) {
        this.toast = toast;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.webService = webService;
        this.eventos = eventos;
        this.fechaNacimiento = new Date().toISOString();
        this.cliente = {};
        this.clienteEncontrado = false;
        //Carga de datos
        this.comunas = [];
        this.ciudades = [];
        this.sectores = [];
        this.actividades = [];
        this.estadosCiviles = [];
        this.sexo = [];
        this.girosEmpleador = [];
        this.parentesco = [];
        this.necesidades = [];
        this.presionArterial = [];
        this.acreditacion = [];
        this.botonDescripcion = "Guardar";
        //Arreglos para nota
        this.empleador = {};
        this.aval = {};
        this.detalle = {};
        this.planimetria = {};
        this.salud = {};
        this.nota = {
            notaVenta: {
                cliente: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    sector: null,
                    actividad: null,
                    ciudad: null,
                    civil: null,
                    correo: null,
                    telefono: null,
                    celular: null,
                    fechaNacimiento: null,
                    edad: null,
                    sexo: null
                },
                empleador: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    actividad: null,
                    ciudad: null,
                    acreditacion: [],
                    renta: null,
                    giro: null,
                    telefono: null,
                    otrosIngresos: []
                },
                aval: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    sector: null,
                    actividad: null,
                    ciudad: null,
                    civil: null,
                    correo: null,
                    telefono: null,
                    celular: null,
                    fechaNacimiento: null,
                    edad: null,
                    sexo: null,
                    renta: null,
                    parentesco: null
                },
                detalle: {
                    necesidad: null,
                    planimetria: {
                        descripcion: null,
                        letra: null,
                        estado: null,
                        sector: 0,
                        porc_descuento: 0,
                        uf: 0,
                        fila: null,
                        latitud: 0,
                        longitud: 0,
                        numero: 0,
                        valor_base: 0,
                        capacidades: 0,
                        reducciones: 0,
                        pie: 0
                    },
                    descuento: null,
                    pie: 0,
                    dia_pago: 0,
                    vencimiento: null,
                    cuotas: null,
                    valor_cuotas: null,
                    uf: null,
                    comentario: null
                },
                salud: {
                    responde: false,
                    peso: 0,
                    estatura: 0,
                    presion: null,
                    ejecutiva: null,
                    comentario: null,
                    firma: null
                }
            }
        };
        this.obtener();
    }
    ModNotaIngresoPage.prototype.ionViewLoaded = function () { };
    ModNotaIngresoPage.prototype.ionViewDidLoad = function () {
        this.cargaDatosWebService();
    };
    ModNotaIngresoPage.prototype.seleccionaComuna = function (cod_ciudad) {
        var _this = this;
        this.webService.obtenerListaComunas(new __WEBPACK_IMPORTED_MODULE_11__modelos_comunaRequest__["a" /* ComunaRequestModel */](-1, cod_ciudad))
            .subscribe(function (data) {
            var listado = data['ListadoComunaResult']['lista'];
            _this.comunas = listado;
        }, function (error) {
            _this.comunas = null;
            console.log('Error comunas WS: ' + error);
        });
    };
    ModNotaIngresoPage.prototype.cargaDatosWebService = function () {
        var _this = this;
        //Lista de sexo
        this.webService.obtenerListaSexo(new __WEBPACK_IMPORTED_MODULE_8__modelos_sexoRequest__["a" /* SexoRequestModel */](-1))
            .subscribe(function (data) {
            var listado = data['ListadoSexoResult']['lista'];
            _this.sexo = listado;
        }, function (error) {
            _this.sexo = null;
            console.log('Error sexo WS: ' + error);
        });
        //Lista de sectores
        this.webService.obtenerListaSector(new __WEBPACK_IMPORTED_MODULE_10__modelos_sectorRequest__["a" /* SectorRequestModel */](-1))
            .subscribe(function (data) {
            var listado = data['ListadoSectorResult']['lista'];
            _this.sectores = listado;
        }, function (error) {
            _this.sectores = null;
            console.log('Error carga Sectores ' + error);
        });
        //Lista Estados civiles
        this.webService.obtenerListaEstadoCivil(new __WEBPACK_IMPORTED_MODULE_9__modelos_civilRequest__["a" /* CivilRequestModel */](-1))
            .subscribe(function (data) {
            var listado = data['ListadoEstadoCivilResult']['lista'];
            _this.estadosCiviles = listado;
        }, function (error) {
            _this.estadosCiviles = null;
            console.log('Error estados civiles WS: ' + error);
        });
        //Lista de actividades
        this.webService.obtenerListaActividades(new __WEBPACK_IMPORTED_MODULE_2__modelos_actividadRequest__["a" /* ActividadRequestModel */](-1))
            .subscribe(function (data) {
            var listado = data['ListadoCargoResult']['lista'];
            _this.actividades = listado;
        }, function (error) {
            _this.actividades = null;
            console.log('Error actividades WS: ' + error);
        });
        //Lista de ciudades
        this.webService.obtenerListaCiudades(new __WEBPACK_IMPORTED_MODULE_7__modelos_ciudadRequest__["a" /* CiudadRequestModel */](-1))
            .subscribe(function (data) {
            var listado = data['ListadoCiudadResult']['lista'];
            _this.ciudades = listado;
        }, function (error) {
            _this.ciudades = null;
            console.log('Error ciudad WS: ' + error);
        });
        //Giros Empleador
        this.webService.obtenerGirosEmpleador(new __WEBPACK_IMPORTED_MODULE_1__modelos_girosEmpleadorRequest__["a" /* GirosEmpeadorRequest */](-1))
            .subscribe(function (data) {
            var lista = data['ListadoGiroResult']['lista'];
            _this.girosEmpleador = lista;
        }, function (error) {
            _this.girosEmpleador = null;
            console.log('Error giros empleador WS: ' + error);
        });
        //Lista parentescos
        this.webService.obtenerListaParentesco(new __WEBPACK_IMPORTED_MODULE_3__modelos_parentescoRequest__["a" /* ParentescoRequestModel */](-1))
            .subscribe(function (data) {
            var lista = data['ListadoParentescoResult']['lista'];
            _this.parentesco = lista;
        }, function (error) {
            _this.parentesco = null;
        });
        //Lista de acreditacion
        this.webService.obtenerListaAcreditacion()
            .subscribe(function (data) {
            var lista = data['TipoAcreditacionResult']['lista'];
            _this.acreditacion = lista;
        }, function (error) {
            _this.acreditacion = null;
        });
        //Lista necesidades
        this.webService.obtenerListaNecesidades(new __WEBPACK_IMPORTED_MODULE_4__modelos_necesidadRequest__["a" /* NecesidadRequestModel */](-1))
            .subscribe(function (data) {
            var lista = data['ListadoTipoNecesidadResult']['lista'];
            _this.necesidades = lista;
        }, function (error) {
            _this.necesidades = null;
        });
        //Lista de presion arterial
        this.webService.obtenerListaPresionArterial(new __WEBPACK_IMPORTED_MODULE_5__modelos_presionRequest__["a" /* PresionRequestModel */](-1))
            .subscribe(function (data) {
            var lista = data['ListadoPresionArterialResult']['lista'];
            _this.presionArterial = lista;
        }, function (error) {
            _this.presionArterial = null;
        });
    };
    ModNotaIngresoPage.prototype.verificaCliente = function () {
        return !this.clienteEncontrado;
    };
    ModNotaIngresoPage.prototype.buscarDatosClienteRut = function (rut) {
        var _this = this;
        this.webService.obtenerDatosCliente(rut)
            .subscribe(function (data) {
            var lista = data['ListadoClienteResult']['lista'];
            var realizado = data['ListadoClienteResult']['realizado'];
            console.log('CLIENTE: ' + JSON.stringify(lista));
            if (realizado) {
                _this.clienteEncontrado = false;
                _this.cliente.nombre = lista[0]['des_nombre_cliente'];
                _this.cliente.direccion = lista[0]['des_direccion_cliente'];
                _this.cliente.sector = lista[0]['cod_sector_sector'];
                _this.cliente.actividad = lista[0]['cod_cargo_cargo'];
                _this.cliente.ciudad = lista[0]['cod_ciudad_ciudad'];
                _this.seleccionaComuna(_this.cliente.ciudad);
                _this.cliente.comuna = lista[0]['cod_comuna_comuna'];
                _this.cliente.civil = lista[0]['cod_estadocivil_estadocivil'];
                _this.cliente.correo = lista[0]['des_mail_cliente'];
                _this.cliente.telefono = lista[0]['des_telefono_cliente'];
                _this.cliente.celular = lista[0]['des_telefono_celular'];
                var fechaFormateada = new Date(parseInt(lista[0]['dat_fecha_nacimiento'].replace(/(^.*\()|([+-].*$)/g, ''))).toISOString();
                // console.log("fechaFormateada " + fechaFormateada);
                _this.cliente.fechaNacimiento = fechaFormateada;
                _this.cliente.edad = lista[0]['edad'];
                _this.cliente.sexo = lista[0]['cod_sexo_sexo'];
                _this.botonDescripcion = "Modificar Cliente";
            }
            else {
                _this.botonDescripcion = "Crear Cliente";
                _this.clienteEncontrado = true;
            }
        }, function (error) {
            _this.clienteEncontrado = false;
            console.log('ERROR DATOS CLIENTE ' + JSON.stringify(error));
        });
    };
    ModNotaIngresoPage.prototype.foco = function () {
        if (this.cliente.rut) {
            if (this.cliente.rut.length !== 12) {
                this.cliente.rut = "";
            }
        }
    };
    ModNotaIngresoPage.prototype.formatoRut = function () {
        if (this.cliente.rut) {
            var dv = this.cliente.rut.substring(8);
            this.cliente.rut = __WEBPACK_IMPORTED_MODULE_15_chilean_rut___default.a.format(this.cliente.rut, dv);
            if (this.cliente.rut.length == 12) {
                if (!__WEBPACK_IMPORTED_MODULE_15_chilean_rut___default.a.validate(this.cliente.rut)) {
                    this.mostarToast('Rut no válido');
                    this.cliente.rut = "";
                }
                else {
                    var rut = __WEBPACK_IMPORTED_MODULE_15_chilean_rut___default.a.unformat(this.cliente.rut);
                    this.buscarDatosClienteRut(rut);
                }
            }
            else if (this.cliente.rut.length < 12) {
                this.cliente.nombre = "";
                this.cliente.direccion = "";
                this.cliente.ciudad = "";
                this.cliente.comuna = "";
                this.cliente.sector = "";
                this.cliente.actividad = "";
                this.cliente.civil = "";
                this.cliente.correo = "";
                this.cliente.telefono = "";
                this.cliente.celular = "";
                this.cliente.fechaNacimiento = "";
                this.cliente.edad = "";
                this.cliente.sexo = "";
            }
        }
        this.clienteEncontrado = false;
    };
    ModNotaIngresoPage.prototype.obtener = function () {
        var data = this.navParams.get('nota');
        if (data !== undefined) {
            if (data.notaVenta.cliente !== undefined) {
                this.cliente = data.notaVenta.cliente;
                this.seleccionaComuna(this.cliente.ciudad);
            }
            if (data.notaVenta.empleador !== undefined) {
                this.empleador = data.notaVenta.empleador;
            }
            if (data.notaVenta.aval !== undefined) {
                this.aval = data.notaVenta.aval;
            }
            if (data.notaVenta.detalle !== undefined) {
                this.detalle = data.notaVenta.detalle;
            }
            if (data.notaVenta.detalle.planimetria !== undefined) {
                this.planimetria = data.notaVenta.detalle.planimetria;
            }
            if (data.notaVenta.salud !== undefined) {
                this.salud = data.notaVenta.salud;
            }
        }
    };
    ModNotaIngresoPage.prototype.continuar = function () {
        var _this = this;
        this.nota.notaVenta.cliente = this.cliente;
        this.nota.notaVenta.empleador = this.empleador;
        this.nota.notaVenta.aval = this.aval;
        this.nota.notaVenta.detalle = this.detalle;
        this.nota.notaVenta.detalle.planimetria = this.planimetria;
        this.nota.notaVenta.salud = this.salud;
        this.nota.notaVenta.salud.ejecutiva = localStorage.getItem('ejecutivo');
        if (this.nota.notaVenta.cliente.rut && this.nota.notaVenta.cliente.nombre && this.nota.notaVenta.cliente.direccion &&
            this.nota.notaVenta.cliente.comuna && this.nota.notaVenta.cliente.sector && this.nota.notaVenta.cliente.actividad &&
            this.nota.notaVenta.cliente.civil && this.nota.notaVenta.cliente.correo && this.nota.notaVenta.cliente.fechaNacimiento &&
            this.nota.notaVenta.cliente.edad && this.nota.notaVenta.cliente.sexo) {
            var modalEmpleador_1 = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_13__mod_nota_ingreso_empleador_mod_nota_ingreso_empleador__["a" /* ModNotaIngresoEmpleadorPage */], {
                nota: this.nota,
                ciudades: this.ciudades,
                sectores: this.sectores,
                actividades: this.actividades,
                estadosCiviles: this.estadosCiviles,
                sexo: this.sexo,
                girosEmpleador: this.girosEmpleador,
                parentesco: this.parentesco,
                necesidades: this.necesidades,
                presionArterial: this.presionArterial,
                acreditacion: this.acreditacion
            }, { enableBackdropDismiss: false });
            var rut = __WEBPACK_IMPORTED_MODULE_15_chilean_rut___default.a.unformat(this.nota.notaVenta.cliente.rut);
            var vendedor = parseInt(localStorage.getItem('login'));
            var clienteRequest = new __WEBPACK_IMPORTED_MODULE_6__modelos_clienteRequest__["a" /* ClienteRequestModel */](vendedor, rut, this.nota.notaVenta.cliente.comuna, this.nota.notaVenta.cliente.giro, this.nota.notaVenta.cliente.nombre, this.nota.notaVenta.cliente.direccion, this.nota.notaVenta.cliente.telefono, this.nota.notaVenta.cliente.correo, this.nota.notaVenta.cliente.ciudad, this.nota.notaVenta.cliente.sector, this.nota.notaVenta.cliente.civil, this.nota.notaVenta.cliente.celular, this.nota.notaVenta.cliente.fechaNacimiento, this.nota.notaVenta.cliente.edad, this.nota.notaVenta.cliente.sexo, this.nota.notaVenta.cliente.renta, this.nota.notaVenta.cliente.actividad, this.nota.notaVenta.cliente.parentesco);
            if (this.clienteEncontrado) {
                this.webService.guardarCliente(clienteRequest)
                    .subscribe(function (data) {
                    var resultado = data['GuardarClienteResult']['resultado'];
                    console.log('RESPUESTA: ' + JSON.stringify(data));
                    if (resultado) {
                        _this.mostarToast('Cliente nuevo creado correctamente');
                        modalEmpleador_1.present();
                        _this.viewCtrl.dismiss();
                    }
                    else {
                        _this.mostarToast('Error al guardar cliente nuevo');
                    }
                }, function (error) {
                    _this.mostarToast('Error en respuesta del servidor');
                    console.log('Error guardar cliente ' + JSON.stringify(error));
                });
            }
            else {
                //Aqui modificar el cliente
                this.webService.guardarCliente(clienteRequest)
                    .subscribe(function (data) {
                    var resultado = data['GuardarClienteResult']['resultado'];
                    console.log('RESPUESTA: ' + JSON.stringify(data));
                    if (resultado) {
                        _this.mostarToast('Cliente modificado correctamente');
                        modalEmpleador_1.present();
                        _this.viewCtrl.dismiss();
                    }
                    else {
                        _this.mostarToast('Error al modificar cliente');
                    }
                }, function (error) {
                    _this.mostarToast('Error en respuesta del servidor');
                    console.log('ERROR EN EL SERVIDOR ' + JSON.stringify(error));
                });
            }
        }
        else {
            this.mostarToast('Faltan parámetros para cliente');
        }
    };
    ModNotaIngresoPage.prototype.mostarToast = function (mensaje) {
        if (mensaje) {
            var toast = this.toast.create({
                message: mensaje,
                duration: 3000,
                position: 'top'
            });
            toast.present();
        }
    };
    ModNotaIngresoPage.prototype.cerrar = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Cancelar Ingreso',
            message: '¿Está seguro que quiere cancelar el ingreso?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () { }
                }, {
                    text: 'Si',
                    handler: function () {
                        _this.viewCtrl.dismiss();
                        _this.eventos.publish('recarga_notas');
                    }
                }
            ]
        });
        alert.present();
    };
    var _a, _b, _c, _d, _e, _f, _g, _h;
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_14__angular_core__["_9" /* ViewChild */])('rutInput'),
        __metadata("design:type", Object)
    ], ModNotaIngresoPage.prototype, "rutInput", void 0);
    ModNotaIngresoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_14__angular_core__["m" /* Component */])({
            selector: 'page-mod-nota-ingreso',template:/*ion-inline-start:"C:\ionicproyectos\parqueCopiapo\src\pages\mod-nota-ingreso\mod-nota-ingreso.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Ingreso nota de venta</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (tap)="cerrar()">\n\n        <ion-icon name="close"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <form (ngSubmit)="continuar()" #formCliente="ngForm">\n\n    <ion-grid no-padding>\n\n      <ion-row>\n\n        <ion-col width-50>\n\n          <ion-card no-padding>\n\n            <!-- Columna 1 -->\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Rut *</ion-label>\n\n              <ion-input #rutInput type="text" [(ngModel)]="cliente.rut" maxlength="12" name="rut" placeholder="Ingrese rut" (ionChange)=formatoRut()\n\n                (focusout)=foco()></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Nombre *</ion-label>\n\n              <ion-input type="text" [(ngModel)]="cliente.nombre" name="nombre" placeholder="Ingrese nombre" required></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Dirección *</ion-label>\n\n              <ion-input type="text" [(ngModel)]="cliente.direccion" name="direccion" placeholder="Ingrese direccion" required></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Ciudad *</ion-label>\n\n              <ion-select [(ngModel)]="cliente.ciudad" name="civil" no-lines placeholder="Seleccione ciudad"\n\n                cancelText="Cancelar" okText="Ok" required>\n\n                <ion-option [value]="ciu.cod_ciudad_ciudad" (ionSelect)="seleccionaComuna(ciu.cod_ciudad_ciudad)" *ngFor="let ciu of ciudades">{{ ciu.des_nombre_ciudad | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Comuna *</ion-label>\n\n              <ion-select [(ngModel)]="cliente.comuna" name="comuna" no-lines placeholder="Seleccione comuna"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="comuna.cod_comuna_comuna" *ngFor="let comuna of comunas">{{ comuna.des_nombre_comuna | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Sector *</ion-label>\n\n              <ion-select [(ngModel)]="cliente.sector" name="sector" no-lines placeholder="Seleccione zona"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="sec.cod_sector_sector" *ngFor="let sec of sectores">{{ sec.des_sector_sector | uppercase}}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Actividad *</ion-label>\n\n              <ion-select [(ngModel)]="cliente.actividad" name="actividad" no-lines placeholder="Seleccione actividad"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="act.cod_cargo_cargo" *ngFor="let act of actividades">{{ act.des_descripcion_cargo | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n          </ion-card>\n\n        </ion-col>\n\n        <ion-col width-50>\n\n          <ion-card>\n\n            <!-- Columna 2 -->\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Estado Civil *</ion-label>\n\n              <ion-select [(ngModel)]="cliente.civil" name="civil" no-lines placeholder="Seleccione estado civil"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="civil.cod_estadocivil_estadocivil" *ngFor="let civil of estadosCiviles">{{ civil.des_descripcion_estadocivil | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Correo electrónico *</ion-label>\n\n              <ion-input type="email" [(ngModel)]="cliente.correo" name="correo" placeholder="Ingrese correo electrónico"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Teléfono fijo</ion-label>\n\n              <ion-input type="tel" [(ngModel)]="cliente.telefono" name="telefono" placeholder="Ingrese teléfono fijo"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Celular</ion-label>\n\n              <ion-input type="tel" [(ngModel)]="cliente.celular" name="celular" placeholder="Ingrese celular"></ion-input>\n\n            </ion-item>\n\n            <button ion-item item-start outline type="button" icon-end (tap)="picker.open()">\n\n              <ion-icon name="calendar" item-end color="primary"></ion-icon>\n\n              <ion-datetime #picker pickerFormat="DD MMMM YYYY" min="1900" max="2019" cancelText="Cancelar" doneText="Aceptar"\n\n                [(ngModel)]="cliente.fechaNacimiento" name="fecha_nacimiento"></ion-datetime>\n\n              <ion-label>Fecha de nacimiento *</ion-label>\n\n            </button>  \n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Edad *</ion-label>\n\n              <ion-input type="number" [(ngModel)]="cliente.edad" name="edad" placeholder="Ingrese edad"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Sexo *</ion-label>\n\n              <ion-select [(ngModel)]="cliente.sexo" name="sexo" no-lines placeholder="Seleccione sexo"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="sex.cod_sexo_sexo" *ngFor="let sex of sexo">{{ sex.des_descripcion_sexo | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n          </ion-card>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </form>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <button type="submit" ion-button full color="primary" (tap)="formCliente.ngSubmit.emit()">{{ botonDescripcion }}</button>\n\n</ion-footer>'/*ion-inline-end:"C:\ionicproyectos\parqueCopiapo\src\pages\mod-nota-ingreso\mod-nota-ingreso.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["y" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["y" /* ToastController */]) === "function" ? _a : Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["t" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["t" /* NavController */]) === "function" ? _b : Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["u" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["u" /* NavParams */]) === "function" ? _c : Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["A" /* ViewController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["A" /* ViewController */]) === "function" ? _d : Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["b" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["b" /* AlertController */]) === "function" ? _e : Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["r" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["r" /* ModalController */]) === "function" ? _f : Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_12__providers_webservice_webservice__["a" /* WebserviceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_12__providers_webservice_webservice__["a" /* WebserviceProvider */]) === "function" ? _g : Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* Events */]) === "function" ? _h : Object])
    ], ModNotaIngresoPage);
    return ModNotaIngresoPage;
}());

// PARA MODULO DE RUT
// import chileanRut from 'chilean-rut'
// chileanRut.validate('')
// chileanRut.format('')
// chileanRut.unformat('')
// chileanRut.validValidatorDigit('')
// chileanRut.correctValidatorDigit('')
// chileanRut.getValidatorDigit('')
//# sourceMappingURL=mod-nota-ingreso.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComunaRequestModel; });
var ComunaRequestModel = /** @class */ (function () {
    function ComunaRequestModel(cod_comuna_comuna, cod_ciudad_ciudad) {
        this.cod_comuna_comuna = cod_comuna_comuna;
        this.cod_ciudad_ciudad = cod_ciudad_ciudad;
    }
    return ComunaRequestModel;
}());

//# sourceMappingURL=comunaRequest.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModNotaIngresoEmpleadorPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ingreso_documentos_ingreso_documentos__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_webservice_webservice__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mod_nota_ingreso_mod_nota_ingreso__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__mod_ingreso_aval_mod_ingreso_aval__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modelos_comunaRequest__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_chilean_rut__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_chilean_rut___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_chilean_rut__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modelos_empleadorRequest__ = __webpack_require__(535);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ModNotaIngresoEmpleadorPage = /** @class */ (function () {
    function ModNotaIngresoEmpleadorPage(navCtrl, navParams, toast, viewCtrl, alertCtrl, modalCtrl, webService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toast = toast;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.webService = webService;
        this.cliente = {};
        this.empleador = {};
        this.aval = {};
        this.detalle = {};
        this.planimetria = {};
        this.salud = {};
        this.empleadorEncontrado = false;
        this.otrosIngresosCarga = false;
        this.acreditacionDoc = false;
        this.botonNombre = "Continuar";
        //Carga de datos
        this.comunas = [];
        this.ciudades = [];
        this.sectores = [];
        this.actividades = [];
        this.estadosCiviles = [];
        this.sexo = [];
        this.girosEmpleador = [];
        this.parentesco = [];
        this.necesidades = [];
        this.presionArterial = [];
        this.acreditacion = [];
        //Para las imagenes 
        this.acreditacionImagenes = [];
        this.otrosIngresosImagenes = [];
        this.nota = {
            notaVenta: {
                cliente: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    sector: null,
                    actividad: null,
                    ciudad: null,
                    civil: null,
                    correo: null,
                    telefono: null,
                    celular: null,
                    fechaNacimiento: null,
                    edad: null,
                    sexo: null
                },
                empleador: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    actividad: null,
                    ciudad: null,
                    acreditacion: [],
                    renta: null,
                    giro: null,
                    telefono: null,
                    otrosIngresos: []
                },
                aval: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    sector: null,
                    actividad: null,
                    ciudad: null,
                    civil: null,
                    correo: null,
                    telefono: null,
                    celular: null,
                    fechaNacimiento: null,
                    edad: null,
                    sexo: null,
                    renta: null,
                    parentesco: null
                },
                detalle: {
                    necesidad: null,
                    planimetria: {
                        descripcion: null,
                        letra: null,
                        estado: null,
                        sector: 0,
                        porc_descuento: 0,
                        uf: 0,
                        fila: null,
                        latitud: 0,
                        longitud: 0,
                        numero: 0,
                        valor_base: 0,
                        capacidades: 0,
                        reducciones: 0,
                        pie: 0
                    },
                    descuento: null,
                    pie: 0,
                    dia_pago: 0,
                    vencimiento: null,
                    cuotas: null,
                    valor_cuotas: null,
                    uf: null,
                    comentario: null
                },
                salud: {
                    responde: false,
                    peso: 0,
                    estatura: 0,
                    presion: null,
                    ejecutiva: null,
                    comentario: null,
                    firma: null
                }
            }
        };
        this.obtener();
        this.verificaCodigoDoc(this.nota.notaVenta.empleador.acreditacion);
        this.verificaCodigo(this.nota.notaVenta.empleador.otrosIngresos);
    }
    ModNotaIngresoEmpleadorPage.prototype.obtener = function () {
        //Asignar datos WS
        this.ciudades = this.navParams.get('ciudades');
        this.sectores = this.navParams.get('sectores');
        this.actividades = this.navParams.get('actividades');
        this.estadosCiviles = this.navParams.get('estadosCiviles');
        this.sexo = this.navParams.get('sexo');
        this.girosEmpleador = this.navParams.get('girosEmpleador');
        this.parentesco = this.navParams.get('parentesco');
        this.necesidades = this.navParams.get('necesidades');
        this.presionArterial = this.navParams.get('presionArterial');
        this.acreditacion = this.navParams.get('acreditacion');
        var data = this.navParams.get('nota');
        if (data !== undefined) {
            if (data.notaVenta.cliente !== undefined) {
                this.cliente = data.notaVenta.cliente;
            }
            if (data.notaVenta.empleador !== undefined) {
                this.empleador = data.notaVenta.empleador;
                this.cargaComunas(this.empleador.ciudad);
            }
            if (data.notaVenta.aval !== undefined) {
                this.aval = data.notaVenta.aval;
            }
            if (data.notaVenta.detalle !== undefined) {
                this.detalle = data.notaVenta.detalle;
            }
            if (data.notaVenta.detalle.planimetria !== undefined) {
                this.planimetria = data.notaVenta.detalle.planimetria;
            }
            if (data.notaVenta.salud !== undefined) {
                this.salud = data.notaVenta.salud;
            }
        }
    };
    ModNotaIngresoEmpleadorPage.prototype.seleccionaComuna = function (cod_ciudad) {
        if (cod_ciudad > 0) {
            this.cargaComunas(cod_ciudad);
        }
    };
    ModNotaIngresoEmpleadorPage.prototype.cargaComunas = function (cod_ciudad) {
        var _this = this;
        var request = new __WEBPACK_IMPORTED_MODULE_6__modelos_comunaRequest__["a" /* ComunaRequestModel */](-1, cod_ciudad);
        this.webService.obtenerListaComunas(request)
            .subscribe(function (data) {
            var listado = data['ListadoComunaResult'];
            var lista = listado['lista'];
            _this.comunas = lista;
        }, function (error) {
            _this.comunas = null;
            console.log('Error comunas WS: ' + JSON.stringify(error));
        });
    };
    ModNotaIngresoEmpleadorPage.prototype.ionViewDidLoad = function () { };
    ModNotaIngresoEmpleadorPage.prototype.verificaEmpleador = function () {
        return !this.empleadorEncontrado;
    };
    ModNotaIngresoEmpleadorPage.prototype.verificaOtrosIngresos = function () {
        return !this.otrosIngresosCarga;
    };
    ModNotaIngresoEmpleadorPage.prototype.verificaDocAcreditacion = function () {
        return !this.acreditacionDoc;
    };
    ModNotaIngresoEmpleadorPage.prototype.verificaCodigo = function (codigo) {
        // console.log(codigo);
        if (codigo.length > 0) {
            this.otrosIngresosCarga = true;
            this.nota.notaVenta.empleador.otrosIngresos = codigo;
        }
        else {
            this.otrosIngresosCarga = false;
        }
    };
    ModNotaIngresoEmpleadorPage.prototype.verificaCodigoDoc = function (cod) {
        if (cod.length > 0) {
            this.acreditacionDoc = true;
            this.nota.notaVenta.empleador.acreditacion = cod;
        }
        else {
            this.acreditacionDoc = false;
        }
    };
    ModNotaIngresoEmpleadorPage.prototype.buscarDatosEmpleador = function (rut) {
        var _this = this;
        this.webService.obtenerDatosEmpleador(rut)
            .subscribe(function (data) {
            // console.log('DATA EMPLEADOR '+JSON.stringify(data));
            var lista = data['ListadoEmpresaResult']['lista'];
            var realizado = data['ListadoEmpresaResult']['realizado'];
            if (realizado) {
                _this.empleadorEncontrado = false;
                _this.empleador.nombre = lista[0]['des_nombre_empresa'];
                _this.empleador.direccion = lista[0]['des_direccion_empresa'];
                _this.empleador.ciudad = lista[0]['cod_ciudad_ciudad'];
                _this.seleccionaComuna(_this.empleador.ciudad);
                _this.empleador.comuna = lista[0]['cod_comuna_comuna'];
                _this.empleador.actividad = lista[0]['cod_cargo_cargo'];
                _this.empleador.acreditacion = lista[0]['cod_acreditacion_acreditacion'];
                _this.empleador.renta = lista[0]['num_renta_renta'];
                _this.empleador.giro = lista[0]['cod_giro_giro'];
                _this.empleador.telefono = lista[0]['des_telefono_empresa'];
                _this.empleador.otrosIngresos = lista[0]['cod_ingresos_ingresos'];
                _this.botonNombre = "Modificar Empleador";
            }
            else {
                _this.botonNombre = "Crear Empleador";
                _this.empleadorEncontrado = true;
            }
        }, function (error) {
            _this.empleadorEncontrado = false;
            console.log('ERROR EMPLEADOR ' + JSON.stringify(error));
        });
    };
    ModNotaIngresoEmpleadorPage.prototype.foco = function () {
        if (this.empleador.rut) {
            if (this.empleador.rut.length !== 12) {
                this.empleador.rut = "";
            }
        }
    };
    ModNotaIngresoEmpleadorPage.prototype.formatoRut = function () {
        if (this.empleador.rut) {
            var dv = this.empleador.rut.substring(8);
            this.empleador.rut = __WEBPACK_IMPORTED_MODULE_7_chilean_rut___default.a.format(this.empleador.rut, dv);
            if (this.empleador.rut.length == 12) {
                if (!__WEBPACK_IMPORTED_MODULE_7_chilean_rut___default.a.validate(this.empleador.rut)) {
                    this.mostarToast('Rut no válido');
                    this.empleador.rut = "";
                }
                else {
                    var rut = __WEBPACK_IMPORTED_MODULE_7_chilean_rut___default.a.unformat(this.empleador.rut);
                    this.buscarDatosEmpleador(rut);
                }
            }
            else if (this.empleador.rut.length < 12) {
                this.empleador.nombre = "";
                this.empleador.direccion = "";
                this.empleador.comuna = "";
                this.empleador.actividad = "";
                this.empleador.ciudad = "";
                this.empleador.acreditacion = "";
                this.empleador.renta = "";
                this.empleador.giro = "";
                this.empleador.telefono = "";
                this.empleador.otrosIngresos = "";
            }
        }
        this.empleadorEncontrado = false;
    };
    ModNotaIngresoEmpleadorPage.prototype.seleccionArchivo = function (ingreso) {
        var _this = this;
        var data = null;
        if (ingreso) {
            data = {
                tipo: true,
                acreditacion: this.nota.notaVenta.empleador.acreditacion
            };
        }
        else {
            data = {
                tipo: false,
                otros_ingresos: this.nota.notaVenta.empleador.otrosIngresos
            };
        }
        var modalSeleccion = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_0__ingreso_documentos_ingreso_documentos__["a" /* IngresoDocumentosPage */], data, { enableBackdropDismiss: false });
        modalSeleccion.onDidDismiss(function (data) {
            console.log('DEVUELVE: ' + JSON.stringify(data));
            if (data) {
                if (data[0].EsAcreditacion) {
                    _this.acreditacionImagenes = data;
                    console.log('ES ACREDITACION');
                }
                else {
                    _this.otrosIngresosImagenes = data;
                    console.log('OTRO INGRESO');
                }
            }
        });
        if (ingreso) {
            if (this.acreditacionDoc && ingreso) {
                modalSeleccion.present();
            }
            else {
                this.mostarToast('Seleccione una acreditación');
            }
        }
        else {
            if (this.otrosIngresosCarga && !ingreso) {
                modalSeleccion.present();
            }
            else {
                this.mostarToast('Seleccione otros ingresos');
            }
        }
    };
    ModNotaIngresoEmpleadorPage.prototype.continuar = function (continuar) {
        var _this = this;
        this.nota.notaVenta.cliente = this.cliente;
        this.nota.notaVenta.empleador = this.empleador;
        this.nota.notaVenta.aval = this.aval;
        this.nota.notaVenta.detalle = this.detalle;
        this.nota.notaVenta.detalle.planimetria = this.planimetria;
        this.nota.notaVenta.salud = this.salud;
        this.nota.notaVenta.empleador.acreditacion = this.acreditacionImagenes;
        this.nota.notaVenta.empleador.otrosIngresos = this.otrosIngresosImagenes;
        // console.log('SALIDA: '+JSON.stringify(this.nota));
        var data = {
            nota: this.nota,
            ciudades: this.ciudades,
            sectores: this.sectores,
            actividades: this.actividades,
            estadosCiviles: this.estadosCiviles,
            sexo: this.sexo,
            girosEmpleador: this.girosEmpleador,
            parentesco: this.parentesco,
            necesidades: this.necesidades,
            presionArterial: this.presionArterial,
            acreditacion: this.acreditacion
        };
        var opcionesModal = { enableBackdropDismiss: false };
        var modalPresent = null;
        if (continuar) {
            if (this.nota.notaVenta.empleador.rut && this.nota.notaVenta.empleador.nombre && this.nota.notaVenta.empleador.direccion
                && this.nota.notaVenta.empleador.comuna && this.nota.notaVenta.empleador.ciudad && this.nota.notaVenta.empleador.giro) {
                var modalAval_1 = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__mod_ingreso_aval_mod_ingreso_aval__["a" /* ModIngresoAvalPage */], data, opcionesModal);
                var rut = __WEBPACK_IMPORTED_MODULE_7_chilean_rut___default.a.unformat(this.nota.notaVenta.empleador.rut);
                var empleadorRequest = new __WEBPACK_IMPORTED_MODULE_8__modelos_empleadorRequest__["a" /* EmpleadorRequestModel */](rut, this.nota.notaVenta.empleador.nombre, this.nota.notaVenta.empleador.direccion, this.nota.notaVenta.empleador.telefono, 'correo@correo.cl', 
                //this.nota.notaVenta.empleador.correo,
                this.nota.notaVenta.empleador.giro, this.nota.notaVenta.empleador.actividad, this.nota.notaVenta.empleador.comuna, this.nota.notaVenta.empleador.ciudad);
                if (this.empleadorEncontrado) {
                    this.webService.guardarEmpleador(empleadorRequest)
                        .subscribe(function (data) {
                        var resultado = data['GuardarEmpresaResult']['resultado'];
                        console.log('RESPUESTA WS ' + JSON.stringify(data));
                        if (resultado) {
                            _this.mostarToast('Empleador nuevo creado correctamente');
                            modalPresent = modalAval_1;
                            modalPresent.present();
                            _this.viewCtrl.dismiss();
                        }
                        else {
                            _this.mostarToast('Error al guardar empleador nuevo');
                            modalPresent = null;
                        }
                    }, function (error) {
                        _this.mostarToast('Error en respuesta del servidor');
                        console.log('Error ' + JSON.stringify(error));
                        modalPresent = null;
                    });
                }
                else {
                    // Aqui modificar empleador
                    this.webService.guardarEmpleador(empleadorRequest)
                        .subscribe(function (data) {
                        var resultado = data['GuardarEmpresaResult']['resultado'];
                        console.log('RESPUESTA WS ' + JSON.stringify(data));
                        if (resultado) {
                            _this.mostarToast('Empleador modificado creado correctamente');
                            modalPresent = modalAval_1;
                            modalPresent.present();
                            _this.viewCtrl.dismiss();
                        }
                        else {
                            _this.mostarToast('Error al modificar empleador');
                            modalPresent = null;
                        }
                    }, function (error) {
                        _this.mostarToast('Error en respuesta del servidor');
                        console.log('Error ' + JSON.stringify(error));
                        modalPresent = null;
                    });
                }
            }
            else {
                modalPresent = null;
                this.mostarToast('Faltan parámetros para empleador');
            }
        }
        else {
            var modalIngreso = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__mod_nota_ingreso_mod_nota_ingreso__["a" /* ModNotaIngresoPage */], data, opcionesModal);
            modalPresent = modalIngreso;
            modalPresent.present();
            this.viewCtrl.dismiss();
        }
    };
    ModNotaIngresoEmpleadorPage.prototype.mostarToast = function (mensaje) {
        if (mensaje) {
            var toast = this.toast.create({
                message: mensaje,
                duration: 3000,
                position: 'top'
            });
            toast.present();
        }
    };
    ModNotaIngresoEmpleadorPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-mod-nota-ingreso-empleador',template:/*ion-inline-start:"C:\ionicproyectos\parqueCopiapo\src\pages\mod-nota-ingreso-empleador\mod-nota-ingreso-empleador.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons left>\n\n      <button ion-button icon-only (tap)="continuar(false)">\n\n        <ion-icon name="arrow-back"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>Ingreso empleador</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <form (ngSubmit)="continuar(true)" #formEmpleador="ngForm">\n\n    <ion-grid no-padding>\n\n      <ion-row>\n\n        <ion-col width-50>\n\n          <ion-card>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Rut *</ion-label>\n\n              <ion-input type="text" [(ngModel)]="empleador.rut" maxlength="12" name="rut" placeholder="Ingrese rut" (ionChange)=formatoRut()\n\n                (focusout)=foco()></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Nombre *</ion-label>\n\n              <ion-input type="text" [(ngModel)]="empleador.nombre" name="nombre" placeholder="Ingrese nombre"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Dirección *</ion-label>\n\n              <ion-input type="text" [(ngModel)]="empleador.direccion" name="direccion" placeholder="Ingrese dirección"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Ciudad *</ion-label>\n\n              <ion-select [(ngModel)]="empleador.ciudad" name="ciudad" no-lines placeholder="Seleccione ciudad"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="ciu.cod_ciudad_ciudad" (ionSelect)="seleccionaComuna(ciu.cod_ciudad_ciudad)" *ngFor="let ciu of ciudades">{{ ciu.des_nombre_ciudad | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Comuna *</ion-label>\n\n              <ion-select [(ngModel)]="empleador.comuna" name="comuna" no-lines placeholder="Seleccione comuna"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="comuna.cod_comuna_comuna" *ngFor="let comuna of comunas">{{ comuna.des_nombre_comuna | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n          </ion-card>\n\n        </ion-col>\n\n        <ion-col width-50>\n\n          <ion-card>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Giro empleador *</ion-label>\n\n              <ion-select [(ngModel)]="empleador.giro" name="giro" no-lines placeholder="Seleccione giro empleador"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="giro.cod_giro_giro" *ngFor="let giro of girosEmpleador">{{ giro.des_giro_giro | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Teléfono</ion-label>\n\n              <ion-input type="text" [(ngModel)]="empleador.telefono" name="telefono" placeholder="Ingrese teléfono"></ion-input>\n\n            </ion-item>\n\n            <ion-list-header>\n\n              <p color="primary">Documento de acreditación *</p>\n\n            </ion-list-header>\n\n            <ion-item>\n\n              <ion-select class="select-1" item-start [(ngModel)]="empleador.acreditacion" name="acreditacion" (ionChange)="verificaCodigoDoc(empleador.acreditacion)"\n\n                no-lines placeholder="Seleccione documento" cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="acre.id_codigo_taingresos" *ngFor="let acre of acreditacion">{{ acre.des_nombre_taingresos | uppercase }}</ion-option>\n\n              </ion-select>\n\n              <ion-icon class="icono" name="cloud-upload" item-end (tap)="seleccionArchivo(true)"></ion-icon>\n\n            </ion-item>\n\n            <ion-list-header>\n\n              <p color="primary">Otros ingresos</p>\n\n            </ion-list-header>\n\n            <ion-item>\n\n              <ion-select [(ngModel)]="empleador.otrosIngresos" name="otrosIngresos" no-lines placeholder="Seleccione otros ingresos" (ionChange)="verificaCodigo(empleador.otrosIngresos)"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option value="1">SIN OTROS INGRESOS</ion-option>\n\n                <ion-option value="2">INGRESOS PASIVOS</ion-option>\n\n              </ion-select>\n\n              <ion-icon class="icono" name="cloud-upload" item-end (tap)="seleccionArchivo(false)"></ion-icon>\n\n            </ion-item>\n\n          </ion-card>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </form>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <button type="submit" ion-button full color="primary" (tap)="formEmpleador.ngSubmit.emit()">{{ botonNombre }}</button>\n\n</ion-footer>'/*ion-inline-end:"C:\ionicproyectos\parqueCopiapo\src\pages\mod-nota-ingreso-empleador\mod-nota-ingreso-empleador.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["u" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["y" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["A" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["r" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1__providers_webservice_webservice__["a" /* WebserviceProvider */]])
    ], ModNotaIngresoEmpleadorPage);
    return ModNotaIngresoEmpleadorPage;
}());

//# sourceMappingURL=mod-nota-ingreso-empleador.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModIngresoAvalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_webservice_webservice__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mod_nota_ingreso_empleador_mod_nota_ingreso_empleador__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mod_nota_ingreso_detalle_mod_nota_ingreso_detalle__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modelos_comunaRequest__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modelos_avalRequest__ = __webpack_require__(534);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modelos_clienteRequest__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_chilean_rut__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_chilean_rut___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_chilean_rut__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ModIngresoAvalPage = /** @class */ (function () {
    function ModIngresoAvalPage(navCtrl, navParams, alertCtrl, viewCtrl, modalCtrl, toast, webService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.toast = toast;
        this.webService = webService;
        this.fechaNacimiento = new Date().toISOString();
        this.cliente = {};
        this.empleador = {};
        this.aval = {};
        this.detalle = {};
        this.planimetria = {};
        this.salud = {};
        this.avalEncontrado = false;
        this.botonNombre = "Continuar";
        this.clienteComoAval = false;
        //Carga de datos
        this.comunas = [];
        this.ciudades = [];
        this.sectores = [];
        this.actividades = [];
        this.estadosCiviles = [];
        this.sexo = [];
        this.girosEmpleador = [];
        this.parentesco = [];
        this.necesidades = [];
        this.presionArterial = [];
        this.acreditacion = [];
        this.nota = {
            notaVenta: {
                cliente: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    sector: null,
                    actividad: null,
                    ciudad: null,
                    civil: null,
                    correo: null,
                    telefono: null,
                    celular: null,
                    fechaNacimiento: null,
                    edad: null,
                    sexo: null
                },
                empleador: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    actividad: null,
                    ciudad: null,
                    acreditacion: [],
                    renta: null,
                    giro: null,
                    telefono: null,
                    otrosIngresos: []
                },
                aval: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    sector: null,
                    actividad: null,
                    ciudad: null,
                    civil: null,
                    correo: null,
                    telefono: null,
                    celular: null,
                    fechaNacimiento: null,
                    edad: null,
                    sexo: null,
                    renta: null,
                    parentesco: null
                },
                detalle: {
                    necesidad: null,
                    planimetria: {
                        descripcion: null,
                        letra: null,
                        estado: null,
                        sector: 0,
                        porc_descuento: 0,
                        uf: 0,
                        fila: null,
                        latitud: 0,
                        longitud: 0,
                        numero: 0,
                        valor_base: 0,
                        capacidades: 0,
                        reducciones: 0,
                        pie: 0
                    },
                    descuento: null,
                    pie: 0,
                    dia_pago: 0,
                    vencimiento: null,
                    cuotas: null,
                    valor_cuotas: null,
                    uf: null,
                    comentario: null
                },
                salud: {
                    responde: false,
                    peso: 0,
                    estatura: 0,
                    presion: null,
                    ejecutiva: null,
                    comentario: null,
                    firma: null
                }
            }
        };
        this.obtener();
    }
    ModIngresoAvalPage.prototype.ionViewDidLoad = function () { };
    ModIngresoAvalPage.prototype.clienteAval = function (check) {
        if (check) {
            this.aval.rut = this.cliente.rut;
            this.buscarDatosAval(__WEBPACK_IMPORTED_MODULE_8_chilean_rut___default.a.unformat(this.cliente.rut));
            this.aval.parentesco = 2;
        }
        else {
            this.aval.rut = "";
            this.aval.nombre = "";
            this.aval.direccion = "";
            this.aval.comuna = "";
            this.aval.sector = "";
            this.aval.actividad = "";
            this.aval.ciudad = "";
            this.aval.ciudad = "";
            this.aval.civil = "";
            this.aval.correo = "";
            this.aval.telefono = "";
            this.aval.celular = "";
            this.aval.fechaNacimiento = "";
            this.aval.edad = "";
            this.aval.renta = "";
            this.aval.parentesco = "";
        }
    };
    ModIngresoAvalPage.prototype.verificaClienteAval = function () {
        return !this.clienteComoAval;
    };
    ModIngresoAvalPage.prototype.obtener = function () {
        //Asignar datos WS
        this.ciudades = this.navParams.get('ciudades');
        this.sectores = this.navParams.get('sectores');
        this.actividades = this.navParams.get('actividades');
        this.estadosCiviles = this.navParams.get('estadosCiviles');
        this.sexo = this.navParams.get('sexo');
        this.girosEmpleador = this.navParams.get('girosEmpleador');
        this.parentesco = this.navParams.get('parentesco');
        //console.log('PARENTESCOS '+ JSON.stringify(this.parentesco));
        this.necesidades = this.navParams.get('necesidades');
        this.presionArterial = this.navParams.get('presionArterial');
        this.acreditacion = this.navParams.get('acreditacion');
        var data = this.navParams.get('nota');
        if (data !== undefined) {
            if (data.notaVenta.cliente !== undefined) {
                this.cliente = data.notaVenta.cliente;
            }
            if (data.notaVenta.empleador !== undefined) {
                this.empleador = data.notaVenta.empleador;
            }
            if (data.notaVenta.aval !== undefined) {
                this.aval = data.notaVenta.aval;
                if (data.notaVenta.aval.rut) {
                    this.buscarDatosAval(__WEBPACK_IMPORTED_MODULE_8_chilean_rut___default.a.unformat(data.notaVenta.aval.rut));
                }
                this.seleccionaComuna(this.aval.ciudad);
            }
            if (data.notaVenta.detalle !== undefined) {
                this.detalle = data.notaVenta.detalle;
            }
            if (data.notaVenta.detalle.planimetria !== undefined) {
                this.planimetria = data.notaVenta.detalle.planimetria;
            }
            if (data.notaVenta.salud !== undefined) {
                this.salud = data.notaVenta.salud;
            }
        }
    };
    ModIngresoAvalPage.prototype.seleccionaComuna = function (cod_ciudad) {
        var _this = this;
        var request = new __WEBPACK_IMPORTED_MODULE_5__modelos_comunaRequest__["a" /* ComunaRequestModel */](-1, cod_ciudad);
        this.webService.obtenerListaComunas(request)
            .subscribe(function (data) {
            var listado = data['ListadoComunaResult'];
            var lista = listado['lista'];
            _this.comunas = lista;
        }, function (error) {
            _this.comunas = null;
            console.log('Error comunas WS: ' + error);
        });
    };
    ModIngresoAvalPage.prototype.buscarDatosAval = function (rut) {
        var _this = this;
        if (rut == __WEBPACK_IMPORTED_MODULE_8_chilean_rut___default.a.unformat(this.cliente.rut)) {
            this.clienteComoAval = true;
        }
        else {
            this.clienteComoAval = false;
        }
        this.webService.obtenerDatosAval(new __WEBPACK_IMPORTED_MODULE_6__modelos_avalRequest__["a" /* AvalRequestModel */](rut))
            .subscribe(function (data) {
            var lista = data['ListadoClienteResult']['lista'];
            console.log('AVAL: ' + JSON.stringify(lista));
            var realizado = data['ListadoClienteResult']['realizado'];
            if (realizado) {
                _this.avalEncontrado = false;
                _this.aval.nombre = lista[0]['des_nombre_cliente'];
                _this.aval.direccion = lista[0]['des_direccion_cliente'];
                _this.aval.ciudad = lista[0]['cod_ciudad_ciudad'];
                _this.seleccionaComuna(_this.aval.ciudad);
                _this.aval.comuna = lista[0]['cod_comuna_comuna'];
                _this.aval.sector = lista[0]['cod_sector_sector'];
                _this.aval.actividad = lista[0]['cod_cargo_cargo'];
                _this.aval.civil = lista[0]['cod_estadocivil_estadocivil'];
                _this.aval.correo = lista[0]['des_mail_cliente'];
                _this.aval.telefono = lista[0]['des_telefono_cliente'];
                _this.aval.celular = lista[0]['des_telefono_celular'];
                _this.aval.sexo = lista[0]['cod_sexo_sexo'];
                var fechaFormateada = new Date(parseInt(lista[0]['dat_fecha_nacimiento'].replace(/(^.*\()|([+-].*$)/g, ''))).toISOString();
                _this.aval.fechaNacimiento = fechaFormateada;
                _this.aval.edad = lista[0]['edad'];
                _this.aval.renta = lista[0]['num_montorentaliquida_clientes'];
                _this.aval.parentesco = lista[0]['cod_parentesco_parentesco'];
                _this.botonNombre = "Modificar Aval";
            }
            else {
                _this.botonNombre = "Crear Aval";
                _this.avalEncontrado = true;
            }
        }, function (error) {
            _this.avalEncontrado = false;
            console.log('ERROR AVAL ' + JSON.stringify(error));
        });
    };
    ModIngresoAvalPage.prototype.verificaDatosAval = function () {
        return !this.avalEncontrado;
    };
    ModIngresoAvalPage.prototype.foco = function () {
        if (this.aval.rut) {
            if (this.aval.rut.length !== 12) {
                this.aval.rut = "";
            }
        }
    };
    ModIngresoAvalPage.prototype.formatoRut = function () {
        if (this.aval.rut) {
            var dv = this.aval.rut.substring(8);
            this.aval.rut = __WEBPACK_IMPORTED_MODULE_8_chilean_rut___default.a.format(this.aval.rut, dv);
            if (this.aval.rut.length == 12) {
                if (!__WEBPACK_IMPORTED_MODULE_8_chilean_rut___default.a.validate(this.aval.rut)) {
                    this.mostarToast('Rut no válido');
                    this.aval.rut = "";
                }
                else {
                    var rut = __WEBPACK_IMPORTED_MODULE_8_chilean_rut___default.a.unformat(this.aval.rut);
                    this.buscarDatosAval(rut);
                }
            }
            else if (this.aval.rut.length < 12) {
                this.aval.nombre = "";
                this.aval.direccion = "";
                this.aval.comuna = "";
                this.aval.sector = "";
                this.aval.actividad = "";
                this.aval.ciudad = "";
                this.aval.ciudad = "";
                this.aval.civil = "";
                this.aval.correo = "";
                this.aval.telefono = "";
                this.aval.celular = "";
                this.aval.fechaNacimiento = "";
                this.aval.edad = "";
                this.aval.renta = "";
                this.aval.parentesco = "";
            }
        }
        this.avalEncontrado = false;
    };
    ModIngresoAvalPage.prototype.continuar = function (continuar) {
        var _this = this;
        this.nota.notaVenta.cliente = this.cliente;
        this.nota.notaVenta.empleador = this.empleador;
        this.nota.notaVenta.aval = this.aval;
        this.nota.notaVenta.detalle = this.detalle;
        this.nota.notaVenta.detalle.planimetria = this.planimetria;
        this.nota.notaVenta.salud = this.salud;
        var data = {
            nota: this.nota,
            ciudades: this.ciudades,
            sectores: this.sectores,
            actividades: this.actividades,
            estadosCiviles: this.estadosCiviles,
            sexo: this.sexo,
            girosEmpleador: this.girosEmpleador,
            parentesco: this.parentesco,
            necesidades: this.necesidades,
            presionArterial: this.presionArterial,
            acreditacion: this.acreditacion
        };
        var opcionesModal = { enableBackdropDismiss: false };
        var modalPresent = null;
        if (continuar) {
            if (this.nota.notaVenta.aval.rut && this.nota.notaVenta.aval.nombre && this.nota.notaVenta.aval.direccion &&
                this.nota.notaVenta.aval.comuna && this.nota.notaVenta.aval.sector &&
                this.nota.notaVenta.aval.actividad && this.nota.notaVenta.aval.ciudad &&
                this.nota.notaVenta.aval.civil && this.nota.notaVenta.aval.correo &&
                this.nota.notaVenta.aval.telefono && this.nota.notaVenta.aval.fechaNacimiento &&
                this.nota.notaVenta.aval.edad && this.nota.notaVenta.aval.renta
            // && this.nota.notaVenta.aval.parentesco
            ) {
                var modalDetalle_1 = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__mod_nota_ingreso_detalle_mod_nota_ingreso_detalle__["a" /* ModNotaIngresoDetallePage */], data, opcionesModal);
                var rut = null;
                rut = __WEBPACK_IMPORTED_MODULE_8_chilean_rut___default.a.unformat(this.nota.notaVenta.aval.rut);
                var vendedor = parseInt(localStorage.getItem('login'));
                var fecha = new Date().toISOString;
                var avalRequest = new __WEBPACK_IMPORTED_MODULE_7__modelos_clienteRequest__["a" /* ClienteRequestModel */](vendedor, rut, this.nota.notaVenta.aval.comuna, this.nota.notaVenta.aval.giro, this.nota.notaVenta.aval.nombre, this.nota.notaVenta.aval.direccion, this.nota.notaVenta.aval.telefono, this.nota.notaVenta.aval.correo, this.nota.notaVenta.aval.ciudad, this.nota.notaVenta.aval.sector, this.nota.notaVenta.aval.civil, this.nota.notaVenta.aval.celular, 
                // this.nota.notaVenaval.aval.fechaNacimiento,
                fecha, this.nota.notaVenta.aval.edad, this.nota.notaVenta.aval.sexo, this.nota.notaVenta.aval.renta, this.nota.notaVenta.aval.actividad, this.nota.notaVenta.aval.parentesco);
                if (this.avalEncontrado) {
                    this.webService.guardarCliente(avalRequest)
                        .subscribe(function (data) {
                        var resultado = data['GuardarClienteResult']['resultado'];
                        console.log('RESPUESTA ' + JSON.stringify(data));
                        if (resultado) {
                            _this.mostarToast('Aval nuevo creado correctamente');
                            modalPresent = modalDetalle_1;
                            modalPresent.present();
                            _this.viewCtrl.dismiss();
                        }
                        else {
                            _this.mostarToast('Error al guardar nuevo aval');
                            modalPresent = null;
                        }
                    }, function (error) {
                        _this.mostarToast('Error en respuesta del servidor');
                        modalPresent = null;
                    });
                }
                else {
                    this.webService.guardarCliente(avalRequest)
                        .subscribe(function (data) {
                        var resultado = data['GuardarClienteResult']['resultado'];
                        console.log('RESPUESTA ' + JSON.stringify(data));
                        if (resultado) {
                            _this.mostarToast('Aval modificado correctamente');
                            modalPresent = modalDetalle_1;
                            modalPresent.present();
                            _this.viewCtrl.dismiss();
                        }
                        else {
                            _this.mostarToast('Error al modificar aval');
                            modalPresent = null;
                        }
                    }, function (error) {
                        _this.mostarToast('Error en respuesta del servidor');
                        modalPresent = null;
                    });
                }
            }
            else {
                modalPresent = null;
                this.mostarToast('Faltan parámetros para aval');
            }
        }
        else {
            var modalEmpleador = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_1__mod_nota_ingreso_empleador_mod_nota_ingreso_empleador__["a" /* ModNotaIngresoEmpleadorPage */], data, opcionesModal);
            modalPresent = modalEmpleador;
            modalPresent.present();
            this.viewCtrl.dismiss();
        }
    };
    ModIngresoAvalPage.prototype.mostarToast = function (mensaje) {
        if (mensaje) {
            var toast = this.toast.create({
                message: mensaje,
                duration: 3000,
                position: 'top'
            });
            toast.present();
        }
    };
    ModIngresoAvalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-mod-ingreso-aval',template:/*ion-inline-start:"C:\ionicproyectos\parqueCopiapo\src\pages\mod-ingreso-aval\mod-ingreso-aval.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons left>\n\n      <button ion-button icon-only (tap)="continuar(false)">\n\n        <ion-icon name="arrow-back"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>Ingreso aval</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <form (ngSubmit)="continuar(true)" #formAval="ngForm">\n\n    <ion-grid no-padding>\n\n      <ion-row>\n\n        <ion-col width-50>\n\n          <ion-card>\n\n            <ion-item>\n\n              <ion-checkbox [(ngModel)]="clienteComoAval" name="cliente_aval" (ionChange)="clienteAval(clienteComoAval)"></ion-checkbox>\n\n              <ion-label>Ingresar cliente como Aval</ion-label>\n\n            </ion-item>\n\n\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Rut *</ion-label>\n\n              <ion-input [disabled]="!verificaClienteAval()" type="text" [(ngModel)]="aval.rut" maxlength="12" name="rut" placeholder="Ingrese rut" (ionChange)=formatoRut()\n\n                (focusout)=foco()></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Nombre *</ion-label>\n\n              <ion-input [disabled]="!verificaClienteAval()" [(ngModel)]="aval.nombre" type="text" name="nombre" placeholder="Ingrese nombre"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Dirección *</ion-label>\n\n              <ion-input [disabled]="!verificaClienteAval()" [(ngModel)]="aval.direccion" type="text" name="direccion" placeholder="Ingrese dirección"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Ciudad *</ion-label>\n\n              <ion-select [disabled]="!verificaClienteAval()" [(ngModel)]="aval.ciudad" name="ciudad" no-lines placeholder="Seleccione ciudad"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="ciu.cod_ciudad_ciudad" (ionSelect)="seleccionaComuna(ciu.cod_ciudad_ciudad)" *ngFor="let ciu of ciudades">{{ ciu.des_nombre_ciudad | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Comuna *</ion-label>\n\n              <ion-select [disabled]="!verificaClienteAval()" [(ngModel)]="aval.comuna" name="comuna" no-lines placeholder="Seleccione comuna"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="comuna.cod_comuna_comuna" *ngFor="let comuna of comunas">{{ comuna.des_nombre_comuna | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Sector *</ion-label>\n\n              <ion-select [disabled]="!verificaClienteAval()" [(ngModel)]="aval.sector" name="sector" no-lines placeholder="Seleccione zona"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="sec.cod_sector_sector" *ngFor="let sec of sectores">{{ sec.des_sector_sector | uppercase}}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Actividad *</ion-label>\n\n              <ion-select [disabled]="!verificaClienteAval()" [(ngModel)]="aval.actividad" name="actividad" no-lines placeholder="Seleccione actividad"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="act.cod_cargo_cargo" *ngFor="let act of actividades">{{ act.des_descripcion_cargo | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Estado Civil *</ion-label>\n\n              <ion-select [disabled]="!verificaClienteAval()" [(ngModel)]="aval.civil" name="civil" no-lines placeholder="Seleccione estado civil"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="civil.cod_estadocivil_estadocivil" *ngFor="let civil of estadosCiviles">{{ civil.des_descripcion_estadocivil | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n          </ion-card>\n\n        </ion-col>\n\n        <ion-col width-50>\n\n          <ion-card>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Correo electrónico *</ion-label>\n\n              <ion-input [disabled]="!verificaClienteAval()" type="text" [(ngModel)]="aval.correo" name="correo" placeholder="Ingrese correo electronico"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Teléfono fijo</ion-label>\n\n              <ion-input [disabled]="!verificaClienteAval()" type="text" [(ngModel)]="aval.telefono" name="telefono" placeholder="Ingrese telefono"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Celular</ion-label>\n\n              <ion-input [disabled]="!verificaClienteAval()" type="text" [(ngModel)]="aval.celular" name="celular" placeholder="Ingrese celular"></ion-input>\n\n            </ion-item>\n\n            <button [disabled]="!verificaClienteAval()" ion-item item-start outline type="button" icon-end (tap)="picker.open()">\n\n              <ion-icon name="calendar" item-end color="primary"></ion-icon>\n\n              <ion-datetime #picker pickerFormat="DD MMMM YYYY" min="1900" max="2019" cancelText="Cancelar" doneText="Aceptar"\n\n                [(ngModel)]="aval.fechaNacimiento" name="fecha_nacimiento"></ion-datetime>\n\n              <ion-label>Fecha de nacimiento *</ion-label>\n\n            </button>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Edad *</ion-label>\n\n              <ion-input [disabled]="!verificaClienteAval()" [(ngModel)]="aval.edad" type="text" name="edad" placeholder="Ingrese edad"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Renta liquida *</ion-label>\n\n              <ion-input [(ngModel)]="aval.renta" type="text" name="renta" placeholder="Ingrese renta liquida"></ion-input>\n\n            </ion-item>\n\n            <ion-item *ngIf="verificaClienteAval()">\n\n              <ion-label color="primary" stacked>Parentesco *</ion-label>\n\n              <ion-select  [(ngModel)]="aval.parentesco" name="parentesco" no-lines placeholder="Seleccione parentesco"\n\n                cancelText="Cancelar" okText="Ok">\n\n                <ion-option [value]="par.cod_parentesco_parentesco" *ngFor="let par of parentesco">{{ par.des_parentesco_parentesco | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n          </ion-card>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </form>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <button type="submit" ion-button full color="primary" (tap)="formAval.ngSubmit.emit()">{{ botonNombre }}</button>\n\n</ion-footer>'/*ion-inline-end:"C:\ionicproyectos\parqueCopiapo\src\pages\mod-ingreso-aval\mod-ingreso-aval.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["u" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["A" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["r" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["y" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_0__providers_webservice_webservice__["a" /* WebserviceProvider */]])
    ], ModIngresoAvalPage);
    return ModIngresoAvalPage;
}());

//# sourceMappingURL=mod-ingreso-aval.js.map

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModNotaIngresoDetallePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_webservice_webservice__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mod_ingreso_aval_mod_ingreso_aval__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mod_declaracion_salud_mod_declaracion_salud__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__mod_planimeria_mod_planimeria__ = __webpack_require__(316);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ModNotaIngresoDetallePage = /** @class */ (function () {
    function ModNotaIngresoDetallePage(navCtrl, navParams, alertCtrl, viewCtrl, modalCtrl, toast, webService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.toast = toast;
        this.webService = webService;
        this.cliente = {};
        this.empleador = {};
        this.aval = {};
        this.planimetria = {};
        this.detalle = {};
        this.salud = {};
        this.necesidad = null;
        this.validaIngresoNecesidad = false;
        //Carga de datos
        this.comunas = [];
        this.ciudades = [];
        this.sectores = [];
        this.actividades = [];
        this.estadosCiviles = [];
        this.sexo = [];
        this.girosEmpleador = [];
        this.parentesco = [];
        this.necesidades = [];
        this.presionArterial = [];
        this.acreditacion = [];
        this.nota = {
            notaVenta: {
                cliente: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    sector: null,
                    actividad: null,
                    ciudad: null,
                    civil: null,
                    correo: null,
                    telefono: null,
                    celular: null,
                    fechaNacimiento: null,
                    edad: null,
                    sexo: null
                },
                empleador: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    actividad: null,
                    ciudad: null,
                    acreditacion: [],
                    renta: null,
                    giro: null,
                    telefono: null,
                    otrosIngresos: []
                },
                aval: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    sector: null,
                    actividad: null,
                    ciudad: null,
                    civil: null,
                    correo: null,
                    telefono: null,
                    celular: null,
                    fechaNacimiento: null,
                    edad: null,
                    sexo: null,
                    renta: null,
                    parentesco: null
                },
                detalle: {
                    necesidad: null,
                    planimetria: {
                        descripcion: null,
                        letra: null,
                        estado: null,
                        sector: 0,
                        porc_descuento: 0,
                        uf: 0,
                        fila: null,
                        latitud: 0,
                        longitud: 0,
                        numero: 0,
                        valor_base: 0,
                        capacidades: 0,
                        reducciones: 0,
                        pie: 0
                    },
                    descuento: null,
                    pie: 0,
                    dia_pago: 0,
                    vencimiento: null,
                    cuotas: null,
                    valor_cuotas: null,
                    uf: null,
                    comentario: null
                },
                salud: {
                    responde: false,
                    peso: 0,
                    estatura: 0,
                    presion: null,
                    ejecutiva: null,
                    comentario: null,
                    firma: null
                }
            }
        };
        this.obtener();
        this.seleccionaNecesidad(this.nota.notaVenta.detalle.necesidad, true, null);
    }
    ModNotaIngresoDetallePage.prototype.obtener = function () {
        //Asignar datos WS
        this.ciudades = this.navParams.get('ciudades');
        this.sectores = this.navParams.get('sectores');
        this.actividades = this.navParams.get('actividades');
        this.estadosCiviles = this.navParams.get('estadosCiviles');
        this.sexo = this.navParams.get('sexo');
        this.girosEmpleador = this.navParams.get('girosEmpleador');
        this.parentesco = this.navParams.get('parentesco');
        this.necesidades = this.navParams.get('necesidades');
        this.presionArterial = this.navParams.get('presionArterial');
        this.acreditacion = this.navParams.get('acreditacion');
        var data = this.navParams.get('nota');
        if (data !== undefined) {
            if (data.notaVenta.cliente !== undefined) {
                this.cliente = data.notaVenta.cliente;
            }
            if (data.notaVenta.empleador !== undefined) {
                this.empleador = data.notaVenta.empleador;
            }
            if (data.notaVenta.aval !== undefined) {
                this.aval = data.notaVenta.aval;
            }
            if (data.notaVenta.detalle !== undefined) {
                this.detalle = data.notaVenta.detalle;
            }
            if (data.notaVenta.detalle.planimetria !== undefined) {
                this.planimetria = data.notaVenta.detalle.planimetria;
            }
            if (data.notaVenta.salud !== undefined) {
                this.salud = data.notaVenta.salud;
            }
        }
    };
    ModNotaIngresoDetallePage.prototype.ionViewDidLoad = function () { };
    ModNotaIngresoDetallePage.prototype.seleccionaNecesidad = function (cod_necesidad, change, bit) {
        console.log('cod: ' + cod_necesidad + ' bit: ' + bit);
        this.detalle.planimetria.descripcion = "";
        this.detalle.planimetria.letra = "";
        this.detalle.planimetria.estado = "";
        this.detalle.planimetria.sector = "";
        this.detalle.planimetria.porc_descuento = "";
        this.detalle.planimetria.uf = "";
        this.detalle.planimetria.fila = "";
        this.detalle.planimetria.latitud = "";
        this.detalle.planimetria.longitud = "";
        this.detalle.planimetria.numero = "";
        this.detalle.planimetria.valor_base = "";
        this.detalle.planimetria.capacidades = "";
        this.detalle.planimetria.reducciones = "";
        this.detalle.planimetria.pie = "";
        if (change) {
            this.validaIngresoNecesidad = true;
        }
        else {
        }
        if (cod_necesidad !== null) {
            if (bit) {
                this.necesidad = true;
            }
            else {
                this.necesidad = false;
            }
        }
        console.log('BIT NECESIDAD ' + bit);
    };
    ModNotaIngresoDetallePage.prototype.continuar = function (continuar) {
        this.nota.notaVenta.cliente = this.cliente;
        this.nota.notaVenta.empleador = this.empleador;
        this.nota.notaVenta.aval = this.aval;
        this.nota.notaVenta.detalle = this.detalle;
        this.nota.notaVenta.detalle.planimetria = this.planimetria;
        this.nota.notaVenta.salud = this.salud;
        var data = {
            nota: this.nota,
            ciudades: this.ciudades,
            sectores: this.sectores,
            actividades: this.actividades,
            estadosCiviles: this.estadosCiviles,
            sexo: this.sexo,
            girosEmpleador: this.girosEmpleador,
            parentesco: this.parentesco,
            necesidades: this.necesidades,
            presionArterial: this.presionArterial,
            acreditacion: this.acreditacion
        };
        var opcionesModal = { enableBackdropDismiss: false };
        var modalPresent = null;
        // console.log('NOTA '+JSON.stringify(this.nota));
        if (continuar) {
            if (this.nota.notaVenta.detalle.necesidad && this.nota.notaVenta.detalle.planimetria.descripcion &&
                this.nota.notaVenta.detalle.planimetria.letra && this.nota.notaVenta.detalle.dia_pago &&
                this.nota.notaVenta.detalle.vencimiento && this.nota.notaVenta.detalle.cuotas &&
                this.nota.notaVenta.detalle.valor_cuotas) {
                var modalSalud = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__mod_declaracion_salud_mod_declaracion_salud__["a" /* ModDeclaracionSaludPage */], data, opcionesModal);
                modalPresent = modalSalud;
            }
            else {
                this.mostarToast('Faltan parámetros para detalle');
            }
        }
        else {
            var modalAval = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_1__mod_ingreso_aval_mod_ingreso_aval__["a" /* ModIngresoAvalPage */], data, opcionesModal);
            modalPresent = modalAval;
        }
        if (modalPresent) {
            modalPresent.present();
            this.viewCtrl.dismiss();
        }
    };
    ModNotaIngresoDetallePage.prototype.abrirMapa = function () {
        var _this = this;
        if (this.validaIngresoNecesidad) {
            var dataMod = null;
            // console.log('NEC: '+this.necesidad);
            dataMod = { "necesidad": this.necesidad };
            var modalPlanimetria = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__mod_planimeria_mod_planimeria__["a" /* ModPlanimeriaPage */], dataMod, { enableBackdropDismiss: false });
            modalPlanimetria.onDidDismiss(function (data) {
                if (data) {
                    _this.detalle.planimetria.descripcion = data.descripcion;
                    _this.detalle.planimetria.letra = data.letra;
                    _this.detalle.planimetria.estado = data.estado;
                    _this.detalle.planimetria.sector = data.sector;
                    _this.detalle.planimetria.porc_descuento = data.porc_descuento;
                    _this.detalle.planimetria.uf = data.uf;
                    _this.detalle.planimetria.fila = data.fila;
                    _this.detalle.planimetria.latitud = data.latitud;
                    _this.detalle.planimetria.longitud = data.longitud;
                    _this.detalle.planimetria.numero = data.numero;
                    _this.detalle.planimetria.valor_base = data.valor_base;
                    _this.detalle.planimetria.capacidades = data.capacidades;
                    _this.detalle.planimetria.reducciones = data.reducciones;
                    _this.detalle.planimetria.pie = data.pie;
                }
                else {
                    _this.detalle.planimetria.descripcion = "";
                    _this.detalle.planimetria.letra = "";
                    _this.detalle.planimetria.estado = "";
                    _this.detalle.planimetria.sector = "";
                    _this.detalle.planimetria.porc_descuento = "";
                    _this.detalle.planimetria.uf = "";
                    _this.detalle.planimetria.fila = "";
                    _this.detalle.planimetria.latitud = "";
                    _this.detalle.planimetria.longitud = "";
                    _this.detalle.planimetria.numero = "";
                    _this.detalle.planimetria.valor_base = "";
                    _this.detalle.planimetria.capacidades = "";
                    _this.detalle.planimetria.reducciones = "";
                    _this.detalle.planimetria.pie = "";
                }
            });
            modalPlanimetria.present();
        }
        else {
            this.mostarToast('Debe seleccionar una necesidad');
        }
    };
    ModNotaIngresoDetallePage.prototype.mostarToast = function (mensaje) {
        if (mensaje) {
            var toast = this.toast.create({
                message: mensaje,
                duration: 3000,
                position: 'top'
            });
            toast.present();
        }
    };
    ModNotaIngresoDetallePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-mod-nota-ingreso-detalle',template:/*ion-inline-start:"C:\ionicproyectos\parqueCopiapo\src\pages\mod-nota-ingreso-detalle\mod-nota-ingreso-detalle.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Ingreso detalle de venta</ion-title>\n\n    <ion-buttons left>\n\n      <button ion-button icon-only (tap)="continuar(false)">\n\n        <ion-icon name="arrow-back"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <form (ngSubmit)="continuar(true)" #formDetalle="ngForm">\n\n    <ion-grid no-padding>\n\n      <ion-row>\n\n        <ion-col width-50>\n\n          <ion-card no-padding>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Tipo de necesidad *</ion-label>\n\n              <ion-select [(ngModel)]="detalle.necesidad" name="necesidad" no-lines placeholder="Seleccione necesidad" cancelText="Cancelar" (ionChange)="seleccionaNecesidad(detalle.necesidad,true,\'\')"\n\n                okText="Ok">\n\n                <ion-option [value]="ned.cod_tipo_necesidad" *ngFor="let ned of necesidades" (ionSelect)="seleccionaNecesidad(detalle.necesidad,false,ned.bit_Inmediato_necesidad)" >{{ ned.des_tipo_necesidad | uppercase }}</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Planimetría *</ion-label>\n\n              <ion-input [(ngModel)]="detalle.planimetria.descripcion" disabled="true" type="text" name="descripcion" placeholder="Planimetría"></ion-input>\n\n              <ion-icon class="icono_mapa" name="map" item-end (tap)="abrirMapa()"></ion-icon>    \n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Valor base *</ion-label>\n\n              <ion-input [(ngModel)]="detalle.planimetria.valor_base" disabled="true" type="text" name="valor_base" placeholder="Valor base"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Porcentaje de descuento *</ion-label>\n\n              <ion-input [(ngModel)]="detalle.planimetria.porc_descuento" disabled="true" type="text" name="descuento" placeholder="Porcentaje descuento"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Capacidades *</ion-label>\n\n              <ion-input [(ngModel)]="detalle.planimetria.capacidades" disabled="true" type="text" name="capacidades" placeholder="Capacidades"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Reducciones *</ion-label>\n\n              <ion-input [(ngModel)]="detalle.planimetria.reducciones" disabled="true" type="text" name="reducciones" placeholder="Reducciones"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>UF *</ion-label>\n\n              <ion-input [(ngModel)]="detalle.planimetria.uf" disabled="true" type="text" name="uf" placeholder="Ingrese uf"></ion-input>\n\n            </ion-item>\n\n          </ion-card>\n\n        </ion-col>\n\n        <ion-col width-50>\n\n          <ion-card no-padding>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Pie *</ion-label>\n\n              <ion-input [(ngModel)]="detalle.planimetria.pie" type="text" name="pie" placeholder="Ingrese pie"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Día de pago *</ion-label>\n\n              <ion-input [(ngModel)]="detalle.dia_pago" type="number" name="dia_pago" placeholder="Ingrese día de pago"></ion-input>\n\n            </ion-item>           \n\n            <button ion-item item-start outline type="button" icon-end (tap)="picker.open()">\n\n              <ion-icon name="calendar" item-end color="primary"></ion-icon>\n\n              <ion-datetime #picker pickerFormat="DD MMMM YYYY" min="2018" max="2030" cancelText="Cancelar" doneText="Aceptar"\n\n                [(ngModel)]="detalle.vencimiento" name="fecha_nacimiento"></ion-datetime>\n\n              <ion-label>Fecha primer vencimiento *</ion-label>\n\n            </button>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Número de cuotas *</ion-label>\n\n              <ion-input [(ngModel)]="detalle.cuotas" type="number" name="nro_cuotas" placeholder="Ingrese número de cuotas"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Valor de cuotas *</ion-label>\n\n              <ion-input [(ngModel)]="detalle.valor_cuotas" type="number" name="val_cuotas" placeholder="Ingrese valor de cuotas"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Comentario</ion-label>\n\n              <ion-input [(ngModel)]="detalle.comentario" type="text" name="val_cuotas" placeholder="Ingrese comentario"></ion-input>\n\n            </ion-item>\n\n          </ion-card>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </form>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <button type="submit" ion-button full color="primary" (tap)="formDetalle.ngSubmit.emit()">Continuar</button>\n\n</ion-footer>'/*ion-inline-end:"C:\ionicproyectos\parqueCopiapo\src\pages\mod-nota-ingreso-detalle\mod-nota-ingreso-detalle.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["u" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["A" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["r" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["y" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_0__providers_webservice_webservice__["a" /* WebserviceProvider */]])
    ], ModNotaIngresoDetallePage);
    return ModNotaIngresoDetallePage;
}());

//# sourceMappingURL=mod-nota-ingreso-detalle.js.map

/***/ }),

/***/ 207:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 207;

/***/ }),

/***/ 254:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 254;

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modelos_usuarioResponse__ = __webpack_require__(501);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__modelos_usuario__ = __webpack_require__(502);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_autenticacion_autenticacion__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5__ = __webpack_require__(513);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_network__ = __webpack_require__(80);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginPage = /** @class */ (function () {
    function LoginPage(auth, toast, navCtrl, navParams, plataforma, cargaCtrl, eventos, network) {
        this.auth = auth;
        this.toast = toast;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.plataforma = plataforma;
        this.cargaCtrl = cargaCtrl;
        this.eventos = eventos;
        this.network = network;
        this.user = [];
        this.logo = "./assets/imgs/logo_parque.png";
        this.esDispositivo = false;
        this.ip = null;
        var ipserver = localStorage.getItem('ip');
        if (ipserver) {
            this.ip = ipserver;
        }
        if (this.plataforma.is('Cordova')) {
            this.esDispositivo = true;
        }
        else {
            this.esDispositivo = false;
        }
    }
    LoginPage.prototype.ionViewDidLoad = function () { };
    LoginPage.prototype.guardarIP = function () {
        localStorage.setItem('ip', this.ip);
    };
    LoginPage.prototype.iniciarSesion = function () {
        var _this = this;
        if (this.usuario != null) {
            if (this.password != null) {
                if (this.ip != null) {
                    this.mostrarCarga();
                    this.passwordMD5 = __WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5__["Md5"].hashAsciiStr(this.password);
                    var usu = new __WEBPACK_IMPORTED_MODULE_1__modelos_usuario__["a" /* UsuarioModel */](this.usuario, this.passwordMD5);
                    this.auth.inciarSesion(usu).subscribe(function (data) {
                        _this.user = data['InciarSesionResult'];
                        if (_this.user['estado']) {
                            var usuResponse = new __WEBPACK_IMPORTED_MODULE_0__modelos_usuarioResponse__["a" /* UsuaurioResponseModel */](_this.user['codigo'], _this.user['cod_cargo'], _this.user['des_cargo'], _this.user['cod_comuna'], _this.user['rut'], _this.user['nombres'], _this.user['ap_paterno'], _this.user['ap_materno'], _this.user['direccion'], _this.user['email'], _this.user['fecha_nac'], _this.user['telefono']);
                            console.log('USUARIO ' + JSON.stringify(usuResponse));
                            if (_this.esDispositivo) {
                                localStorage.setItem('login', _this.user['codigo']);
                                localStorage.setItem('ejecutivo', _this.user['nombres'] + ' ' + _this.user['ap_paterno'] + ' ' + _this.user['ap_materno']);
                                localStorage.setItem('cod_cargo', _this.user['cod_cargo']);
                                _this.eventos.publish('verifica_ruta');
                            }
                            else {
                                localStorage.setItem('login', _this.user['codigo']);
                                localStorage.setItem('ejecutivo', _this.user['nombres'] + ' ' + _this.user['ap_paterno'] + ' ' + _this.user['ap_materno']);
                                localStorage.setItem('cod_cargo', _this.user['cod_cargo']);
                                _this.eventos.publish('verifica_ruta');
                            }
                            _this.cargando.dismiss();
                        }
                        else {
                            _this.cargando.dismiss();
                            _this.mostrarToast('Usuario/contraseña incorrectos');
                        }
                    }, function (error) {
                        _this.mostrarToast('Error en servidor');
                        _this.cargando.dismiss();
                    });
                }
                else {
                    this.mostrarToast('Ingrese ip del servidor');
                }
            }
            else {
                this.mostrarToast('Ingrese contraseña');
            }
        }
        else {
            this.mostrarToast('Ingrese usuario');
        }
    };
    LoginPage.prototype.mostrarCarga = function () {
        this.cargando = this.cargaCtrl.create({
            content: "Iniciando sesión..."
        });
        this.cargando.present();
    };
    LoginPage.prototype.mostrarToast = function (mensaje) {
        if (mensaje) {
            var toast = this.toast.create({
                message: mensaje,
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        }
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\ionicproyectos\parqueCopiapo\src\pages\login\login.html"*/'<ion-content>\n\n  <ion-card center>\n\n    <ion-card-header text-center>\n\n      <h1>\n\n        <strong>SISTEMA AVIZOR - PARQUE COPIAPO</strong>\n\n      </h1>\n\n    </ion-card-header>\n\n    <ion-card-content>\n\n      <div class="centrar">\n\n        <ion-img class="logo" [src]=logo></ion-img>\n\n      </div>\n\n      <ion-item>\n\n        <ion-label color="primary" floating>Ingrese IP servidor</ion-label>\n\n        <ion-input type="text" name="ip" [(ngModel)]="ip" (ionChange)="guardarIP()"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label color="primary" floating>Ingrese usuario</ion-label>\n\n        <ion-input type="text" name="usuario" [(ngModel)]="usuario"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label color="primary" floating>Ingrese contraseña</ion-label>\n\n        <ion-input type="password" name="password" [(ngModel)]="password"></ion-input>\n\n      </ion-item>\n\n      <div class="centrar" padding>\n\n        <button ion-button round (tap)="iniciarSesion()">Iniciar sesión</button>\n\n      </div>\n\n    </ion-card-content>\n\n  </ion-card>\n\n</ion-content>\n\n<ion-footer text-center>\n\n  <strong>PALL PROTECHNOLOGY®</strong>\n\n</ion-footer>'/*ion-inline-end:"C:\ionicproyectos\parqueCopiapo\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_autenticacion_autenticacion__["a" /* AutenticacionProvider */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["y" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["u" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["w" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["p" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_network__["a" /* Network */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutenticacionProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_retry__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_retry___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_retry__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_timeout__ = __webpack_require__(303);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_timeout__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//MODULOS





var AutenticacionProvider = /** @class */ (function () {
    function AutenticacionProvider(http) {
        this.http = http;
        this.ip = null;
        this.apiUrl = null;
        this.refrescarIP();
    }
    AutenticacionProvider.prototype.refrescarIP = function () {
        this.ip = localStorage.getItem('ip');
        if (this.ip) {
            this.apiUrl = "http://" + this.ip + "/wsJesusPons/Servicios/Usuario.svc/rest/login";
        }
    };
    AutenticacionProvider.prototype.inciarSesion = function (usuario) {
        this.refrescarIP();
        if (usuario.usuario && usuario.password) {
            var body = {
                "usuario": {
                    "IdUsuario": usuario.usuario,
                    "PswUsuario": usuario.password
                }
            };
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({
                'Content-type': 'application/json'
            });
            return this.http.post(this.apiUrl, body, { headers: headers })
                .retry(3)
                .timeout(5000)
                .catch(function (error) {
                return error;
            });
        }
    };
    AutenticacionProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], AutenticacionProvider);
    return AutenticacionProvider;
}());

//# sourceMappingURL=autenticacion.js.map

/***/ }),

/***/ 304:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_menu_principal_menu_principal__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, popoverCtrl, eventos, domSanitizer) {
        this.navCtrl = navCtrl;
        this.popoverCtrl = popoverCtrl;
        this.eventos = eventos;
        this.domSanitizer = domSanitizer;
    }
    HomePage.prototype.ionViewDidLoad = function () {
        this.urlLimpia = "http://186.10.19.170/dashboardmap/visor.aspx?id=1&index=1";
        this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(this.urlLimpia);
    };
    HomePage.prototype.mostrarMenu = function (evento) {
        var menu = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_0__components_menu_principal_menu_principal__["a" /* MenuPrincipalComponent */]);
        menu.present({
            ev: evento
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\ionicproyectos\parqueCopiapo\src\pages\home\home.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Inicio</ion-title>\n\n    <ion-buttons right>\n\n      <button ion-button icon-only (tap)="mostrarMenu($event)">\n\n        <ion-icon name="more"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <div>\n\n    <!-- <iframe class="iframe" [src]="url" scrolling="no" frameborder="0" style="top: 0;left: 0;width: 100%;"></iframe> -->\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\ionicproyectos\parqueCopiapo\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["x" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotaVentaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modelos_notaRequest__ = __webpack_require__(514);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_webservice_webservice__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mod_nota_ingreso_mod_nota_ingreso__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_menu_principal_menu_principal__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_chilean_rut__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_chilean_rut___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_chilean_rut__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NotaVentaPage = /** @class */ (function () {
    function NotaVentaPage(navCtrl, navParams, popoverCtrl, modalCtrl, webservice, loadCtrl, toast, eventos) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.modalCtrl = modalCtrl;
        this.webservice = webservice;
        this.loadCtrl = loadCtrl;
        this.toast = toast;
        this.eventos = eventos;
        this.fecha = new Date().toISOString();
        this.notas = [];
        this.cargado = false;
        this.cambiaFecha(this.fecha);
        eventos.subscribe('recarga_notas', function () {
            _this.cambiaFecha(new Date().toISOString());
        });
    }
    NotaVentaPage.prototype.ionViewDidLoad = function () { };
    NotaVentaPage.prototype.cargarNotasVentas = function (nota) {
        var _this = this;
        if (nota) {
            this.mostrarCarga();
            setTimeout(function () {
                _this.webservice.obtenerListaNotaVenta(nota).subscribe(function (data) {
                    // console.log('NOTAS: ' + JSON.stringify(data));
                    var resultado = data['ListadoNotaVentaCementerioResult']['lista'];
                    if (resultado !== null) {
                        resultado.forEach(function (item, index) {
                            var fecha = null;
                            fecha = resultado[index]['dat_fecha_notavta'];
                            var fechaFormateada = null;
                            if (fecha) {
                                fechaFormateada = new Date(parseInt(fecha.replace(/(^.*\()|([+-].*$)/g, ''))).toISOString();
                            }
                            else {
                                fechaFormateada = "";
                            }
                            var rutCliente = resultado[index]['cod_cliente_cliente'];
                            var rutFormateado = null;
                            if (rutCliente.length == 8 || rutCliente.length == 9) {
                                var dv = rutCliente.substring(8);
                                rutFormateado = __WEBPACK_IMPORTED_MODULE_6_chilean_rut___default.a.format(rutCliente, dv);
                            }
                            if (fechaFormateada) {
                                resultado[index]['dat_fecha_notavta'] = fechaFormateada;
                            }
                            else {
                                resultado[index]['dat_fecha_notavta'] = "";
                            }
                            resultado[index]['cod_cliente_cliente'] = rutFormateado;
                            _this.notas = resultado;
                        });
                        _this.cargado = true;
                    }
                    else {
                        _this.cargado = false;
                    }
                    _this.cargando.dismiss();
                }, function (error) {
                    _this.cargado = false;
                    _this.cargando.dismiss();
                    _this.mostarToast('Error en la conexión con servidor');
                    console.log('Error en consulta a WS ' + JSON.stringify(error));
                });
            }, 5000);
        }
    };
    NotaVentaPage.prototype.cambiaFecha = function (fecha) {
        if (fecha) {
            var mes = fecha.split("-")[1];
            var ano = fecha.split("-")[0];
            var usuario = parseInt(localStorage.getItem('login'));
            if (mes < 99 && ano < 9999 && usuario > 0) {
                var notaRequest = new __WEBPACK_IMPORTED_MODULE_0__modelos_notaRequest__["a" /* NotaRequestModel */](usuario, mes, ano);
                this.cargarNotasVentas(notaRequest);
            }
        }
    };
    NotaVentaPage.prototype.mostarToast = function (mensaje) {
        if (mensaje) {
            var toast = this.toast.create({
                message: mensaje,
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        }
    };
    NotaVentaPage.prototype.editarNota = function (nota) { };
    NotaVentaPage.prototype.eliminarNota = function (nota) { };
    NotaVentaPage.prototype.mostrarCarga = function () {
        this.cargando = this.loadCtrl.create({
            content: "Cargando notas de venta..."
        });
        this.cargando.present();
    };
    NotaVentaPage.prototype.ingresarNotaVenta = function () {
        var modalIngreso = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__mod_nota_ingreso_mod_nota_ingreso__["a" /* ModNotaIngresoPage */], null, { enableBackdropDismiss: false });
        modalIngreso.present();
    };
    NotaVentaPage.prototype.mostrarMenuPrincipal = function (evento) {
        var menu = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_5__components_menu_principal_menu_principal__["a" /* MenuPrincipalComponent */]);
        menu.present({
            ev: evento
        });
    };
    var _a, _b, _c, _d, _e, _f, _g, _h;
    NotaVentaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-nota-venta',template:/*ion-inline-start:"C:\ionicproyectos\parqueCopiapo\src\pages\nota-venta\nota-venta.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Notas de venta</ion-title>\n\n    <ion-buttons right>\n\n      <button ion-button icon-only (tap)="mostrarMenuPrincipal($event)">\n\n        <ion-icon name="more"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <!-- Seleccion de fecha -->\n\n  <ion-card>\n\n    <ion-item>\n\n      <ion-label>Seleccione fecha</ion-label>\n\n      <ion-datetime [(ngModel)]="fecha" min="2018" (ionChange)="cambiaFecha(fecha)" max="2030" displayFormat="MMMM YYYY" cancelText="Cancelar"\n\n        doneText="Aceptar">\n\n      </ion-datetime>\n\n    </ion-item>\n\n  </ion-card>\n\n  <!-- Iteracion de notas de ventas -->\n\n  <ion-card [hidden]="!cargado">\n\n    <!-- Contenido tarjeta -->\n\n    <ion-card-content>\n\n      <ion-row class="row header">\n\n        <ion-col class="col" col-1>\n\n          <strong>#</strong>\n\n        </ion-col>\n\n        <ion-col class="col" col>\n\n          <strong>RUT CLIENTE</strong>\n\n        </ion-col>\n\n        <ion-col class="col" col>\n\n          <strong>EMPLEADOR</strong>\n\n        </ion-col>\n\n        <ion-col class="col" col>\n\n          <strong>FECHA</strong>\n\n        </ion-col>\n\n        <ion-col class="col" col>\n\n          <strong>TIPO</strong>\n\n        </ion-col>\n\n        <ion-col class="col" col>\n\n          <strong>ESTADO</strong>\n\n        </ion-col>\n\n        <ion-col class="col" col>\n\n          <strong>ACCION</strong>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row *ngFor="let item of notas">\n\n        <ion-col class="col" col-1>{{ item.num_nvta_contrato }}</ion-col>\n\n        <ion-col class="col" col>{{ item.cod_cliente_cliente }}</ion-col>\n\n        <ion-col class="col" col>{{ item.des_nombre_empresa | uppercase }}</ion-col>\n\n        <ion-col class="col" col>{{ item.dat_fecha_notavta | date: \'dd MMMM yyyy\' }}</ion-col>\n\n        <ion-col class="col" col>{{ item.des_tipo_necesidad | uppercase }}</ion-col>\n\n        <ion-col class="col" col>{{ item.des_estado_contrato | uppercase }}</ion-col>\n\n        <ion-col class="col" center>\n\n          <!-- Boton editar -->\n\n          <ion-icon class="icono" name="document" item-end (tap)="editarNota(item)"></ion-icon>\n\n          <!-- Boton eliminar -->\n\n          <ion-icon class="icono" name="close" item-end (tap)="eliminarNota(item)"></ion-icon>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-content>\n\n  </ion-card>\n\n\n\n  <!-- Boton Flotante -->\n\n  <ion-fab bottom right>\n\n    <button ion-fab (tap)="ingresarNotaVenta()">\n\n      <ion-icon name="add"></ion-icon>\n\n    </button>\n\n  </ion-fab>\n\n</ion-content>'/*ion-inline-end:"C:\ionicproyectos\parqueCopiapo\src\pages\nota-venta\nota-venta.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["t" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["t" /* NavController */]) === "function" ? _a : Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["u" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["u" /* NavParams */]) === "function" ? _b : Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["x" /* PopoverController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["x" /* PopoverController */]) === "function" ? _c : Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["r" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["r" /* ModalController */]) === "function" ? _d : Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1__providers_webservice_webservice__["a" /* WebserviceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__providers_webservice_webservice__["a" /* WebserviceProvider */]) === "function" ? _e : Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["p" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["p" /* LoadingController */]) === "function" ? _f : Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["y" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["y" /* ToastController */]) === "function" ? _g : Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* Events */]) === "function" ? _h : Object])
    ], NotaVentaPage);
    return NotaVentaPage;
}());

//# sourceMappingURL=nota-venta.js.map

/***/ }),

/***/ 306:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClienteRequestModel; });
var ClienteRequestModel = /** @class */ (function () {
    function ClienteRequestModel(cod_vendedor_fichapersonal, cod_cliente_cliente, cod_comuna_comuna, cod_giro_giro, des_nombre_cliente, des_direccion_cliente, des_telefono_cliente, des_mail_cliente, cod_ciudad_ciudad, cod_sector_sector, cod_estadocivil_estadocivil, des_telefono_celular, dat_fecha_nacimiento, edad, cod_sexo_sexo, num_montorentaliquida_clientes, cod_cargo_cargo, cod_parentesco_parentesco) {
        this.cod_vendedor_fichapersonal = cod_vendedor_fichapersonal;
        this.cod_cliente_cliente = cod_cliente_cliente;
        this.cod_comuna_comuna = cod_comuna_comuna;
        this.cod_giro_giro = cod_giro_giro;
        this.des_nombre_cliente = des_nombre_cliente;
        this.des_direccion_cliente = des_direccion_cliente;
        this.des_telefono_cliente = des_telefono_cliente;
        this.des_mail_cliente = des_mail_cliente;
        this.cod_ciudad_ciudad = cod_ciudad_ciudad;
        this.cod_sector_sector = cod_sector_sector;
        this.cod_estadocivil_estadocivil = cod_estadocivil_estadocivil;
        this.des_telefono_celular = des_telefono_celular;
        this.dat_fecha_nacimiento = dat_fecha_nacimiento;
        this.edad = edad;
        this.cod_sexo_sexo = cod_sexo_sexo;
        this.num_montorentaliquida_clientes = num_montorentaliquida_clientes;
        this.cod_cargo_cargo = cod_cargo_cargo;
        this.cod_parentesco_parentesco = cod_parentesco_parentesco;
    }
    return ClienteRequestModel;
}());

//# sourceMappingURL=clienteRequest.js.map

/***/ }),

/***/ 307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IngresoDocumentosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_ionic_native_file_chooser__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_file_path__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_base64__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_img_viewer__ = __webpack_require__(312);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var IngresoDocumentosPage = /** @class */ (function () {
    function IngresoDocumentosPage(platform, navCtrl, navParams, viewCtrl, toast, alertCtrl, fileChooser, filePath, base64, camera, actionSheetCtrl, imageViewerCtrl) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.toast = toast;
        this.alertCtrl = alertCtrl;
        this.fileChooser = fileChooser;
        this.filePath = filePath;
        this.base64 = base64;
        this.camera = camera;
        this.actionSheetCtrl = actionSheetCtrl;
        this.imageViewerCtrl = imageViewerCtrl;
        this.imagenes = [];
        this.test = [];
        this._imageViewerCtrl = imageViewerCtrl;
        console.log('TIPO ' + this.navParams.get('tipo'));
        if (this.navParams.get('tipo')) {
            this.tipo = this.navParams.get('tipo');
            this.cod = parseInt(this.navParams.get('acreditacion'));
        }
        else {
            this.tipo = this.navParams.get('tipo');
            this.cod = parseInt(this.navParams.get('otros_ingresos'));
        }
    }
    IngresoDocumentosPage.prototype.verificaDispositivo = function () {
        if (this.platform.is('cordova')) {
            this.esDispositivo = true;
            console.log('DISPOSITIVO');
        }
        else {
            this.esDispositivo = false;
            console.log('NAVEGADOR');
        }
    };
    IngresoDocumentosPage.prototype.ionViewDidLoad = function () { };
    IngresoDocumentosPage.prototype.camara = function () {
        var _this = this;
        var opcionesCamara = {
            quality: 70,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            targetWidth: 720,
            correctOrientation: true,
            allowEdit: false
        };
        this.camera.getPicture(opcionesCamara).then(function (imageData) {
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            // console.log('cam: ' + base64Image.substring(0, 50));
            _this.imagenes.push(base64Image);
        }, function (err) {
            console.log('Error Cámara ' + JSON.stringify(err));
        });
    };
    IngresoDocumentosPage.prototype.agregarImagenes = function () {
        var _this = this;
        this.fileChooser.open().then(function (uri) {
            console.log('URI: ' + uri);
            _this.filePath.resolveNativePath(uri)
                .then(function (file) {
                console.log('FILE: ' + file);
                var filePath = file.replace('file://', '');
                if (filePath) {
                    _this.imagenes.push(filePath);
                    console.log('PATH: ' + filePath);
                    _this.base64.encodeFile(filePath).then(function (base64File) {
                        // let formateada = base64File.replace(base64File.substring(0, 34), 'data:image/jpeg;base64,');
                        // console.log('arc: ' + formateada.substring(0, 50));
                    }, function (err) {
                        console.log('Error al obtener base64 ' + JSON.stringify(err));
                    });
                }
            }).catch(function (err) {
                return console.log(err);
            });
        }).catch(function (error) {
            console.log('Error al conseguir URI ' + JSON.stringify(error));
        });
    };
    IngresoDocumentosPage.prototype.opcionesImagen = function (id, myImage) {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Opciones imagen',
            buttons: [
                {
                    text: 'Mostrar',
                    handler: function () {
                        var imageViewer = _this._imageViewerCtrl.create(myImage);
                        imageViewer.present();
                    }
                }, {
                    text: 'Eliminar',
                    role: 'destructive',
                    handler: function () {
                        _this.imagenes.splice(id, 1);
                    }
                }, {
                    text: 'Cancelar',
                    role: 'cancel',
                }
            ]
        });
        actionSheet.present();
    };
    IngresoDocumentosPage.prototype.cerrar = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Cancelar Ingreso',
            message: '¿Está seguro que quiere cancelar el ingreso de documentos?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () { }
                }, {
                    text: 'Si',
                    handler: function () {
                        _this.viewCtrl.dismiss(null);
                    }
                }
            ]
        });
        alert.present();
    };
    IngresoDocumentosPage.prototype.guardar = function () {
        var data = [];
        if (this.monto) {
            var insert = {
                EsAcreditacion: this.tipo,
                id_codigo_taingresos: String(this.cod),
                num_monto_sctaingresos: this.monto,
                Glosa: this.comentario,
                img_imagen_sctaingresos: null
            };
            for (var _i = 0, _a = this.imagenes; _i < _a.length; _i++) {
                var i = _a[_i];
                insert.img_imagen_sctaingresos = i;
                data.push(insert);
            }
            this.viewCtrl.dismiss(data);
        }
        else {
            this.mostarToast("Falta ingreso de monto");
        }
    };
    IngresoDocumentosPage.prototype.mostarToast = function (mensaje) {
        if (mensaje) {
            var toast = this.toast.create({
                message: mensaje,
                duration: 3000,
                position: 'top'
            });
            toast.present();
        }
    };
    IngresoDocumentosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-ingreso-documentos',template:/*ion-inline-start:"C:\ionicproyectos\parqueCopiapo\src\pages\ingreso-documentos\ingreso-documentos.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Ingreso documentos</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (tap)="cerrar()">\n\n        <ion-icon name="close"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <form (ngSubmit)="guardar()" #formDocumentos="ngForm">\n\n    <ion-grid no-padding>\n\n      <ion-row>\n\n        <ion-card>\n\n          <ion-label text-center color="primary"><strong>ARCHIVOS</strong></ion-label>\n\n          <ion-item no-lines>\n\n            <ion-scroll scrollX="true">\n\n              <img *ngFor="let item of imagenes; let i = index" src="{{ item }}" class="scroll-item selectable-img" #miImagen (tap)="opcionesImagen(i,miImagen)">             \n\n            </ion-scroll> \n\n          </ion-item>\n\n        </ion-card>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-card>\n\n          <ion-item>\n\n            <ion-label color="primary" stacked>Monto *</ion-label>\n\n            <ion-input type="text" [(ngModel)]="monto" name="monto" placeholder="Ingrese monto"></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label color="primary" stacked>Comentario</ion-label>\n\n            <ion-textarea type="text" [(ngModel)]="comentario" name="comentario" placeholder="Ingrese comentario" rows="4"></ion-textarea>\n\n          </ion-item>\n\n        </ion-card>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </form>\n\n\n\n  <!-- Botones Flotantes -->\n\n  <ion-fab bottom right>\n\n    <ion-row>\n\n      <!-- <ion-col>\n\n        <button (click)="agregarImagenes()" color="orange" ion-fab>\n\n          <ion-icon name="add"></ion-icon>\n\n        </button>\n\n      </ion-col> -->\n\n      <ion-col>\n\n        <button (click)="camara()" color="secondary" ion-fab>\n\n          <ion-icon name="camera"></ion-icon>\n\n        </button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-fab>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <button ion-button full color="primary" (tap)="formDocumentos.ngSubmit.emit()">Guardar</button>\n\n</ion-footer>'/*ion-inline-end:"C:\ionicproyectos\parqueCopiapo\src\pages\ingreso-documentos\ingreso-documentos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["A" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["y" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__node_modules_ionic_native_file_chooser__["a" /* FileChooser */],
            __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_base64__["a" /* Base64 */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_img_viewer__["a" /* ImageViewerController */]])
    ], IngresoDocumentosPage);
    return IngresoDocumentosPage;
}());

//# sourceMappingURL=ingreso-documentos.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModDeclaracionSaludPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modelos_notaPlanimetriaRequest__ = __webpack_require__(530);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__modelos_notaSaludRequest__ = __webpack_require__(531);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mod_nota_ingreso_detalle_mod_nota_ingreso_detalle__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_webservice_webservice__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modelos_notaCabeceraRequest__ = __webpack_require__(532);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_chilean_rut__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_chilean_rut___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_chilean_rut__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ModDeclaracionSaludPage = /** @class */ (function () {
    function ModDeclaracionSaludPage(navCtrl, navParams, platform, alertCtrl, viewCtrl, toast, modalCtrl, loadCtrl, webservice, eventos) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.toast = toast;
        this.modalCtrl = modalCtrl;
        this.loadCtrl = loadCtrl;
        this.webservice = webservice;
        this.eventos = eventos;
        this.selectedColor = '#000';
        this.firmaBase64 = null;
        this.cliente = {};
        this.empleador = {};
        this.aval = {};
        this.detalle = {};
        this.planimetria = {};
        this.salud = {};
        //Carga de datos
        this.comunas = [];
        this.ciudades = [];
        this.sectores = [];
        this.actividades = [];
        this.estadosCiviles = [];
        this.sexo = [];
        this.girosEmpleador = [];
        this.parentesco = [];
        this.necesidades = [];
        this.presionArterial = [];
        this.acreditacion = [];
        this.documentos = [];
        this.nota = {
            notaVenta: {
                cliente: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    sector: null,
                    actividad: null,
                    ciudad: null,
                    civil: null,
                    correo: null,
                    telefono: null,
                    celular: null,
                    fechaNacimiento: null,
                    edad: null,
                    sexo: null
                },
                empleador: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    actividad: null,
                    ciudad: null,
                    acreditacion: [],
                    renta: null,
                    giro: null,
                    telefono: null,
                    otrosIngresos: []
                },
                aval: {
                    rut: null,
                    nombre: null,
                    direccion: null,
                    comuna: null,
                    sector: null,
                    actividad: null,
                    ciudad: null,
                    civil: null,
                    correo: null,
                    telefono: null,
                    celular: null,
                    fechaNacimiento: null,
                    edad: null,
                    sexo: null,
                    renta: null,
                    parentesco: null
                },
                detalle: {
                    necesidad: null,
                    planimetria: {
                        descripcion: null,
                        letra: null,
                        estado: null,
                        sector: 0,
                        porc_descuento: 0,
                        uf: 0,
                        fila: null,
                        latitud: 0,
                        longitud: 0,
                        numero: 0,
                        valor_base: 0,
                        capacidades: 0,
                        reducciones: 0,
                        pie: 0
                    },
                    descuento: null,
                    pie: 0,
                    dia_pago: 0,
                    vencimiento: null,
                    cuotas: null,
                    valor_cuotas: null,
                    uf: null,
                    comentario: null
                },
                salud: {
                    responde: false,
                    peso: 0,
                    estatura: 0,
                    presion: null,
                    ejecutiva: null,
                    comentario: null,
                    firma: null
                }
            }
        };
        var ejecutivo = localStorage.getItem('ejecutivo');
        this.nota.notaVenta.salud.ejecutiva = ejecutivo;
        this.obtener();
    }
    ModDeclaracionSaludPage.prototype.obtener = function () {
        //Asignar datos WS
        this.ciudades = this.navParams.get('ciudades');
        this.sectores = this.navParams.get('sectores');
        this.actividades = this.navParams.get('actividades');
        this.estadosCiviles = this.navParams.get('estadosCiviles');
        this.sexo = this.navParams.get('sexo');
        this.girosEmpleador = this.navParams.get('girosEmpleador');
        this.parentesco = this.navParams.get('parentesco');
        this.necesidades = this.navParams.get('necesidades');
        this.presionArterial = this.navParams.get('presionArterial');
        this.acreditacion = this.navParams.get('acreditacion');
        var data = this.navParams.get('nota');
        if (data !== undefined) {
            if (data.notaVenta.cliente !== undefined) {
                this.cliente = data.notaVenta.cliente;
            }
            if (data.notaVenta.empleador !== undefined) {
                this.empleador = data.notaVenta.empleador;
            }
            if (data.notaVenta.aval !== undefined) {
                this.aval = data.notaVenta.aval;
            }
            if (data.notaVenta.detalle !== undefined) {
                this.detalle = data.notaVenta.detalle;
            }
            if (data.notaVenta.detalle.planimetria !== undefined) {
                this.planimetria = data.notaVenta.detalle.planimetria;
            }
            if (data.notaVenta.salud !== undefined) {
                this.salud = data.notaVenta.salud;
            }
        }
    };
    ModDeclaracionSaludPage.prototype.ionViewLoaded = function () { };
    ModDeclaracionSaludPage.prototype.ionViewDidLoad = function () {
        this.canvasElement = this.canvas.nativeElement;
        this.canvasElement.width = 450;
        this.canvasElement.height = 300;
    };
    ModDeclaracionSaludPage.prototype.guardarFirma = function () {
        var dataUrl = this.canvasElement.toDataURL();
        var ctx = this.canvasElement.getContext('2d');
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        var data = dataUrl.split(',')[1];
        if (data) {
            this.firmaBase64 = data;
        }
    };
    ModDeclaracionSaludPage.prototype.limpiaFirma = function () {
        var ctx = this.canvasElement.getContext('2d');
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        this.firmaBase64 = '';
    };
    ModDeclaracionSaludPage.prototype.b64toBlob = function (b64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };
    ModDeclaracionSaludPage.prototype.terminar = function (terminar) {
        var _this = this;
        this.guardarFirma();
        this.nota.notaVenta.cliente = this.cliente;
        this.nota.notaVenta.empleador = this.empleador;
        this.nota.notaVenta.aval = this.aval;
        this.nota.notaVenta.detalle = this.detalle;
        this.nota.notaVenta.detalle.planimetria = this.planimetria;
        this.nota.notaVenta.salud = this.salud;
        this.nota.notaVenta.salud.firma = this.firmaBase64;
        if (this.nota.notaVenta.salud.responde &&
            // this.nota.notaVenta.salud.peso && this.nota.notaVenta.salud.estatura && this.nota.notaVenta.salud.presion &&
            this.nota.notaVenta.salud.ejecutiva && this.nota.notaVenta.salud.firma) {
            this.mostrarCarga();
            var cod_vendedor = parseInt(localStorage.getItem('login'));
            var cabeceraRequest = new __WEBPACK_IMPORTED_MODULE_6__modelos_notaCabeceraRequest__["a" /* NotaCabeceraRequest */](__WEBPACK_IMPORTED_MODULE_7_chilean_rut___default.a.unformat(this.nota.notaVenta.cliente.rut), __WEBPACK_IMPORTED_MODULE_7_chilean_rut___default.a.unformat(this.nota.notaVenta.empleador.rut), __WEBPACK_IMPORTED_MODULE_7_chilean_rut___default.a.unformat(this.nota.notaVenta.aval.rut), __WEBPACK_IMPORTED_MODULE_7_chilean_rut___default.a.unformat(this.nota.notaVenta.aval.rut), cod_vendedor, this.nota.notaVenta.detalle.planimetria.pie, this.nota.notaVenta.detalle.necesidad, this.nota.notaVenta.detalle.planimetria.valor_base, this.nota.notaVenta.detalle.planimetria.porc_descuento, parseInt(this.nota.notaVenta.detalle.dia_pago), parseInt(this.nota.notaVenta.detalle.cuotas), parseInt(this.nota.notaVenta.detalle.valor_cuotas), parseInt(this.nota.notaVenta.detalle.planimetria.uf), this.nota.notaVenta.detalle.comentario, this.nota.notaVenta.detalle.vencimiento, this.nota.notaVenta.salud.firma);
            var planimetriaRequest = new __WEBPACK_IMPORTED_MODULE_0__modelos_notaPlanimetriaRequest__["a" /* NotaPlanimetriaRequest */](this.nota.notaVenta.detalle.planimetria.numero, this.nota.notaVenta.detalle.planimetria.sector, this.nota.notaVenta.detalle.planimetria.letra, this.nota.notaVenta.detalle.planimetria.capacidades, this.nota.notaVenta.detalle.planimetria.capacidades, this.nota.notaVenta.detalle.planimetria.reducciones);
            var saludRequest = new __WEBPACK_IMPORTED_MODULE_1__modelos_notaSaludRequest__["a" /* NotaSaludRequestModel */](this.nota.notaVenta.salud.responde, this.nota.notaVenta.salud.peso, this.nota.notaVenta.salud.estatura, this.nota.notaVenta.salud.comentario, this.nota.notaVenta.salud.presion);
            if (this.nota.notaVenta.empleador.acreditacion) {
                this.documentos.push(this.nota.notaVenta.empleador.acreditacion);
            }
            if (this.nota.notaVenta.empleador.otrosIngresos) {
                this.documentos.push(this.nota.notaVenta.empleador.otrosIngresos);
            }
            this.webservice.guardarNotaVenta(cabeceraRequest, planimetriaRequest, saludRequest, this.documentos[0]).subscribe(function (data) {
                console.log('RESPUESTA WS ' + JSON.stringify(data));
                var resultado = data['GuardarNotaVentaResult']['resultado'];
                var mensaje = data['GuardarNotaVentaResult']['MensajeError'];
                var num_notavta = data['GuardarNotaVentaResult']['num_nvta_contrato'];
                if (resultado) {
                    _this.cargando.dismiss();
                    _this.mostarToast('Nota de venta Nº ' + num_notavta + ' ingresada correctamente');
                    _this.eventos.publish('recarga_notas');
                    _this.viewCtrl.dismiss();
                }
                else {
                    _this.cargando.dismiss();
                    console.log('Mensaje Server: ' + JSON.stringify(mensaje));
                    _this.mostarToast('Error al crear nota de venta, por favor reintente...');
                }
            }, function (error) {
                console.log('ERROR WS ' + JSON.stringify(error));
                _this.cargando.dismiss();
                _this.mostarToast('Error al ingresar nota de venta, error en el servidor');
            });
        }
        else {
            this.mostarToast('Faltan parámetros para declaración salud');
        }
    };
    ModDeclaracionSaludPage.prototype.volver = function () {
        this.nota.notaVenta.cliente = this.cliente;
        this.nota.notaVenta.empleador = this.empleador;
        this.nota.notaVenta.aval = this.aval;
        this.nota.notaVenta.detalle = this.detalle;
        this.nota.notaVenta.detalle.planimetria = this.planimetria;
        this.nota.notaVenta.salud = this.salud;
        var data = {
            nota: this.nota,
            ciudades: this.ciudades,
            sectores: this.sectores,
            actividades: this.actividades,
            estadosCiviles: this.estadosCiviles,
            sexo: this.sexo,
            girosEmpleador: this.girosEmpleador,
            parentesco: this.parentesco,
            necesidades: this.necesidades,
            presionArterial: this.presionArterial,
            acreditacion: this.acreditacion
        };
        var opcionesModal = { enableBackdropDismiss: false };
        var modalDetalle = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__mod_nota_ingreso_detalle_mod_nota_ingreso_detalle__["a" /* ModNotaIngresoDetallePage */], data, opcionesModal);
        modalDetalle.present();
        this.viewCtrl.dismiss();
    };
    ModDeclaracionSaludPage.prototype.mostrarCarga = function () {
        this.cargando = this.loadCtrl.create({
            content: "Ingresando nota de venta..."
        });
        this.cargando.present();
    };
    ModDeclaracionSaludPage.prototype.mostarToast = function (mensaje) {
        if (mensaje) {
            var toast = this.toast.create({
                message: mensaje,
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        }
    };
    ModDeclaracionSaludPage.prototype.iniciarDibujo = function (ev) {
        var canvasPosition = this.canvasElement.getBoundingClientRect();
        this.saveX = ev.touches[0].pageX - canvasPosition.x;
        this.saveY = ev.touches[0].pageY - canvasPosition.y;
    };
    ModDeclaracionSaludPage.prototype.dibujado = function (ev) {
        var canvasPosition = this.canvasElement.getBoundingClientRect();
        var ctx = this.canvasElement.getContext('2d');
        var currentX = ev.touches[0].pageX - canvasPosition.x;
        var currentY = ev.touches[0].pageY - canvasPosition.y;
        ctx.lineJoin = 'round';
        ctx.strokeStyle = this.selectedColor;
        ctx.lineWidth = 3;
        ctx.beginPath();
        ctx.moveTo(this.saveX, this.saveY);
        ctx.lineTo(currentX, currentY);
        ctx.closePath();
        ctx.stroke();
        this.saveX = currentX;
        this.saveY = currentY;
    };
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l;
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["_9" /* ViewChild */])('imageCanvas'),
        __metadata("design:type", Object)
    ], ModDeclaracionSaludPage.prototype, "canvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["_9" /* ViewChild */])('fixedContainer'),
        __metadata("design:type", Object)
    ], ModDeclaracionSaludPage.prototype, "fixedContainer", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* Content */]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* Content */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* Content */]) === "function" ? _a : Object)
    ], ModDeclaracionSaludPage.prototype, "content", void 0);
    ModDeclaracionSaludPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-mod-declaracion-salud',template:/*ion-inline-start:"C:\ionicproyectos\parqueCopiapo\src\pages\mod-declaracion-salud\mod-declaracion-salud.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Registro de Nota de Venta</ion-title>\n\n    <ion-buttons left>\n\n      <button ion-button icon-only (tap)="volver()">\n\n        <ion-icon name="arrow-back"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <form (ngSubmit)="terminar(true)" #fromSalud>\n\n    <ion-grid no-padding>\n\n      <ion-row>\n\n        <ion-col width-50>\n\n          <ion-card no-padding>\n\n            <ion-list radio-group [(ngModel)]="salud.responde" name="responde">\n\n              <ion-list-header>\n\n                ¿Responde lo solicitado? *\n\n              </ion-list-header>\n\n              <ion-item>\n\n                <ion-label>Sí</ion-label>\n\n                <ion-radio value="true" checked></ion-radio>\n\n              </ion-item>\n\n              <ion-item>\n\n                <ion-label>No</ion-label>\n\n                <ion-radio value="false"></ion-radio>\n\n              </ion-item>\n\n            </ion-list>\n\n            <!-- <ion-item>\n\n              <ion-label color="primary" stacked>Peso *</ion-label>\n\n              <ion-input [(ngModel)]="salud.peso" maxlength="3" type="number" name="peso" placeholder="Ingrese peso"></ion-input>\n\n            </ion-item> -->\n\n            <!-- <ion-item>\n\n              <ion-label color="primary" stacked>Estatura *</ion-label>\n\n              <ion-input [(ngModel)]="salud.estatura" name="estatura" maxlength="3" type="number" name="estatura" placeholder="Ingrese estatura"></ion-input>\n\n            </ion-item> -->\n\n            <!-- <ion-item>\n\n              <ion-label color="primary" stacked>Presión arterial *</ion-label>\n\n              <ion-select [(ngModel)]="salud.presion" name="presion" no-lines placeholder="Seleccione presión arterial" cancelText="Cancelar"\n\n                okText="Ok">\n\n                <ion-option [value]="pres.id_codigo_parterial" *ngFor="let pres of presionArterial">{{ pres.des_tipo_parterial | uppercase}}</ion-option>\n\n              </ion-select>\n\n            </ion-item> -->\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Nombre ejecutivo *</ion-label>\n\n              <ion-input [(ngModel)]="salud.ejecutiva" disabled="true" type="text" name="ejecutiva" placeholder="Nombre ejecutivo"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-label color="primary" stacked>Comentario</ion-label>\n\n              <ion-input [(ngModel)]="salud.comentario" maxlength="100" type="text" name="comentario" placeholder="Ingrese comentario"></ion-input>\n\n            </ion-item>\n\n          </ion-card>\n\n        </ion-col>\n\n        <ion-col width-50>\n\n          <ion-card no-padding>\n\n            <div #fixedContainer ion-fixed>\n\n              <h3>Firma titular</h3>\n\n                <canvas #imageCanvas (touchstart)="iniciarDibujo($event)" (touchmove)="dibujado($event)"></canvas>\n\n                <button ion-button type="button" (tap)="limpiaFirma()" >\n\n                  <ion-icon name="trash"></ion-icon>\n\n                </button>    \n\n            </div>\n\n          </ion-card>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </form>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <button type="submit" ion-button full color="primary" (tap)="terminar()">Terminar</button>\n\n</ion-footer>'/*ion-inline-end:"C:\ionicproyectos\parqueCopiapo\src\pages\mod-declaracion-salud\mod-declaracion-salud.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["t" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["t" /* NavController */]) === "function" ? _b : Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["u" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["u" /* NavParams */]) === "function" ? _c : Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["w" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["w" /* Platform */]) === "function" ? _d : Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* AlertController */]) === "function" ? _e : Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["A" /* ViewController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["A" /* ViewController */]) === "function" ? _f : Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["y" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["y" /* ToastController */]) === "function" ? _g : Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["r" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["r" /* ModalController */]) === "function" ? _h : Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["p" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["p" /* LoadingController */]) === "function" ? _j : Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_5__providers_webservice_webservice__["a" /* WebserviceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__providers_webservice_webservice__["a" /* WebserviceProvider */]) === "function" ? _k : Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* Events */]) === "function" ? _l : Object])
    ], ModDeclaracionSaludPage);
    return ModDeclaracionSaludPage;
}());

//# sourceMappingURL=mod-declaracion-salud.js.map

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModPlanimeriaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modelos_planimetriaRequest__ = __webpack_require__(533);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_webservice_webservice__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ModPlanimeriaPage = /** @class */ (function () {
    function ModPlanimeriaPage(navCtrl, navParams, viewCtrl, toast, webService, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.toast = toast;
        this.webService = webService;
        this.alertCtrl = alertCtrl;
        this.mapa = "./assets/imgs/mapa.png";
        this.planMan = false;
        this.planimetria = {
            descripcion: null,
            letra: null,
            estado: null,
            sector: 0,
            porc_descuento: 0,
            uf: 0,
            fila: null,
            latitud: 0,
            longitud: 0,
            numero: 0,
            valor_base: 0,
            capacidades: 0,
            reducciones: 0,
            pie: 0
        };
        this.listaPlanimetria = [];
        this.necesidad = null;
        this.codigo = 0;
        //Estilo del mapa
        this.estiloMapa = [{
                elementType: "labels.icon",
                stylers: [{
                        visibility: "off"
                    }]
            }];
        this.zoom = 18;
        this.obtener();
    }
    ModPlanimeriaPage.prototype.ionViewDidLoad = function () {
        this.lat = -27.377015;
        this.lng = -70.334138;
    };
    ModPlanimeriaPage.prototype.obtener = function () {
        this.necesidad = this.navParams.get('necesidad');
        this.listarPlanimetria(this.navParams.get('necesidad'));
    };
    ModPlanimeriaPage.prototype.planimetriaManual = function (check) {
    };
    ModPlanimeriaPage.prototype.verificaManual = function () {
        return !this.planMan;
    };
    ModPlanimeriaPage.prototype.listarPlanimetria = function (necesidad) {
        var _this = this;
        var request = new __WEBPACK_IMPORTED_MODULE_0__modelos_planimetriaRequest__["a" /* PlanimetriaRequestModel */](necesidad, true, "", 0, 0);
        this.webService.obtenerGeoPlanmetria(request)
            .subscribe(function (data) {
            // console.log('DATA ' + JSON.stringify(data));
            var lista = data['Listadosnap_geoplanimetriaResult']['lista'];
            _this.listaPlanimetria = lista;
        }, function (error) {
            console.log('Error al cargar ' + JSON.stringify(error));
            _this.mostarToast('Error al listar planimetría');
        });
    };
    ModPlanimeriaPage.prototype.seleccionaPlanimetria = function (letra, nro_fila, id_sector) {
        var _this = this;
        var request = new __WEBPACK_IMPORTED_MODULE_0__modelos_planimetriaRequest__["a" /* PlanimetriaRequestModel */](this.necesidad, false, letra, nro_fila, id_sector);
        this.webService.obtenerGeoPlanmetria(request)
            .subscribe(function (data) {
            // console.log(JSON.stringify(data));
            var lista = data['Listadosnap_geoplanimetriaResult']['lista'];
            _this.planimetria.descripcion = lista[0]['DescPlanimetria'];
            _this.planimetria.letra = lista[0]['Letra'];
            // this.planimetria.estado = lista[0]['Estado'];
            _this.planimetria.estado = lista[0]['DescEstado'];
            _this.planimetria.sector = lista[0]['id_codigo_sector'];
            _this.planimetria.porc_descuento = lista[0]['PorcDesc'];
            _this.planimetria.uf = lista[0]['UF_Valor_'];
            _this.planimetria.fila = lista[0]['Numero_fila'];
            _this.planimetria.latitud = lista[0]['latitud_sector'];
            _this.planimetria.longitud = lista[0]['longitud_sector'];
            _this.planimetria.numero = lista[0]['Numero_fila'];
            _this.planimetria.valor_base = lista[0]['ValorBase'];
            _this.planimetria.capacidades = lista[0]['num_capacidades_snapsector'];
            _this.planimetria.reducciones = lista[0]['num_reducciones_snapsector'];
            _this.planimetria.pie = lista[0]['Pie'];
        }, function (error) {
            _this.mostarToast('Error al cargar planimetria seleccionada');
            console.log('Error al cargar ' + JSON.stringify(error));
        });
    };
    ModPlanimeriaPage.prototype.cerrar = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Cancelar Ingreso',
            message: '¿Cancelar ingreso de planimetría?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () { }
                }, {
                    text: 'Si',
                    handler: function () {
                        _this.viewCtrl.dismiss(_this.planimetria);
                    }
                }
            ]
        });
        alert.present();
    };
    ModPlanimeriaPage.prototype.guardar = function () {
        if (this.planimetria.sector && this.planimetria.fila &&
            this.planimetria.numero && this.planimetria.valor_base) {
            this.viewCtrl.dismiss(this.planimetria);
        }
        else {
            this.mostarToast('Faltan parámetros de planimetría');
        }
    };
    ModPlanimeriaPage.prototype.mostarToast = function (mensaje) {
        if (mensaje) {
            var toast = this.toast.create({
                message: mensaje,
                duration: 3000,
                position: 'top'
            });
            toast.present();
        }
    };
    ModPlanimeriaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-mod-planimeria',template:/*ion-inline-start:"C:\ionicproyectos\parqueCopiapo\src\pages\mod-planimeria\mod-planimeria.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Planimería</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (tap)="cerrar()">\n\n        <ion-icon name="close"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col width-70>\n\n        <agm-map *ngIf="verificaManual()" [mapTypeId]="\'satellite\'" [styles]="estiloMapa" [zoom]="zoom" [streetViewControl]="false" [latitude]="lat" [longitude]="lng">\n\n          <agm-marker *ngFor="let m of listaPlanimetria; let i = index" (markerClick)="seleccionaPlanimetria(m.Letra,m.Numero_fila,m.id_codigo_sector)"\n\n            [iconUrl]="\'assets/imgs/cuadradoV.png\'" [latitude]="m.latitud_sector" [longitude]="m.longitud_sector" [label]="m.Letra">\n\n          </agm-marker>\n\n        </agm-map>\n\n      </ion-col>\n\n      <ion-col width-30>\n\n        <ion-card no-padding>\n\n          <ion-item>\n\n            <ion-checkbox [(ngModel)]="planMan" name="planimetria_manual" (ionChange)="planimetriaManual(planMan)"></ion-checkbox>\n\n            <ion-label>Seleccionar Planimería Manualmente</ion-label>\n\n          </ion-item>\n\n          <!-- Para pruebas  -->\n\n          <ion-item *ngIf="!verificaManual()">\n\n            <ion-label color="primary" stacked>Planimetrías disponibles</ion-label>\n\n            <ion-select [(ngModel)]="codigo" name="sector" no-lines placeholder="Seleccione planimetría" cancelText="Cancelar" okText="Ok">\n\n              <ion-option [value]="plan.Letra" *ngFor="let plan of listaPlanimetria" (ionSelect)="seleccionaPlanimetria(plan.DescPlanimetria)">{{\n\n                plan.DescPlanimetria | uppercase}}</ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n          <!-- Fin Pruebas -->\n\n          <ion-item>\n\n            <ion-label color="primary" stacked>Sector *</ion-label>\n\n            <ion-input [disabled]=true [(ngModel)]="planimetria.sector" type="text" name="sector" placeholder="Sector"></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label color="primary" stacked>Fila *</ion-label>\n\n            <ion-input [disabled]=true [(ngModel)]="planimetria.letra" type="text" name="fila" placeholder="Fila"></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label color="primary" stacked>Número fila *</ion-label>\n\n            <ion-input [disabled]=true [(ngModel)]="planimetria.numero" type="number" name="numero" placeholder="Número"></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label color="primary" stacked>Valor base *</ion-label>\n\n            <ion-input [disabled]=true [(ngModel)]="planimetria.valor_base" type="text" name="valor_base" placeholder="Valor base"></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label color="primary" stacked>Estado *</ion-label>\n\n            <ion-input [disabled]=true [(ngModel)]="planimetria.estado" type="text" name="estado" placeholder="Estado"></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label color="primary" stacked>Capacidad *</ion-label>\n\n            <ion-input [disabled]=true [(ngModel)]="planimetria.capacidades" type="number" name="capacidades" placeholder="Capacidad"></ion-input>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label color="primary" stacked>Reducciones *</ion-label>\n\n            <ion-input [disabled]=true [(ngModel)]="planimetria.reducciones" type="text" name="reducciones" placeholder="Reducciones"></ion-input>\n\n          </ion-item>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <button ion-button full color="primary" (tap)="guardar()">Guardar</button>\n\n</ion-footer>'/*ion-inline-end:"C:\ionicproyectos\parqueCopiapo\src\pages\mod-planimeria\mod-planimeria.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["u" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["A" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["y" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1__providers_webservice_webservice__["a" /* WebserviceProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */]])
    ], ModPlanimeriaPage);
    return ModPlanimeriaPage;
}());

//# sourceMappingURL=mod-planimeria.js.map

/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ConnectionStatusEnum */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(80);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ConnectionStatusEnum;
(function (ConnectionStatusEnum) {
    ConnectionStatusEnum[ConnectionStatusEnum["Online"] = 0] = "Online";
    ConnectionStatusEnum[ConnectionStatusEnum["Offline"] = 1] = "Offline";
})(ConnectionStatusEnum || (ConnectionStatusEnum = {}));
var NetworkProvider = /** @class */ (function () {
    function NetworkProvider(alertCtrl, network, eventCtrl) {
        this.alertCtrl = alertCtrl;
        this.network = network;
        this.eventCtrl = eventCtrl;
        this.previousStatus = ConnectionStatusEnum.Online;
    }
    NetworkProvider.prototype.initializeNetworkEvents = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function () {
            if (_this.previousStatus === ConnectionStatusEnum.Online) {
                _this.eventCtrl.publish('network:offline');
            }
            _this.previousStatus = ConnectionStatusEnum.Offline;
        });
        this.network.onConnect().subscribe(function () {
            if (_this.previousStatus === ConnectionStatusEnum.Offline) {
                _this.eventCtrl.publish('network:online');
            }
            _this.previousStatus = ConnectionStatusEnum.Online;
        });
    };
    NetworkProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Events */]])
    ], NetworkProvider);
    return NetworkProvider;
}());

//# sourceMappingURL=network.js.map

/***/ }),

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(442);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);



Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_16" /* enableProdMode */])();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 442:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_component__ = __webpack_require__(443);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_menu_principal_menu_principal__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_autenticacion_autenticacion__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_webservice_webservice__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_network_network__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_login_login__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_home_home__ = __webpack_require__(304);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_nota_venta_nota_venta__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_reportes_reportes__ = __webpack_require__(536);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_mod_nota_ingreso_mod_nota_ingreso__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_mod_nota_ingreso_empleador_mod_nota_ingreso_empleador__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_ingreso_documentos_ingreso_documentos__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_mod_ingreso_aval_mod_ingreso_aval__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_mod_nota_ingreso_detalle_mod_nota_ingreso_detalle__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_mod_planimeria_mod_planimeria__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_mod_declaracion_salud_mod_declaracion_salud__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_file_chooser__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_file_path__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_base64__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_camera__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_network__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__agm_core__ = __webpack_require__(537);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_database_database__ = __webpack_require__(542);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29_ionic_img_viewer__ = __webpack_require__(312);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
//INTERNOS





//COMPONENTES



//PROVIDERS



//PAGINAS











//PLUGINS








var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_0__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_12__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_nota_venta_nota_venta__["a" /* NotaVentaPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_reportes_reportes__["a" /* ReportesPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_ingreso_documentos_ingreso_documentos__["a" /* IngresoDocumentosPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_mod_nota_ingreso_mod_nota_ingreso__["a" /* ModNotaIngresoPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_mod_nota_ingreso_empleador_mod_nota_ingreso_empleador__["a" /* ModNotaIngresoEmpleadorPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_mod_ingreso_aval_mod_ingreso_aval__["a" /* ModIngresoAvalPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_mod_nota_ingreso_detalle_mod_nota_ingreso_detalle__["a" /* ModNotaIngresoDetallePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_mod_declaracion_salud_mod_declaracion_salud__["a" /* ModDeclaracionSaludPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_mod_planimeria_mod_planimeria__["a" /* ModPlanimeriaPage */],
                __WEBPACK_IMPORTED_MODULE_5__components_menu_principal_menu_principal__["a" /* MenuPrincipalComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_29_ionic_img_viewer__["b" /* IonicImageViewerModule */],
                //AgmCoreModule.forRoot({ apiKey: '' }),
                __WEBPACK_IMPORTED_MODULE_27__agm_core__["a" /* AgmCoreModule */].forRoot({ apiKey: 'AIzaSyA4a4T2YIbgNA9ce_1rJIxKdvn2TgiC6Us' }),
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_0__app_component__["a" /* MyApp */], {
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthShortNames: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
                    dayNames: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
                    dayShortNames: ['lun', 'mar', 'mie', 'jue', 'vie', 'sab', 'dom'],
                }, {
                    links: []
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_0__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_12__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_nota_venta_nota_venta__["a" /* NotaVentaPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_reportes_reportes__["a" /* ReportesPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_ingreso_documentos_ingreso_documentos__["a" /* IngresoDocumentosPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_mod_nota_ingreso_mod_nota_ingreso__["a" /* ModNotaIngresoPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_mod_nota_ingreso_empleador_mod_nota_ingreso_empleador__["a" /* ModNotaIngresoEmpleadorPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_mod_ingreso_aval_mod_ingreso_aval__["a" /* ModIngresoAvalPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_mod_nota_ingreso_detalle_mod_nota_ingreso_detalle__["a" /* ModNotaIngresoDetallePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_mod_declaracion_salud_mod_declaracion_salud__["a" /* ModDeclaracionSaludPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_mod_planimeria_mod_planimeria__["a" /* ModPlanimeriaPage */],
                __WEBPACK_IMPORTED_MODULE_5__components_menu_principal_menu_principal__["a" /* MenuPrincipalComponent */],
            ],
            providers: [
                { provide: __WEBPACK_IMPORTED_MODULE_2__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_8__providers_autenticacion_autenticacion__["a" /* AutenticacionProvider */],
                __WEBPACK_IMPORTED_MODULE_9__providers_webservice_webservice__["a" /* WebserviceProvider */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_file_chooser__["a" /* FileChooser */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_base64__["a" /* Base64 */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_10__providers_network_network__["a" /* NetworkProvider */],
                __WEBPACK_IMPORTED_MODULE_28__providers_database_database__["a" /* DatabaseProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 443:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_network__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(304);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_nota_venta_nota_venta__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_network_network__ = __webpack_require__(317);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//MODULOS





//PAGINAS



//PROVIDERS

var MyApp = /** @class */ (function () {
    function MyApp(platform, eventos, statusBar, splashScreen, menu, network, networkProvider, toast) {
        var _this = this;
        this.platform = platform;
        this.eventos = eventos;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.menu = menu;
        this.network = network;
        this.networkProvider = networkProvider;
        this.toast = toast;
        this.paginasAdmin = [
            { titulo: 'Inicio', component: __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */], icono: "home" },
            { titulo: 'Nota de venta', component: __WEBPACK_IMPORTED_MODULE_7__pages_nota_venta_nota_venta__["a" /* NotaVentaPage */], icono: "document" }
        ];
        this.iconoUsuario = "./assets/imgs/user1.png";
        this.initializeApp();
        eventos.subscribe('verifica_ruta', function () {
            _this.verificaUsuarioRuta();
        });
        eventos.subscribe('verifica_internet', function () {
            _this.verificaAppOnline();
        });
        this.obtieneUsuario();
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.networkProvider.initializeNetworkEvents();
            //APP offline
            _this.eventos.subscribe('network:offline', function () {
                console.log('APLICACION SIN INTERNET');
                _this.mostarToast('La aplicación ha perdido conexión con el servidor, verifique conexión');
            });
            // APP online
            _this.eventos.subscribe('network:online', function () {
                console.log('APLICACION CON INTERNET');
                _this.mostarToast('Aplicación conectada correctamente al servidor');
            });
            _this.verificaUsuarioRuta();
            // this.statusBar.styleDefault();
            _this.statusBar.backgroundColorByHexString("#000000");
            _this.splashScreen.hide();
            _this.verificaDispositivo();
        });
    };
    MyApp.prototype.verificaAppOnline = function () { };
    MyApp.prototype.verificaDispositivo = function () {
        if (this.platform.is('cordova')) {
            this.esDispositivo = true;
            console.log('DISPOSITIVO');
        }
        else {
            this.esDispositivo = false;
            console.log('NAVEGADOR');
        }
    };
    MyApp.prototype.obtieneUsuario = function () {
        this.usuarioMenu = localStorage.getItem('ejecutivo');
        this.perfilMenu = null;
    };
    MyApp.prototype.verificaUsuarioRuta = function () {
        var usuario;
        var cargo;
        usuario = parseInt(localStorage.getItem('login'));
        cargo = parseInt(localStorage.getItem('cod_cargo'));
        if (usuario) {
            switch (cargo) {
                case 11: {
                    // console.log('Usuario Administrador');
                    break;
                }
                default: {
                    // console.log('CARGO NO DEFINIDO');
                    this.pages = this.paginasAdmin;
                    break;
                }
            }
            // console.log('USUARIO LOGUEADO: MENU HABILIDADO');
            this.openPage(this.pages[0]);
            this.menu.enable(true);
        }
        else {
            // console.log('USUARIO NO LOGUEADO: MENU DESHABILIDADO');
            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
            this.menu.enable(false);
        }
    };
    MyApp.prototype.mostarToast = function (mensaje) {
        if (mensaje) {
            var toast = this.toast.create({
                message: mensaje,
                duration: 3000,
                position: 'top'
            });
            toast.present();
        }
    };
    MyApp.prototype.openPage = function (page) {
        // console.log('ABRIR PAGINA ' + JSON.stringify(page));
        this.nav.setRoot(page.component);
        this.paginaActiva = page;
    };
    MyApp.prototype.verificaPagina = function (page) {
        return page === this.paginaActiva;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["s" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["s" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\ionicproyectos\parqueCopiapo\src\app\app.html"*/'<ion-menu [content]="content" width="35">\n\n  <ion-content class="menu-content fondo">\n\n    <div text-center class="cabecera">\n\n      <br>\n\n      <img [src]=iconoUsuario class="custom-avatar" />\n\n      <p>{{ usuarioMenu }}</p>\n\n      <p>{{ perfilMenu }}</p>\n\n      <br>\n\n    </div>\n\n    <ion-list no-lines>\n\n      <button class="fondo" menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)" [class.active]="verificaPagina(p)">\n\n        <ion-icon [name]="p.icono" item-left></ion-icon>\n\n        {{p.titulo}}\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n</ion-menu>\n\n\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\ionicproyectos\parqueCopiapo\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["w" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_8__providers_network_network__["a" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["y" /* ToastController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WebserviceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_retry__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_retry___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_retry__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_timeout__ = __webpack_require__(303);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_timeout__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//MODULOS







var WebserviceProvider = /** @class */ (function () {
    function WebserviceProvider(http) {
        this.http = http;
        this.ip = null;
        this.urlCrearNota = null;
        this.urlListadoNotaVenta = null;
        this.urlListarComunas = null;
        this.urlListarCiudades = null;
        this.urlListarSector = null;
        this.urlListarActividades = null;
        this.urlListarSexo = null;
        this.urlListarEstadoCivil = null;
        this.urlListarGirosEmpleador = null;
        this.urlListarParentescos = null;
        this.urlListarNecesidades = null;
        this.urlListarPresion = null;
        this.urlListarAcreditacion = null;
        this.urlDatosCliente = null;
        this.urlDatosPlanimetria = null;
        this.urlGuardarCliente = null;
        this.urlGuardarEmpleador = null;
        this.urlDatosEmpleador = null;
        this.refrescarIP();
        this.cabeceras = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({
            'Content-Type': 'application/json'
        });
    }
    WebserviceProvider.prototype.refrescarIP = function () {
        //let ipServer = localStorage.getItem('ip');
        var ipServer = '192.168.0.85';
        //let ipServer = '192.168.0.66';
        this.ip = ipServer;
        if (this.ip) {
            this.urlCrearNota = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/GuardarNotaVenta';
            this.urlListadoNotaVenta = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoNotaVentaCementerio';
            this.urlListarComunas = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoComuna';
            this.urlListarCiudades = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoCiudad';
            this.urlListarSector = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoSector';
            this.urlListarActividades = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoCargo';
            this.urlListarSexo = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoSexo';
            this.urlListarEstadoCivil = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoEstadoCivil';
            this.urlListarGirosEmpleador = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoGiro';
            this.urlListarParentescos = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoParentesco';
            this.urlListarNecesidades = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoTipoNecesidad';
            this.urlListarPresion = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoPresionArterial';
            this.urlListarAcreditacion = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/TipoAcreditacion';
            this.urlDatosCliente = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoCliente';
            this.urlDatosPlanimetria = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/Listadosnap_geoplanimetria';
            this.urlGuardarCliente = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/GuardarCliente';
            this.urlGuardarEmpleador = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/GuardarEmpresa';
            this.urlDatosEmpleador = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoEmpresa';
        }
    };
    //Lista de notas de venta
    WebserviceProvider.prototype.obtenerListaNotaVenta = function (req) {
        this.refrescarIP();
        var cuerpo = { "ListadoNotaVentaCementerioRequest": { "Cod_Vendedor": req.cod_usuario, "Mes": req.mes, "Año": req.ano } };
        return this.http.post(this.urlListadoNotaVenta, cuerpo, { headers: this.cabeceras })
            .timeout(1000)
            .catch(function (err) {
            return err;
        });
    };
    //Lista de Comunas
    WebserviceProvider.prototype.obtenerListaComunas = function (com) {
        this.refrescarIP();
        var cuerpo = {
            "ListadoComunarequest": {
                "cod_comuna_comuna": com.cod_comuna_comuna,
                "cod_ciudad_ciudad": com.cod_ciudad_ciudad
            }
        };
        return this.http.post(this.urlListarComunas, cuerpo, { headers: this.cabeceras });
    };
    //Lista de ciudades
    WebserviceProvider.prototype.obtenerListaCiudades = function (ciu) {
        this.refrescarIP();
        var cuerpo = { "ListadoCiudadrequest": { "cod_ciudad_ciudad": ciu.cod_ciudad_ciudad } };
        return this.http.post(this.urlListarCiudades, cuerpo, { headers: this.cabeceras });
    };
    //Lista de Sectores
    WebserviceProvider.prototype.obtenerListaSector = function (sec) {
        this.refrescarIP();
        var cuerpo = { "ListadoSectorrequest": { "cod_sector_sector": sec.cod_sector_sector } };
        return this.http.post(this.urlListarSector, cuerpo, { headers: this.cabeceras });
    };
    //Lista de actividades
    WebserviceProvider.prototype.obtenerListaActividades = function (act) {
        this.refrescarIP();
        var cuerpo = { "ListadoCargorequest": { "cod_cargo_cargo": act.cod_cargo_cargo } };
        return this.http.post(this.urlListarActividades, cuerpo, { headers: this.cabeceras });
    };
    //Lista de sexo
    WebserviceProvider.prototype.obtenerListaSexo = function (sex) {
        this.refrescarIP();
        var cuerpo = { "ListadoSexorequest": { "cod_sexo_sexo": sex.cod_sexo_sexo } };
        return this.http.post(this.urlListarSexo, cuerpo, { headers: this.cabeceras });
    };
    //Lista estado civil
    WebserviceProvider.prototype.obtenerListaEstadoCivil = function (civil) {
        this.refrescarIP();
        var cuerpo = { "ListadoEstadoCivilrequest": { "cod_estadocivil_estadocivil": civil.cod_estadocivil_estadocivil } };
        return this.http.post(this.urlListarEstadoCivil, cuerpo, { headers: this.cabeceras });
    };
    //Lista giros del empleador
    WebserviceProvider.prototype.obtenerGirosEmpleador = function (giros) {
        this.refrescarIP();
        var cuerpo = { "ListadoGirorequest": { "cod_giro_giro": giros.cod_giro_giro } };
        return this.http.post(this.urlListarGirosEmpleador, cuerpo, { headers: this.cabeceras });
    };
    //Lista parentesco
    WebserviceProvider.prototype.obtenerListaParentesco = function (parent) {
        this.refrescarIP();
        var cuerpo = { "ListadoParentescorequest": { "cod_parentesco_parentesco": parent.cod_parentesco_parentesco } };
        return this.http.post(this.urlListarParentescos, cuerpo, { headers: this.cabeceras });
    };
    //Lista de necesidades
    WebserviceProvider.prototype.obtenerListaNecesidades = function (nec) {
        this.refrescarIP();
        var cuerpo = { "ListadoTipoNecesidadrequest": { "cod_tipo_necesidad": nec.cod_tipo_necesidad } };
        return this.http.post(this.urlListarNecesidades, cuerpo, { headers: this.cabeceras });
    };
    //Lista presion arterial
    WebserviceProvider.prototype.obtenerListaPresionArterial = function (pres) {
        this.refrescarIP();
        var cuerpo = { "ListadoPresionArterialrequest": { "id_codigo_parterial": pres.id_codigo_parterial } };
        return this.http.post(this.urlListarPresion, cuerpo, { headers: this.cabeceras });
    };
    //Lista acreditacion
    WebserviceProvider.prototype.obtenerListaAcreditacion = function () {
        this.refrescarIP();
        var cuerpo = { "ListadoTipoAcreditacionrequest": { "id_codigo_taingresos": "" } };
        return this.http.post(this.urlListarAcreditacion, cuerpo, { headers: this.cabeceras });
    };
    //Datos cliente
    WebserviceProvider.prototype.obtenerDatosCliente = function (cli_cod) {
        this.refrescarIP();
        var cuerpo = { "ListadoClienterequest": { "cod_cliente_cliente": cli_cod } };
        return this.http.post(this.urlDatosCliente, cuerpo, { headers: this.cabeceras });
    };
    //Datos empleador
    WebserviceProvider.prototype.obtenerDatosEmpleador = function (emp_cod) {
        this.refrescarIP();
        var cuerpo = { "ListadoEmpresaRequest": { "cod_empresa_empresa": emp_cod } };
        return this.http.post(this.urlDatosEmpleador, cuerpo, { headers: this.cabeceras });
    };
    //Datos aval
    WebserviceProvider.prototype.obtenerDatosAval = function (aval) {
        this.refrescarIP();
        var cuerpo = { "ListadoClienterequest": { "cod_cliente_cliente": aval.cod_aval_aval } };
        return this.http.post(this.urlDatosCliente, cuerpo, { headers: this.cabeceras });
    };
    //Guarda cliente/aval
    WebserviceProvider.prototype.guardarCliente = function (cli) {
        this.refrescarIP();
        var cuerpo = {
            "GuardarClienteRequest": {
                "Cliente": {
                    "cod_vendedor_fichapersonal": cli.cod_vendedor_fichapersonal,
                    "cod_cliente_cliente": cli.cod_cliente_cliente,
                    "cod_comuna_comuna": cli.cod_comuna_comuna,
                    "cod_giro_giro": cli.cod_giro_giro,
                    "des_nombre_cliente": cli.des_nombre_cliente,
                    "des_direccion_cliente": cli.des_direccion_cliente,
                    "des_telefono_cliente": cli.des_telefono_cliente,
                    "des_mail_cliente": cli.des_mail_cliente,
                    "cod_ciudad_ciudad": cli.cod_ciudad_ciudad,
                    "cod_sector_sector": cli.cod_sector_sector,
                    "cod_estadocivil_estadocivil": cli.cod_estadocivil_estadocivil,
                    "des_telefono_celular": cli.des_telefono_celular,
                    "edad": cli.edad,
                    "cod_sexo_sexo": cli.cod_sexo_sexo,
                    "num_montorentaliquida_clientes": cli.num_montorentaliquida_clientes,
                    "cod_cargo_cargo": cli.cod_cargo_cargo,
                    "cod_parentesco_parentesco": cli.cod_parentesco_parentesco
                },
                "FechaNacimientoString": cli.dat_fecha_nacimiento
            }
        };
        console.log('REQUEST: ' + JSON.stringify(cuerpo));
        return this.http.post(this.urlGuardarCliente, cuerpo, { headers: this.cabeceras });
    };
    //Guarda empleador
    WebserviceProvider.prototype.guardarEmpleador = function (emp) {
        this.refrescarIP();
        var cuerpo = {
            "GuardarEmpresarequest": {
                "Empresa": {
                    "cod_empresa_empresa": emp.cod_empresa_empresa,
                    "des_nombre_empresa": emp.des_nombre_empresa,
                    "des_direccion_empresa": emp.des_direccion_empresa,
                    "des_telefono_empresa": emp.des_telefono_empresa,
                    "des_mail_empresa": emp.des_mail_empresa,
                    "cod_giro_giro": emp.cod_giro_giro,
                    "cod_actividadeconomica_empresa": emp.cod_actividadeconomica_empresa,
                    "cod_comuna_comuna": emp.cod_comuna_comuna,
                    "cod_ciudad_ciudad": emp.cod_ciudad_ciudad
                }
            }
        };
        console.log('REQUEST EMPLEADOR ' + JSON.stringify(cuerpo));
        return this.http.post(this.urlGuardarEmpleador, cuerpo, { headers: this.cabeceras });
    };
    //Guardar nota de venta
    WebserviceProvider.prototype.guardarNotaVenta = function (nota, planimetria, salud, documentos) {
        this.refrescarIP();
        var cuerpo = {
            "GuardarNotaVentarequest": {
                "Encabezado": {
                    "cod_cliente_cliente": nota.cod_cliente_cliente,
                    "cod_referencia_cliente": nota.cod_referencia_cliente,
                    "cod_aval_cliente": nota.cod_aval_cliente,
                    "Cod_Vendedor": nota.Cod_Vendedor,
                    "num_pie_contrato": nota.num_pie_contrato,
                    "cod_tipo_necesidad": nota.cod_tipo_necesidad,
                    "Valor_base": nota.Valor_base,
                    "Poc_descuento": nota.Poc_descuento,
                    "Dia_pago": nota.Dia_pago,
                    "num_cuotas": nota.num_cuotas,
                    "Valor_cuota": nota.Valor_cuota,
                    "UF_Valor": nota.UF_Valor,
                    "Glosa": nota.Glosa
                },
                "fotofirma": nota.img_imagen_firma,
                "FechaPrimerVenc": nota.FechaPrimerVenc,
                "snapEmpCliente": {
                    "cod_empresa_empresa": nota.cod_empresa_empresa,
                    "cod_cliente_cliente": nota.cod_cliente_cliente
                },
                "listaDetalle_PlanimetriaContrato": [{
                        //listaDetalle_PlanimetriaContrato
                        "Numero_fila": planimetria.Numero_fila,
                        "id_codigo_sector": planimetria.id_codigo_sector,
                        "Letra": planimetria.Letra,
                        "num_capacidades_snapsector": planimetria.num_capacidades_snapsector,
                        "num_capautilizada_snapsector": planimetria.num_capautilizada_snapsector,
                        "num_reducciones_snapsector": planimetria.num_reducciones_snapsector,
                    }],
                // "listasnap_ContratoTAIngresos": documentos,
                "Xsnap_ContratoTAIngresosTablet": documentos,
                "listaDecla_Salud": [{
                        "cod_cliente_cliente": nota.cod_cliente_cliente,
                        "bit_responde_dsalud": salud.bit_responde_dsalud,
                        "num_peso_dsalud": salud.num_peso_dsalud,
                        "num_estatura_dsalud": salud.num_estatura_dsalud,
                        "des_obs_dsalud": salud.des_obs_dsalud,
                        "id_codigo_parterial": salud.id_codigo_parterial
                    }]
            }
        };
        console.log('REQUEST NOTA : ' + JSON.stringify(cuerpo));
        return this.http.post(this.urlCrearNota, cuerpo, { headers: this.cabeceras })
            .retry(3)
            .timeout(5000)
            .catch(function (err) {
            return err;
        });
    };
    WebserviceProvider.prototype.obtenerGeoPlanmetria = function (necesidad) {
        this.refrescarIP();
        var cuerpo = {
            "Listadosnap_geoplanimetriarequest": {
                "Letra": necesidad.Letra,
                "Disponible": necesidad.Disponible,
                "id_codigo_sector": necesidad.id_codigo_sector,
                "Numero_fila": necesidad.Numero_fila,
                "necesidad_inmediata": necesidad.necesidad_inmediata
            }
        };
        // console.log('REQUEST: ' + JSON.stringify(cuerpo));
        return this.http.post(this.urlDatosPlanimetria, cuerpo, { headers: this.cabeceras });
    };
    //Eliminar nota de venta
    WebserviceProvider.prototype.eliminarNotaVenta = function (nota) {
        this.refrescarIP();
    };
    //Editar nota de venta
    WebserviceProvider.prototype.editarNotaVenta = function (nota) {
        this.refrescarIP();
    };
    var _a;
    WebserviceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]) === "function" ? _a : Object])
    ], WebserviceProvider);
    return WebserviceProvider;
}());

//# sourceMappingURL=webservice.js.map

/***/ }),

/***/ 501:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuaurioResponseModel; });
var UsuaurioResponseModel = /** @class */ (function () {
    function UsuaurioResponseModel(codigo, cod_cargo, des_cargo, cod_comuna, rut, nombres, ap_paterno, ap_materno, direccion, email, fecha_nac, telefono) {
        this.codigo = codigo;
        this.cod_cargo = cod_cargo;
        this.des_cargo = des_cargo;
        this.cod_comuna = cod_comuna;
        this.rut = rut;
        this.nombres = nombres;
        this.ap_paterno = ap_paterno;
        this.ap_materno = ap_materno;
        this.direccion = direccion;
        this.email = email;
        this.fecha_nac = fecha_nac;
        this.telefono = telefono;
    }
    return UsuaurioResponseModel;
}());

//# sourceMappingURL=usuarioResponse.js.map

/***/ }),

/***/ 502:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuarioModel; });
var UsuarioModel = /** @class */ (function () {
    function UsuarioModel(usuario, password) {
        this.usuario = usuario;
        this.password = password;
    }
    return UsuarioModel;
}());

//# sourceMappingURL=usuario.js.map

/***/ }),

/***/ 514:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotaRequestModel; });
var NotaRequestModel = /** @class */ (function () {
    function NotaRequestModel(cod_usuario, mes, ano) {
        this.cod_usuario = cod_usuario;
        this.mes = mes;
        this.ano = ano;
    }
    return NotaRequestModel;
}());

//# sourceMappingURL=notaRequest.js.map

/***/ }),

/***/ 515:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GirosEmpeadorRequest; });
var GirosEmpeadorRequest = /** @class */ (function () {
    function GirosEmpeadorRequest(cod_giro_giro) {
        this.cod_giro_giro = cod_giro_giro;
    }
    return GirosEmpeadorRequest;
}());

//# sourceMappingURL=girosEmpleadorRequest.js.map

/***/ }),

/***/ 516:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActividadRequestModel; });
var ActividadRequestModel = /** @class */ (function () {
    function ActividadRequestModel(cod_cargo_cargo) {
        this.cod_cargo_cargo = cod_cargo_cargo;
    }
    return ActividadRequestModel;
}());

//# sourceMappingURL=actividadRequest.js.map

/***/ }),

/***/ 517:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParentescoRequestModel; });
var ParentescoRequestModel = /** @class */ (function () {
    function ParentescoRequestModel(cod_parentesco_parentesco) {
        this.cod_parentesco_parentesco = cod_parentesco_parentesco;
    }
    return ParentescoRequestModel;
}());

//# sourceMappingURL=parentescoRequest.js.map

/***/ }),

/***/ 518:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NecesidadRequestModel; });
var NecesidadRequestModel = /** @class */ (function () {
    function NecesidadRequestModel(cod_tipo_necesidad) {
        this.cod_tipo_necesidad = cod_tipo_necesidad;
    }
    return NecesidadRequestModel;
}());

//# sourceMappingURL=necesidadRequest.js.map

/***/ }),

/***/ 519:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PresionRequestModel; });
var PresionRequestModel = /** @class */ (function () {
    function PresionRequestModel(id_codigo_parterial) {
        this.id_codigo_parterial = id_codigo_parterial;
    }
    return PresionRequestModel;
}());

//# sourceMappingURL=presionRequest.js.map

/***/ }),

/***/ 520:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CiudadRequestModel; });
var CiudadRequestModel = /** @class */ (function () {
    function CiudadRequestModel(cod_ciudad_ciudad) {
        this.cod_ciudad_ciudad = cod_ciudad_ciudad;
    }
    return CiudadRequestModel;
}());

//# sourceMappingURL=ciudadRequest.js.map

/***/ }),

/***/ 521:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SexoRequestModel; });
var SexoRequestModel = /** @class */ (function () {
    function SexoRequestModel(cod_sexo_sexo) {
        this.cod_sexo_sexo = cod_sexo_sexo;
    }
    return SexoRequestModel;
}());

//# sourceMappingURL=sexoRequest.js.map

/***/ }),

/***/ 522:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CivilRequestModel; });
var CivilRequestModel = /** @class */ (function () {
    function CivilRequestModel(cod_estadocivil_estadocivil) {
        this.cod_estadocivil_estadocivil = cod_estadocivil_estadocivil;
    }
    return CivilRequestModel;
}());

//# sourceMappingURL=civilRequest.js.map

/***/ }),

/***/ 523:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SectorRequestModel; });
var SectorRequestModel = /** @class */ (function () {
    function SectorRequestModel(cod_sector_sector) {
        this.cod_sector_sector = cod_sector_sector;
    }
    return SectorRequestModel;
}());

//# sourceMappingURL=sectorRequest.js.map

/***/ }),

/***/ 530:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotaPlanimetriaRequest; });
var NotaPlanimetriaRequest = /** @class */ (function () {
    function NotaPlanimetriaRequest(Numero_fila, id_codigo_sector, Letra, num_capacidades_snapsector, num_capautilizada_snapsector, num_reducciones_snapsector) {
        this.Numero_fila = Numero_fila;
        this.id_codigo_sector = id_codigo_sector;
        this.Letra = Letra;
        this.num_capacidades_snapsector = num_capacidades_snapsector;
        this.num_capautilizada_snapsector = num_capautilizada_snapsector;
        this.num_reducciones_snapsector = num_reducciones_snapsector;
    }
    return NotaPlanimetriaRequest;
}());

//# sourceMappingURL=notaPlanimetriaRequest.js.map

/***/ }),

/***/ 531:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotaSaludRequestModel; });
var NotaSaludRequestModel = /** @class */ (function () {
    function NotaSaludRequestModel(bit_responde_dsalud, num_peso_dsalud, num_estatura_dsalud, des_obs_dsalud, id_codigo_parterial) {
        this.bit_responde_dsalud = bit_responde_dsalud;
        this.num_peso_dsalud = num_peso_dsalud;
        this.num_estatura_dsalud = num_estatura_dsalud;
        this.des_obs_dsalud = des_obs_dsalud;
        this.id_codigo_parterial = id_codigo_parterial;
    }
    return NotaSaludRequestModel;
}());

//# sourceMappingURL=notaSaludRequest.js.map

/***/ }),

/***/ 532:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotaCabeceraRequest; });
var NotaCabeceraRequest = /** @class */ (function () {
    function NotaCabeceraRequest(cod_cliente_cliente, cod_empresa_empresa, cod_referencia_cliente, cod_aval_cliente, Cod_Vendedor, num_pie_contrato, cod_tipo_necesidad, Valor_base, Poc_descuento, Dia_pago, num_cuotas, Valor_cuota, UF_Valor, Glosa, FechaPrimerVenc, img_imagen_firma) {
        this.cod_cliente_cliente = cod_cliente_cliente;
        this.cod_empresa_empresa = cod_empresa_empresa;
        this.cod_referencia_cliente = cod_referencia_cliente;
        this.cod_aval_cliente = cod_aval_cliente;
        this.Cod_Vendedor = Cod_Vendedor;
        this.num_pie_contrato = num_pie_contrato;
        this.cod_tipo_necesidad = cod_tipo_necesidad;
        this.Valor_base = Valor_base;
        this.Poc_descuento = Poc_descuento;
        this.Dia_pago = Dia_pago;
        this.num_cuotas = num_cuotas;
        this.Valor_cuota = Valor_cuota;
        this.UF_Valor = UF_Valor;
        this.Glosa = Glosa;
        this.FechaPrimerVenc = FechaPrimerVenc;
        this.img_imagen_firma = img_imagen_firma;
    }
    return NotaCabeceraRequest;
}());

//# sourceMappingURL=notaCabeceraRequest.js.map

/***/ }),

/***/ 533:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlanimetriaRequestModel; });
var PlanimetriaRequestModel = /** @class */ (function () {
    function PlanimetriaRequestModel(necesidad_inmediata, Disponible, Letra, Numero_fila, id_codigo_sector) {
        this.necesidad_inmediata = necesidad_inmediata;
        this.Disponible = Disponible;
        this.Letra = Letra;
        this.Numero_fila = Numero_fila;
        this.id_codigo_sector = id_codigo_sector;
    }
    return PlanimetriaRequestModel;
}());

//# sourceMappingURL=planimetriaRequest.js.map

/***/ }),

/***/ 534:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AvalRequestModel; });
var AvalRequestModel = /** @class */ (function () {
    function AvalRequestModel(cod_aval_aval) {
        this.cod_aval_aval = cod_aval_aval;
    }
    return AvalRequestModel;
}());

//# sourceMappingURL=avalRequest.js.map

/***/ }),

/***/ 535:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmpleadorRequestModel; });
var EmpleadorRequestModel = /** @class */ (function () {
    function EmpleadorRequestModel(cod_empresa_empresa, des_nombre_empresa, des_direccion_empresa, des_telefono_empresa, des_mail_empresa, cod_giro_giro, cod_actividadeconomica_empresa, cod_comuna_comuna, cod_ciudad_ciudad) {
        this.cod_empresa_empresa = cod_empresa_empresa;
        this.des_nombre_empresa = des_nombre_empresa;
        this.des_direccion_empresa = des_direccion_empresa;
        this.des_telefono_empresa = des_telefono_empresa;
        this.des_mail_empresa = des_mail_empresa;
        this.cod_giro_giro = cod_giro_giro;
        this.cod_actividadeconomica_empresa = cod_actividadeconomica_empresa;
        this.cod_comuna_comuna = cod_comuna_comuna;
        this.cod_ciudad_ciudad = cod_ciudad_ciudad;
    }
    return EmpleadorRequestModel;
}());

//# sourceMappingURL=empleadorRequest.js.map

/***/ }),

/***/ 536:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ReportesPage = /** @class */ (function () {
    function ReportesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ReportesPage.prototype.ionViewDidLoad = function () { };
    ReportesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-reportes',template:/*ion-inline-start:"C:\ionicproyectos\parqueCopiapo\src\pages\reportes\reportes.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Reportes</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n</ion-content>'/*ion-inline-end:"C:\ionicproyectos\parqueCopiapo\src\pages\reportes\reportes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */]])
    ], ReportesPage);
    return ReportesPage;
}());

//# sourceMappingURL=reportes.js.map

/***/ }),

/***/ 542:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatabaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_sqlite__ = __webpack_require__(543);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DatabaseProvider = /** @class */ (function () {
    function DatabaseProvider(storage, platform) {
        var _this = this;
        this.storage = storage;
        this.platform = platform;
        if (!this.isOpen) {
            this.storage = new __WEBPACK_IMPORTED_MODULE_1__ionic_native_sqlite__["a" /* SQLite */]();
            // Crear base de datos
            this.storage.create({
                name: "parque.db",
                location: "default"
            }).then(function (db) {
                console.log('Base de datos creada');
                _this.db = db;
                //db.executeSql(this.crea_usuario, []);
                _this.isOpen = true;
            })
                .catch(function (error) {
                console.log(error);
            });
        }
    }
    DatabaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["w" /* Platform */]])
    ], DatabaseProvider);
    return DatabaseProvider;
}());

//# sourceMappingURL=database.js.map

/***/ })

},[328]);
//# sourceMappingURL=main.js.map