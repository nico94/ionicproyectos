import { Events } from 'ionic-angular';
//MODELOS
import { GirosEmpeadorRequest } from './../../modelos/girosEmpleadorRequest';
import { ActividadRequestModel } from '../../modelos/actividadRequest';
import { ParentescoRequestModel } from '../../modelos/parentescoRequest';
import { NecesidadRequestModel } from '../../modelos/necesidadRequest';
import { PresionRequestModel } from '../../modelos/presionRequest';
import { ClienteRequestModel } from '../../modelos/clienteRequest';
import { CiudadRequestModel } from './../../modelos/ciudadRequest';
import { SexoRequestModel } from './../../modelos/sexoRequest';
import { CivilRequestModel } from './../../modelos/civilRequest';
import { SectorRequestModel } from './../../modelos/sectorRequest';
import { ComunaRequestModel } from './../../modelos/comunaRequest';
//MODULOS
import { WebserviceProvider } from './../../providers/webservice/webservice';
import { ModNotaIngresoEmpleadorPage } from './../mod-nota-ingreso-empleador/mod-nota-ingreso-empleador';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, ModalController, ToastController } from 'ionic-angular';
import chileanRut from 'chilean-rut';

@Component({
  selector: 'page-mod-nota-ingreso',
  templateUrl: 'mod-nota-ingreso.html',
})
export class ModNotaIngresoPage {
  @ViewChild('rutInput') rutInput;
  fechaNacimiento: any = new Date().toISOString();
  cliente: any = {};
  clienteEncontrado: boolean = false;
  //Carga de datos
  comunas: any[] = [];
  ciudades: any[] = [];
  sectores: any[] = [];
  actividades: any[] = [];
  estadosCiviles: any[] = [];
  sexo: any[] = [];
  girosEmpleador: any[] = [];
  parentesco: any[] = [];
  necesidades: any[] = [];
  presionArterial: any[] = [];
  acreditacion: any[] = [];
  botonDescripcion: string = "Guardar";

  //Arreglos para nota
  empleador: any = {};
  aval: any = {};
  detalle: any = {};
  planimetria: any = {};
  salud: any = {};

  nota: any = {
    notaVenta: {
      cliente: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        sector: null,
        actividad: null,
        ciudad: null,
        civil: null,
        correo: null,
        telefono: null,
        celular: null,
        fechaNacimiento: null,
        edad: null,
        sexo: null
      },
      empleador: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        actividad: null,
        ciudad: null,
        acreditacion: [],
        renta: null,
        giro: null,
        telefono: null,
        otrosIngresos: []
      },
      aval: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        sector: null,
        actividad: null,
        ciudad: null,
        civil: null,
        correo: null,
        telefono: null,
        celular: null,
        fechaNacimiento: null,
        edad: null,
        sexo: null,
        renta: null,
        parentesco: null
      },
      detalle: {
        necesidad: null,
        planimetria: {
          descripcion: null,
          letra: null,
          estado: null,
          sector: 0,
          porc_descuento: 0,
          uf: 0,
          fila: null,
          latitud: 0,
          longitud: 0,
          numero: 0,
          valor_base: 0,
          capacidades: 0,
          reducciones: 0,
          pie: 0
        },
        descuento: null,
        pie: 0,
        dia_pago: 0,
        vencimiento: null,
        cuotas: null,
        valor_cuotas: null,
        uf: null,
        comentario: null
      },
      salud: {
        responde: false,
        peso: 0,
        estatura: 0,
        presion: null,
        ejecutiva: null,
        comentario: null,
        firma: null
      }
    }
  };

  constructor(
    public toast: ToastController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public webService: WebserviceProvider,
    public eventos: Events
  ) {
    this.obtener();
  }

  ionViewLoaded() { }

  ionViewDidLoad() {
    this.cargaDatosWebService();
  }

  seleccionaComuna(cod_ciudad: number) {
    this.webService.obtenerListaComunas(new ComunaRequestModel(-1, cod_ciudad))
      .subscribe(data => {
        let listado = data['ListadoComunaResult']['lista'];
        this.comunas = listado;
      }, error => {
        this.comunas = null;
        console.log('Error comunas WS: ' + error);
      });
  }

  cargaDatosWebService() {
    //Lista de sexo
    this.webService.obtenerListaSexo(new SexoRequestModel(-1))
      .subscribe(data => {
        let listado = data['ListadoSexoResult']['lista'];
        this.sexo = listado;
      }, error => {
        this.sexo = null;
        console.log('Error sexo WS: ' + error);
      });
    //Lista de sectores
    this.webService.obtenerListaSector(new SectorRequestModel(-1))
      .subscribe(data => {
        let listado = data['ListadoSectorResult']['lista'];
        this.sectores = listado;
      }, error => {
        this.sectores = null;
        console.log('Error carga Sectores ' + error);
      });
    //Lista Estados civiles
    this.webService.obtenerListaEstadoCivil(new CivilRequestModel(-1))
      .subscribe(data => {
        let listado = data['ListadoEstadoCivilResult']['lista'];
        this.estadosCiviles = listado;
      }, error => {
        this.estadosCiviles = null;
        console.log('Error estados civiles WS: ' + error);
      });
    //Lista de actividades
    this.webService.obtenerListaActividades(new ActividadRequestModel(-1))
      .subscribe(data => {
        let listado = data['ListadoCargoResult']['lista'];
        this.actividades = listado;
      }, error => {
        this.actividades = null;
        console.log('Error actividades WS: ' + error);
      });
    //Lista de ciudades
    this.webService.obtenerListaCiudades(new CiudadRequestModel(-1))
      .subscribe(data => {
        let listado = data['ListadoCiudadResult']['lista'];
        this.ciudades = listado;
      }, error => {
        this.ciudades = null;
        console.log('Error ciudad WS: ' + error);
      });
    //Giros Empleador
    this.webService.obtenerGirosEmpleador(new GirosEmpeadorRequest(-1))
      .subscribe(data => {
        let lista = data['ListadoGiroResult']['lista'];
        this.girosEmpleador = lista;
      }, error => {
        this.girosEmpleador = null;
        console.log('Error giros empleador WS: ' + error);
      });
    //Lista parentescos
    this.webService.obtenerListaParentesco(new ParentescoRequestModel(-1))
      .subscribe(data => {
        let lista = data['ListadoParentescoResult']['lista'];
        this.parentesco = lista;
      }, error => {
        this.parentesco = null;
      });
    //Lista de acreditacion
    this.webService.obtenerListaAcreditacion()
      .subscribe(data => {
        let lista = data['TipoAcreditacionResult']['lista'];
        this.acreditacion = lista;
      }, error => {
        this.acreditacion = null;
      });
    //Lista necesidades
    this.webService.obtenerListaNecesidades(new NecesidadRequestModel(-1))
      .subscribe(data => {
        let lista = data['ListadoTipoNecesidadResult']['lista'];
        this.necesidades = lista;
      }, error => {
        this.necesidades = null;
      });
    //Lista de presion arterial
    this.webService.obtenerListaPresionArterial(new PresionRequestModel(-1))
      .subscribe(data => {
        let lista = data['ListadoPresionArterialResult']['lista'];
        this.presionArterial = lista;
      }, error => {
        this.presionArterial = null;
      });
  }

  verificaCliente() {
    return !this.clienteEncontrado;
  }

  buscarDatosClienteRut(rut: string) {
    this.webService.obtenerDatosCliente(rut)
      .subscribe(data => {
        let lista = data['ListadoClienteResult']['lista'];
        let realizado = data['ListadoClienteResult']['realizado'];
        console.log('CLIENTE: ' + JSON.stringify(lista));
        if (realizado) {
          this.clienteEncontrado = false;
          this.cliente.nombre = lista[0]['des_nombre_cliente'];
          this.cliente.direccion = lista[0]['des_direccion_cliente'];
          this.cliente.sector = lista[0]['cod_sector_sector'];
          this.cliente.actividad = lista[0]['cod_cargo_cargo'];
          this.cliente.ciudad = lista[0]['cod_ciudad_ciudad'];
          this.seleccionaComuna(this.cliente.ciudad);
          this.cliente.comuna = lista[0]['cod_comuna_comuna'];
          this.cliente.civil = lista[0]['cod_estadocivil_estadocivil'];
          this.cliente.correo = lista[0]['des_mail_cliente'];
          this.cliente.telefono = lista[0]['des_telefono_cliente'];
          this.cliente.celular = lista[0]['des_telefono_celular'];
          let fechaFormateada = new Date(parseInt(lista[0]['dat_fecha_nacimiento'].replace(/(^.*\()|([+-].*$)/g, ''))).toISOString();
          // console.log("fechaFormateada " + fechaFormateada);
          this.cliente.fechaNacimiento = fechaFormateada;
          this.cliente.edad = lista[0]['edad'];
          this.cliente.sexo = lista[0]['cod_sexo_sexo'];
          this.botonDescripcion = "Modificar Cliente";
        } else {
          this.botonDescripcion = "Crear Cliente";
          this.clienteEncontrado = true;
        }
      }, error => {
        this.clienteEncontrado = false;
        console.log('ERROR DATOS CLIENTE ' + JSON.stringify(error));
      });
  }

  foco() {
    if (this.cliente.rut) {
      if (this.cliente.rut.length !== 12) {
        this.cliente.rut = "";
      }
    }
  }

  formatoRut() {
    if (this.cliente.rut) {
      var dv = this.cliente.rut.substring(8);
      this.cliente.rut = chileanRut.format(this.cliente.rut, dv);
      if (this.cliente.rut.length == 12) {
        if (!chileanRut.validate(this.cliente.rut)) {
          this.mostarToast('Rut no válido');
          this.cliente.rut = "";
        } else {
          var rut = chileanRut.unformat(this.cliente.rut);
          this.buscarDatosClienteRut(rut);
        }
      } else if (this.cliente.rut.length < 12) {
        this.cliente.nombre = "";
        this.cliente.direccion = "";
        this.cliente.ciudad = "";
        this.cliente.comuna = "";
        this.cliente.sector = "";
        this.cliente.actividad = "";
        this.cliente.civil = "";
        this.cliente.correo = "";
        this.cliente.telefono = "";
        this.cliente.celular = "";
        this.cliente.fechaNacimiento = "";
        this.cliente.edad = "";
        this.cliente.sexo = "";
      }
    }
    this.clienteEncontrado = false;
  }

  obtener() {
    let data = this.navParams.get('nota');
    if (data !== undefined) {
      if (data.notaVenta.cliente !== undefined) {
        this.cliente = data.notaVenta.cliente;
        this.seleccionaComuna(this.cliente.ciudad);
      }
      if (data.notaVenta.empleador !== undefined) {
        this.empleador = data.notaVenta.empleador;
      }
      if (data.notaVenta.aval !== undefined) {
        this.aval = data.notaVenta.aval;
      }
      if (data.notaVenta.detalle !== undefined) {
        this.detalle = data.notaVenta.detalle;
      }
      if (data.notaVenta.detalle.planimetria !== undefined) {
        this.planimetria = data.notaVenta.detalle.planimetria;
      }
      if (data.notaVenta.salud !== undefined) {
        this.salud = data.notaVenta.salud;
      }
    }
  }

  continuar() {
    this.nota.notaVenta.cliente = this.cliente;
    this.nota.notaVenta.empleador = this.empleador;
    this.nota.notaVenta.aval = this.aval;
    this.nota.notaVenta.detalle = this.detalle;
    this.nota.notaVenta.detalle.planimetria = this.planimetria;
    this.nota.notaVenta.salud = this.salud;
    this.nota.notaVenta.salud.ejecutiva = localStorage.getItem('ejecutivo');
    if (this.nota.notaVenta.cliente.rut && this.nota.notaVenta.cliente.nombre && this.nota.notaVenta.cliente.direccion &&
      this.nota.notaVenta.cliente.comuna && this.nota.notaVenta.cliente.sector && this.nota.notaVenta.cliente.actividad &&
      this.nota.notaVenta.cliente.civil && this.nota.notaVenta.cliente.correo && this.nota.notaVenta.cliente.fechaNacimiento &&
      this.nota.notaVenta.cliente.edad && this.nota.notaVenta.cliente.sexo
    ) {
      let modalEmpleador = this.modalCtrl.create(ModNotaIngresoEmpleadorPage,
        {
          nota: this.nota,
          ciudades: this.ciudades,
          sectores: this.sectores,
          actividades: this.actividades,
          estadosCiviles: this.estadosCiviles,
          sexo: this.sexo,
          girosEmpleador: this.girosEmpleador,
          parentesco: this.parentesco,
          necesidades: this.necesidades,
          presionArterial: this.presionArterial,
          acreditacion: this.acreditacion
        },
        { enableBackdropDismiss: false });

      var rut = chileanRut.unformat(this.nota.notaVenta.cliente.rut);
      var vendedor = parseInt(localStorage.getItem('login'));
      let clienteRequest = new ClienteRequestModel(
        vendedor,
        rut,
        this.nota.notaVenta.cliente.comuna,
        this.nota.notaVenta.cliente.giro,
        this.nota.notaVenta.cliente.nombre,
        this.nota.notaVenta.cliente.direccion,
        this.nota.notaVenta.cliente.telefono,
        this.nota.notaVenta.cliente.correo,
        this.nota.notaVenta.cliente.ciudad,
        this.nota.notaVenta.cliente.sector,
        this.nota.notaVenta.cliente.civil,
        this.nota.notaVenta.cliente.celular,
        this.nota.notaVenta.cliente.fechaNacimiento,
        this.nota.notaVenta.cliente.edad,
        this.nota.notaVenta.cliente.sexo,
        this.nota.notaVenta.cliente.renta,
        this.nota.notaVenta.cliente.actividad,
        this.nota.notaVenta.cliente.parentesco
      );

      if (this.clienteEncontrado) {
        this.webService.guardarCliente(clienteRequest)
          .subscribe(data => {
            let resultado = data['GuardarClienteResult']['resultado'];
            console.log('RESPUESTA: ' + JSON.stringify(data));
            if (resultado) {
              this.mostarToast('Cliente nuevo creado correctamente');
              modalEmpleador.present();
              this.viewCtrl.dismiss();
            } else {
              this.mostarToast('Error al guardar cliente nuevo');
            }
          }, error => {
            this.mostarToast('Error en respuesta del servidor');
            console.log('Error guardar cliente ' + JSON.stringify(error));
          });
      } else {
        //Aqui modificar el cliente
        this.webService.guardarCliente(clienteRequest)
          .subscribe(data => {
            let resultado = data['GuardarClienteResult']['resultado'];
            console.log('RESPUESTA: ' + JSON.stringify(data));
            if (resultado) {           
              this.mostarToast('Cliente modificado correctamente');
              modalEmpleador.present();
              this.viewCtrl.dismiss();
            } else {
              this.mostarToast('Error al modificar cliente');
            }
          }, error => {
            this.mostarToast('Error en respuesta del servidor');
            console.log('ERROR EN EL SERVIDOR ' + JSON.stringify(error));
          });
      }
    } else {
      this.mostarToast('Faltan parámetros para cliente');
    }
  }

  mostarToast(mensaje: string) {
    if (mensaje) {
      let toast = this.toast.create({
        message: mensaje,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
  }

  cerrar() {
    let alert = this.alertCtrl.create({
      title: 'Cancelar Ingreso',
      message: '¿Está seguro que quiere cancelar el ingreso?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => { }
        }, {
          text: 'Si',
          handler: () => {
            this.viewCtrl.dismiss();
            this.eventos.publish('recarga_notas');
          }
        }
      ]
    });
    alert.present();
  }
}

  // PARA MODULO DE RUT
  // import chileanRut from 'chilean-rut'
  // chileanRut.validate('')
  // chileanRut.format('')
  // chileanRut.unformat('')
  // chileanRut.validValidatorDigit('')
  // chileanRut.correctValidatorDigit('')
  // chileanRut.getValidatorDigit('')