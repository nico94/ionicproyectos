import { UsuaurioResponseModel } from './../../modelos/usuarioResponse';
import { UsuarioModel } from './../../modelos/usuario';
import { AutenticacionProvider } from './../../providers/autenticacion/autenticacion';
import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, Platform, LoadingController, Events } from 'ionic-angular';
import { Md5 } from 'ts-md5/dist/md5';
import { Network } from '@ionic-native/network';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  user: any[] = [];
  usuario: string;
  password: string;
  ip: string;
  passwordMD5: any;
  logo: string = "./assets/imgs/logo_parque.png";
  esDispositivo: boolean = false;
  cargando: any;

  constructor(
    public auth: AutenticacionProvider,
    public toast: ToastController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public plataforma: Platform,
    public cargaCtrl: LoadingController,
    public eventos: Events,
    public network: Network,
  ) {
    this.ip = null;
    let ipserver = localStorage.getItem('ip');
    if (ipserver) {
      this.ip = ipserver;
    }
    if (this.plataforma.is('Cordova')) {
      this.esDispositivo = true;
    } else {
      this.esDispositivo = false;
    }
  }

  ionViewDidLoad() { }

  guardarIP() {
    localStorage.setItem('ip', this.ip);
  }

  iniciarSesion() {
    if (this.usuario != null) {
      if (this.password != null) {
        if (this.ip != null) {
          this.mostrarCarga();
          this.passwordMD5 = Md5.hashAsciiStr(this.password);
          let usu = new UsuarioModel(this.usuario, this.passwordMD5);
          this.auth.inciarSesion(usu).subscribe(
            data => {
              this.user = data['InciarSesionResult'];
              if (this.user['estado']) {
                let usuResponse = new UsuaurioResponseModel(
                  this.user['codigo'],
                  this.user['cod_cargo'],
                  this.user['des_cargo'],
                  this.user['cod_comuna'],
                  this.user['rut'],
                  this.user['nombres'],
                  this.user['ap_paterno'],
                  this.user['ap_materno'],
                  this.user['direccion'],
                  this.user['email'],
                  this.user['fecha_nac'],
                  this.user['telefono']
                );
                console.log('USUARIO ' + JSON.stringify(usuResponse));
                if (this.esDispositivo) {
                  localStorage.setItem('login', this.user['codigo']);
                  localStorage.setItem('ejecutivo', this.user['nombres'] + ' ' + this.user['ap_paterno'] + ' ' + this.user['ap_materno']);
                  localStorage.setItem('cod_cargo', this.user['cod_cargo']);
                  this.eventos.publish('verifica_ruta');
                } else {
                  localStorage.setItem('login', this.user['codigo']);
                  localStorage.setItem('ejecutivo', this.user['nombres'] + ' ' + this.user['ap_paterno'] + ' ' + this.user['ap_materno']);
                  localStorage.setItem('cod_cargo', this.user['cod_cargo']);
                  this.eventos.publish('verifica_ruta');
                }
                this.cargando.dismiss();
              } else {
                this.cargando.dismiss();
                this.mostrarToast('Usuario/contraseña incorrectos');
              }
            },
            error => {
              this.mostrarToast('Error en servidor');
              this.cargando.dismiss();
            });
        } else {
          this.mostrarToast('Ingrese ip del servidor');
        }
      } else {
        this.mostrarToast('Ingrese contraseña');
      }
    } else {
      this.mostrarToast('Ingrese usuario');
    }
  }

  mostrarCarga() {
    this.cargando = this.cargaCtrl.create({
      content: "Iniciando sesión..."
    });
    this.cargando.present();
  }

  mostrarToast(mensaje: string) {
    if (mensaje) {
      let toast = this.toast.create({
        message: mensaje,
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
  }
}