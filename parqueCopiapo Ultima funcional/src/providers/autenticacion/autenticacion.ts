import { UsuarioModel } from './../../modelos/usuario';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//MODULOS
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';

@Injectable()
export class AutenticacionProvider {
  ip: string = null;
  apiUrl: string = null;

  constructor(
    public http: HttpClient
  ) {
    this.refrescarIP();
  }

  refrescarIP() {
    this.ip = localStorage.getItem('ip');
    if (this.ip) {
      this.apiUrl = "http://" + this.ip + "/wsJesusPons/Servicios/Usuario.svc/rest/login";
    }
  }

  inciarSesion(usuario: UsuarioModel) {
    this.refrescarIP();
    if (usuario.usuario && usuario.password) {
      let body = {
        "usuario": {
          "IdUsuario": usuario.usuario,
          "PswUsuario": usuario.password
        }
      };
      let headers = new HttpHeaders({
        'Content-type': 'application/json'
      });
      return this.http.post(this.apiUrl, body, { headers: headers })
        .retry(3)
        .timeout(5000)
        .catch((error: any)=>{
          return error;
        });
    }
  }
}