import { NotaSaludRequestModel } from './../../modelos/notaSaludRequest';
import { NotaCabeceraRequest } from '../../modelos/notaCabeceraRequest';
import { PlanimetriaRequestModel } from '../../modelos/planimetriaRequest';
import { NotaRequestModel } from '../../modelos/notaRequest';
import { EmpleadorRequestModel } from '../../modelos/empleadorRequest';
import { ClienteRequestModel } from '../../modelos/clienteRequest';
import { PresionRequestModel } from '../../modelos/presionRequest';
import { NecesidadRequestModel } from '../../modelos/necesidadRequest';
import { ParentescoRequestModel } from '../../modelos/parentescoRequest';
import { GirosEmpeadorRequest } from '../../modelos/girosEmpleadorRequest';
//MODULOS
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
//MODELOS REQUEST
import { SectorRequestModel } from '../../modelos/sectorRequest';
import { ComunaRequestModel } from '../../modelos/comunaRequest';
import { ActividadRequestModel } from '../../modelos/actividadRequest';
import { CivilRequestModel } from '../../modelos/civilRequest';
import { SexoRequestModel } from '../../modelos/sexoRequest';
import { CiudadRequestModel } from '../../modelos/ciudadRequest';
import { AvalRequestModel } from '../../modelos/avalRequest';
import { NotaPlanimetriaRequest } from '../../modelos/notaPlanimetriaRequest';

@Injectable()
export class WebserviceProvider {
  ip: string = null;
  urlCrearNota: string = null;
  urlListadoNotaVenta: string = null;
  urlListarComunas: string = null;
  urlListarCiudades: string = null;
  urlListarSector: string = null;
  urlListarActividades: string = null;
  urlListarSexo: string = null;
  urlListarEstadoCivil: string = null;
  urlListarGirosEmpleador: string = null;
  urlListarParentescos: string = null;
  urlListarNecesidades: string = null;
  urlListarPresion: string = null;
  urlListarAcreditacion: string = null;
  urlDatosCliente: string = null;
  urlDatosPlanimetria: string = null;
  urlGuardarCliente: string = null;
  urlGuardarEmpleador: string = null;
  urlDatosEmpleador: string = null;
  cabeceras: any;

  constructor(
    public http: HttpClient
  ) {
    this.refrescarIP();
    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  refrescarIP() {
    //let ipServer = localStorage.getItem('ip');
    let ipServer = '192.168.0.85';
    //let ipServer = '192.168.0.66';
    this.ip = ipServer;
    if (this.ip) {
      this.urlCrearNota = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/GuardarNotaVenta';
      this.urlListadoNotaVenta = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoNotaVentaCementerio';
      this.urlListarComunas = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoComuna';
      this.urlListarCiudades = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoCiudad';
      this.urlListarSector = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoSector';
      this.urlListarActividades = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoCargo';
      this.urlListarSexo = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoSexo';
      this.urlListarEstadoCivil = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoEstadoCivil';
      this.urlListarGirosEmpleador = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoGiro';
      this.urlListarParentescos = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoParentesco';
      this.urlListarNecesidades = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoTipoNecesidad';
      this.urlListarPresion = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoPresionArterial';
      this.urlListarAcreditacion = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/TipoAcreditacion';
      this.urlDatosCliente = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoCliente';
      this.urlDatosPlanimetria = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/Listadosnap_geoplanimetria';
      this.urlGuardarCliente = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/GuardarCliente';
      this.urlGuardarEmpleador = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/GuardarEmpresa';
      this.urlDatosEmpleador = 'http://' + this.ip + '/wsCementerio/Servicios/Contrato.svc/rest/ListadoEmpresa';
    }
  }

  //Lista de notas de venta
  obtenerListaNotaVenta(req: NotaRequestModel) {
    this.refrescarIP();
    let cuerpo = { "ListadoNotaVentaCementerioRequest": { "Cod_Vendedor": req.cod_usuario, "Mes": req.mes, "Año": req.ano } };
    return this.http.post(this.urlListadoNotaVenta, cuerpo, { headers: this.cabeceras })
      .timeout(1000)
      .catch((err: any) => {
        return err;
      });
  }

  //Lista de Comunas
  obtenerListaComunas(com: ComunaRequestModel) {
    this.refrescarIP();
    let cuerpo = {
      "ListadoComunarequest": {
        "cod_comuna_comuna": com.cod_comuna_comuna,
        "cod_ciudad_ciudad": com.cod_ciudad_ciudad
      }
    };
    return this.http.post(this.urlListarComunas, cuerpo, { headers: this.cabeceras });
  }

  //Lista de ciudades
  obtenerListaCiudades(ciu: CiudadRequestModel) {
    this.refrescarIP();
    let cuerpo = { "ListadoCiudadrequest": { "cod_ciudad_ciudad": ciu.cod_ciudad_ciudad } };
    return this.http.post(this.urlListarCiudades, cuerpo, { headers: this.cabeceras });
  }

  //Lista de Sectores
  obtenerListaSector(sec: SectorRequestModel) {
    this.refrescarIP();
    let cuerpo = { "ListadoSectorrequest": { "cod_sector_sector": sec.cod_sector_sector } };
    return this.http.post(this.urlListarSector, cuerpo, { headers: this.cabeceras });
  }

  //Lista de actividades
  obtenerListaActividades(act: ActividadRequestModel) {
    this.refrescarIP();
    let cuerpo = { "ListadoCargorequest": { "cod_cargo_cargo": act.cod_cargo_cargo } };
    return this.http.post(this.urlListarActividades, cuerpo, { headers: this.cabeceras });
  }

  //Lista de sexo
  obtenerListaSexo(sex: SexoRequestModel) {
    this.refrescarIP();
    let cuerpo = { "ListadoSexorequest": { "cod_sexo_sexo": sex.cod_sexo_sexo } };
    return this.http.post(this.urlListarSexo, cuerpo, { headers: this.cabeceras });
  }

  //Lista estado civil
  obtenerListaEstadoCivil(civil: CivilRequestModel) {
    this.refrescarIP();
    let cuerpo = { "ListadoEstadoCivilrequest": { "cod_estadocivil_estadocivil": civil.cod_estadocivil_estadocivil } };
    return this.http.post(this.urlListarEstadoCivil, cuerpo, { headers: this.cabeceras });
  }

  //Lista giros del empleador
  obtenerGirosEmpleador(giros: GirosEmpeadorRequest) {
    this.refrescarIP();
    let cuerpo = { "ListadoGirorequest": { "cod_giro_giro": giros.cod_giro_giro } };
    return this.http.post(this.urlListarGirosEmpleador, cuerpo, { headers: this.cabeceras });
  }

  //Lista parentesco
  obtenerListaParentesco(parent: ParentescoRequestModel) {
    this.refrescarIP();
    let cuerpo = { "ListadoParentescorequest": { "cod_parentesco_parentesco": parent.cod_parentesco_parentesco } };
    return this.http.post(this.urlListarParentescos, cuerpo, { headers: this.cabeceras });
  }

  //Lista de necesidades
  obtenerListaNecesidades(nec: NecesidadRequestModel) {
    this.refrescarIP();
    let cuerpo = { "ListadoTipoNecesidadrequest": { "cod_tipo_necesidad": nec.cod_tipo_necesidad } };
    return this.http.post(this.urlListarNecesidades, cuerpo, { headers: this.cabeceras });
  }

  //Lista presion arterial
  obtenerListaPresionArterial(pres: PresionRequestModel) {
    this.refrescarIP();
    let cuerpo = { "ListadoPresionArterialrequest": { "id_codigo_parterial": pres.id_codigo_parterial } };
    return this.http.post(this.urlListarPresion, cuerpo, { headers: this.cabeceras });
  }

  //Lista acreditacion
  obtenerListaAcreditacion() {
    this.refrescarIP();
    let cuerpo = { "ListadoTipoAcreditacionrequest": { "id_codigo_taingresos": "" } };
    return this.http.post(this.urlListarAcreditacion, cuerpo, { headers: this.cabeceras });
  }

  //Datos cliente
  obtenerDatosCliente(cli_cod: string) {
    this.refrescarIP();
    let cuerpo = { "ListadoClienterequest": { "cod_cliente_cliente": cli_cod } };
    return this.http.post(this.urlDatosCliente, cuerpo, { headers: this.cabeceras });
  }

  //Datos empleador
  obtenerDatosEmpleador(emp_cod: string) {
    this.refrescarIP();
    let cuerpo = { "ListadoEmpresaRequest": { "cod_empresa_empresa": emp_cod } };
    return this.http.post(this.urlDatosEmpleador, cuerpo, { headers: this.cabeceras });
  }

  //Datos aval
  obtenerDatosAval(aval: AvalRequestModel) {
    this.refrescarIP();
    let cuerpo = { "ListadoClienterequest": { "cod_cliente_cliente": aval.cod_aval_aval } };
    return this.http.post(this.urlDatosCliente, cuerpo, { headers: this.cabeceras });
  }

  //Guarda cliente/aval
  guardarCliente(cli: ClienteRequestModel) {
    this.refrescarIP();
    let cuerpo = {
      "GuardarClienteRequest":
      {
        "Cliente": {
          "cod_vendedor_fichapersonal": cli.cod_vendedor_fichapersonal,
          "cod_cliente_cliente": cli.cod_cliente_cliente,
          "cod_comuna_comuna": cli.cod_comuna_comuna,
          "cod_giro_giro": cli.cod_giro_giro,
          "des_nombre_cliente": cli.des_nombre_cliente,
          "des_direccion_cliente": cli.des_direccion_cliente,
          "des_telefono_cliente": cli.des_telefono_cliente,
          "des_mail_cliente": cli.des_mail_cliente,
          "cod_ciudad_ciudad": cli.cod_ciudad_ciudad,
          "cod_sector_sector": cli.cod_sector_sector,
          "cod_estadocivil_estadocivil": cli.cod_estadocivil_estadocivil,
          "des_telefono_celular": cli.des_telefono_celular,
          "edad": cli.edad,
          "cod_sexo_sexo": cli.cod_sexo_sexo,
          "num_montorentaliquida_clientes": cli.num_montorentaliquida_clientes,
          "cod_cargo_cargo": cli.cod_cargo_cargo,
          "cod_parentesco_parentesco": cli.cod_parentesco_parentesco
        },
        "FechaNacimientoString": cli.dat_fecha_nacimiento
      }
    };
    console.log('REQUEST: ' + JSON.stringify(cuerpo));
    return this.http.post(this.urlGuardarCliente, cuerpo, { headers: this.cabeceras });
  }

  //Guarda empleador
  guardarEmpleador(emp: EmpleadorRequestModel) {
    this.refrescarIP();
    let cuerpo = {
      "GuardarEmpresarequest":
      {
        "Empresa": {
          "cod_empresa_empresa": emp.cod_empresa_empresa,
          "des_nombre_empresa": emp.des_nombre_empresa,
          "des_direccion_empresa": emp.des_direccion_empresa,
          "des_telefono_empresa": emp.des_telefono_empresa,
          "des_mail_empresa": emp.des_mail_empresa,
          "cod_giro_giro": emp.cod_giro_giro,
          "cod_actividadeconomica_empresa": emp.cod_actividadeconomica_empresa,
          "cod_comuna_comuna": emp.cod_comuna_comuna,
          "cod_ciudad_ciudad": emp.cod_ciudad_ciudad
        }
      }
    };
    console.log('REQUEST EMPLEADOR ' + JSON.stringify(cuerpo));
    return this.http.post(this.urlGuardarEmpleador, cuerpo, { headers: this.cabeceras });
  }

  //Guardar nota de venta
  guardarNotaVenta(nota: NotaCabeceraRequest, planimetria: NotaPlanimetriaRequest, salud: NotaSaludRequestModel, documentos: any) {
    this.refrescarIP();
    let cuerpo = {
      "GuardarNotaVentarequest":
      {
        "Encabezado": {
          "cod_cliente_cliente": nota.cod_cliente_cliente,
          "cod_referencia_cliente": nota.cod_referencia_cliente,
          "cod_aval_cliente": nota.cod_aval_cliente,
          "Cod_Vendedor": nota.Cod_Vendedor,
          "num_pie_contrato": nota.num_pie_contrato,
          "cod_tipo_necesidad": nota.cod_tipo_necesidad,
          "Valor_base": nota.Valor_base,
          "Poc_descuento": nota.Poc_descuento,
          "Dia_pago": nota.Dia_pago,
          "num_cuotas": nota.num_cuotas,
          "Valor_cuota": nota.Valor_cuota,
          "UF_Valor": nota.UF_Valor,
          "Glosa": nota.Glosa
        },
        "fotofirma": nota.img_imagen_firma,
        "FechaPrimerVenc": nota.FechaPrimerVenc,

        "snapEmpCliente": {
          "cod_empresa_empresa": nota.cod_empresa_empresa,
          "cod_cliente_cliente": nota.cod_cliente_cliente
        },
        "listaDetalle_PlanimetriaContrato": [{
          //listaDetalle_PlanimetriaContrato
          "Numero_fila": planimetria.Numero_fila,
          "id_codigo_sector": planimetria.id_codigo_sector,
          "Letra": planimetria.Letra,
          "num_capacidades_snapsector": planimetria.num_capacidades_snapsector,
          "num_capautilizada_snapsector": planimetria.num_capautilizada_snapsector,
          "num_reducciones_snapsector": planimetria.num_reducciones_snapsector,
        }],
        // "listasnap_ContratoTAIngresos": documentos,
        "Xsnap_ContratoTAIngresosTablet": documentos,
        "listaDecla_Salud": [{
          "cod_cliente_cliente": nota.cod_cliente_cliente,
          "bit_responde_dsalud": salud.bit_responde_dsalud,
          "num_peso_dsalud": salud.num_peso_dsalud,
          "num_estatura_dsalud": salud.num_estatura_dsalud,
          "des_obs_dsalud": salud.des_obs_dsalud,
          "id_codigo_parterial": salud.id_codigo_parterial
        }]
      }
    };
    console.log('REQUEST NOTA : ' + JSON.stringify(cuerpo));
    return this.http.post(this.urlCrearNota, cuerpo, { headers: this.cabeceras })
      .retry(3)
      .timeout(5000)
      .catch((err: any) => {
        return err;
      });  
  }

  obtenerGeoPlanmetria(necesidad: PlanimetriaRequestModel) {
    this.refrescarIP();
    let cuerpo = {
      "Listadosnap_geoplanimetriarequest": {
        "Letra": necesidad.Letra,
        "Disponible": necesidad.Disponible,
        "id_codigo_sector": necesidad.id_codigo_sector,
        "Numero_fila": necesidad.Numero_fila,
        "necesidad_inmediata": necesidad.necesidad_inmediata
      }
    };
    // console.log('REQUEST: ' + JSON.stringify(cuerpo));
    return this.http.post(this.urlDatosPlanimetria, cuerpo, { headers: this.cabeceras });
  }

  //Eliminar nota de venta
  eliminarNotaVenta(nota) {
    this.refrescarIP();
  }

  //Editar nota de venta
  editarNotaVenta(nota) {
    this.refrescarIP();
  }
}