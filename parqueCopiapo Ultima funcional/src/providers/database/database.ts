import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from "@ionic-native/sqlite";
import { Platform } from 'ionic-angular';

@Injectable()
export class DatabaseProvider {
  public db: SQLiteObject;
  private isOpen: boolean;

  constructor(
    public storage: SQLite,
    public platform: Platform
  ) {
    if (!this.isOpen) {
      this.storage = new SQLite();
      // Crear base de datos
      this.storage.create({
        name: "parque.db",
        location: "default"
      }).then((db: SQLiteObject) => {
        console.log('Base de datos creada');
        this.db = db;
        //db.executeSql(this.crea_usuario, []);
        this.isOpen = true;
      })
        .catch((error) => {
          console.log(error);
        });
    }
  }
}