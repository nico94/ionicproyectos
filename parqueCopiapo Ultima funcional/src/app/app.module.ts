//INTERNOS
import { MyApp } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
//COMPONENTES
import { MenuPrincipalComponent } from '../components/menu-principal/menu-principal';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//PROVIDERS
import { AutenticacionProvider } from '../providers/autenticacion/autenticacion';
import { WebserviceProvider } from '../providers/webservice/webservice';
import { NetworkProvider } from '../providers/network/network';
//PAGINAS
import { LoginPage } from './../pages/login/login';
import { HomePage } from '../pages/home/home';
import { NotaVentaPage } from './../pages/nota-venta/nota-venta';
import { ReportesPage } from './../pages/reportes/reportes';
import { ModNotaIngresoPage } from './../pages/mod-nota-ingreso/mod-nota-ingreso';
import { ModNotaIngresoEmpleadorPage } from '../pages/mod-nota-ingreso-empleador/mod-nota-ingreso-empleador';
import { IngresoDocumentosPage } from '../pages/ingreso-documentos/ingreso-documentos';
import { ModIngresoAvalPage } from './../pages/mod-ingreso-aval/mod-ingreso-aval';
import { ModNotaIngresoDetallePage } from './../pages/mod-nota-ingreso-detalle/mod-nota-ingreso-detalle';
import { ModPlanimeriaPage } from './../pages/mod-planimeria/mod-planimeria';
import { ModDeclaracionSaludPage } from './../pages/mod-declaracion-salud/mod-declaracion-salud';
//PLUGINS
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { Base64 } from '@ionic-native/base64';
import { Camera } from '@ionic-native/camera';
import { Network } from '@ionic-native/network';
import { AgmCoreModule } from '@agm/core';
import { DatabaseProvider } from '../providers/database/database';
import { IonicImageViewerModule } from 'ionic-img-viewer';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    NotaVentaPage,
    ReportesPage,
    IngresoDocumentosPage,
    ModNotaIngresoPage,
    ModNotaIngresoEmpleadorPage,
    ModIngresoAvalPage,
    ModNotaIngresoDetallePage,
    ModDeclaracionSaludPage,
    ModPlanimeriaPage,
    MenuPrincipalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicImageViewerModule,
    //AgmCoreModule.forRoot({ apiKey: '' }),
    AgmCoreModule.forRoot({ apiKey: 'AIzaSyA4a4T2YIbgNA9ce_1rJIxKdvn2TgiC6Us' }), //MapasAngular
    IonicModule.forRoot(MyApp,
      {
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthShortNames: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
        dayNames: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
        dayShortNames: ['lun', 'mar', 'mie', 'jue', 'vie', 'sab', 'dom'],
      }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    NotaVentaPage,
    ReportesPage,
    IngresoDocumentosPage,
    ModNotaIngresoPage,
    ModNotaIngresoEmpleadorPage,
    ModIngresoAvalPage,
    ModNotaIngresoDetallePage,
    ModDeclaracionSaludPage,
    ModPlanimeriaPage,
    MenuPrincipalComponent,
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    StatusBar,
    SplashScreen,
    AutenticacionProvider,
    WebserviceProvider,
    FileChooser,
    FilePath,
    Camera,
    Base64,
    Network,
    NetworkProvider,
    DatabaseProvider
  ]
})
export class AppModule { }