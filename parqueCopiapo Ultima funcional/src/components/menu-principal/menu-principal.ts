import { Component } from '@angular/core';
import { ViewController, NavController, AlertController, App, Platform, Events, ToastController } from 'ionic-angular';

@Component({
  selector: 'menu-principal',
  templateUrl: 'menu-principal.html'
})
export class MenuPrincipalComponent {
  items: any;
  esDispositivo: boolean;

  constructor(
    public platform: Platform,
    public viewControl: ViewController,
    public nav: NavController,
    public app: App,
    public alertCtrl: AlertController,
    public eventos: Events,
    public toast: ToastController
  ) {
    if (this.platform.is('cordova')) {
      this.esDispositivo = true;
    } else {
      this.esDispositivo = false;
    }
    this.items = [
      { id: 1, titulo: 'Cerrar sesión' }
    ]
  }

  clickItem(item) {
    console.log(item.id);
    switch (item.id) {
      case 1: {
        this.mostrarAlerta();
        break;
      }
      default: {
        break;
      }
    }
    this.viewControl.dismiss(item);
  }

  mostrarAlerta() {
    let alert = this.alertCtrl.create({
      title: 'Cerrar sesión',
      message: '¿Está seguro que quiere cerrar la sesión?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancela');
          }
        }, {
          text: 'Si',
          handler: () => {
            this.cerrarSesion();
          }
        }
      ]
    });
    alert.present();
  }

  cerrarSesion() {
    if (this.esDispositivo) {
      localStorage.removeItem('login');
      localStorage.removeItem('cod_cargo');
      localStorage.removeItem('ejecutivo');
      this.eventos.publish('verifica_ruta');
    } else {
      localStorage.removeItem('login');
      localStorage.removeItem('cod_cargo');
      localStorage.removeItem('ejecutivo');
      this.eventos.publish('verifica_ruta');
    }
  }
}