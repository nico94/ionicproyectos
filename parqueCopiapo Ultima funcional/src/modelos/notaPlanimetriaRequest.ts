export class NotaPlanimetriaRequest {
    constructor(
        public Numero_fila: number,
        public id_codigo_sector: number,
        public Letra: string,
        public num_capacidades_snapsector: number,
        public num_capautilizada_snapsector: number,
        public num_reducciones_snapsector: number
    ) { }
}