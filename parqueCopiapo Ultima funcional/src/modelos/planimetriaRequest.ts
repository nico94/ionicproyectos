export class PlanimetriaRequestModel {
    constructor(
        public necesidad_inmediata: boolean,
        public Disponible: boolean,
        public Letra: string,
        public Numero_fila: number,
        public id_codigo_sector: number,
    ) { }
}