export class NotaCabeceraRequest {
    constructor(
        public cod_cliente_cliente: string,
        public cod_empresa_empresa: string,
        public cod_referencia_cliente: string,
        public cod_aval_cliente: string,
        public Cod_Vendedor: number,
        public num_pie_contrato: number,
        public cod_tipo_necesidad: number,
        public Valor_base: number,
        public Poc_descuento: number,
        public Dia_pago: number,
        public num_cuotas: number,
        public Valor_cuota: number,
        public UF_Valor: number,
        public Glosa: string,
        public FechaPrimerVenc: string,
        public img_imagen_firma: string,
    ) { }
}