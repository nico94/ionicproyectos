import { NgModule } from '@angular/core';
import { MenuCardRecepcionComponent } from './menu-card-recepcion/menu-card-recepcion';
import { MenuPrincipalComponent } from './menu-principal/menu-principal';
import { MenuCardOtComponent } from './menu-card-ot/menu-card-ot';

@NgModule({
	declarations: [
        MenuCardRecepcionComponent,
        MenuPrincipalComponent,
        MenuCardOtComponent
    ],
	imports: [],
	exports: [
        MenuCardRecepcionComponent,
        MenuPrincipalComponent,
        MenuCardOtComponent
    ]
})
export class ComponentsModule {}
