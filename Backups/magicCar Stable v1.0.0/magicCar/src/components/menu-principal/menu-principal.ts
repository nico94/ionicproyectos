import { DatabaseProvider } from './../../providers/database/database';
import { Component } from '@angular/core';
import { ViewController, NavController, AlertController, App } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';

@Component({
  selector: 'menu-principal',
  templateUrl: 'menu-principal.html'
})
export class MenuPrincipalComponent {
  items: any;
  rootPage:any = LoginPage;

  constructor(
    public viewControl: ViewController,
    public nav:NavController,
    public app:App,
    private alertCtrl: AlertController,
    public db:DatabaseProvider
  ){
    this.items=[
      { id:1, titulo: 'Cerrar sesión' }
    ]
  }

  clickItem(item){  
    console.log(item.id);
    switch (item.id){
      case 1:{
        this.mostrarAlerta();
        break;
      }
      default:{
        break;
      }
    }
    this.viewControl.dismiss(item);
  }

  mostrarAlerta(){
    let alert = this.alertCtrl.create({
      title: 'Cerrar sesión',
      message: '¿Está seguro que quiere cerrar la sesión',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancela');
          }
        },{
          text: 'Si',
          handler: () => {
            this.cerrarSesion();
          }
        }
      ]
    });
    alert.present();
  }
  
  cerrarSesion(){
    this.db.borrarUsuario().
      then(()=>{
        this.app.getRootNav().setRoot(LoginPage);
        localStorage.removeItem('login');
        console.log('USUARIO ELIMINADO');
      }).catch((error)=>{
        console.log('ERROR AL ELIMINAR');
      });
  }
}