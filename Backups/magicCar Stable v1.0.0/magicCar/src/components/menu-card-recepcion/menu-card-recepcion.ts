import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({
  selector: 'menu-card-recepcion',
  templateUrl: 'menu-card-recepcion.html'
})
export class MenuCardRecepcionComponent {
  items:any;

  constructor(
    public viewControl: ViewController
  ){
    this.items=[
      { id:1, titulo: 'Editar' },
      { id:2, titulo: 'Eliminar' },
      { id:3, titulo: 'Imprimir' }
    ]
  }

  clickItem(item){  
    console.log(item.id);
    switch (item.id){
      case 1:{
        break;
      }
      case 2:{
        break;
      }
      case 3:{
        break;
      }
      default:{
        break;
      }
    }
    this.viewControl.dismiss(item);
  }
}