import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AutenticacionProvider {

  apiUrl: string = "http://186.10.19.170/wsJesusPons/Servicios/Usuario.svc/rest/login";

  constructor(
    public http: HttpClient
  ) {}

  iniciarSesion(usuario:string, password:string){
    if(usuario!=null && password!=null){
      let body =
        {
          "usuario":
            {
              "IdUsuario": usuario,
              "PswUsuario": password
            }
        };
      let headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });
      return this.http.post(this.apiUrl, body, { headers: headers });
    }
  }
}