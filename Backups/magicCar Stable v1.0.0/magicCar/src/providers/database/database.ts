import { RecCabeceraModel } from './../../modelos/rec_cabecera';
import { UsuarioModel } from './../../modelos/usuario';
import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from "@ionic-native/sqlite";

@Injectable()
export class DatabaseProvider {
  public db: SQLiteObject;
  private isOpen: boolean;
  public crea_usuario: string = "CREATE TABLE IF NOT EXISTS usuario "+
    "(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
      "codigo INTEGER, "+ 
      "cod_cargo INTEGER, "+ 
      "cod_comuna INTEGER, "+ 
      "rut TEXT, "+ 
      "nombres TEXT, "+ 
      "ap_paterno TEXT, "+ 
      "ap_materno TEXT, "+ 
      "direccion TEXT, "+ 
      "email TEXT, "+ 
      "fecha_nac TEXT, "+ 
      "telefono TEXT)";
  public crea_rec_cabecera = "CREATE TABLE IF NOT EXISTS rec_cabecera "+
      "(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
      "codigo INTEGER, "+
      "patente TEXT, "+
      "rut TEXT, "+
      "cod_empresa INTEGER, "+
      "fecha TEXT, "+
      "tipo TEXT, "
      "estado INTEGER)";

  constructor(
    public storage: SQLite,
    public platform: Platform
  ){
    if(!this.isOpen){
      this.storage = new SQLite();
      // Crear base de datos
      this.storage.create({
        name: "magic.db",
        location: "default"
      }).then((db: SQLiteObject)=>
        {
          console.log('Base de datos creada');
          this.db = db; 
          db.executeSql(this.crea_usuario,[]);
          // db.executeSql(this.crea_rec_cabecera,[]);
          this.isOpen = true;
        })
        .catch((error)=>{

          console.log(error);
        });
    }
  }

  /////////////////////
  // METODOS USUARIO //
  /////////////////////
  crearUsuario(usuario:UsuarioModel){
    let sql = 'INSERT INTO usuario(codigo, cod_cargo, cod_comuna, rut, nombres, ap_paterno, ap_materno, direccion, email, fecha_nac, telefono) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
    return new Promise((resolve,reject)=>{
      this.db.executeSql(sql, [
        usuario.codigo,
        usuario.cod_cargo,
        usuario.cod_comuna,
        usuario.rut,
        usuario.nombres,
        usuario.ap_paterno,
        usuario.ap_materno,
        usuario.direccion,
        usuario.email,
        usuario.fecha_nac,
        usuario.telefono
      ]).then((data)=>{
        resolve(data);
      }).catch((error)=>{
        reject(error);
      });
    }); 
  
  }

  obtenerUsuario(){
    let sql = "SELECT * FROM usuario";
    return new Promise((resolve,reject)=>{
      this.db.executeSql(sql,null)
      .then((data)=>{
        let usuarioArr = [];
        if(data.rows.length > 0){
          for(let i=0; i<data.rows.length; i++){
            usuarioArr.push({
              codigo: data.rows.item(i).codigo,
              cod_cargo: data.rows.item(i).cod_cargo,
              cod_comuna: data.rows.item(i).codigo,
              rut: data.rows.item(i).rut,
              nombres: data.rows.item(i).nombres,
              ap_paterno: data.rows.item(i).ap_paterno,
              ap_materno: data.rows.item(i).ap_materno,
              direccion: data.rows.item(i).direccion,
              email: data.rows.item(i).email,
              fecha_nac: data.rows.item(i).fecha_nac,
              telefono: data.rows.item(i).telefono
            });
          }
        }
        resolve(usuarioArr);
      })
      .catch((error)=>{
        reject(error);
      });
    });
  }

  borrarUsuario(){
    let sql = "DELETE FROM usuario";
    return new Promise((resolve,reject)=>{
      this.db.executeSql(sql,null)
      .then(()=>{
        console.log('Usuario eliminado');
        resolve();
      })
      .catch((err)=>{
        console.log('Error Eliminar '+err);
        reject(err);
      });
    });
  }

  ////////////////////////////////
  // METODOS CABECERA RECEPCION //
  ////////////////////////////////
  crearRecCabecera(cabecera:RecCabeceraModel){
    let sql = 'INSERT INTO rec_cabecera(codigo, patente, rut, cod_empresa, fecha, tipo, estado) VALUES (?,?,?,?,?,?,?)';
    return new Promise((resolve,reject)=>{
      this.db.executeSql(sql,[
        cabecera.codigo,
        cabecera.patente,
        cabecera.rut,
        cabecera.cod_empresa,
        cabecera.fecha,
        cabecera.tipo,
        cabecera.estado
      ]).then((data)=>{
        resolve(data);
      }).catch((error)=>{
        reject(error);
      });
    });
  }

  editarRecCabecera(cabecera:RecCabeceraModel){

  }

  listarRecCabecera(){
    let sql ='SELECT * FROM rec_cabecera ORDER BY id DESC';
    return new Promise((resolve,reject)=>{
      this.db.executeSql(sql,[])
      .then((data)=>{
        let cebecerasArray = [];
        if(data.rows.length > 0){
          for(var i = 0; i < data.rows.length; i++){
            cebecerasArray.push({
              id:data.rows.item(i).id,
              codigo:data.rows.item(i).codigo,
              patente:data.rows.item(i).patente,
              rut:data.rows.item(i).rut,
              cod_empresa:data.rows.item(i).cod_empresa,
              fecha: data.rows.item(i).fecha,
              tipo: data.rows.item(i).tipo,
              estado:data.rows.item(i).estado
            });
          }
        }
        resolve(cebecerasArray);
      },(error)=>{
        reject(error);
      });
    });
  }

  eliminarRecCabecera(id:number){
    let sql = 'DELETE FROM rec_cabecera WHERE id ='+id;
    return new Promise((resolve,reject)=>{
      this.db.executeSql(sql,[])
      .then((data)=>{
        console.log('Se elimina cabecera id '+id);
        resolve(data);
      })
      .catch((error)=>{
        reject(error);
      });
    });
  }
}