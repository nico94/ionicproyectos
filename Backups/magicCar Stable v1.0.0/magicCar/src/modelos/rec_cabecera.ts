export class RecCabeceraModel{
    constructor(
        public id:number,
        public codigo:number,
        public patente:string,
        public rut:string,
        public cod_empresa:number,
        public fecha:string,
        public tipo:string,
        public estado:number
    ){}
}