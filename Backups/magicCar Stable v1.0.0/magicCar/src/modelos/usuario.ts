export class UsuarioModel{
    constructor(
        public codigo:number,
        public cod_cargo: number,
        public cod_comuna: number,
        public rut: string,
        public nombres: string,
        public ap_paterno: string,
        public ap_materno: string,
        public direccion: string,
        public email: string,
        public fecha_nac: string,
        public telefono: string
    )
    {}
}