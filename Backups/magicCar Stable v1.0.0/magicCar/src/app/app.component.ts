import { DatabaseProvider } from './../providers/database/database';
import { LoginPage } from './../pages/login/login';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//PAGINAS
import { HomePage } from '../pages/home/home';
import { ReportesPage } from './../pages/reportes/reportes';
import { OrdenTrabajoPage } from './../pages/orden-trabajo/orden-trabajo';
import { RecepcionPage } from './../pages/recepcion/recepcion';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any;
  pages: Array<{ titulo: string, component: any, icono: string }>;
  activePage: any;
  sqlite:any;
  usuarioMenu:string;
  perfilMenu:string;
  iconoUsuario: string ="./assets/imgs/man.png";

  constructor(
    public platform: Platform,
    public eventos: Events,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public db: DatabaseProvider
  ) {
    this.initializeApp();
    this.pages = [
      { titulo: 'Inicio', component: HomePage, icono: "home" },
      { titulo: 'Recepción vehículos', component: RecepcionPage, icono: "calendar" },
      { titulo: 'Orden trabajo vehículos', component: OrdenTrabajoPage, icono: "document" },
      { titulo: 'Reportes', component: ReportesPage, icono: "print" }
    ];
    eventos.subscribe('obtenerUsuario',()=>{
      this.obtieneUsuario();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.verificaLogin();
      this.statusBar.backgroundColorByHexString("#264865");
      this.splashScreen.hide(); 
      if (this.platform.is('cordova')) {
        console.log('Es cordova');
      } else {
        console.log('Es navegador');
      }   
    });
  }
  
  verificaLogin(){
    if(localStorage.getItem('login')){
      this.openPage(this.pages[0]);
    }else{
      this.rootPage=LoginPage;
    }
  }

  obtieneUsuario(){
    this.usuarioMenu= null;
    this.perfilMenu= null;
    this.db.obtenerUsuario()
      .then(data => {
        this.usuarioMenu = data[0]['nombres'];
        this.perfilMenu = data[0]['cod_cargo'];
      }).catch(error => {
        console.log('Error en BD al leer usuario');
      });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
    this.activePage = page;
  }

  verificaPagina(page): boolean{
    return page === this.activePage;
  }
}