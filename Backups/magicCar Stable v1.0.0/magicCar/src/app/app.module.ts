import { MenuPrincipalComponent } from './../components/menu-principal/menu-principal';
import { ModRecepcionDocumentacionPage } from './../pages/mod-recepcion-documentacion/mod-recepcion-documentacion';
import { ModRecepcionEstadoPage } from './../pages/mod-recepcion-estado/mod-recepcion-estado';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { SQLite } from "@ionic-native/sqlite";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from './../pages/login/login';
import { HomePage } from '../pages/home/home';
import { RecepcionIngresoPage } from './../pages/recepcion-ingreso/recepcion-ingreso';
import { RecepcionPage } from './../pages/recepcion/recepcion';
import { OrdenTrabajoPage } from '../pages/orden-trabajo/orden-trabajo';
import { ReportesPage } from './../pages/reportes/reportes';
import { AutenticacionProvider } from '../providers/autenticacion/autenticacion';
import { HttpClientModule } from '@angular/common/http';
import { DatabaseProvider } from '../providers/database/database';
import { OrdenTrabajoIngresoPage } from '../pages/orden-trabajo-ingreso/orden-trabajo-ingreso';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    RecepcionPage,
    RecepcionIngresoPage,
    OrdenTrabajoPage,
    OrdenTrabajoIngresoPage,
    ReportesPage,
    ModRecepcionEstadoPage,
    ModRecepcionDocumentacionPage,
    MenuPrincipalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp,
      {
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre' ],
        monthShortNames: ['ene', 'feb', 'mar',  'abr' , 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic' ],
        dayNames: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo' ],
        dayShortNames: ['lun', 'mar', 'mie' , 'jue', 'vie', 'sab', 'dom' ],
      }),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
      MyApp,
      LoginPage,
      HomePage,
      RecepcionPage,
      RecepcionIngresoPage,
      OrdenTrabajoPage,
      OrdenTrabajoIngresoPage,
      ReportesPage,
      ModRecepcionEstadoPage,
      ModRecepcionDocumentacionPage,
      MenuPrincipalComponent
    ],
    providers: [
      StatusBar,
      SplashScreen,
      SQLite,
      {provide: ErrorHandler, useClass: IonicErrorHandler},
      AutenticacionProvider,
      DatabaseProvider
  ]
})
export class AppModule {}