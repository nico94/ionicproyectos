import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-orden-trabajo-ingreso',
  templateUrl: 'orden-trabajo-ingreso.html',
})
export class OrdenTrabajoIngresoPage {
  fechaIngreso: any = new Date().toISOString();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ){}

  ionViewDidLoad() {}

}