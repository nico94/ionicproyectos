import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-mod-recepcion-estado',
  templateUrl: 'mod-recepcion-estado.html',
})
export class ModRecepcionEstadoPage {
  carroceria: any [] = [];
  faroles:any[]=[];
  inventario:any[]=[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController  
  ){
    this.cargarCaroceria();
    this.cargarFaroles();
    this.cargarInventario();
  }

  ionViewDidLoad() {}

  guardar(){
    //console.log(this.carroceria);
    this.viewCtrl.dismiss();
  }

  cargarCaroceria(){
    this.carroceria = [
      { "id": 1, "nombre": "Asientos", "observacion":null, "estado" : false }, 
      { "id": 2, "nombre": "Camada Pick Up", "observacion": null, "estado": false },
      { "id": 3, "nombre": "Asientos", "observacion": null, "estado": false },
      { "id": 4, "nombre": "Capot", "observacion": null, "estado": false },
      { "id": 5, "nombre": "Luneta", "observacion": null, "estado": false },
      { "id": 6, "nombre": "Parabrisas", "observacion": null, "estado": false },
      { "id": 7, "nombre": "Parachoque Del.", "observacion": null, "estado": false },
      { "id": 8, "nombre": "Parachoque Tras.", "observacion": null, "estado": false },
      { "id": 9, "nombre": "Puerta Del. Der.", "observacion": null, "estado": false },
      { "id": 10, "nombre": "Puerta Del. Izq.", "observacion": null, "estado": false },
      { "id": 11, "nombre": "Puerta Tra. Der.", "observacion": null, "estado": false },
      { "id": 12, "nombre": "Puerta Tra. Izq.", "observacion": null, "estado": false },
      { "id": 13, "nombre": "Tapabarros Der.", "observacion": null, "estado": false },
      { "id": 14, "nombre": "Tapabarro Izq.", "observacion": null, "estado": false },
      { "id": 15, "nombre": "Techo", "observacion": null, "estado": false },
      { "id": 16, "nombre": "Portalon Tras.", "observacion": null, "estado": false }
    ];
  }

  cargarFaroles(){
    this.faroles = [
      { "id": 1, "nombre": "Del. Der.", "observacion": null, "estado": false }, 
      { "id": 2, "nombre": "Del. Izq.", "observacion": null, "estado": false }, 
      { "id": 3, "nombre": "Señaliz. Der.", "observacion": null, "estado": false }, 
      { "id": 4, "nombre": "Señaliz. Izq.", "observacion": null, "estado": false }, 
      { "id": 5, "nombre": "Estacion. Der.", "observacion": null, "estado": false }, 
      { "id": 6, "nombre": "Estacion. Izq.", "observacion": null, "estado": false }, 
      { "id": 7, "nombre": "Tras. Der.", "observacion": null, "estado": false }, 
      { "id": 8, "nombre": "Tras. Izq.", "observacion": null, "estado": false }, 
      { "id": 9, "nombre": "Equipo Esp.", "observacion": null, "estado": false }, 
      { "id": 10, "nombre": "Cadena", "observacion": null, "estado": false }, 
      { "id": 11, "nombre": "Tensores", "observacion": null, "estado": false }, 
      { "id": 12, "nombre": "Pala", "observacion": null,  "estado": false }, 
      { "id": 13, "nombre": "Cuñas", "observacion": null, "estado": false }, 
      { "id": 14, "nombre": "Estrobo", "observacion": null, "estado": false }, 
      { "id": 15, "nombre": "Caj. Metal", "observacion": null, "estado": false }, 
      { "id": 16, "nombre": "B. Antivuelco", "observacion": null, "estado": false }
    ];
  }

  cargarInventario(){
    this.inventario = [
      { "id": 1, "nombre": "Botiquín", "observacion": null, "estado": false }, 
      { "id": 2, "nombre": "Ceniceros", "observacion": null, "estado": false }, 
      { "id": 3, "nombre": "Documentos", "observacion": null, "estado": false }, 
      { "id": 4, "nombre": "Encendedor", "observacion": null, "estado": false }, 
      { "id": 5, "nombre": "Espejos", "observacion": null, "estado": false }, 
      { "id": 6, "nombre": "Extintor", "observacion": null, "estado": false }, 
      { "id": 7, "nombre": "Gata", "observacion": null, "estado": false }, 
      { "id": 8, "nombre": "Llave Rueda", "observacion": null, "estado": false }, 
      { "id": 9, "nombre": "Neu. Aux.", "observacion": null, "estado": false }, 
      { "id": 10, "nombre": "Pertiga", "observacion": null, "estado": false }, 
      { "id": 11, "nombre": "Radio", "observacion": null, "estado": false }, 
      { "id": 12, "nombre": "Tapa Com", "observacion": null, "estado": false }, 
      { "id": 13, "nombre": "Triangulos", "observacion": null, "estado": false }, 
      { "id": 14, "nombre": "Varillas", "observacion": null, "estado": false }, 
      { "id": 15, "nombre": "Tag", "observacion": null, "estado": false }, 
      { "id": 16, "nombre": "C. Reflectante", "observacion": null, "estado": false }
    ];
  }
}