import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-mod-recepcion-documentacion',
  templateUrl: 'mod-recepcion-documentacion.html',
})
export class ModRecepcionDocumentacionPage {
  fechaRevisionTec: any = new Date().toISOString();
  fechaRevGases: any = new Date().toISOString();
  fechaPermCirculacion: any = new Date().toISOString();
  fechaSeguroObl: any = new Date().toISOString();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController  
  ){}

  ionViewDidLoad() {}

  guardar(){
    this.viewCtrl.dismiss();
  }
}