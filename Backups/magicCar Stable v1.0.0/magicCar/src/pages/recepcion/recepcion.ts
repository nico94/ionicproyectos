import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import { MenuCardRecepcionComponent } from '../../components/menu-card-recepcion/menu-card-recepcion';
import { RecepcionIngresoPage } from '../recepcion-ingreso/recepcion-ingreso';

@Component({
  selector: 'page-recepcion',
  templateUrl: 'recepcion.html',
})
export class RecepcionPage {
  recepciones:any[]=[];
  fecha:any = new Date().toISOString();
  esBusqueda:boolean=false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController
  ){
   this.cargaRecepciones();
  }

  ionViewDidLoad() {}
  
  mostrarMenu(evento){
    let menu = this.popoverCtrl.create(MenuCardRecepcionComponent);
    menu.present({
      ev: evento
    });
  }

  cargaRecepciones(){
    this.recepciones = [
      {
        id: 1,
        patente: "AA2312",
        rut: "18.682.101-7",
        empresa: "Linderos",
        fec_rec: "11 jun 2018",
        tipo: "D&P",
        estado: 1
      }, {
        id: 2,
        patente: "BB1234",
        rut: "18.682.101-7",
        empresa: "Linderos",
        fec_rec: "9 jun 2018",
        tipo: "Mecánica",
        estado: 2
      }
    ]
  }

  cambiarBusqueda(){  
    this.esBusqueda = !this.esBusqueda;
  }

  ingresarRecepcion(){
    this.navCtrl.push(RecepcionIngresoPage,null);
  }
}