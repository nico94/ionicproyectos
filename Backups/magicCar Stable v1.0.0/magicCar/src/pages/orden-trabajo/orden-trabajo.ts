import { MenuCardOtComponent } from './../../components/menu-card-ot/menu-card-ot';
import { OrdenTrabajoIngresoPage } from './../orden-trabajo-ingreso/orden-trabajo-ingreso';
import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';

@Component({
  selector: 'page-orden-trabajo',
  templateUrl: 'orden-trabajo.html',
})
export class OrdenTrabajoPage {
  ordenes:any[]=[];
  esBusqueda:boolean;
  fecha: any = new Date().toISOString();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController
  ){
    this.cargaOrdenes();
  }

  ionViewDidLoad() {}

  cargaOrdenes(){
    this.ordenes = [
      {
        id: 1,
        patente: "AA2312",
        rut: "18.682.101-7",
        fec_ingreso_ot: "11 jun 2018",
        tipo: "D&P",
        estado: 1
      }
    ]
  }

  mostrarMenu(evento) {
    let menu = this.popoverCtrl.create(MenuCardOtComponent);
    menu.present({
      ev: evento
    });
  }

  cambiarBusqueda(){
    this.esBusqueda = !this.esBusqueda;
  }

  ingresarOT(){
    this.navCtrl.push(OrdenTrabajoIngresoPage,null);
  }
}