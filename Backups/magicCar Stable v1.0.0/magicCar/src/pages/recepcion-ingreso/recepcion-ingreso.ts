import { ModRecepcionEstadoPage } from './../mod-recepcion-estado/mod-recepcion-estado';
import { Component, ViewChild } from '@angular/core';
import { NavController, Navbar, AlertController, ModalController } from 'ionic-angular';
import { ModRecepcionDocumentacionPage } from '../mod-recepcion-documentacion/mod-recepcion-documentacion';

@Component({
  selector: 'page-recepcion-ingreso',
  templateUrl: 'recepcion-ingreso.html',
})
export class RecepcionIngresoPage {
  //Variables globales
  @ViewChild(Navbar) navBar: Navbar;
  motivos:any[]=[];
  prioridades:any[]=[];
  fechaIngreso:any = new Date().toISOString();
  valorBencina:any;

  constructor(
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    public modalCtrl:ModalController
  ){
    this.cargaMotivos();
    this.cargaPrioridades();
  }

  ionViewDidLoad() {
    this.navBar.backButtonClick = (e: UIEvent) => {
      this.mostrarAlerta();
    }  
  }

  cargaMotivos(){
    this.motivos=[
      { id:1, nombre: "D&P", estado: false},
      { id:2, nombre: "Mecánica Preventiva", estado: false },
      { id:3, nombre: "Mecánica Correctiva", estado: false}
    ];
  }

  cargaPrioridades(){
    this.prioridades=[
      { id: 1, nombre: "Normal", estado: false },
      { id: 2, nombre: "Urgente", estado: false },
      { id: 3, nombre: "Mediana", estado: false }
    ];
  }

  registrarEstado(){
    let modalEstado = this.modalCtrl.create(ModRecepcionEstadoPage, null, { enableBackdropDismiss: false });
    modalEstado.present();
  }
  
  registrarDocumentacion(){
    let modalDocumentacion = this.modalCtrl.create(ModRecepcionDocumentacionPage, null, { enableBackdropDismiss: false });
    modalDocumentacion.present();
  }

  mostrarAlerta(){
    let alert = this.alertCtrl.create({
      title: 'Cancelar ingreso',
      message: '¿Está seguro que desea cancelar?',
      buttons: [
        { text: 'No', role: 'cancel' },
        { text: 'Si', 
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }
}