import { UsuarioModel } from './../../modelos/usuario';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AutenticacionProvider {
  apiUrl: string = "http://186.10.19.170/wsJesusPons/Servicios/Usuario.svc/rest/login";

  constructor(
    public http: HttpClient
  ){}

  inciarSesion(usuario:UsuarioModel){
    if(usuario.usuario && usuario.password){
      let body = {
        "usuario":{
          "IdUsuario":usuario.usuario,
          "PswUsuario":usuario.password
        }
      };
      let headers = new HttpHeaders({
        'Content-type':'application/json'
      });
      return this.http.post(this.apiUrl,body,{headers:headers});
    }
  }
}