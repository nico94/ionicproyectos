import { ModNotaIngresoPage } from './../pages/mod-nota-ingreso/mod-nota-ingreso';
import { ReportesPage } from './../pages/reportes/reportes';
import { NotaVentaPage } from './../pages/nota-venta/nota-venta';
import { LoginPage } from './../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AutenticacionProvider } from '../providers/autenticacion/autenticacion';
import { HttpClientModule } from '@angular/common/http';
import { MenuPrincipalComponent } from '../components/menu-principal/menu-principal';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    NotaVentaPage,
    ReportesPage,
    ModNotaIngresoPage,
    MenuPrincipalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp,
      {
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthShortNames: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
        dayNames: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
        dayShortNames: ['lun', 'mar', 'mie', 'jue', 'vie', 'sab', 'dom'],
      }),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
      MyApp,
      HomePage,
      LoginPage,
      NotaVentaPage,
      ReportesPage,
      ModNotaIngresoPage,
      MenuPrincipalComponent,
    ],
    providers: [
      StatusBar,
      SplashScreen,
      {provide: ErrorHandler, useClass: IonicErrorHandler},
      AutenticacionProvider
    ]
  })
export class AppModule {}