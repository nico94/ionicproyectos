import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// Paginas
import { LoginPage } from './../pages/login/login';
import { HomePage } from './../pages/home/home';
import { NotaVentaPage } from './../pages/nota-venta/nota-venta';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  pages: Array<{titulo: string, component: any, icono:string}>;
  rootPage: any;
  paginaActiva:any;
  paginasAdmin: any=[
    { titulo: 'Inicio', component: HomePage, icono:"home" },
    { titulo: 'Nota de venta', component: NotaVentaPage, icono:"document" }
  ];
  usuarioMenu: string;
  perfilMenu: string;
  esDispositivo:boolean;
  iconoUsuario:string = "./assets/imgs/man.png"

  constructor(
    public platform: Platform,
    public eventos:Events,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public menu: MenuController
  ){
    this.initializeApp();
    eventos.subscribe('verifica_ruta',()=>{
      this.verificaUsuarioRuta();
    });
  }
  
  initializeApp() {
    this.platform.ready().then(() => {
      this.verificaUsuarioRuta();
      // this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString("#264865");
      this.splashScreen.hide();
      this.verificaDispositivo();
    });
  }

  verificaDispositivo(){
    if(this.platform.is('cordova')){
      this.esDispositivo = true;
      console.log('DISPOSITIVO');
    }else{
      this.esDispositivo = false;
      console.log('NAVEGADOR');
    }
  }

  verificaUsuarioRuta(){
    let usuario;
    let cargo;
    usuario = parseInt(localStorage.getItem('login'));
    cargo = parseInt(localStorage.getItem('cod_cargo'));
    if(usuario){
      switch(cargo){
        case 11: {
         console.log('Usuario Administrador');
         break;
        }
        default:{
          console.log('CARGO NO DEFINIDO');
          this.pages = this.paginasAdmin;
          break;
        }
      }
      console.log('USUARIO LOGUEADO: MENU HABILIDADO');
      this.openPage(this.pages[0]);
      this.menu.enable(true);
    }else{
      console.log('USUARIO NO LOGUEADO: MENU DESHABILIDADO');
      this.nav.setRoot(LoginPage);
      this.menu.enable(false);
    }
  }

  obtieneUsuario(){

  }

  openPage(page) {
    console.log('ABRIR PAGINA '+JSON.stringify(page));
    this.nav.setRoot(page.component);
    this.paginaActiva = page;
  }

  verificaPagina(page):boolean{
    return page === this.paginaActiva;
  }
}
