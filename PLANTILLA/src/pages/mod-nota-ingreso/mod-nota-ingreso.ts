import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-mod-nota-ingreso',
  templateUrl: 'mod-nota-ingreso.html',
})
export class ModNotaIngresoPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController
  ){}

  ionViewDidLoad() {}

  cerrar(){
    let alert = this.alertCtrl.create({
      title: 'Cancelar Ingreso',
      message: '¿Está seguro que quiere cancelar el ingreso?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancela');
          }
        }, {
          text: 'Si',
          handler: () => {
            this.viewCtrl.dismiss();
          }
        }
      ]
    });
    alert.present();
  }
}