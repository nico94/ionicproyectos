import { ModNotaIngresoPage } from './../mod-nota-ingreso/mod-nota-ingreso';
import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, ModalController } from 'ionic-angular';
import { MenuPrincipalComponent } from '../../components/menu-principal/menu-principal';

@Component({
  selector: 'page-nota-venta',
  templateUrl: 'nota-venta.html',
})
export class NotaVentaPage {
  fecha:any = new Date().toDateString();
  notas:any[]=[];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public modalCtrl:ModalController
  ) {
    this.cargarNotasVentas();
  }

  ionViewDidLoad() {}

  cargarNotasVentas(){
    this.notas=[
      { id: 1, rut: "12.345.678-9", empresa: "Linderos", fecha: "24/04/2018", tipo: "NI", estado: 1 },
      { id: 2, rut: "11.781.191-k", empresa: "Linderos", fecha: "26/04/2018", tipo: "NF", estado: 1 }
    ]
  }

  editarNota(nota){

  }

  eliminarNota(nota){

  }

  ingresarNotaVenta(){
//    this.navCtrl.push(ModNotaIngresoPage,null);
    let modalIngreso = this.modalCtrl.create(ModNotaIngresoPage,null, {enableBackdropDismiss: false});
    modalIngreso.present();
  }
  
  mostrarMenuPrincipal(evento){
    let menu = this.popoverCtrl.create(MenuPrincipalComponent);
    menu.present({
      ev: evento
    });
  }
}