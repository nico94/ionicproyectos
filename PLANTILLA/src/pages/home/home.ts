import { MenuPrincipalComponent } from './../../components/menu-principal/menu-principal';
import { Component } from '@angular/core';
import { NavController, PopoverController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public navCtrl: NavController,
    public popoverCtrl: PopoverController
  ){}

  mostrarMenu(evento){
    let menu = this.popoverCtrl.create(MenuPrincipalComponent);
    menu.present({
      ev:evento
    });
  }
}