/**
 * Automatically generated file. DO NOT MODIFY
 */
package cl.pall.parquecopiapo;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "cl.pall.parquecopiapo";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 10003;
  public static final String VERSION_NAME = "1.0.3";
}
