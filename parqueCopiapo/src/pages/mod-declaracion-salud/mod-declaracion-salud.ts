import { VerificaPlanimetriaModel } from './../../modelos/verificaPlanimetriaRequest';
import { NotaPlanimetriaRequest } from './../../modelos/notaPlanimetriaRequest';
import { NotaSaludRequestModel } from './../../modelos/notaSaludRequest';
import { ModNotaIngresoDetallePage } from './../mod-nota-ingreso-detalle/mod-nota-ingreso-detalle';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, Platform, AlertController, ViewController, ToastController, ModalController, LoadingController, Events } from 'ionic-angular';
import { WebserviceProvider } from '../../providers/webservice/webservice';
import { NotaCabeceraRequest } from '../../modelos/notaCabeceraRequest';
import chileanRut from 'chilean-rut';

@Component({
  selector: 'page-mod-declaracion-salud',
  templateUrl: 'mod-declaracion-salud.html',
})
export class ModDeclaracionSaludPage {
  @ViewChild('imageCanvas') canvas: any;
  @ViewChild('fixedContainer') fixedContainer: any;
  @ViewChild(Content) content: Content;
  cargando: any;
  canvasElement: any;
  saveX: number;
  saveY: number;
  selectedColor = '#000';
  firmaBase64: string = null;
  cliente: any = {};
  empleador: any = {};
  aval: any = {};
  detalle: any = {};
  planimetria: any = {};
  salud: any = {};
  errores: number = 0;
  completos: number = 0;
  //Carga de datos
  comunas: any[] = [];
  ciudades: any[] = [];
  sectores: any[] = [];
  actividades: any[] = [];
  estadosCiviles: any[] = [];
  sexo: any[] = [];
  girosEmpleador: any[] = [];
  parentesco: any[] = [];
  necesidades: any[] = [];
  presionArterial: any[] = [];
  acreditacion: any[] = [];

  documentos: any[] = [];

  nota: any = {
    notaVenta: {
      cliente: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        sector: null,
        actividad: null,
        ciudad: null,
        civil: null,
        correo: null,
        telefono: null,
        celular: null,
        fechaNacimiento: null,
        edad: null,
        sexo: null
      },
      empleador: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        actividad: null,
        ciudad: null,
        acreditacion: [],
        renta: null,
        giro: null,
        telefono: null,
        otrosIngresos: []
      },
      aval: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        sector: null,
        actividad: null,
        ciudad: null,
        civil: null,
        correo: null,
        telefono: null,
        celular: null,
        fechaNacimiento: null,
        edad: null,
        sexo: null,
        renta: null,
        parentesco: null
      },
      detalle: {
        necesidad: null,
        planimetria: {
          descripcion: null,
          letra: null,
          estado: null,
          sector: 0,
          porc_descuento: 0,
          uf: 0,
          fila: null,
          latitud: 0,
          longitud: 0,
          numero: 0,
          valor_base: 0,
          capacidades: 0,
          reducciones: 0,
          pie: 0
        },
        descuento: null,
        pie: 0,
        dia_pago: 0,
        vencimiento: null,
        cuotas: null,
        valor_cuotas: null,
        uf: null,
        comentario: null
      },
      salud: {
        responde: false,
        peso: 0,
        estatura: 0,
        presion: null,
        ejecutiva: null,
        comentario: null,
        firma: null
      }
    }
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public toast: ToastController,
    public modalCtrl: ModalController,
    public loadCtrl: LoadingController,
    public webservice: WebserviceProvider,
    public eventos: Events
  ) {
    let ejecutivo = localStorage.getItem('ejecutivo');
    this.nota.notaVenta.salud.ejecutiva = ejecutivo;
    this.obtener();
  }

  obtener() {
    //Asignar datos WS
    this.ciudades = this.navParams.get('ciudades');
    this.sectores = this.navParams.get('sectores');
    this.actividades = this.navParams.get('actividades');
    this.estadosCiviles = this.navParams.get('estadosCiviles');
    this.sexo = this.navParams.get('sexo');
    this.girosEmpleador = this.navParams.get('girosEmpleador');
    this.parentesco = this.navParams.get('parentesco');
    this.necesidades = this.navParams.get('necesidades');
    this.presionArterial = this.navParams.get('presionArterial');
    this.acreditacion = this.navParams.get('acreditacion');
    let data = this.navParams.get('nota');
    if (data !== undefined) {
      if (data.notaVenta.cliente !== undefined) {
        this.cliente = data.notaVenta.cliente;
      }
      if (data.notaVenta.empleador !== undefined) {
        this.empleador = data.notaVenta.empleador;
      }
      if (data.notaVenta.aval !== undefined) {
        this.aval = data.notaVenta.aval;
      }
      if (data.notaVenta.detalle !== undefined) {
        this.detalle = data.notaVenta.detalle;
      }
      if (data.notaVenta.detalle.planimetria !== undefined) {
        this.planimetria = data.notaVenta.detalle.planimetria;
      }
      if (data.notaVenta.salud !== undefined) {
        this.salud = data.notaVenta.salud;
      }
    }
  }

  ionViewLoaded() { }

  ionViewDidLoad() {
    this.canvasElement = this.canvas.nativeElement;
    this.canvasElement.width = 450;
    this.canvasElement.height = 300;
  }

  guardarFirma() {
    var dataUrl = this.canvasElement.toDataURL();
    let ctx = this.canvasElement.getContext('2d');
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    var data = dataUrl.split(',')[1];
    if (data) {
      this.firmaBase64 = data;
    }
  }

  limpiaFirma() {
    let ctx = this.canvasElement.getContext('2d');
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    this.firmaBase64 = '';
  }

  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  terminar(terminar: boolean) {
    this.guardarFirma();
    this.nota.notaVenta.cliente = this.cliente;
    this.nota.notaVenta.empleador = this.empleador;
    this.nota.notaVenta.aval = this.aval;
    this.nota.notaVenta.detalle = this.detalle;
    this.nota.notaVenta.detalle.planimetria = this.planimetria;
    this.nota.notaVenta.salud = this.salud;
    this.nota.notaVenta.salud.firma = this.firmaBase64;
    if (this.nota.notaVenta.salud.responde &&
      // this.nota.notaVenta.salud.peso && this.nota.notaVenta.salud.estatura && this.nota.notaVenta.salud.presion &&
      this.nota.notaVenta.salud.ejecutiva && this.nota.notaVenta.salud.firma
    ) {
      this.mostrarCarga();
      let cod_vendedor = parseInt(localStorage.getItem('login'));
      let cabeceraRequest = new NotaCabeceraRequest(
        chileanRut.unformat(this.nota.notaVenta.cliente.rut),
        chileanRut.unformat(this.nota.notaVenta.empleador.rut),
        chileanRut.unformat(this.nota.notaVenta.aval.rut),
        chileanRut.unformat(this.nota.notaVenta.aval.rut),
        cod_vendedor,
        this.nota.notaVenta.detalle.planimetria.pie,
        this.nota.notaVenta.detalle.necesidad,
        this.nota.notaVenta.detalle.planimetria.valor_base,
        this.nota.notaVenta.detalle.planimetria.porc_descuento,
        parseInt(this.nota.notaVenta.detalle.dia_pago),
        parseInt(this.nota.notaVenta.detalle.cuotas),
        parseInt(this.nota.notaVenta.detalle.valor_cuotas),
        parseInt(this.nota.notaVenta.detalle.planimetria.uf),
        this.nota.notaVenta.detalle.comentario,
        this.nota.notaVenta.detalle.vencimiento,
        this.nota.notaVenta.salud.firma
      );
      let planimetriaRequest = new NotaPlanimetriaRequest(
        this.nota.notaVenta.detalle.planimetria.numero,
        this.nota.notaVenta.detalle.planimetria.sector,
        this.nota.notaVenta.detalle.planimetria.letra,
        this.nota.notaVenta.detalle.planimetria.capacidades,
        this.nota.notaVenta.detalle.planimetria.capacidades,
        this.nota.notaVenta.detalle.planimetria.reducciones
      );
      let saludRequest = new NotaSaludRequestModel(
        this.nota.notaVenta.salud.responde,
        this.nota.notaVenta.salud.peso,
        this.nota.notaVenta.salud.estatura,
        this.nota.notaVenta.salud.comentario,
        this.nota.notaVenta.salud.presion
      );
      if (this.nota.notaVenta.empleador.acreditacion) {
        this.documentos.push(this.nota.notaVenta.empleador.acreditacion);
      }
      if (this.nota.notaVenta.empleador.otrosIngresos) {
        this.documentos.push(this.nota.notaVenta.empleador.otrosIngresos);
      }

      //this.webservice.guardarNotaVenta(cabeceraRequest, planimetriaRequest, saludRequest, this.documentos[0]).subscribe(
      this.webservice.guardarNotaVenta(cabeceraRequest, planimetriaRequest, saludRequest).subscribe(
        (data) => {
          console.log('RESPUESTA WS ' + JSON.stringify(data));
          let resultado = data['GuardarNotaVentaResult']['resultado'];
          let mensaje = data['GuardarNotaVentaResult']['MensajeError'];
          let codigo_contrato = data['GuardarNotaVentaResult']['id_codigo_contrato'];
          if (resultado) {
            this.mostarToast('Nota de venta Nº ' + codigo_contrato + ' ingresada correctamente');
            //this.guardarDocumentosAcreditacion(num_notavta);
            if (codigo_contrato > 0) {
              let validaRequest = new VerificaPlanimetriaModel(
                codigo_contrato,
                this.nota.notaVenta.detalle.planimetria.sector,
                this.nota.notaVenta.detalle.planimetria.numero,
                this.nota.notaVenta.detalle.planimetria.letra
              );
              this.webservice.validarPlanimetriaAsiganda(validaRequest).subscribe(
                (data)=>{
                  console.log('RESPUESTA VALIDA PLANMETRIA '+JSON.stringify(data));
                  let resultado = data['ValidarPlanimetriaResult']['existe'];
                  if (resultado) {
                    if (this.documentos[0].length > 0) {
                      this.guardarDocumentosAcreditacion(codigo_contrato)
                        .then((result: any) => {
                          if (result) {
                            this.cargando.dismiss();
                            this.eventos.publish('recarga_notas');
                            this.viewCtrl.dismiss();
                          } else {
                            //console.log('ERROR AL GUARDAR LISTA DOCUMENTOS ' + JSON.stringify(error));
                            this.cargando.dismiss();
                            this.eventos.publish('recarga_notas');
                            this.viewCtrl.dismiss();
                            this.mostarToast('Error al guardar documentos en el servidor');
                          }
                        });
                    } else {
                      console.log('Sin docuemntos');
                      this.cargando.dismiss();
                      this.eventos.publish('recarga_notas');
                      this.viewCtrl.dismiss();
                    }
                  }else{
                    this.mostarToast('Error al guardar la planimetría seleccionada');
                    this.cargando.dismiss();
                    this.eventos.publish('recarga_notas');
                    this.viewCtrl.dismiss();
                  }
                },(error)=>{
                  console.log('Error verificando la planimetria '+JSON.stringify(error));
                });
            } else {
              this.mostarToast('Error en código de nota de venta');
              this.cargando.dismiss();
            }
            // this.cargando.dismiss();
            // this.eventos.publish('recarga_notas');
            // this.viewCtrl.dismiss();
          } else {
            this.cargando.dismiss();
            console.log('Mensaje Server: ' + JSON.stringify(mensaje));
            this.mostarToast('Error al crear nota de venta, por favor reintente...');
          }
        },
        (error) => {
          console.log('ERROR WS ' + JSON.stringify(error));
          this.cargando.dismiss();
          this.mostarToast('Error al ingresar nota de venta, error en el servidor');
        }
      );
    } else {
      this.mostarToast('Faltan parámetros para declaración salud');
    }
  }

  guardarDocumentosAcreditacion(id_contrato: number) {
    // let vta = {};
    // this.documentos[0].forEach((doc,index) => {
    //   vta = {"num_nvta_contrato":nta_vta};
    //   this.documentos[doc].push(vta);
    // });
    // for(var doc in this.documentos[0]){
    //   vta = {"num_nvta_contrato":nta_vta};    
    // }
    //this.documentos[0].push({ "num_nvta_contrato": nta_vta });
    return new Promise((resolve) => {
      //let total = this.documentos[0].length();
      for (let doc of this.documentos[0]) {
        this.webservice.guardarDocumento(id_contrato, doc).subscribe(
          (data) => {
            console.log('GUARDAR DOCUMENTO ' + JSON.stringify(data));
            let resultado = data['GuardarDocAcreditacionResult']['resultado'];
            if (resultado) {
              this.completos = this.completos + 1;
              resolve(true);
            } else {
              this.errores = this.errores + 1;
              resolve(false);
            }
          },
          (error) => {
            this.errores = this.errores + 1;
            resolve(false);
          });
      }
    });
  }

  volver() {
    this.nota.notaVenta.cliente = this.cliente;
    this.nota.notaVenta.empleador = this.empleador;
    this.nota.notaVenta.aval = this.aval;
    this.nota.notaVenta.detalle = this.detalle;
    this.nota.notaVenta.detalle.planimetria = this.planimetria;
    this.nota.notaVenta.salud = this.salud;
    let data = {
      nota: this.nota,
      ciudades: this.ciudades,
      sectores: this.sectores,
      actividades: this.actividades,
      estadosCiviles: this.estadosCiviles,
      sexo: this.sexo,
      girosEmpleador: this.girosEmpleador,
      parentesco: this.parentesco,
      necesidades: this.necesidades,
      presionArterial: this.presionArterial,
      acreditacion: this.acreditacion
    };
    let opcionesModal = { enableBackdropDismiss: false };
    let modalDetalle = this.modalCtrl.create(ModNotaIngresoDetallePage, data, opcionesModal);
    modalDetalle.present();
    this.viewCtrl.dismiss();
  }

  mostrarCarga() {
    this.cargando = this.loadCtrl.create({
      content: "Ingresando nota de venta..."
    });
    this.cargando.present();
  }

  mostarToast(mensaje: string) {
    if (mensaje) {
      let toast = this.toast.create({
        message: mensaje,
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
  }

  iniciarDibujo(ev) {
    var canvasPosition = this.canvasElement.getBoundingClientRect();
    this.saveX = ev.touches[0].pageX - canvasPosition.x;
    this.saveY = ev.touches[0].pageY - canvasPosition.y;
  }

  dibujado(ev) {
    var canvasPosition = this.canvasElement.getBoundingClientRect();
    let ctx = this.canvasElement.getContext('2d');
    let currentX = ev.touches[0].pageX - canvasPosition.x;
    let currentY = ev.touches[0].pageY - canvasPosition.y;
    ctx.lineJoin = 'round';
    ctx.strokeStyle = this.selectedColor;
    ctx.lineWidth = 3;
    ctx.beginPath();
    ctx.moveTo(this.saveX, this.saveY);
    ctx.lineTo(currentX, currentY);
    ctx.closePath();
    ctx.stroke();
    this.saveX = currentX;
    this.saveY = currentY;
  }
}