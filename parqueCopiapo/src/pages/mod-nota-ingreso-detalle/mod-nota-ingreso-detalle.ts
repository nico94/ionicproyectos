import { WebserviceProvider } from './../../providers/webservice/webservice';
import { ModIngresoAvalPage } from './../mod-ingreso-aval/mod-ingreso-aval';
import { ModDeclaracionSaludPage } from './../mod-declaracion-salud/mod-declaracion-salud';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, ModalController, ToastController } from 'ionic-angular';
import { ModPlanimeriaPage } from '../mod-planimeria/mod-planimeria';

@Component({
  selector: 'page-mod-nota-ingreso-detalle',
  templateUrl: 'mod-nota-ingreso-detalle.html',
})
export class ModNotaIngresoDetallePage {
  cliente: any = {};
  empleador: any = {};
  aval: any = {};
  planimetria: any = {};
  detalle: any = {};
  salud: any = {};

  necesidad: boolean = null;
  validaIngresoNecesidad: boolean = false;

  //Carga de datos
  comunas: any[] = [];
  ciudades: any[] = [];
  sectores: any[] = [];
  actividades: any[] = [];
  estadosCiviles: any[] = [];
  sexo: any[] = [];
  girosEmpleador: any[] = [];
  parentesco: any[] = [];
  necesidades: any[] = [];
  presionArterial: any[] = [];
  acreditacion: any[] = [];

  nota: any = {
    notaVenta: {
      cliente: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        sector: null,
        actividad: null,
        ciudad: null,
        civil: null,
        correo: null,
        telefono: null,
        celular: null,
        fechaNacimiento: null,
        edad: null,
        sexo: null
      },
      empleador: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        actividad: null,
        ciudad: null,
        acreditacion: null,
        renta: null,
        giro: null,
        telefono: null,
        otrosIngresos: null
      },
      aval: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        sector: null,
        actividad: null,
        ciudad: null,
        civil: null,
        correo: null,
        telefono: null,
        celular: null,
        fechaNacimiento: null,
        edad: null,
        sexo: null,
        renta: null,
        parentesco: null
      },
      detalle: {
        necesidad: null,
        planimetria: {
          descripcion: null,
          letra: null,
          estado: null,
          sector: 0,
          porc_descuento: 0,
          uf: 0,
          fila: null,
          latitud: 0,
          longitud: 0,
          numero: 0,
          valor_base: 0,
          capacidades: 0,
          reducciones: 0,
          pie: 0
        },
        descuento: null,
        pie: 0,
        dia_pago: 0,
        vencimiento: null,
        cuotas: null,
        valor_cuotas: null,
        uf: null,
        comentario: null
      },
      salud: {
        responde: false,
        peso: 0,
        estatura: 0,
        presion: null,
        ejecutiva: null,
        comentario: null,
        firma: null
      }
    }
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public toast: ToastController,
    public webService: WebserviceProvider
  ) {
    this.obtener();
    this.seleccionaNecesidad(this.nota.notaVenta.detalle.necesidad, true, null);
  }

  obtener() {
    //Asignar datos WS
    this.ciudades = this.navParams.get('ciudades');
    this.sectores = this.navParams.get('sectores');
    this.actividades = this.navParams.get('actividades');
    this.estadosCiviles = this.navParams.get('estadosCiviles');
    this.sexo = this.navParams.get('sexo');
    this.girosEmpleador = this.navParams.get('girosEmpleador');
    this.parentesco = this.navParams.get('parentesco');
    this.necesidades = this.navParams.get('necesidades');
    this.presionArterial = this.navParams.get('presionArterial');
    this.acreditacion = this.navParams.get('acreditacion');
    let data = this.navParams.get('nota');
    if (data !== undefined) {
      if (data.notaVenta.cliente !== undefined) {
        this.cliente = data.notaVenta.cliente;
      }
      if (data.notaVenta.empleador !== undefined) {
        this.empleador = data.notaVenta.empleador;
      }
      if (data.notaVenta.aval !== undefined) {
        this.aval = data.notaVenta.aval;
      }
      if (data.notaVenta.detalle !== undefined) {
        this.detalle = data.notaVenta.detalle;
      }
      if (data.notaVenta.detalle.planimetria !== undefined) {
        this.planimetria = data.notaVenta.detalle.planimetria;
      }
      if (data.notaVenta.salud !== undefined) {
        this.salud = data.notaVenta.salud;
      }
    }
  }

  ionViewDidLoad() { }

  seleccionaNecesidad(cod_necesidad: string, change:boolean, bit:boolean) {
    console.log('cod: '+cod_necesidad + ' bit: '+bit );
    this.detalle.planimetria.descripcion = "";
    this.detalle.planimetria.letra = "";
    this.detalle.planimetria.estado = "";
    this.detalle.planimetria.sector = "";
    this.detalle.planimetria.porc_descuento = "";
    this.detalle.planimetria.uf = "";
    this.detalle.planimetria.fila = "";
    this.detalle.planimetria.latitud = "";
    this.detalle.planimetria.longitud = "";
    this.detalle.planimetria.numero = "";
    this.detalle.planimetria.valor_base = "";
    this.detalle.planimetria.capacidades = "";
    this.detalle.planimetria.reducciones = "";
    this.detalle.planimetria.pie = "";
    if(change){
      this.validaIngresoNecesidad = true;
    }else{

    }

    if (cod_necesidad !== null) {
      if(bit){
        this.necesidad = true;
      }else{
        this.necesidad = false;
      }
    }
    console.log('BIT NECESIDAD '+bit);
  }

  continuar(continuar: boolean) {
    this.nota.notaVenta.cliente = this.cliente;
    this.nota.notaVenta.empleador = this.empleador;
    this.nota.notaVenta.aval = this.aval;
    this.nota.notaVenta.detalle = this.detalle;
    this.nota.notaVenta.detalle.planimetria = this.planimetria;
    this.nota.notaVenta.salud = this.salud;
    let data = {
      nota: this.nota,
      ciudades: this.ciudades,
      sectores: this.sectores,
      actividades: this.actividades,
      estadosCiviles: this.estadosCiviles,
      sexo: this.sexo,
      girosEmpleador: this.girosEmpleador,
      parentesco: this.parentesco,
      necesidades: this.necesidades,
      presionArterial: this.presionArterial,
      acreditacion: this.acreditacion
    };
    let opcionesModal = { enableBackdropDismiss: false };
    let modalPresent = null
    // console.log('NOTA '+JSON.stringify(this.nota));
    if (continuar) {
      if (this.nota.notaVenta.detalle.necesidad && this.nota.notaVenta.detalle.planimetria.descripcion &&
        this.nota.notaVenta.detalle.planimetria.letra && this.nota.notaVenta.detalle.dia_pago &&
        this.nota.notaVenta.detalle.vencimiento && this.nota.notaVenta.detalle.cuotas &&
        this.nota.notaVenta.detalle.valor_cuotas
      ) {
        let modalSalud = this.modalCtrl.create(ModDeclaracionSaludPage, data, opcionesModal);
        modalPresent = modalSalud;
      } else {
        this.mostarToast('Faltan parámetros para detalle');
      }
    } else {
      let modalAval = this.modalCtrl.create(ModIngresoAvalPage, data, opcionesModal);
      modalPresent = modalAval;
    }
    if (modalPresent) {
      modalPresent.present();
      this.viewCtrl.dismiss();
    }
  }

  abrirMapa() {
    if (this.validaIngresoNecesidad) {
      let dataMod = null;
      // console.log('NEC: '+this.necesidad);
      dataMod = { "necesidad": this.necesidad };
      let modalPlanimetria = this.modalCtrl.create(ModPlanimeriaPage, dataMod, { enableBackdropDismiss: false });
      modalPlanimetria.onDidDismiss(data => {
        if (data) {
          this.detalle.planimetria.descripcion = data.descripcion;
          this.detalle.planimetria.letra = data.letra;
          this.detalle.planimetria.estado = data.estado;
          this.detalle.planimetria.sector = data.sector;
          this.detalle.planimetria.porc_descuento = data.porc_descuento;
          this.detalle.planimetria.uf = data.uf;
          this.detalle.planimetria.fila = data.fila;
          this.detalle.planimetria.latitud = data.latitud;
          this.detalle.planimetria.longitud = data.longitud;
          this.detalle.planimetria.numero = data.numero;
          this.detalle.planimetria.valor_base = data.valor_base;
          this.detalle.planimetria.capacidades = data.capacidades;
          this.detalle.planimetria.reducciones = data.reducciones;
          this.detalle.planimetria.pie = data.pie;
        } else {
          this.detalle.planimetria.descripcion = "";
          this.detalle.planimetria.letra = "";
          this.detalle.planimetria.estado = "";
          this.detalle.planimetria.sector = "";
          this.detalle.planimetria.porc_descuento = "";
          this.detalle.planimetria.uf = "";
          this.detalle.planimetria.fila = "";
          this.detalle.planimetria.latitud = "";
          this.detalle.planimetria.longitud = "";
          this.detalle.planimetria.numero = "";
          this.detalle.planimetria.valor_base = "";
          this.detalle.planimetria.capacidades = "";
          this.detalle.planimetria.reducciones = "";
          this.detalle.planimetria.pie = "";
        }
      });
      modalPlanimetria.present();
    } else {
      this.mostarToast('Debe seleccionar una necesidad');
    }
  }

  mostarToast(mensaje: string) {
    if (mensaje) {
      let toast = this.toast.create({
        message: mensaje,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
  }
}