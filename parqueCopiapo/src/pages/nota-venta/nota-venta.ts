import { NotaRequestModel } from './../../modelos/notaRequest';
import { WebserviceProvider } from './../../providers/webservice/webservice';
import { ModNotaIngresoPage } from './../mod-nota-ingreso/mod-nota-ingreso';
import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, ModalController, LoadingController, ToastController, Events } from 'ionic-angular';
import { MenuPrincipalComponent } from '../../components/menu-principal/menu-principal';
import chileanRut from 'chilean-rut';

@Component({
  selector: 'page-nota-venta',
  templateUrl: 'nota-venta.html',
})
export class NotaVentaPage {
  fecha: any = new Date().toISOString();
  notas: any[] = [];
  cargando: any;
  cargado: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public modalCtrl: ModalController,
    public webservice: WebserviceProvider,
    public loadCtrl: LoadingController,
    public toast: ToastController,
    public eventos: Events
  ) {
    this.cambiaFecha(this.fecha);
    eventos.subscribe('recarga_notas', () => {
      this.cambiaFecha(new Date().toISOString());
    });
  }

  ionViewDidLoad() { }

  cargarNotasVentas(nota: NotaRequestModel) {
    if (nota) {
      this.mostrarCarga();
      setTimeout(() => {
        this.webservice.obtenerListaNotaVenta(nota).subscribe(
          data => {
            console.log('NOTAS: ' + JSON.stringify(data));
            var resultado = data['ListadoNotaVentaCementerioResult']['lista'];
            if (resultado !== null) {
              resultado.forEach((item, index) => {
                let fecha = null;
                fecha = resultado[index]['dat_fecha_notavta'];
                let fechaFormateada = null;
                if (fecha) {
                  fechaFormateada = new Date(parseInt(fecha.replace(/(^.*\()|([+-].*$)/g, ''))).toISOString();
                } else {
                  fechaFormateada = "";
                }
                let rutCliente = resultado[index]['cod_cliente_cliente'];
                let rutFormateado = null;
                if (rutCliente != null) {
                  if (rutCliente.length == 8 || rutCliente.length == 9) {
                    let dv = rutCliente.substring(8);
                    rutFormateado = chileanRut.format(rutCliente, dv);
                  }
                }
                if (fechaFormateada) {
                  resultado[index]['dat_fecha_notavta'] = fechaFormateada;
                } else {
                  resultado[index]['dat_fecha_notavta'] = "";
                }
                resultado[index]['cod_cliente_cliente'] = rutFormateado;
                this.notas = resultado;
              });
              this.cargado = true;
            } else {
              this.cargado = false;
            }
            this.cargando.dismiss();
          },
          error => {
            this.cargado = false;
            this.cargando.dismiss();
            this.mostarToast('Error en la conexión con servidor');
            console.log('Error en consulta a WS ' + JSON.stringify(error));
          });
      }, 5000);
    }
  }

  cambiaFecha(fecha) {
    if (fecha) {
      var mes = fecha.split("-")[1];
      var ano = fecha.split("-")[0];
      var usuario = parseInt(localStorage.getItem('login'));
      if (mes < 99 && ano < 9999 && usuario > 0) {
        let notaRequest = new NotaRequestModel(usuario, mes, ano);
        this.cargarNotasVentas(notaRequest);
      }
    }
  }

  mostarToast(mensaje: string) {
    if (mensaje) {
      let toast = this.toast.create({
        message: mensaje,
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
  }

  editarNota(nota) { }

  eliminarNota(nota) { }

  mostrarCarga() {
    this.cargando = this.loadCtrl.create({
      content: "Cargando notas de venta..."
    });
    this.cargando.present();
  }

  ingresarNotaVenta() {
    let modalIngreso = this.modalCtrl.create(ModNotaIngresoPage, null, { enableBackdropDismiss: false });
    modalIngreso.present();
  }

  mostrarMenuPrincipal(evento) {
    let menu = this.popoverCtrl.create(MenuPrincipalComponent);
    menu.present({
      ev: evento
    });
  }
}