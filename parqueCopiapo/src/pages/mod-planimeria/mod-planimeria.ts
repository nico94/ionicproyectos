import { PlanimetriaRequestModel } from './../../modelos/planimetriaRequest';
import { WebserviceProvider } from './../../providers/webservice/webservice';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-mod-planimeria',
  templateUrl: 'mod-planimeria.html',
})
export class ModPlanimeriaPage {
  mapa: string = "./assets/imgs/mapa.png";
  planMan: boolean = false;
  planimetria = {
    descripcion: null,
    letra: null,
    estado: null,
    sector: 0,
    porc_descuento: 0,
    uf: 0,
    fila: null,
    latitud: 0,
    longitud: 0,
    numero: 0,
    valor_base: 0,
    capacidades: 0,
    reducciones: 0,
    pie: 0
  };

  listaPlanimetria: any[] = [];
  necesidad: boolean = null;
  codigo: number = 0;

  //Estilo del mapa
  estiloMapa = [{
    elementType: "labels.icon",
    stylers: [{
      visibility: "off"
    }]
  }];
  lat: number;
  lng: number;
  zoom: number = 18;
  markers: any;
  arregloCoords: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public toast: ToastController,
    public webService: WebserviceProvider,
    public alertCtrl: AlertController
  ) {
    this.obtener();
  }

  ionViewDidLoad() {
    this.lat = -27.377015;
    this.lng = -70.334138;
  }

  obtener() {
    this.necesidad = this.navParams.get('necesidad');
    this.listarPlanimetria(this.navParams.get('necesidad'));
  }

  planimetriaManual(check: boolean){

  }

  verificaManual(){
    return !this.planMan;
  }

  listarPlanimetria(necesidad: boolean) {
    let request = new PlanimetriaRequestModel(necesidad, true, "", 0, 0);
    this.webService.obtenerGeoPlanmetria(request)
      .subscribe(data => {
        // console.log('DATA ' + JSON.stringify(data));
        let lista = data['Listadosnap_geoplanimetriaResult']['lista'];
        this.listaPlanimetria = lista;
      }, error => {
        console.log('Error al cargar ' + JSON.stringify(error));
        this.mostarToast('Error al listar planimetría');
      });
  }

  seleccionaPlanimetria(letra: string, nro_fila: number, id_sector: number) {
    let request = new PlanimetriaRequestModel(this.necesidad, false, letra, nro_fila, id_sector);
    this.webService.obtenerGeoPlanmetria(request)
      .subscribe(data => {
        // console.log(JSON.stringify(data));
        let lista = data['Listadosnap_geoplanimetriaResult']['lista'];
        this.planimetria.descripcion = lista[0]['DescPlanimetria'];
        this.planimetria.letra = lista[0]['Letra'];
        // this.planimetria.estado = lista[0]['Estado'];
        this.planimetria.estado = lista[0]['DescEstado'];
        this.planimetria.sector = lista[0]['id_codigo_sector'];
        this.planimetria.porc_descuento = lista[0]['PorcDesc'];
        this.planimetria.uf = lista[0]['UF_Valor_'];
        this.planimetria.fila = lista[0]['Numero_fila'];
        this.planimetria.latitud = lista[0]['latitud_sector'];
        this.planimetria.longitud = lista[0]['longitud_sector'];
        this.planimetria.numero = lista[0]['Numero_fila'];
        this.planimetria.valor_base = lista[0]['ValorBase'];
        this.planimetria.capacidades = lista[0]['num_capacidades_snapsector'];
        this.planimetria.reducciones = lista[0]['num_reducciones_snapsector'];
        this.planimetria.pie = lista[0]['Pie'];
      }, error => {
        this.mostarToast('Error al cargar planimetria seleccionada');
        console.log('Error al cargar ' + JSON.stringify(error));
      });
  }

  cerrar() {
    let alert = this.alertCtrl.create({
      title: 'Cancelar Ingreso',
      message: '¿Cancelar ingreso de planimetría?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => { }
        }, {
          text: 'Si',
          handler: () => {
            this.viewCtrl.dismiss(this.planimetria);
          }
        }
      ]
    });
    alert.present();
  }

  guardar() {
    if (this.planimetria.sector && this.planimetria.fila &&
      this.planimetria.numero && this.planimetria.valor_base
    ) {
      this.viewCtrl.dismiss(this.planimetria);
    } else {
      this.mostarToast('Faltan parámetros de planimetría');
    }
  }

  mostarToast(mensaje: string) {
    if (mensaje) {
      let toast = this.toast.create({
        message: mensaje,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
  }
}