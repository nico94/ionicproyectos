import { WebserviceProvider } from './../../providers/webservice/webservice';
import { ModNotaIngresoEmpleadorPage } from './../mod-nota-ingreso-empleador/mod-nota-ingreso-empleador';
import { ModNotaIngresoDetallePage } from './../mod-nota-ingreso-detalle/mod-nota-ingreso-detalle';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, ModalController, ToastController } from 'ionic-angular';
import { ComunaRequestModel } from '../../modelos/comunaRequest';
import { AvalRequestModel } from '../../modelos/avalRequest';
import { ClienteRequestModel } from '../../modelos/clienteRequest';
import chileanRut from 'chilean-rut';

@Component({
  selector: 'page-mod-ingreso-aval',
  templateUrl: 'mod-ingreso-aval.html',
})
export class ModIngresoAvalPage {
  fechaNacimiento: any = new Date().toISOString();
  cliente: any = {};
  empleador: any = {};
  aval: any = {};
  detalle: any = {};
  planimetria: any = {};
  salud: any = {};
  avalEncontrado: boolean = false;
  botonNombre: string = "Continuar";
  clienteComoAval: boolean = false;

  //Carga de datos
  comunas: any[] = [];
  ciudades: any[] = [];
  sectores: any[] = [];
  actividades: any[] = [];
  estadosCiviles: any[] = [];
  sexo: any[] = [];
  girosEmpleador: any[] = [];
  parentesco: any[] = [];
  necesidades: any[] = [];
  presionArterial: any[] = [];
  acreditacion: any[] = [];

  nota: any = {
    notaVenta: {
      cliente: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        sector: null,
        actividad: null,
        ciudad: null,
        civil: null,
        correo: null,
        telefono: null,
        celular: null,
        fechaNacimiento: null,
        edad: null,
        sexo: null
      },
      empleador: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        actividad: null,
        ciudad: null,
        acreditacion: [],
        renta: null,
        giro: null,
        telefono: null,
        otrosIngresos: []
      },
      aval: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        sector: null,
        actividad: null,
        ciudad: null,
        civil: null,
        correo: null,
        telefono: null,
        celular: null,
        fechaNacimiento: null,
        edad: null,
        sexo: null,
        renta: null,
        parentesco: null
      },
      detalle: {
        necesidad: null,
        planimetria: {
          descripcion: null,
          letra: null,
          estado: null,
          sector: 0,
          porc_descuento: 0,
          uf: 0,
          fila: null,
          latitud: 0,
          longitud: 0,
          numero: 0,
          valor_base: 0,
          capacidades: 0,
          reducciones: 0,
          pie: 0
        },
        descuento: null,
        pie: 0,
        dia_pago: 0,
        vencimiento: null,
        cuotas: null,
        valor_cuotas: null,
        uf: null,
        comentario: null
      },
      salud: {
        responde: false,
        peso: 0,
        estatura: 0,
        presion: null,
        ejecutiva: null,
        comentario: null,
        firma: null
      }
    }
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public toast: ToastController,
    private webService: WebserviceProvider
  ) {
    this.obtener();
  }

  ionViewDidLoad() { }

  clienteAval(check: boolean) {
    if(check){
      this.aval.rut = this.cliente.rut;
      this.buscarDatosAval(chileanRut.unformat(this.cliente.rut));
      this.aval.parentesco = 2;
    }else{
      this.aval.rut = "";
      this.aval.nombre = "";
      this.aval.direccion = "";
      this.aval.comuna = "";
      this.aval.sector = "";
      this.aval.actividad = "";
      this.aval.ciudad = "";
      this.aval.ciudad = "";
      this.aval.civil = "";
      this.aval.correo = "";
      this.aval.telefono = "";
      this.aval.celular = "";
      this.aval.fechaNacimiento = "";
      this.aval.edad = "";
      this.aval.renta = "";
      this.aval.parentesco = "";
    }
  }

  verificaClienteAval() {
    return !this.clienteComoAval;
  }

  obtener() {
    //Asignar datos WS
    this.ciudades = this.navParams.get('ciudades');
    this.sectores = this.navParams.get('sectores');
    this.actividades = this.navParams.get('actividades');
    this.estadosCiviles = this.navParams.get('estadosCiviles');
    this.sexo = this.navParams.get('sexo');
    this.girosEmpleador = this.navParams.get('girosEmpleador');
    this.parentesco = this.navParams.get('parentesco');
    //console.log('PARENTESCOS '+ JSON.stringify(this.parentesco));
    
    this.necesidades = this.navParams.get('necesidades');
    this.presionArterial = this.navParams.get('presionArterial');
    this.acreditacion = this.navParams.get('acreditacion');
    let data = this.navParams.get('nota');
    if (data !== undefined) {
      if (data.notaVenta.cliente !== undefined) {
        this.cliente = data.notaVenta.cliente;
      }
      if (data.notaVenta.empleador !== undefined) {
        this.empleador = data.notaVenta.empleador;
      }
      if (data.notaVenta.aval !== undefined) {
        this.aval = data.notaVenta.aval;
        if (data.notaVenta.aval.rut){
          this.buscarDatosAval(chileanRut.unformat(data.notaVenta.aval.rut));
        }
        this.seleccionaComuna(this.aval.ciudad);
      }
      if (data.notaVenta.detalle !== undefined) {
        this.detalle = data.notaVenta.detalle;
      }
      if (data.notaVenta.detalle.planimetria !== undefined) {
        this.planimetria = data.notaVenta.detalle.planimetria;
      }
      if (data.notaVenta.salud !== undefined) {
        this.salud = data.notaVenta.salud;
      }
    }
  }

  seleccionaComuna(cod_ciudad: number) {
    let request = new ComunaRequestModel(-1, cod_ciudad);
    this.webService.obtenerListaComunas(request)
      .subscribe(data => {
        let listado = data['ListadoComunaResult'];
        let lista = listado['lista'];
        this.comunas = lista;
      },
        error => {
          this.comunas = null;
          console.log('Error comunas WS: ' + error);
        });
  }

  buscarDatosAval(rut: string) { 
    if (rut == chileanRut.unformat(this.cliente.rut)){
      this.clienteComoAval = true;
    }else{
      this.clienteComoAval = false;
    }
    this.webService.obtenerDatosAval(new AvalRequestModel(rut))
      .subscribe(data => {
        let lista = data['ListadoClienteResult']['lista'];
        console.log('AVAL: '+JSON.stringify(lista));
        let realizado = data['ListadoClienteResult']['realizado'];
        if (realizado) {
          this.avalEncontrado = false;
          this.aval.nombre = lista[0]['des_nombre_cliente'];
          this.aval.direccion = lista[0]['des_direccion_cliente'];
          this.aval.ciudad = lista[0]['cod_ciudad_ciudad'];
          this.seleccionaComuna(this.aval.ciudad);
          this.aval.comuna = lista[0]['cod_comuna_comuna'];
          this.aval.sector = lista[0]['cod_sector_sector'];
          this.aval.actividad = lista[0]['cod_cargo_cargo'];
          this.aval.civil = lista[0]['cod_estadocivil_estadocivil'];
          this.aval.correo = lista[0]['des_mail_cliente'];
          this.aval.telefono = lista[0]['des_telefono_cliente'];
          this.aval.celular = lista[0]['des_telefono_celular'];
          this.aval.sexo = lista[0]['cod_sexo_sexo'];
          let fechaFormateada = new Date(parseInt(lista[0]['dat_fecha_nacimiento'].replace(/(^.*\()|([+-].*$)/g, ''))).toISOString();
          this.aval.fechaNacimiento = fechaFormateada;
          this.aval.edad = lista[0]['edad'];
          this.aval.renta = lista[0]['num_montorentaliquida_clientes'];
          this.aval.parentesco = lista[0]['cod_parentesco_parentesco'];
          this.botonNombre = "Modificar Aval";
        } else {
          this.botonNombre = "Crear Aval";
          this.avalEncontrado = true;
        }
      }, error => {
        this.avalEncontrado = false;
        console.log('ERROR AVAL ' + JSON.stringify(error));
      });
  }

  verificaDatosAval() {
    return !this.avalEncontrado;
  }

  foco() {
    if (this.aval.rut) {
      if (this.aval.rut.length !== 12) {
        this.aval.rut = "";
      }
    }
  }

  formatoRut() {
    if (this.aval.rut) {
      var dv = this.aval.rut.substring(8);
      this.aval.rut = chileanRut.format(this.aval.rut, dv);
      if (this.aval.rut.length == 12) {
        if (!chileanRut.validate(this.aval.rut)) {
          this.mostarToast('Rut no válido');
          this.aval.rut = "";
        } else {
          var rut = chileanRut.unformat(this.aval.rut);
          this.buscarDatosAval(rut);
        }
      } else if (this.aval.rut.length < 12) {
        this.aval.nombre = "";
        this.aval.direccion = "";
        this.aval.comuna = "";
        this.aval.sector = "";
        this.aval.actividad = "";
        this.aval.ciudad = "";
        this.aval.ciudad = "";
        this.aval.civil = "";
        this.aval.correo = "";
        this.aval.telefono = "";
        this.aval.celular = "";
        this.aval.fechaNacimiento = "";
        this.aval.edad = "";
        this.aval.renta = "";
        this.aval.parentesco = "";
      }
    }
    this.avalEncontrado = false;
  }

  continuar(continuar: boolean) {
    this.nota.notaVenta.cliente = this.cliente;
    this.nota.notaVenta.empleador = this.empleador;
    this.nota.notaVenta.aval = this.aval;
    this.nota.notaVenta.detalle = this.detalle;
    this.nota.notaVenta.detalle.planimetria = this.planimetria;
    this.nota.notaVenta.salud = this.salud;
    let data = {
      nota: this.nota,
      ciudades: this.ciudades,
      sectores: this.sectores,
      actividades: this.actividades,
      estadosCiviles: this.estadosCiviles,
      sexo: this.sexo,
      girosEmpleador: this.girosEmpleador,
      parentesco: this.parentesco,
      necesidades: this.necesidades,
      presionArterial: this.presionArterial,
      acreditacion: this.acreditacion
    };
    let opcionesModal = { enableBackdropDismiss: false };
    let modalPresent = null
    if (continuar) {
      if (this.nota.notaVenta.aval.rut && this.nota.notaVenta.aval.nombre && this.nota.notaVenta.aval.direccion &&
        this.nota.notaVenta.aval.comuna && this.nota.notaVenta.aval.sector &&
        this.nota.notaVenta.aval.actividad && this.nota.notaVenta.aval.ciudad &&
        this.nota.notaVenta.aval.civil && this.nota.notaVenta.aval.correo &&
        this.nota.notaVenta.aval.telefono && this.nota.notaVenta.aval.fechaNacimiento &&
        this.nota.notaVenta.aval.edad && this.nota.notaVenta.aval.renta 
        // && this.nota.notaVenta.aval.parentesco
      ) {
        let modalDetalle = this.modalCtrl.create(ModNotaIngresoDetallePage, data, opcionesModal);
        let rut = null;
        rut = chileanRut.unformat(this.nota.notaVenta.aval.rut);
        var vendedor = parseInt(localStorage.getItem('login'));
        var fecha = new Date().toISOString;
        let avalRequest = new ClienteRequestModel(
          vendedor,
          rut,
          this.nota.notaVenta.aval.comuna,
          this.nota.notaVenta.aval.giro,
          this.nota.notaVenta.aval.nombre,
          this.nota.notaVenta.aval.direccion,
          this.nota.notaVenta.aval.telefono,
          this.nota.notaVenta.aval.correo,
          this.nota.notaVenta.aval.ciudad,
          this.nota.notaVenta.aval.sector,
          this.nota.notaVenta.aval.civil,
          this.nota.notaVenta.aval.celular,
          // this.nota.notaVenaval.aval.fechaNacimiento,
          fecha,
          this.nota.notaVenta.aval.edad,
          this.nota.notaVenta.aval.sexo,
          this.nota.notaVenta.aval.renta,
          this.nota.notaVenta.aval.actividad,
          this.nota.notaVenta.aval.parentesco
        );

        if (this.avalEncontrado) {
          this.webService.guardarCliente(avalRequest)
            .subscribe(data => {
              let resultado = data['GuardarClienteResult']['resultado'];
              console.log('RESPUESTA ' + JSON.stringify(data));
              if (resultado) {
                this.mostarToast('Aval nuevo creado correctamente');
                modalPresent = modalDetalle;
                modalPresent.present();
                this.viewCtrl.dismiss();
              } else {
                this.mostarToast('Error al guardar nuevo aval');
                modalPresent = null;
              }
            }, error => {
              this.mostarToast('Error en respuesta del servidor');
              modalPresent = null;
            });
        } else {
          this.webService.guardarCliente(avalRequest)
          .subscribe(data => {
            let resultado = data['GuardarClienteResult']['resultado'];
            console.log('RESPUESTA ' + JSON.stringify(data));
            if (resultado) {
              this.mostarToast('Aval modificado correctamente');
              modalPresent = modalDetalle;
              modalPresent.present();
              this.viewCtrl.dismiss();
            } else {
              this.mostarToast('Error al modificar aval');
              modalPresent = null;
            }
          }, error => {
            this.mostarToast('Error en respuesta del servidor');
            modalPresent = null;
          });
        }
      } else {
        modalPresent = null;
        this.mostarToast('Faltan parámetros para aval');
      }
    } else {
      let modalEmpleador = this.modalCtrl.create(ModNotaIngresoEmpleadorPage, data, opcionesModal);
      modalPresent = modalEmpleador;
      modalPresent.present();
      this.viewCtrl.dismiss();
    }
  }

  mostarToast(mensaje: string) {
    if (mensaje) {
      let toast = this.toast.create({
        message: mensaje,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
  }
}