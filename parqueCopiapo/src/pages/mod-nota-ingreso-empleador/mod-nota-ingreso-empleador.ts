import { IngresoDocumentosPage } from './../ingreso-documentos/ingreso-documentos';
import { WebserviceProvider } from './../../providers/webservice/webservice';
import { ModNotaIngresoPage } from './../mod-nota-ingreso/mod-nota-ingreso';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, ModalController, ToastController } from 'ionic-angular';
import { ModIngresoAvalPage } from '../mod-ingreso-aval/mod-ingreso-aval';
import { ComunaRequestModel } from '../../modelos/comunaRequest';
import chileanRut from 'chilean-rut';
import { EmpleadorRequestModel } from '../../modelos/empleadorRequest';

@Component({
  selector: 'page-mod-nota-ingreso-empleador',
  templateUrl: 'mod-nota-ingreso-empleador.html',
})
export class ModNotaIngresoEmpleadorPage {
  cliente: any = {};
  empleador: any = {};
  aval: any = {};
  detalle: any = {};
  planimetria: any = {};
  salud: any = {};
  empleadorEncontrado: boolean = false;
  otrosIngresosCarga: boolean = false;
  acreditacionDoc: boolean = false;
  botonNombre: string = "Continuar";

  //Carga de datos
  comunas: any[] = [];
  ciudades: any[] = [];
  sectores: any[] = [];
  actividades: any[] = [];
  estadosCiviles: any[] = [];
  sexo: any[] = [];
  girosEmpleador: any[] = [];
  parentesco: any[] = [];
  necesidades: any[] = [];
  presionArterial: any[] = [];
  acreditacion: any[] = [];
  //Para las imagenes 
  acreditacionImagenes: any[] = []
  otrosIngresosImagenes: any[] = []

  nota: any = {
    notaVenta: {
      cliente: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        sector: null,
        actividad: null,
        ciudad: null,
        civil: null,
        correo: null,
        telefono: null,
        celular: null,
        fechaNacimiento: null,
        edad: null,
        sexo: null
      },
      empleador: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        actividad: null,
        ciudad: null,
        acreditacion: [],
        renta: null,
        giro: null,
        telefono: null,
        otrosIngresos: []
      },
      aval: {
        rut: null,
        nombre: null,
        direccion: null,
        comuna: null,
        sector: null,
        actividad: null,
        ciudad: null,
        civil: null,
        correo: null,
        telefono: null,
        celular: null,
        fechaNacimiento: null,
        edad: null,
        sexo: null,
        renta: null,
        parentesco: null
      },
      detalle: {
        necesidad: null,
        planimetria: {
          descripcion: null,
          letra: null,
          estado: null,
          sector: 0,
          porc_descuento: 0,
          uf: 0,
          fila: null,
          latitud: 0,
          longitud: 0,
          numero: 0,
          valor_base: 0,
          capacidades: 0,
          reducciones: 0,
          pie: 0
        },
        descuento: null,
        pie: 0,
        dia_pago: 0,
        vencimiento: null,
        cuotas: null,
        valor_cuotas: null,
        uf: null,
        comentario: null
      },
      salud: {
        responde: false,
        peso: 0,
        estatura: 0,
        presion: null,
        ejecutiva: null,
        comentario: null,
        firma: null
      }
    }
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private webService: WebserviceProvider
  ) {
    this.obtener();
    this.verificaCodigoDoc(this.nota.notaVenta.empleador.acreditacion);
    this.verificaCodigo(this.nota.notaVenta.empleador.otrosIngresos);
  }

  obtener() {
    //Asignar datos WS
    this.ciudades = this.navParams.get('ciudades');
    this.sectores = this.navParams.get('sectores');
    this.actividades = this.navParams.get('actividades');
    this.estadosCiviles = this.navParams.get('estadosCiviles');
    this.sexo = this.navParams.get('sexo');
    this.girosEmpleador = this.navParams.get('girosEmpleador');
    this.parentesco = this.navParams.get('parentesco');
    this.necesidades = this.navParams.get('necesidades');
    this.presionArterial = this.navParams.get('presionArterial');
    this.acreditacion = this.navParams.get('acreditacion');
    let data = this.navParams.get('nota');
    if (data !== undefined) {
      if (data.notaVenta.cliente !== undefined) {
        this.cliente = data.notaVenta.cliente;
      }
      if (data.notaVenta.empleador !== undefined) {
        this.empleador = data.notaVenta.empleador;
        this.cargaComunas(this.empleador.ciudad);
      }
      if (data.notaVenta.aval !== undefined) {
        this.aval = data.notaVenta.aval;
      }
      if (data.notaVenta.detalle !== undefined) {
        this.detalle = data.notaVenta.detalle;
      }
      if (data.notaVenta.detalle.planimetria !== undefined) {
        this.planimetria = data.notaVenta.detalle.planimetria;
      }
      if (data.notaVenta.salud !== undefined) {
        this.salud = data.notaVenta.salud;
      }
    }
  }

  seleccionaComuna(cod_ciudad: number) {
    if (cod_ciudad > 0) {
      this.cargaComunas(cod_ciudad);
    }
  }

  cargaComunas(cod_ciudad: number) {
    let request = new ComunaRequestModel(-1, cod_ciudad);
    this.webService.obtenerListaComunas(request)
      .subscribe(data => {
        let listado = data['ListadoComunaResult'];
        let lista = listado['lista'];
        this.comunas = lista;
      },
        error => {
          this.comunas = null;
          console.log('Error comunas WS: ' + JSON.stringify(error));
        });
  }

  ionViewDidLoad() { }

  verificaEmpleador() {
    return !this.empleadorEncontrado;
  }

  verificaOtrosIngresos() {
    return !this.otrosIngresosCarga;
  }

  verificaDocAcreditacion() {
    return !this.acreditacionDoc;
  }

  verificaCodigo(codigo: string) {
    // console.log(codigo);
    if (codigo.length > 0) {
      this.otrosIngresosCarga = true;
      this.nota.notaVenta.empleador.otrosIngresos = codigo;
    } else {
      this.otrosIngresosCarga = false;
    }
  }

  verificaCodigoDoc(cod: string) {
    if (cod.length > 0) {
      this.acreditacionDoc = true;
      this.nota.notaVenta.empleador.acreditacion = cod;
    } else {
      this.acreditacionDoc = false;
    }
  }

  buscarDatosEmpleador(rut: string) {
    this.webService.obtenerDatosEmpleador(rut)
      .subscribe(data => {
        // console.log('DATA EMPLEADOR '+JSON.stringify(data));
        let lista = data['ListadoEmpresaResult']['lista'];
        let realizado = data['ListadoEmpresaResult']['realizado'];
        if (realizado) {
          this.empleadorEncontrado = false;
          this.empleador.nombre = lista[0]['des_nombre_empresa'];
          this.empleador.direccion = lista[0]['des_direccion_empresa'];
          this.empleador.ciudad = lista[0]['cod_ciudad_ciudad'];
          this.seleccionaComuna(this.empleador.ciudad);
          this.empleador.comuna = lista[0]['cod_comuna_comuna'];
          this.empleador.actividad = lista[0]['cod_cargo_cargo'];
          this.empleador.acreditacion = lista[0]['cod_acreditacion_acreditacion'];
          this.empleador.renta = lista[0]['num_renta_renta'];
          this.empleador.giro = lista[0]['cod_giro_giro'];
          this.empleador.telefono = lista[0]['des_telefono_empresa'];
          this.empleador.otrosIngresos = lista[0]['cod_ingresos_ingresos'];
          this.botonNombre = "Modificar Empleador";
        } else {
          this.botonNombre = "Crear Empleador";
          this.empleadorEncontrado = true;
        }
      },
        error => {
          this.empleadorEncontrado = false;
          console.log('ERROR EMPLEADOR ' + JSON.stringify(error));
        });
  }

  foco() {
    if (this.empleador.rut) {
      if (this.empleador.rut.length !== 12) {
        this.empleador.rut = "";
      }
    }
  }

  formatoRut() {
    if (this.empleador.rut) {
      var dv = this.empleador.rut.substring(8);
      this.empleador.rut = chileanRut.format(this.empleador.rut, dv);
      if (this.empleador.rut.length == 12) {
        if (!chileanRut.validate(this.empleador.rut)) {
          this.mostarToast('Rut no válido');
          this.empleador.rut = "";
        } else {
          var rut = chileanRut.unformat(this.empleador.rut);
          this.buscarDatosEmpleador(rut);
        }
      } else if (this.empleador.rut.length < 12) {
        this.empleador.nombre = "";
        this.empleador.direccion = "";
        this.empleador.comuna = "";
        this.empleador.actividad = "";
        this.empleador.ciudad = "";
        this.empleador.acreditacion = "";
        this.empleador.renta = "";
        this.empleador.giro = "";
        this.empleador.telefono = "";
        this.empleador.otrosIngresos = "";
      }
    }
    this.empleadorEncontrado = false;
  }

  seleccionArchivo(ingreso: boolean) {
    let data = null;
    if (ingreso) {
      data = {
        tipo: true,
        acreditacion: this.nota.notaVenta.empleador.acreditacion
      };
    } else {
      data = {
        tipo: false,
        otros_ingresos: this.nota.notaVenta.empleador.otrosIngresos
      };
    }
    let modalSeleccion = this.modalCtrl.create(IngresoDocumentosPage, data, { enableBackdropDismiss: false })
    modalSeleccion.onDidDismiss(data => {
      console.log('DEVUELVE: ' + JSON.stringify(data));
      if (data) {
        if (data[0].EsAcreditacion) {
          this.acreditacionImagenes = data;
          console.log('ES ACREDITACION');
        } else {
          this.otrosIngresosImagenes = data;
          console.log('OTRO INGRESO');
        }
      }
    });
    if (ingreso) {
      if (this.acreditacionDoc && ingreso) {
        modalSeleccion.present();
      } else {
        this.mostarToast('Seleccione una acreditación');
      }
    } else {
      if (this.otrosIngresosCarga && !ingreso) {
        modalSeleccion.present();
      } else {
        this.mostarToast('Seleccione otros ingresos');
      }
    }
  }

  continuar(continuar: boolean) {
    this.nota.notaVenta.cliente = this.cliente;
    this.nota.notaVenta.empleador = this.empleador;
    this.nota.notaVenta.aval = this.aval;
    this.nota.notaVenta.detalle = this.detalle;
    this.nota.notaVenta.detalle.planimetria = this.planimetria;
    this.nota.notaVenta.salud = this.salud;
    this.nota.notaVenta.empleador.acreditacion = this.acreditacionImagenes;
    this.nota.notaVenta.empleador.otrosIngresos = this.otrosIngresosImagenes;
    // console.log('SALIDA: '+JSON.stringify(this.nota));
    let data = {
      nota: this.nota,
      ciudades: this.ciudades,
      sectores: this.sectores,
      actividades: this.actividades,
      estadosCiviles: this.estadosCiviles,
      sexo: this.sexo,
      girosEmpleador: this.girosEmpleador,
      parentesco: this.parentesco,
      necesidades: this.necesidades,
      presionArterial: this.presionArterial,
      acreditacion: this.acreditacion
    };
    let opcionesModal = { enableBackdropDismiss: false };
    let modalPresent = null;
    if (continuar) {
      if (this.nota.notaVenta.empleador.rut && this.nota.notaVenta.empleador.nombre && this.nota.notaVenta.empleador.direccion
        && this.nota.notaVenta.empleador.comuna && this.nota.notaVenta.empleador.ciudad && this.nota.notaVenta.empleador.giro
      ) {
        let modalAval = this.modalCtrl.create(ModIngresoAvalPage, data, opcionesModal);
        let rut = chileanRut.unformat(this.nota.notaVenta.empleador.rut);
        let empleadorRequest = new EmpleadorRequestModel(
          rut,
          this.nota.notaVenta.empleador.nombre,
          this.nota.notaVenta.empleador.direccion,
          this.nota.notaVenta.empleador.telefono,
          'correo@correo.cl',
          //this.nota.notaVenta.empleador.correo,
          this.nota.notaVenta.empleador.giro,
          this.nota.notaVenta.empleador.actividad,
          this.nota.notaVenta.empleador.comuna,
          this.nota.notaVenta.empleador.ciudad
        );

        if (this.empleadorEncontrado) {
          this.webService.guardarEmpleador(empleadorRequest)
            .subscribe(data => {
              let resultado = data['GuardarEmpresaResult']['resultado'];
              console.log('RESPUESTA WS ' + JSON.stringify(data));
              if (resultado) {
                this.mostarToast('Empleador nuevo creado correctamente');
                modalPresent = modalAval;
                modalPresent.present();
                this.viewCtrl.dismiss();
              } else {
                this.mostarToast('Error al guardar empleador nuevo');
                modalPresent = null;
              }
            }, error => {
              this.mostarToast('Error en respuesta del servidor');
              console.log('Error ' + JSON.stringify(error));
              modalPresent = null;
            });
        } else {
          // Aqui modificar empleador
          this.webService.guardarEmpleador(empleadorRequest)
          .subscribe(data => {
            let resultado = data['GuardarEmpresaResult']['resultado'];
            console.log('RESPUESTA WS ' + JSON.stringify(data));
            if (resultado) {
              this.mostarToast('Empleador modificado creado correctamente');
              modalPresent = modalAval;
              modalPresent.present();
              this.viewCtrl.dismiss();
            } else {
              this.mostarToast('Error al modificar empleador');
              modalPresent = null;
            }
          }, error => {
            this.mostarToast('Error en respuesta del servidor');
            console.log('Error ' + JSON.stringify(error));
            modalPresent = null;
          });
        }
      } else {
        modalPresent = null;
        this.mostarToast('Faltan parámetros para empleador');
      }
    } else {
      let modalIngreso = this.modalCtrl.create(ModNotaIngresoPage, data, opcionesModal);
      modalPresent = modalIngreso;
      modalPresent.present();
      this.viewCtrl.dismiss();
    }
  }

  mostarToast(mensaje: string) {
    if (mensaje) {
      let toast = this.toast.create({
        message: mensaje,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
  }
}