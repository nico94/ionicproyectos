import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform, AlertController, ToastController, ActionSheetController } from 'ionic-angular';
import { FileChooser } from '../../../node_modules/@ionic-native/file-chooser';
import { FilePath } from '../../../node_modules/@ionic-native/file-path';
import { Base64 } from '../../../node_modules/@ionic-native/base64';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImageViewerController } from 'ionic-img-viewer';

@Component({
  selector: 'page-ingreso-documentos',
  templateUrl: 'ingreso-documentos.html',
})
export class IngresoDocumentosPage {
  imagenes: any[] = [];
  test: any[] = [];
  foto: any;
  comentario: string;
  monto: number;
  esDispositivo: boolean;
  cod_acreditacion: number;
  cod_otroIngresos: number;
  tipo: boolean;
  cod: number;
  _imageViewerCtrl: ImageViewerController;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public toast: ToastController,
    public alertCtrl: AlertController,
    private fileChooser: FileChooser,
    public filePath: FilePath,
    public base64: Base64,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public imageViewerCtrl: ImageViewerController
  ) {
    this._imageViewerCtrl = imageViewerCtrl;
    console.log('TIPO ' + this.navParams.get('tipo'));
    if (this.navParams.get('tipo')) {
      this.tipo = this.navParams.get('tipo');
      this.cod = parseInt(this.navParams.get('acreditacion'));
    } else {
      this.tipo = this.navParams.get('tipo');
      this.cod = parseInt(this.navParams.get('otros_ingresos'));
    }
  }

  verificaDispositivo() {
    if (this.platform.is('cordova')) {
      this.esDispositivo = true;
      console.log('DISPOSITIVO');
    } else {
      this.esDispositivo = false;
      console.log('NAVEGADOR');
    }
  }

  ionViewDidLoad() { }

  camara() {
    const opcionesCamara: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 720,
      correctOrientation: true,
      allowEdit: false
    }
    this.camera.getPicture(opcionesCamara).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      // console.log('cam: ' + base64Image.substring(0, 50));
      this.imagenes.push(base64Image);
    }, (err) => {
      console.log('Error Cámara ' + JSON.stringify(err));
    });
  }

  agregarImagenes() {
    this.fileChooser.open().then(uri => {
      console.log('URI: ' + uri);
      this.filePath.resolveNativePath(uri)
        .then(file => {
          console.log('FILE: ' + file);
          let filePath: string = file.replace('file://', '');
          if (filePath) {
            this.imagenes.push(filePath);
            console.log('PATH: ' + filePath);
            this.base64.encodeFile(filePath).then((base64File: string) => {
              // let formateada = base64File.replace(base64File.substring(0, 34), 'data:image/jpeg;base64,');
              // console.log('arc: ' + formateada.substring(0, 50));
            }, (err) => {
              console.log('Error al obtener base64 ' + JSON.stringify(err));
            });
          }
        }).catch(err =>
          console.log(err)
        );
    }).catch(error => {
      console.log('Error al conseguir URI ' + JSON.stringify(error));
    });
  }

  opcionesImagen(id: number, myImage) {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Opciones imagen',
      buttons: [
        {
          text: 'Mostrar',
          handler: () => {
            const imageViewer = this._imageViewerCtrl.create(myImage);
            imageViewer.present();
          }
        }, {
          text: 'Eliminar',
          role: 'destructive',
          handler: () => {
            this.imagenes.splice(id, 1);
          }
        }, {
          text: 'Cancelar',
          role: 'cancel',
        }
      ]
    });
    actionSheet.present();
  }

  cerrar() {
    let alert = this.alertCtrl.create({
      title: 'Cancelar Ingreso',
      message: '¿Está seguro que quiere cancelar el ingreso de documentos?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => { }
        }, {
          text: 'Si',
          handler: () => {
            this.viewCtrl.dismiss(null);
          }
        }
      ]
    });
    alert.present();
  }

  guardar() {
    let data: any[] = [];
    if (this.monto) {
      let insert = {
        EsAcreditacion: this.tipo,
        id_codigo_taingresos: String(this.cod),
        num_monto_sctaingresos: this.monto,
        Glosa: this.comentario,
        img_imagen_sctaingresos: null
      };

      for (let i of this.imagenes) {
        insert.img_imagen_sctaingresos = i;
        data.push(insert);
      }
      this.viewCtrl.dismiss(data);
    } else {
      this.mostarToast("Falta ingreso de monto");
    }
  }

  mostarToast(mensaje: string) {
    if (mensaje) {
      let toast = this.toast.create({
        message: mensaje,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
  }
}