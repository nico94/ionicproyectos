export class NotaRequestModel {
    constructor(
        public cod_usuario: number,
        public mes: number,
        public ano: number
    ) { }
}