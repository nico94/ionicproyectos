export class DocumentoAcreditacionRequestModel {
    constructor(
        public id_codigo_taingresos: string, //
        public id_codigo_contrato: number,
        public num_monto_sctaingresos: number,
        public img_imagen_sctaingresos: string,
        public EsAcreditacion: boolean
    ) { }
}