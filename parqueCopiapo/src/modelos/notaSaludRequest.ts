export class NotaSaludRequestModel {
    constructor(
        public bit_responde_dsalud: boolean,
        public num_peso_dsalud: number,
        public num_estatura_dsalud: number,
        public des_obs_dsalud: string,
        public id_codigo_parterial: number
    ) { }
}