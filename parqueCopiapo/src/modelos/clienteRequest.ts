export class ClienteRequestModel {
	constructor(
		public cod_vendedor_fichapersonal: number,
		public cod_cliente_cliente: string,
		public cod_comuna_comuna: string,
		public cod_giro_giro: number,
		public des_nombre_cliente: string,
		public des_direccion_cliente: string,
		public des_telefono_cliente: string,
		public des_mail_cliente: string,
		public cod_ciudad_ciudad: number,
		public cod_sector_sector: number,
		public cod_estadocivil_estadocivil: number,
		public des_telefono_celular: string,
		public dat_fecha_nacimiento: any,
		public edad: number,
		public cod_sexo_sexo: number,
		public num_montorentaliquida_clientes: number,
		public cod_cargo_cargo: number,
		public cod_parentesco_parentesco: number
	) { }
}