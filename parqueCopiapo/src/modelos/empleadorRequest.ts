export class EmpleadorRequestModel {
    constructor(
        public cod_empresa_empresa: string,
        public des_nombre_empresa: string,
        public des_direccion_empresa: string,
        public des_telefono_empresa: string,
        public des_mail_empresa: string,
        public cod_giro_giro: number,
        public cod_actividadeconomica_empresa: number,
        public cod_comuna_comuna: number,
        public cod_ciudad_ciudad: number
    ) { }
}