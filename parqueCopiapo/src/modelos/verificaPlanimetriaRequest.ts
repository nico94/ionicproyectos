export class VerificaPlanimetriaModel {
    constructor(
        public id_codigo_contrato:number,
        public id_codigo_sector:number,
        public numero_fila:number,       
        public letra:number
    ){}
}