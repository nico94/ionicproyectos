webpackJsonp([0],{

/***/ 147:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modelos_usuario__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_database_database__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_autenticacion_autenticacion__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5__ = __webpack_require__(448);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginPage = /** @class */ (function () {
    function LoginPage(toast, navCtrl, navParams, auth, plataforma, loadingCtrl, db, eventos) {
        this.toast = toast;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.plataforma = plataforma;
        this.loadingCtrl = loadingCtrl;
        this.db = db;
        this.eventos = eventos;
        this.user = [];
        this.logo = "./assets/imgs/LogoSolo.png";
        this.esDispositivo = false;
        if (this.plataforma.is('cordova')) {
            this.esDispositivo = true;
        }
        else {
            this.esDispositivo = false;
        }
    }
    LoginPage.prototype.ionViewDidLoad = function () { };
    LoginPage.prototype.iniciarSesion = function () {
        var _this = this;
        if (this.usuario != null) {
            if (this.password != null) {
                this.mostrarCarga();
                this.passwordMD5 = __WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5__["Md5"].hashAsciiStr(this.password);
                this.auth.iniciarSesion(this.usuario, this.passwordMD5).subscribe(function (data) {
                    _this.user = data['InciarSesionResult'];
                    if (_this.user['estado']) {
                        var usuModel = new __WEBPACK_IMPORTED_MODULE_0__modelos_usuario__["a" /* UsuarioModel */](_this.user['codigo'], _this.user['cod_cargo'], _this.user['des_cargo'], _this.user['cod_comuna'], _this.user['rut'], _this.user['nombres'], _this.user['ap_paterno'], _this.user['ap_materno'], _this.user['direccion'], _this.user['email'], _this.user['fecha_nac'], _this.user['telefono']);
                        if (_this.esDispositivo) {
                            _this.db.crearUsuario(usuModel).then(function (data) {
                                localStorage.setItem('login', _this.user['codigo']);
                                localStorage.setItem('cod_cargo', _this.user['cod_cargo']);
                                _this.eventos.publish('verifica_ruta');
                            }).catch(function (error) {
                                _this.mostrarToast('Error en base de datos');
                            });
                        }
                        else {
                            localStorage.setItem('login', _this.user['codigo']);
                            localStorage.setItem('cod_cargo', _this.user['cod_cargo']);
                            _this.eventos.publish('verifica_ruta');
                        }
                        _this.cargando.dismiss();
                    }
                    else {
                        _this.cargando.dismiss();
                        _this.mostrarToast('Usuario/contraseña incorrectos');
                    }
                }, function (error) {
                    _this.cargando.dismiss();
                    _this.mostrarToast('Error en servidor');
                });
            }
            else {
                this.mostrarToast('Ingrese contraseña');
            }
        }
        else {
            this.mostrarToast('Ingrese usuario');
        }
    };
    LoginPage.prototype.mostrarCarga = function () {
        this.cargando = this.loadingCtrl.create({
            content: "Iniciando sesión..."
        });
        this.cargando.present();
    };
    LoginPage.prototype.mostrarToast = function (mensaje) {
        var toast = this.toast.create({
            message: mensaje,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\pages\login\login.html"*/'<ion-content>\n  <ion-card center>\n    <ion-card-header text-center>\n      <h1><strong>SISTEMA AVIZOR - MAGIC CAR</strong></h1>\n    </ion-card-header>\n    <ion-card-content>\n        <div class="centrar">\n          <ion-img class="logo" [src]=logo></ion-img>\n        </div>\n        <ion-item>\n          <ion-label color="primary" floating>Ingrese usuario</ion-label>\n          <ion-input type="text" name="usuario" [(ngModel)]="usuario"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label color="primary" floating>Ingrese contraseña</ion-label>\n          <ion-input type="password" name="password" [(ngModel)]="password"></ion-input>\n        </ion-item>\n      <div class="centrar" padding>\n        <button ion-button round (click)="iniciarSesion()">Iniciar sesión</button>    \n      </div>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n<ion-footer text-center>\n  <strong>PALL PROTECHNOLOGY®</strong>\n</ion-footer>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__providers_autenticacion_autenticacion__["a" /* AutenticacionProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* Events */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 180:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 180;

/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TareasIniciarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_interval__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_interval___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_interval__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TareasIniciarPage = /** @class */ (function () {
    function TareasIniciarPage(navCtrl, navParams, viewCtrl, parametros) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.parametros = parametros;
        this.tarea = [];
        this.cargaTarea();
    }
    TareasIniciarPage.prototype.ionViewDidLoad = function () { };
    TareasIniciarPage.prototype.cargaTarea = function () {
        this.tarea = this.navParams.get('tarea');
    };
    TareasIniciarPage.prototype.iniciarTarea = function () {
        var _this = this;
        this.timerVar = __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__["Observable"].interval(1000).subscribe(function (x) {
            //console.log(x);
            _this.timerVal = x;
        });
    };
    TareasIniciarPage.prototype.cerrar = function () {
        this.viewCtrl.dismiss();
    };
    TareasIniciarPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-tareas-iniciar',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\pages\tareas-iniciar\tareas-iniciar.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Iniciar tarea</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="cerrar()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-item>\n    <h1>{{ tarea.patente }}</h1>\n    <h1>{{ timerVal }}</h1>\n  </ion-item>\n\n  <!-- Grilla con detalle -->\n  <ion-grid>\n    <ion-row >\n      <ion-col class="columna"><strong>Actividad</strong></ion-col>\n      <ion-col >{{ tarea.descripcion }}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class="columna"><strong>Fecha y hora asignada</strong></ion-col>\n      <ion-col>{{ tarea.fecha }} {{ tarea.hora}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class="columna"><strong>Modelo</strong></ion-col>\n      <ion-col>{{ tarea.modelo }}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class="columna"><strong>Prioridad</strong></ion-col>\n      <ion-col>{{ tarea.prioridad }}</ion-col>\n    </ion-row>\n  </ion-grid>\n  <!-- Grilla de insumos -->\n  <ion-grid>\n    <ion-row>\n      <ion-col class="columna"><strong>Insumos</strong></ion-col>\n      <ion-col class="columna"><strong>Cantidad</strong></ion-col>\n    </ion-row>\n  </ion-grid>\n  \n  <!-- Ingreso de patente -->\n  <ion-item>\n    <ion-label color="primary" stacked>Ingrese comentario</ion-label>\n    <ion-input type="text" name="comentario" placeholder="Comentario"></ion-input>\n  </ion-item>\n</ion-content>\n\n<ion-footer>\n  <button ion-button full color="primary" (click)="iniciarTarea()">Iniciar tarea</button>\n</ion-footer>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\pages\tareas-iniciar\tareas-iniciar.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */]])
    ], TareasIniciarPage);
    return TareasIniciarPage;
}());

//# sourceMappingURL=tareas-iniciar.js.map

/***/ }),

/***/ 223:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 223;

/***/ }),

/***/ 263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModTareaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModTareaPage = /** @class */ (function () {
    function ModTareaPage(viewCtrl, navCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.estados = [];
        this.fechaAsiganda = new Date().toISOString();
        this.fechaTermino = new Date().toISOString();
        this.titulo = "Editar tarea";
        this.cargaEstados();
    }
    ModTareaPage.prototype.ionViewDidLoad = function () { };
    ModTareaPage.prototype.cargaEstados = function () {
        this.estados = [
            { id: 1, nombre: "Por iniciar" },
            { id: 2, nombre: "En proceso" },
            { id: 3, nombre: "Pendiente" }
        ];
    };
    ModTareaPage.prototype.guardar = function () {
        this.viewCtrl.dismiss();
    };
    ModTareaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mod-tarea',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\pages\mod-tarea\mod-tarea.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{ titulo }}</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="guardar()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content no-padding>\n    <ion-item no-lines>\n      <ion-label color="primary" stacked>\n        Responsable\n      </ion-label>\n      <ion-select [(ngModel)]="seleccionResponsable" no-lines placeholder="Seleccione responsable" cancelText="Cancelar" okText="Ok">\n        <ion-option value="1">Nicolas Galdames</ion-option>\n        <ion-option value="2">Eduardo Figueroa</ion-option>\n        <ion-option value="3">Carlos Montenegro</ion-option>\n      </ion-select>\n    </ion-item>\n\n    <ion-item no-lines>\n      <ion-list radio-group [(ngModel)]="estados"></ion-list>\n      <ion-list-header>\n        Estado\n      </ion-list-header>\n    </ion-item>\n    <ion-item *ngFor="let estado of estados">\n      <ion-label>{{ estado.nombre }}</ion-label>\n      <ion-radio value="{{ estado.id }}"></ion-radio>\n    </ion-item>\n\n    <ion-item no-lines>\n      <ion-label>\n        <strong>\n          Fechas\n        </strong>       \n      </ion-label>\n    </ion-item>\n    <ion-item>\n      <ion-label item-start color="primary" stacked>Fecha y hora asiganda</ion-label>\n      <button item-end ion-button outline icon-start (click)="picker.open()">\n        <ion-icon name="calendar"> </ion-icon>\n        <ion-datetime #picker pickerFormat="DD MMMM YYYY HH:mm" min="2017" max="2030" cancelText="Cancelar" doneText="Aceptar" [(ngModel)]="fechaAsiganda"></ion-datetime>\n      </button>\n    </ion-item>\n    <ion-item>\n      <ion-label item-start color="primary" stacked>Fecha y hora termino</ion-label>\n      <button item-end ion-button outline icon-start (click)="picker.open()">\n        <ion-icon name="calendar"> </ion-icon>\n        <ion-datetime #picker pickerFormat="DD MMMM YYYY HH:mm" min="2017" max="2030" cancelText="Cancelar" doneText="Aceptar" [(ngModel)]="fechaTermino"></ion-datetime>\n      </button>\n    </ion-item>\n\n\n</ion-content>\n\n<ion-footer>\n  <button ion-button full color="primary" (click)="guardar()">Guardar</button>\n</ion-footer>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\pages\mod-tarea\mod-tarea.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ModTareaPage);
    return ModTareaPage;
}());

//# sourceMappingURL=mod-tarea.js.map

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutenticacionProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(267);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AutenticacionProvider = /** @class */ (function () {
    function AutenticacionProvider(http) {
        this.http = http;
        this.apiUrl = "http://186.10.19.170/wsJesusPons/Servicios/Usuario.svc/rest/login";
    }
    AutenticacionProvider.prototype.iniciarSesion = function (usuario, password) {
        if (usuario != null && password != null) {
            var body = {
                "usuario": {
                    "IdUsuario": usuario,
                    "PswUsuario": password
                }
            };
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({
                'Content-Type': 'application/json'
            });
            return this.http.post(this.apiUrl, body, { headers: headers });
        }
    };
    AutenticacionProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], AutenticacionProvider);
    return AutenticacionProvider;
}());

//# sourceMappingURL=autenticacion.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_database_database__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_menu_principal_menu_principal__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, eventos, popoverCtrl, domSanitizer, db, navParams) {
        this.navCtrl = navCtrl;
        this.eventos = eventos;
        this.popoverCtrl = popoverCtrl;
        this.domSanitizer = domSanitizer;
        this.db = db;
        this.navParams = navParams;
        eventos.publish('obtenerUsuario');
    }
    HomePage.prototype.ionViewDidLoad = function () {
        this.urlLimpia = "http://186.10.19.170/dashboardmap/visor.aspx?id=1&index=1";
        this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(this.urlLimpia);
    };
    HomePage.prototype.mostrarMenu = function (evento) {
        var menu = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_1__components_menu_principal_menu_principal__["a" /* MenuPrincipalComponent */]);
        menu.present({
            ev: evento
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    <ion-title>Inicio</ion-title>\n    <ion-buttons right>\n        <button ion-button icon-only (click)="mostrarMenu($event)">\n         <ion-icon name="more"></ion-icon>\n        </button>\n    </ion-buttons> \n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <div>\n      <iframe class="iframe" [src]="url" scrolling="no" frameborder="0" style="top: 0;left: 0;width: 100%;"></iframe>\n    </div>\n</ion-content>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_0__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavParams */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 271:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ReportesPage = /** @class */ (function () {
    function ReportesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ReportesPage.prototype.ionViewDidLoad = function () { };
    ReportesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-reportes',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\pages\reportes\reportes.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Reportes</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n</ion-content>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\pages\reportes\reportes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ReportesPage);
    return ReportesPage;
}());

//# sourceMappingURL=reportes.js.map

/***/ }),

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrdenTrabajoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_menu_card_ot_menu_card_ot__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__orden_trabajo_ingreso_orden_trabajo_ingreso__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_menu_principal_menu_principal__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OrdenTrabajoPage = /** @class */ (function () {
    function OrdenTrabajoPage(navCtrl, navParams, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.ordenes = [];
        this.fecha = new Date().toISOString();
        this.cargaOrdenes();
    }
    OrdenTrabajoPage.prototype.ionViewDidLoad = function () { };
    OrdenTrabajoPage.prototype.cargaOrdenes = function () {
        this.ordenes = [
            { id: 1, patente: "AA2312", rut: "18.682.101-7", fec_ingreso_ot: "11 jun 2018", tipo: "D&P", estado: 1 }
        ];
    };
    OrdenTrabajoPage.prototype.mostrarMenu = function (evento) {
        var menu = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_0__components_menu_card_ot_menu_card_ot__["a" /* MenuCardOtComponent */]);
        menu.present({
            ev: evento
        });
    };
    OrdenTrabajoPage.prototype.mostrarMenuPrincipal = function (evento) {
        var menu = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_4__components_menu_principal_menu_principal__["a" /* MenuPrincipalComponent */]);
        menu.present({
            ev: evento
        });
    };
    OrdenTrabajoPage.prototype.cambiarBusqueda = function () {
        this.esBusqueda = !this.esBusqueda;
    };
    OrdenTrabajoPage.prototype.ingresarOT = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_1__orden_trabajo_ingreso_orden_trabajo_ingreso__["a" /* OrdenTrabajoIngresoPage */], null);
    };
    OrdenTrabajoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-orden-trabajo',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\pages\orden-trabajo\orden-trabajo.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title *ngIf="!esBusqueda">Orden de Trabajo</ion-title>\n    <!-- Boton de menu -->\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <!-- Barra de busqueda -->\n    <ion-searchbar\n      *ngIf="esBusqueda"\n      cancelButtonText="Cancelar"\n      placeholder="Buscar orden de trabajo...">\n    </ion-searchbar>\n    <!-- Boton de la barra -->\n    <ion-buttons end>\n      <button ion-button icon-only *ngIf="!esBusqueda" (click)="cambiarBusqueda()">\n        <ion-icon name="search"></ion-icon>\n      </button>\n      <button ion-button icon-only *ngIf="esBusqueda" (click)="cambiarBusqueda()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n      <button ion-button icon-only (click)="mostrarMenuPrincipal($event)">\n        <ion-icon name="more"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<!-- Contenedor principal -->\n<ion-content>\n  <!-- Seleccion de fecha -->\n  <ion-card>\n    <ion-item>\n      <ion-label>Seleccione fecha</ion-label>\n      <ion-datetime [(ngModel)]="fecha" min="2018" max="2030" displayFormat="MMMM YYYY" cancelText="Cancelar" doneText="Aceptar">\n      </ion-datetime>\n    </ion-item>\n  </ion-card>\n\n  <!-- Iteracion del arreglo -->\n  <ion-card *ngFor="let item of ordenes">\n    <ion-card-header>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-10>{{ item.patente }}</ion-col>\n          <ion-col col-2>\n            <button ion-button small clear float-right (click)="mostrarMenu($event)">\n              <ion-icon name="more"></ion-icon>\n            </button>\n          </ion-col>\n        </ion-row>\n        </ion-grid>\n    </ion-card-header>\n    <!-- Contenido tarjeta -->\n    <ion-card-content>\n      <strong>Rut </strong> {{ item.rut }}\n      <strong>Fecha Ingreso </strong> {{ item.fec_ingreso_ot }}\n      <strong>Tipo </strong> {{ item.tipo }}\n    </ion-card-content>\n    <!-- Estado tarjeta -->\n    <footer class="estado{{item.estado}}">\n    </footer>\n  </ion-card>\n\n  <!-- Boton Flotante -->\n  <ion-fab bottom right>\n    <button ion-fab (click)="ingresarOT()">\n      <ion-icon name="add"></ion-icon>\n    </button>\n  </ion-fab>\n\n</ion-content>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\pages\orden-trabajo\orden-trabajo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* PopoverController */]])
    ], OrdenTrabajoPage);
    return OrdenTrabajoPage;
}());

//# sourceMappingURL=orden-trabajo.js.map

/***/ }),

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuCardOtComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuCardOtComponent = /** @class */ (function () {
    function MenuCardOtComponent(viewControl) {
        this.viewControl = viewControl;
        this.items = [
            { id: 1, titulo: 'Editar' },
            { id: 2, titulo: 'Imprimir' }
        ];
    }
    MenuCardOtComponent.prototype.clickItem = function (item) {
        switch (item.id) {
            case 1: {
                break;
            }
            case 2: {
                break;
            }
        }
        this.viewControl.dismiss(item);
    };
    MenuCardOtComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'menu-card-ot',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\components\menu-card-ot\menu-card-ot.html"*/'<ion-content>\n  <ion-list no-lines>\n    <ion-item *ngFor="let item of items" (click)="clickItem(item)">\n      {{item.titulo}}\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\components\menu-card-ot\menu-card-ot.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["q" /* ViewController */]])
    ], MenuCardOtComponent);
    return MenuCardOtComponent;
}());

//# sourceMappingURL=menu-card-ot.js.map

/***/ }),

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrdenTrabajoIngresoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__mod_tarea_mod_tarea__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OrdenTrabajoIngresoPage = /** @class */ (function () {
    function OrdenTrabajoIngresoPage(navCtrl, modalCtrl, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.fechaIngreso = new Date().toISOString();
        this.tareas = [];
        this.cargaTareas();
    }
    OrdenTrabajoIngresoPage.prototype.ionViewDidLoad = function () { };
    OrdenTrabajoIngresoPage.prototype.editarTarea = function (tarea) {
        var modalEdit = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_0__mod_tarea_mod_tarea__["a" /* ModTareaPage */], { tarea: tarea }, { enableBackdropDismiss: false });
        modalEdit.present();
    };
    OrdenTrabajoIngresoPage.prototype.agregarTarea = function () {
        var modalAdd = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_0__mod_tarea_mod_tarea__["a" /* ModTareaPage */], { tarea: null }, { enableBackdropDismiss: false });
        modalAdd.present();
    };
    // mostrarModal(tipo:number, tarea:any){
    //   let alert = null; 
    //   if(tipo == 1 && tarea){
    //     alert = this.alertCtrl.create({ enableBackdropDismiss: false });
    //     alert.setTitle('Editar tarea');
    //     alert.addInput({ type: 'input', placeholder: 'Descripcion', value: tarea.descripcion });
    //     alert.addInput({ type: 'number', placeholder: 'Valor', value: tarea.valor });
    //     alert.addInput({ type: 'input', placeholder: 'Responsable', value: tarea.responsable });
    //     // alert.addInput({ type: 'radio', label: 'Por iniciar', value: '1', checked: true });
    //     // alert.addInput({ type: 'radio', label: 'En proceso', value: '2' });
    //     alert.addInput({ type: 'checkbox', label: 'Blue', value: 'blue', checked: true });
    //     alert.addButton('Cancelar');
    //     alert.addButton({
    //       text: 'Editar',
    //       handler:(data:any)=>{
    //       }
    //     });
    //     //   {
    //     //   title: 'Editar tarea',
    //     //   message: 'Editando tarea',
    //     //   enableBackdropDismiss: false,
    //     //   inputs: [
    //     //     { name: 'Descipcion', placeholder: 'Descripcion', value: tarea.descripcion },
    //     //     { name: 'Valor', placeholder: 'Valor', value: tarea.valor },
    //     //     { name: 'Responsable', placeholder: 'Responsable', value: tarea.responsable},
    //     //     { name: 'Estado', placeholder: 'Estado', value: tarea.estado},
    //     //     { name: 'Programada', placeholder: 'Programada', value: tarea.mantencion },
    //     //     { type: 'checkbox', label: 'Blue', value: 'blue', checked: true }
    //     //   ],
    //     //   buttons:[
    //     //     { text: 'Cancelar',
    //     //       handler: () => {
    //     //         console.log('Presiona cancelar');
    //     //       }
    //     //     },
    //     //     { text: 'Editar',
    //     //       handler: () => {
    //     //         console.log('Aqui editar');
    //     //       }
    //     //     }
    //     //   ]
    //     // });
    //   }else{
    //     //Es nuevo
    //     alert = this.alertCtrl.create({
    //       title: 'Agregar tarea',
    //       message: 'Añadiendo tarea',
    //       enableBackdropDismiss: false,
    //       inputs: [
    //         { name: 'Descipcion', placeholder: 'Descripcion' },
    //         { name: 'Valor', placeholder: 'Valor' },
    //         { name: 'Responsable', placeholder: 'Responsable' },
    //         { name: 'Estado', placeholder: 'Estado' },
    //         { name: 'Programada', placeholder: 'Programada' }
    //       ],
    //       buttons: [
    //         {
    //           text: 'Cancel',
    //           handler: () => {
    //             console.log('Cancel clicked');
    //           }
    //         },
    //         {
    //           text: 'Agregar',
    //           handler: () => {
    //             console.log('Saved clicked');
    //           }
    //         }
    //       ]
    //     });
    //   }
    //   if(alert){
    //     alert.present();
    //   }
    // }
    OrdenTrabajoIngresoPage.prototype.eliminarTarea = function (tarea) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Eliminar actividad',
            message: '¿Está seguro que quiere eliminar ' + tarea.descripcion + '?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancela');
                    }
                }, {
                    text: 'Si',
                    handler: function () {
                        var indice = _this.tareas.indexOf(tarea);
                        if (indice !== -1) {
                            _this.tareas.splice(indice, 1);
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    OrdenTrabajoIngresoPage.prototype.cargaTareas = function () {
        this.tareas = [
            { id: 1, descripcion: "Cambiar Filtro purificador de aire", valor: "$9.990", responsable: "Nicolas", estado: "Por iniciar", mantencion: "Si" },
            { id: 1, descripcion: "Cambiar aceite motor", valor: "$20.990", responsable: "Nicolas", estado: "Por iniciar", mantencion: "Si" },
            { id: 1, descripcion: "Lavar asientos", valor: "$1.990", responsable: "Nicolas", estado: "Por iniciar", mantencion: "Si" },
            { id: 1, descripcion: "Inflar neumaticos", valor: "$0", responsable: "Nicolas", estado: "Por iniciar", mantencion: "Si" },
            { id: 1, descripcion: "Pintar", valor: "$150.990", responsable: "Nicolas", estado: "Por iniciar", mantencion: "Si" }
        ];
    };
    OrdenTrabajoIngresoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-orden-trabajo-ingreso',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\pages\orden-trabajo-ingreso\orden-trabajo-ingreso.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Ingreso orden de trabajo</ion-title>\n  </ion-navbar>\n</ion-header>\n<!-- Contenido de la vista -->\n<ion-content>\n  <ion-card center>\n    <ion-card-content no-padding>\n      <!-- Ingreso de patente -->\n      <ion-item>\n        <ion-label color="primary" stacked>Ingrese patente</ion-label>\n        <ion-input type="text" name="patente" placeholder="Ingrese patente" maxlength="6"></ion-input>\n      </ion-item>\n      <!-- Ingreso de fecha hora OT -->\n      <ion-item>\n        <ion-label item-start color="primary" stacked>Fecha y hora de OT</ion-label>\n        <button item-end ion-button outline icon-start (click)="picker.open()">\n          <ion-icon name="calendar"> </ion-icon>\n          <ion-datetime #picker pickerFormat="DD MMMM YYYY HH:mm" min="2017" max="2030" cancelText="Cancelar" doneText="Aceptar" [(ngModel)]="fechaIngreso"></ion-datetime>\n        </button>\n      </ion-item>\n      <!-- Selecion de estado -->\n      <ion-item no-lines>\n        <ion-label color="primary" stacked>\n          Estado OT\n        </ion-label>\n        <ion-select [(ngModel)]="seleccionEstado" no-lines\n          placeholder="Seleccione estado"\n          cancelText="Cancelar" okText="Ok">\n          <ion-option value="PROCESO">En proceso</ion-option>\n          <ion-option value="PENDIENTE">Pendiente</ion-option>\n        </ion-select>\n      </ion-item>\n      <!-- Grilla con estados -->\n      <ion-grid>\n        <ion-buttons end>\n          <button ion-button icon-start (click)="agregarTarea()">\n            <ion-icon icon-left name="add"></ion-icon> \n            Agregar tarea\n          </button>\n        </ion-buttons>\n        <ion-label color="primary" text-center>\n          <h1>\n            <strong>\n              Tareas\n            </strong>\n          </h1>\n        </ion-label>\n        <!-- Cabecera  -->\n        <ion-row class="row header"> \n          <ion-col class="col" col-2><strong>Descripción</strong></ion-col>\n          <ion-col class="col" col-2><strong>Valor</strong></ion-col>\n          <ion-col class="col" col-2><strong>Responsable</strong></ion-col>\n          <ion-col class="col" col-2><strong>Estado</strong></ion-col>\n          <ion-col class="col" col-2><strong>Programada</strong></ion-col>\n          <ion-col class="col" col-2><strong>Acción</strong></ion-col>\n        </ion-row>\n        <!-- Registro -->\n        <ion-row class="row" *ngFor="let tar of tareas" >\n          <ion-col class="col" col-2>{{ tar.descripcion }}</ion-col>\n          <ion-col class="col" col-2>{{ tar.valor}}</ion-col>\n          <ion-col class="col" col-2>{{ tar.responsable}}</ion-col>\n          <ion-col class="col" col-2>{{ tar.estado}}</ion-col>\n          <ion-col class="col" col-2>{{ tar.mantencion }}</ion-col>\n          <ion-col class="col" center>\n            <!-- Boton editar -->\n            <button ion-button round (click)="editarTarea(tar)">\n              <ion-icon name="list-box"></ion-icon>\n            </button>\n            <!-- Boton eliminar -->\n            <button ion-button round (click)="eliminarTarea(tar)">\n              <ion-icon name="close" ></ion-icon>\n            </button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n\n'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\pages\orden-trabajo-ingreso\orden-trabajo-ingreso.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]])
    ], OrdenTrabajoIngresoPage);
    return OrdenTrabajoIngresoPage;
}());

//# sourceMappingURL=orden-trabajo-ingreso.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecepcionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_menu_principal_menu_principal__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_menu_card_recepcion_menu_card_recepcion__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__recepcion_ingreso_recepcion_ingreso__ = __webpack_require__(277);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RecepcionPage = /** @class */ (function () {
    function RecepcionPage(navCtrl, navParams, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.recepciones = [];
        this.fecha = new Date().toISOString();
        this.esBusqueda = false;
        this.cargaRecepciones();
    }
    RecepcionPage.prototype.ionViewDidLoad = function () { };
    RecepcionPage.prototype.mostrarMenu = function (evento) {
        var menu = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__components_menu_card_recepcion_menu_card_recepcion__["a" /* MenuCardRecepcionComponent */]);
        menu.present({
            ev: evento
        });
    };
    RecepcionPage.prototype.mostrarMenuPrincipal = function (evento) {
        var menu = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_0__components_menu_principal_menu_principal__["a" /* MenuPrincipalComponent */]);
        menu.present({
            ev: evento
        });
    };
    RecepcionPage.prototype.cargaRecepciones = function () {
        this.recepciones = [
            {
                id: 1,
                patente: "AA2312",
                rut: "18.682.101-7",
                empresa: "Linderos",
                fec_rec: "11 jun 2018",
                tipo: "D&P",
                estado: 1
            }, {
                id: 2,
                patente: "BB1234",
                rut: "18.682.101-7",
                empresa: "Linderos",
                fec_rec: "9 jun 2018",
                tipo: "Mecánica",
                estado: 2
            }
        ];
    };
    RecepcionPage.prototype.cambiarBusqueda = function () {
        this.esBusqueda = !this.esBusqueda;
    };
    RecepcionPage.prototype.ingresarRecepcion = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__recepcion_ingreso_recepcion_ingreso__["a" /* RecepcionIngresoPage */], null);
    };
    RecepcionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-recepcion',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\pages\recepcion\recepcion.html"*/'<!-- Cabecera de la vista -->\n<ion-header>\n  <ion-navbar>\n    <ion-title *ngIf="!esBusqueda">Recepción Vehículo</ion-title>\n    <!-- Boton de menu -->\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <!-- Barra de busqueda -->\n    <ion-searchbar \n      *ngIf="esBusqueda" \n      cancelButtonText="Cancelar"\n      placeholder="Buscar recepción...">\n    </ion-searchbar>\n    <!-- Botones de la barra -->\n    <ion-buttons end>\n      <button ion-button icon-only *ngIf="!esBusqueda" (click)="cambiarBusqueda()">\n        <ion-icon name="search"></ion-icon>\n      </button>\n      <button ion-button icon-only *ngIf="esBusqueda" (click)="cambiarBusqueda()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n      <button ion-button icon-only (click)="mostrarMenuPrincipal($event)">\n        <ion-icon name="more"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<!-- Contenedor principal -->\n<ion-content>\n  <!-- Seleccion de fecha -->\n  <ion-card>\n    <ion-item>\n      <ion-label>Seleccione fecha</ion-label>\n      <ion-datetime [(ngModel)]="fecha" min="2018" max="2030" displayFormat="MMMM YYYY" cancelText="Cancelar" doneText="Aceptar">\n      </ion-datetime>\n    </ion-item>\n  </ion-card>\n\n  <!-- Iteracion del arreglo -->\n  <ion-card *ngFor="let item of recepciones">\n    <ion-card-header>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-10>{{ item.patente }}</ion-col>\n          <ion-col col-2>\n            <button ion-button small clear float-right (click)="mostrarMenu($event)">\n              <ion-icon name="more"></ion-icon>\n            </button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card-header>\n\n    <!-- Contenido tarjeta -->\n    <ion-card-content>\n      <strong>Rut </strong> {{ item.rut }}\n      <strong>Empresa </strong> {{ item.empresa }}\n      <strong>Fecha </strong> {{ item.fec_rec }}\n      <strong>Tipo </strong> {{ item.tipo }}\n    </ion-card-content>\n\n    <!-- Estado de la tarjeta -->\n    <footer class="estado{{item.estado}}">\n    </footer>\n  </ion-card>\n\n  <!-- Boton Flotante -->\n  <ion-fab bottom right>\n    <button ion-fab (click)="ingresarRecepcion()">\n      <ion-icon name="add"></ion-icon>\n    </button>\n  </ion-fab>\n</ion-content>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\pages\recepcion\recepcion.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* PopoverController */]])
    ], RecepcionPage);
    return RecepcionPage;
}());

//# sourceMappingURL=recepcion.js.map

/***/ }),

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuCardRecepcionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuCardRecepcionComponent = /** @class */ (function () {
    function MenuCardRecepcionComponent(navCtrl, viewControl) {
        this.navCtrl = navCtrl;
        this.viewControl = viewControl;
        this.items = [];
        this.items = [
            { id: 1, titulo: 'Editar' },
            { id: 2, titulo: 'Eliminar' },
            { id: 3, titulo: 'Imprimir' }
        ];
    }
    MenuCardRecepcionComponent.prototype.ionViewDidLoad = function () { };
    MenuCardRecepcionComponent.prototype.clickItem = function (item) {
        console.log(item.id);
        switch (item.id) {
            case 1: {
                break;
            }
            case 2: {
                break;
            }
            case 3: {
                break;
            }
            default: {
                break;
            }
        }
        this.viewControl.dismiss(item);
    };
    MenuCardRecepcionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'menu-card-recepcion',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\components\menu-card-recepcion\menu-card-recepcion.html"*/'<ion-list no-lines>\n  <ion-item *ngFor="let item of items"\n  (click)="clickItem(item)">\n    {{item.titulo}}\n  </ion-item>\n</ion-list>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\components\menu-card-recepcion\menu-card-recepcion.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], MenuCardRecepcionComponent);
    return MenuCardRecepcionComponent;
}());

//# sourceMappingURL=menu-card-recepcion.js.map

/***/ }),

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecepcionIngresoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__mod_recepcion_estado_mod_recepcion_estado__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__mod_recepcion_documentacion_mod_recepcion_documentacion__ = __webpack_require__(279);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RecepcionIngresoPage = /** @class */ (function () {
    function RecepcionIngresoPage(navCtrl, alertCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.motivos = [];
        this.prioridades = [];
        this.fechaIngreso = new Date().toISOString();
        this.cargaMotivos();
        this.cargaPrioridades();
    }
    RecepcionIngresoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.navBar.backButtonClick = function (e) {
            _this.mostrarAlerta();
        };
    };
    RecepcionIngresoPage.prototype.cargaMotivos = function () {
        this.motivos = [
            { id: 1, nombre: "D&P", estado: false },
            { id: 2, nombre: "Mecánica Preventiva", estado: false },
            { id: 3, nombre: "Mecánica Correctiva", estado: false }
        ];
    };
    RecepcionIngresoPage.prototype.cargaPrioridades = function () {
        this.prioridades = [
            { id: 1, nombre: "Normal", estado: false },
            { id: 2, nombre: "Urgente", estado: false },
            { id: 3, nombre: "Mediana", estado: false }
        ];
    };
    RecepcionIngresoPage.prototype.registrarEstado = function () {
        var modalEstado = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_0__mod_recepcion_estado_mod_recepcion_estado__["a" /* ModRecepcionEstadoPage */], null, { enableBackdropDismiss: false });
        modalEstado.present();
    };
    RecepcionIngresoPage.prototype.registrarDocumentacion = function () {
        var modalDocumentacion = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__mod_recepcion_documentacion_mod_recepcion_documentacion__["a" /* ModRecepcionDocumentacionPage */], null, { enableBackdropDismiss: false });
        modalDocumentacion.present();
    };
    RecepcionIngresoPage.prototype.mostrarAlerta = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Cancelar ingreso',
            message: '¿Está seguro que desea cancelar?',
            buttons: [
                { text: 'No', role: 'cancel' },
                { text: 'Si',
                    handler: function () {
                        _this.navCtrl.pop();
                    }
                }
            ]
        });
        alert.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* Navbar */])
    ], RecepcionIngresoPage.prototype, "navBar", void 0);
    RecepcionIngresoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-recepcion-ingreso',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\pages\recepcion-ingreso\recepcion-ingreso.html"*/'<!-- Cabecera -->\n<ion-header>\n  <ion-navbar>\n    <ion-title>Ingreso de Recepción</ion-title>\n  </ion-navbar>\n</ion-header>\n<!-- Contenido de la vista -->\n<ion-content>\n  <ion-grid no-padding>\n    <ion-row>\n      <ion-col width-50>\n      <!-- Tarjeta de la vista -->\n      <ion-card center>\n        <!-- Contenido -->\n        <ion-card-content no-padding>\n          <!-- Ingreso Patente -->\n          <ion-item>\n            <ion-label color="primary" stacked>Ingrese patente</ion-label>\n            <ion-input type="text" name="patente" placeholder="Ingrese patente" maxlength="6"></ion-input>\n          </ion-item>\n          <!-- Ingreso Visita -->\n          <ion-item>\n            <ion-label color="primary" stacked>¿Quién trae el vehículo?</ion-label>\n            <ion-input type="text" name="visita" placeholder="Visitante"></ion-input>\n          </ion-item>\n          <!-- Es propietario -->\n          <ion-item>\n            <ion-label>Propietario</ion-label>\n            <ion-checkbox></ion-checkbox>\n          </ion-item>\n          <!-- Ingrese Motivo -->\n          <ion-list>\n            <ion-list-header>\n              Motivo\n            </ion-list-header>\n            <ion-item *ngFor="let mot of motivos">\n              <ion-label>{{ mot.nombre }}</ion-label>\n              <ion-checkbox checked="{{ mot.estado }}"></ion-checkbox>\n            </ion-item>\n          </ion-list>       \n          <!-- Prioridad -->\n          <ion-list radio-group>\n            <ion-list-header>\n              Prioridad\n            </ion-list-header>\n            <ion-item *ngFor="let prio of prioridades">\n              <ion-label>{{ prio.nombre }}</ion-label>\n              <ion-radio checked="{{ prio.estado }}"></ion-radio>\n            </ion-item>\n          </ion-list>\n        </ion-card-content>\n      </ion-card>\n      </ion-col>\n      <!-- Columna 2 -->\n      <ion-col width-50>\n        <ion-card>\n          <!-- Contenido tarjeta 2 -->\n          <ion-card-content no-padding>\n            <!-- Fecha de ingreso de recepcion -->\n            <ion-item>\n              <ion-label item-start color="primary" stacked>Fecha de Recepción</ion-label>\n              <button item-end ion-button outline icon-right (click)="picker.open()">\n                <ion-icon name="calendar"> </ion-icon>\n                <ion-datetime #picker pickerFormat="DD MMMM YYYY HH:mm" min="2017" max="2030" cancelText="Cancelar" doneText="Aceptar" [(ngModel)]="fechaIngreso"></ion-datetime>\n              </button>\n            </ion-item>\n            <!-- Ingreso de kilometraje -->\n            <ion-item>\n              <ion-label color="primary" stacked>Kilometraje</ion-label>\n              <ion-input type="number" name="kilometraje" placeholder="Ingrese kilometraje" maxlength="10"></ion-input>\n            </ion-item>\n            <!-- Rango de bencina -->\n            <ion-item>\n              <ion-label color="primary" stacked>Bencina {{ valorBencina }}</ion-label>\n              <ion-range [(ngModel)]="valorBencina"\n                color="primary" \n                pin="true"\n                snaps="true"\n                min="0" max="100" step="25">\n                <ion-icon small range-left name="cloud-outline"></ion-icon>\n                <ion-icon range-right name="cloud"></ion-icon>\n              </ion-range>\n            </ion-item>\n            <!-- Milimetros de neumaticos -->\n            <ion-list>\n              <ion-item no-lines>\n                <ion-label color="primary" stacked>Profundidad neumáticos</ion-label>\n                <ion-grid item-content>\n                  <ion-row>\n                    <!-- Fila 1 de neumaticos -->\n                    <ion-col col-3>\n                      <ion-label color="secondary" stacked>Delantero Izquierdo</ion-label>\n                      <ion-input type="number" placeholder="MM" value=""></ion-input>\n                    </ion-col>\n                    <ion-col col-3>\n                      <ion-label color="secondary" stacked>Delantero Derecho</ion-label>\n                      <ion-input type="number" placeholder="MM" value=""></ion-input>\n                    </ion-col>\n                  </ion-row>\n                  <!-- Fila 2 de neumaticos -->                 \n                  <ion-row>\n                    <ion-col col-3>\n                      <ion-label color="secondary" stacked>Trasero Izquierdo</ion-label>\n                      <ion-input type="number" placeholder="MM" value=""></ion-input>\n                    </ion-col>\n                    <ion-col col-3>\n                      <ion-label color="secondary" stacked>Trasero Derecho</ion-label>\n                      <ion-input type="number" placeholder="MM" value=""></ion-input>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n              </ion-item>\n            </ion-list>\n            <!-- Boton de estado de vehiculo -->\n            <ion-item>\n              <button ion-button full color="primary" (click)="registrarEstado()">Registrar estado vehículo</button>\n            </ion-item>\n            <!-- Boton documentacion de vehiculo -->\n            <ion-item>\n                <button ion-button full color="primary" (click)="registrarDocumentacion()">Documentacion del vehículo</button>\n            </ion-item>\n          </ion-card-content>\n          <!-- Fin contenido tarjeta -->\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <!-- Boton flotante -->\n  <ion-fab end bottom >\n    <button ion-fab color="azul">\n      <ion-icon name="arrow-dropright"></ion-icon>\n    </button>\n  </ion-fab>\n</ion-content>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\pages\recepcion-ingreso\recepcion-ingreso.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* ModalController */]])
    ], RecepcionIngresoPage);
    return RecepcionIngresoPage;
}());

//# sourceMappingURL=recepcion-ingreso.js.map

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModRecepcionEstadoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModRecepcionEstadoPage = /** @class */ (function () {
    function ModRecepcionEstadoPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.carroceria = [];
        this.faroles = [];
        this.inventario = [];
        this.cargarCaroceria();
        this.cargarFaroles();
        this.cargarInventario();
    }
    ModRecepcionEstadoPage.prototype.ionViewDidLoad = function () { };
    ModRecepcionEstadoPage.prototype.guardar = function () {
        //console.log(this.carroceria);
        this.viewCtrl.dismiss();
    };
    ModRecepcionEstadoPage.prototype.cargarCaroceria = function () {
        this.carroceria = [
            { "id": 1, "nombre": "Asientos", "observacion": null, "estado": false },
            { "id": 2, "nombre": "Camada Pick Up", "observacion": null, "estado": false },
            { "id": 3, "nombre": "Asientos", "observacion": null, "estado": false },
            { "id": 4, "nombre": "Capot", "observacion": null, "estado": false },
            { "id": 5, "nombre": "Luneta", "observacion": null, "estado": false },
            { "id": 6, "nombre": "Parabrisas", "observacion": null, "estado": false },
            { "id": 7, "nombre": "Parachoque Del.", "observacion": null, "estado": false },
            { "id": 8, "nombre": "Parachoque Tras.", "observacion": null, "estado": false },
            { "id": 9, "nombre": "Puerta Del. Der.", "observacion": null, "estado": false },
            { "id": 10, "nombre": "Puerta Del. Izq.", "observacion": null, "estado": false },
            { "id": 11, "nombre": "Puerta Tra. Der.", "observacion": null, "estado": false },
            { "id": 12, "nombre": "Puerta Tra. Izq.", "observacion": null, "estado": false },
            { "id": 13, "nombre": "Tapabarros Der.", "observacion": null, "estado": false },
            { "id": 14, "nombre": "Tapabarro Izq.", "observacion": null, "estado": false },
            { "id": 15, "nombre": "Techo", "observacion": null, "estado": false },
            { "id": 16, "nombre": "Portalon Tras.", "observacion": null, "estado": false }
        ];
    };
    ModRecepcionEstadoPage.prototype.cargarFaroles = function () {
        this.faroles = [
            { "id": 1, "nombre": "Del. Der.", "observacion": null, "estado": false },
            { "id": 2, "nombre": "Del. Izq.", "observacion": null, "estado": false },
            { "id": 3, "nombre": "Señaliz. Der.", "observacion": null, "estado": false },
            { "id": 4, "nombre": "Señaliz. Izq.", "observacion": null, "estado": false },
            { "id": 5, "nombre": "Estacion. Der.", "observacion": null, "estado": false },
            { "id": 6, "nombre": "Estacion. Izq.", "observacion": null, "estado": false },
            { "id": 7, "nombre": "Tras. Der.", "observacion": null, "estado": false },
            { "id": 8, "nombre": "Tras. Izq.", "observacion": null, "estado": false },
            { "id": 9, "nombre": "Equipo Esp.", "observacion": null, "estado": false },
            { "id": 10, "nombre": "Cadena", "observacion": null, "estado": false },
            { "id": 11, "nombre": "Tensores", "observacion": null, "estado": false },
            { "id": 12, "nombre": "Pala", "observacion": null, "estado": false },
            { "id": 13, "nombre": "Cuñas", "observacion": null, "estado": false },
            { "id": 14, "nombre": "Estrobo", "observacion": null, "estado": false },
            { "id": 15, "nombre": "Caj. Metal", "observacion": null, "estado": false },
            { "id": 16, "nombre": "B. Antivuelco", "observacion": null, "estado": false }
        ];
    };
    ModRecepcionEstadoPage.prototype.cargarInventario = function () {
        this.inventario = [
            { "id": 1, "nombre": "Botiquín", "observacion": null, "estado": false },
            { "id": 2, "nombre": "Ceniceros", "observacion": null, "estado": false },
            { "id": 3, "nombre": "Documentos", "observacion": null, "estado": false },
            { "id": 4, "nombre": "Encendedor", "observacion": null, "estado": false },
            { "id": 5, "nombre": "Espejos", "observacion": null, "estado": false },
            { "id": 6, "nombre": "Extintor", "observacion": null, "estado": false },
            { "id": 7, "nombre": "Gata", "observacion": null, "estado": false },
            { "id": 8, "nombre": "Llave Rueda", "observacion": null, "estado": false },
            { "id": 9, "nombre": "Neu. Aux.", "observacion": null, "estado": false },
            { "id": 10, "nombre": "Pertiga", "observacion": null, "estado": false },
            { "id": 11, "nombre": "Radio", "observacion": null, "estado": false },
            { "id": 12, "nombre": "Tapa Com", "observacion": null, "estado": false },
            { "id": 13, "nombre": "Triangulos", "observacion": null, "estado": false },
            { "id": 14, "nombre": "Varillas", "observacion": null, "estado": false },
            { "id": 15, "nombre": "Tag", "observacion": null, "estado": false },
            { "id": 16, "nombre": "C. Reflectante", "observacion": null, "estado": false }
        ];
    };
    ModRecepcionEstadoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mod-recepcion-estado',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\pages\mod-recepcion-estado\mod-recepcion-estado.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Registrar estado vehículo</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="guardar()">\n          <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n    <ion-grid no-padding>\n      <ion-row>\n        <ion-col col-md-4>\n          <ion-card center >\n            <ion-card-header text-center>\n              Carrocería\n            </ion-card-header>\n            <!-- Contenido -->\n            <ion-card-content no-padding>\n              <ion-list>\n                <ion-item *ngFor="let car of carroceria">\n                  <ion-label>{{ car.nombre }}</ion-label>\n                  <ion-checkbox checked="{{ car.estado }}"></ion-checkbox>\n                </ion-item>               \n              </ion-list>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n        <ion-col col-md-4>\n          <ion-card center>\n            <ion-card-header text-center>\n              Faroles\n            </ion-card-header>\n            <!-- Contenido -->\n            <ion-card-content no-padding>\n              <ion-list>\n                <ion-item *ngFor="let far of faroles">\n                  <ion-label>{{ far.nombre }}</ion-label>\n                  <ion-checkbox checked="{{ far.estado }}"></ion-checkbox>\n                </ion-item>\n              </ion-list>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n        <ion-col col-md-4>\n          <ion-card center>\n            <ion-card-header text-center>\n              Inventario\n            </ion-card-header>\n            <!-- Contenido -->\n            <ion-card-content no-padding>\n              <ion-list>\n                <ion-item *ngFor="let inv of inventario">\n                  <ion-label>{{ inv.nombre }}</ion-label>\n                  <ion-checkbox checked="{{ inv.estado }}"></ion-checkbox>\n                </ion-item>\n              </ion-list>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n</ion-content>\n<ion-footer>\n  <button ion-button full color="primary" (click)="guardar()">Guardar</button>\n</ion-footer>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\pages\mod-recepcion-estado\mod-recepcion-estado.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], ModRecepcionEstadoPage);
    return ModRecepcionEstadoPage;
}());

//# sourceMappingURL=mod-recepcion-estado.js.map

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModRecepcionDocumentacionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModRecepcionDocumentacionPage = /** @class */ (function () {
    function ModRecepcionDocumentacionPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.fechaRevisionTec = new Date().toISOString();
        this.fechaRevGases = new Date().toISOString();
        this.fechaPermCirculacion = new Date().toISOString();
        this.fechaSeguroObl = new Date().toISOString();
    }
    ModRecepcionDocumentacionPage.prototype.ionViewDidLoad = function () { };
    ModRecepcionDocumentacionPage.prototype.guardar = function () {
        this.viewCtrl.dismiss();
    };
    ModRecepcionDocumentacionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mod-recepcion-documentacion',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\pages\mod-recepcion-documentacion\mod-recepcion-documentacion.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Registar documentos del vehículo</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="guardar()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content no-padding>\n  <ion-item>\n    <ion-label item-start color="primary">Revisión Técnica</ion-label>\n    <button item-end ion-button outline icon-right (click)="picker.open()">\n      <ion-icon name="calendar"></ion-icon>\n      <ion-datetime #picker pickerFormat="DD MMMM YYYY HH:mm" \n      min="2017" \n      max="2030" \n      cancelText="Cancelar" \n      doneText="Aceptar" \n      [(ngModel)]="fechaRevisionTec">\n    </ion-datetime>\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <ion-label item-start color="primary">Revisión de Gases</ion-label>\n    <button item-end ion-button outline icon-right (click)="picker.open()">\n      <ion-icon name="calendar"> </ion-icon>\n      <ion-datetime #picker pickerFormat="DD MMMM YYYY HH:mm" \n      min="2017" \n      max="2030" \n      cancelText="Cancelar" \n      doneText="Aceptar" \n      [(ngModel)]="fechaRevGases">\n    </ion-datetime>\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <ion-label item-start color="primary">Permiso de Circulación</ion-label>\n    <button item-end ion-button outline icon-right (click)="picker.open()">\n      <ion-icon name="calendar"> </ion-icon>\n      <ion-datetime #picker pickerFormat="DD MMMM YYYY HH:mm" \n      min="2017" \n      max="2030" \n      cancelText="Cancelar" \n      doneText="Aceptar" \n      [(ngModel)]="fechaPermCirculacion">\n    </ion-datetime>\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <ion-label item-start color="primary">Seguro Obligatorio</ion-label>\n    <button item-end ion-button outline icon-right (click)="picker.open()">\n      <ion-icon name="calendar"> </ion-icon>\n      <ion-datetime #picker pickerFormat="DD MMMM YYYY HH:mm" \n      min="2017" \n      max="2030" \n      cancelText="Cancelar" \n      doneText="Aceptar" \n      [(ngModel)]="fechaSeguroObl">\n    </ion-datetime>\n    </button>\n  </ion-item>\n</ion-content>\n\n<ion-footer>\n  <button ion-button full color="primary" (click)="guardar()">Guardar</button>\n</ion-footer>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\pages\mod-recepcion-documentacion\mod-recepcion-documentacion.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], ModRecepcionDocumentacionPage);
    return ModRecepcionDocumentacionPage;
}());

//# sourceMappingURL=mod-recepcion-documentacion.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TareasPendientesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tareas_iniciar_tareas_iniciar__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_menu_principal_menu_principal__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TareasPendientesPage = /** @class */ (function () {
    function TareasPendientesPage(navCtrl, navParams, popoverCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.modalCtrl = modalCtrl;
        this.tareasPendientes = [];
        this.fecha = new Date().toISOString();
        this.cargarPendientes();
    }
    TareasPendientesPage.prototype.ionViewDidLoad = function () { };
    TareasPendientesPage.prototype.cargarPendientes = function () {
        this.tareasPendientes = [
            { id: 1, patente: "BBDD99", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "Mazda 3 S 2.0", prioridad: 1, estado: 1 },
            { id: 2, patente: "BBDD01", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "Jeep", prioridad: 1, estado: 1 },
            { id: 3, patente: "BBDD02", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "BMW RX-5", prioridad: 1, estado: 1 },
            { id: 4, patente: "BBDD03", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "Nissan Terrano", prioridad: 1, estado: 1 },
            { id: 5, patente: "BBDD04", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "Montero Sport", prioridad: 1, estado: 1 },
            { id: 6, patente: "BBDD05", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "Mazda 3 S 2.0", prioridad: 1, estado: 2 },
            { id: 7, patente: "BBDD06", descripcion: "Pulir el auto", fecha: "22/04/2018", hora: "16:10", modelo: "ToyotaTercel 1.6", prioridad: 2, estado: 1 },
            { id: 8, patente: "BBDD07", descripcion: "Pulir el auto", fecha: "22/04/2018", hora: "16:10", modelo: "Mazda 2", prioridad: 2, estado: 1 }
        ];
    };
    TareasPendientesPage.prototype.mostrarMenu = function (evento) {
        var menu = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__components_menu_principal_menu_principal__["a" /* MenuPrincipalComponent */]);
        menu.present({ ev: evento });
    };
    TareasPendientesPage.prototype.cambiarBusqueda = function () {
        this.esBusqueda = !this.esBusqueda;
    };
    TareasPendientesPage.prototype.seleccionaTarea = function (tarea) {
        console.log('Tarea ' + tarea.descripcion);
        var modalTarea = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_0__tareas_iniciar_tareas_iniciar__["a" /* TareasIniciarPage */], { tarea: tarea }, { enableBackdropDismiss: false });
        modalTarea.present();
    };
    TareasPendientesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-tareas-pendientes',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\pages\tareas-pendientes\tareas-pendientes.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title *ngIf="!esBusqueda">Tareas pendientes</ion-title>\n    <!-- Boton d emenu -->\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n\n    <!-- Barra de busqueda -->\n    <ion-searchbar *ngIf="esBusqueda"\n      cancelButtonText="Cancelar"   \n      placeholder="Buscar tareas...">\n    </ion-searchbar>\n\n    <!-- Iconos de la barra -->\n    <ion-buttons right>\n      <button ion-button icon-only *ngIf="!esBusqueda" (click)="cambiarBusqueda()">\n        <ion-icon name="search"></ion-icon>\n      </button>\n      <button ion-button icon-only *ngIf="esBusqueda" (click)="cambiarBusqueda()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n      <button ion-button icon-only (click)="mostrarMenu($event)">\n        <ion-icon name="more"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <!-- Iteracion del arreglo -->\n    <ion-card *ngFor="let item of tareasPendientes" (click)="seleccionaTarea(item)">\n      <ion-card-header>\n        <ion-grid>\n          <ion-row>\n            <ion-col col-10><strong>{{ item.patente }}</strong></ion-col>\n            <ion-col col-2>\n              <button ion-button small clear float-right (click)="mostrarMenu($event)">\n                <ion-icon name="more"></ion-icon>\n              </button>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-card-header>\n      <!-- Contenido tarjeta -->\n      <ion-card-content>\n        <strong>Actividad </strong> {{ item.descripcion }}\n        <strong>Fecha </strong> {{ item.fecha }}\n        <strong>Hora </strong> {{ item.hora }}\n        <strong>Modelo </strong> {{ item.modelo }}\n      </ion-card-content>\n      <!-- Estado tarjeta -->\n      <footer class="estado{{item.estado}}">\n      </footer>\n    </ion-card>\n</ion-content>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\pages\tareas-pendientes\tareas-pendientes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* ModalController */]])
    ], TareasPendientesPage);
    return TareasPendientesPage;
}());

//# sourceMappingURL=tareas-pendientes.js.map

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TareasRealizadasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_menu_principal_menu_principal__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TareasRealizadasPage = /** @class */ (function () {
    function TareasRealizadasPage(navCtrl, navParams, popoverCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.modalCtrl = modalCtrl;
        this.tareasRealizadas = [];
        this.fecha = new Date().toISOString();
        this.cargaRealizadas();
    }
    TareasRealizadasPage.prototype.ionViewDidLoad = function () { };
    TareasRealizadasPage.prototype.cargaRealizadas = function () {
        this.tareasRealizadas = [
            { id: 1, patente: "BBDD99", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "Mazda 3 S 2.0", prioridad: 1 },
            { id: 2, patente: "BBDD99", descripcion: "Pulido completo", fecha: "22/04/2018", hora: "16:10", modelo: "ToyotaTercel 1.6", prioridad: 2 }
        ];
    };
    TareasRealizadasPage.prototype.cambiarBusqueda = function () {
        this.esBusqueda = !this.esBusqueda;
    };
    TareasRealizadasPage.prototype.mostrarMenuPrincipal = function (evento) {
        var menu = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_menu_principal_menu_principal__["a" /* MenuPrincipalComponent */]);
        menu.present({ ev: evento });
    };
    TareasRealizadasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tareas-realizadas',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\pages\tareas-realizadas\tareas-realizadas.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title *ngIf="!esBusqueda">Tareas realizadas</ion-title>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n\n      <ion-searchbar *ngIf="esBusqueda"\n        cancelButtonText="Cancelar"\n        placeholder="Buscar tareas...">\n      </ion-searchbar>\n\n\n      <ion-buttons right>\n        <button ion-button icon-only *ngIf="!esBusqueda" (click)="cambiarBusqueda()">\n          <ion-icon name="search"></ion-icon>\n        </button>\n        <button ion-button icon-only *ngIf="esBusqueda" (click)="cambiarBusqueda()">\n          <ion-icon name="close"></ion-icon>\n        </button>\n        <button ion-button icon-only (click)="mostrarMenuPrincipal($event)">\n          <ion-icon name="more"></ion-icon>\n        </button>\n      </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<!-- Contenedor de la vista -->\n<ion-content>\n  <!-- Seleccion de fecha -->\n  <ion-card>\n    <ion-item>\n      <ion-label>Seleccione fecha</ion-label>\n      <ion-datetime [(ngModel)]="fecha" min="2018" max="2030" displayFormat="MMMM YYYY" cancelText="Cancelar" doneText="Aceptar">\n      </ion-datetime>\n    </ion-item>\n  </ion-card>\n\n  <!-- Iteracion de tareas realizadas -->\n  <ion-card *ngFor="let tarea of tareasRealizadas">\n      <ion-card-header>\n        <ion-grid>\n          <ion-row>\n            <ion-col col-10><strong>{{ tarea.patente }}</strong></ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-card-header>\n    <!-- Contenido tarjeta -->\n    <ion-card-content>\n        <strong>Actividad </strong> {{ tarea.descripcion }}\n        <strong>Fecha finalizada </strong> {{ tarea.fecha }}\n        <strong>Hora finalizada</strong> {{ tarea.hora }}\n        <strong>Modelo </strong> {{ tarea.modelo }}\n    </ion-card-content>\n    <!-- Footer tarjeta -->\n    <footer class="estado{{tarea.estado}}">\n    </footer>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\pages\tareas-realizadas\tareas-realizadas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]])
    ], TareasRealizadasPage);
    return TareasRealizadasPage;
}());

//# sourceMappingURL=tareas-realizadas.js.map

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(398);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_tareas_iniciar_tareas_iniciar__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_mod_tarea_mod_tarea__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(439);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_sqlite__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common_http__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_mod_recepcion_estado_mod_recepcion_estado__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_mod_recepcion_documentacion_mod_recepcion_documentacion__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_menu_principal_menu_principal__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_login_login__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_home_home__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_recepcion_ingreso_recepcion_ingreso__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_recepcion_recepcion__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_orden_trabajo_orden_trabajo__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_reportes_reportes__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_orden_trabajo_ingreso_orden_trabajo_ingreso__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_tareas_realizadas_tareas_realizadas__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_tareas_pendientes_tareas_pendientes__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_autenticacion_autenticacion__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__providers_database_database__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_menu_card_recepcion_menu_card_recepcion__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__components_menu_card_ot_menu_card_ot__ = __webpack_require__(273);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


//Modulos








//Modals


//Menus

//Paginas









//Providers




var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_13__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_recepcion_recepcion__["a" /* RecepcionPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_recepcion_ingreso_recepcion_ingreso__["a" /* RecepcionIngresoPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_orden_trabajo_orden_trabajo__["a" /* OrdenTrabajoPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_orden_trabajo_ingreso_orden_trabajo_ingreso__["a" /* OrdenTrabajoIngresoPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_reportes_reportes__["a" /* ReportesPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_mod_recepcion_estado_mod_recepcion_estado__["a" /* ModRecepcionEstadoPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_mod_recepcion_documentacion_mod_recepcion_documentacion__["a" /* ModRecepcionDocumentacionPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_mod_tarea_mod_tarea__["a" /* ModTareaPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_tareas_pendientes_tareas_pendientes__["a" /* TareasPendientesPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_tareas_realizadas_tareas_realizadas__["a" /* TareasRealizadasPage */],
                __WEBPACK_IMPORTED_MODULE_0__pages_tareas_iniciar_tareas_iniciar__["a" /* TareasIniciarPage */],
                __WEBPACK_IMPORTED_MODULE_12__components_menu_principal_menu_principal__["a" /* MenuPrincipalComponent */],
                __WEBPACK_IMPORTED_MODULE_24__components_menu_card_recepcion_menu_card_recepcion__["a" /* MenuCardRecepcionComponent */],
                __WEBPACK_IMPORTED_MODULE_25__components_menu_card_ot_menu_card_ot__["a" /* MenuCardOtComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthShortNames: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
                    dayNames: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
                    dayShortNames: ['lun', 'mar', 'mie', 'jue', 'vie', 'sab', 'dom'],
                }, {
                    links: []
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_13__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_recepcion_recepcion__["a" /* RecepcionPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_recepcion_ingreso_recepcion_ingreso__["a" /* RecepcionIngresoPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_orden_trabajo_orden_trabajo__["a" /* OrdenTrabajoPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_orden_trabajo_ingreso_orden_trabajo_ingreso__["a" /* OrdenTrabajoIngresoPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_reportes_reportes__["a" /* ReportesPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_tareas_pendientes_tareas_pendientes__["a" /* TareasPendientesPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_tareas_realizadas_tareas_realizadas__["a" /* TareasRealizadasPage */],
                __WEBPACK_IMPORTED_MODULE_0__pages_tareas_iniciar_tareas_iniciar__["a" /* TareasIniciarPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_mod_recepcion_estado_mod_recepcion_estado__["a" /* ModRecepcionEstadoPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_mod_recepcion_documentacion_mod_recepcion_documentacion__["a" /* ModRecepcionDocumentacionPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_mod_tarea_mod_tarea__["a" /* ModTareaPage */],
                __WEBPACK_IMPORTED_MODULE_12__components_menu_principal_menu_principal__["a" /* MenuPrincipalComponent */],
                __WEBPACK_IMPORTED_MODULE_24__components_menu_card_recepcion_menu_card_recepcion__["a" /* MenuCardRecepcionComponent */],
                __WEBPACK_IMPORTED_MODULE_25__components_menu_card_ot_menu_card_ot__["a" /* MenuCardOtComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_sqlite__["a" /* SQLite */],
                { provide: __WEBPACK_IMPORTED_MODULE_3__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_22__providers_autenticacion_autenticacion__["a" /* AutenticacionProvider */],
                __WEBPACK_IMPORTED_MODULE_23__providers_database_database__["a" /* DatabaseProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 439:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_database_database__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_login_login__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_reportes_reportes__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_orden_trabajo_orden_trabajo__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_recepcion_recepcion__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_tareas_pendientes_tareas_pendientes__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_tareas_realizadas_tareas_realizadas__ = __webpack_require__(281);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//PAGINAS






var MyApp = /** @class */ (function () {
    function MyApp(platform, eventos, statusBar, splashScreen, db, menu) {
        var _this = this;
        this.platform = platform;
        this.eventos = eventos;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.db = db;
        this.menu = menu;
        this.paginasAdmin = [
            { titulo: 'Inicio', component: __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */], icono: "home" },
            { titulo: 'Recepción vehículos', component: __WEBPACK_IMPORTED_MODULE_9__pages_recepcion_recepcion__["a" /* RecepcionPage */], icono: "calendar" },
            { titulo: 'Orden trabajo vehículos', component: __WEBPACK_IMPORTED_MODULE_8__pages_orden_trabajo_orden_trabajo__["a" /* OrdenTrabajoPage */], icono: "document" },
            { titulo: 'Reportes', component: __WEBPACK_IMPORTED_MODULE_7__pages_reportes_reportes__["a" /* ReportesPage */], icono: "print" }
        ];
        this.paginasUsuario = [
            { titulo: 'Inicio', component: __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */], icono: "home" },
            { titulo: 'Tareas pendientes', component: __WEBPACK_IMPORTED_MODULE_10__pages_tareas_pendientes_tareas_pendientes__["a" /* TareasPendientesPage */], icono: "alarm" },
            { titulo: 'Tareas realizadas', component: __WEBPACK_IMPORTED_MODULE_11__pages_tareas_realizadas_tareas_realizadas__["a" /* TareasRealizadasPage */], icono: "done-all" }
        ];
        this.iconoUsuario = "./assets/imgs/man.png";
        this.initializeApp();
        eventos.subscribe('obtenerUsuario', function () {
            _this.obtieneUsuario();
        });
        eventos.subscribe('inicializa_app', function () {
            _this.initializeApp();
        });
        eventos.subscribe('verifica_ruta', function () {
            console.log('Pasa por el verifica ruta');
            _this.verificaUsuarioRuta();
        });
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.verificaUsuarioRuta();
            _this.statusBar.backgroundColorByHexString("#264865");
            _this.splashScreen.hide();
            _this.verificaDispositivo();
        });
    };
    MyApp.prototype.verificaDispositivo = function () {
        if (this.platform.is('cordova')) {
            this.esDispositivo = true;
            console.log('DISPOSITIVO');
        }
        else {
            this.esDispositivo = false;
            console.log('NAVEGADOR');
        }
    };
    MyApp.prototype.verificaUsuarioRuta = function () {
        var usuario;
        var cargo;
        usuario = parseInt(localStorage.getItem('login'));
        cargo = parseInt(localStorage.getItem('cod_cargo'));
        if (usuario) {
            switch (cargo) {
                case 11: {
                    console.log('CARGO: ES ADMIN');
                    this.pages = this.paginasAdmin;
                    break;
                }
                case 2: {
                    console.log('CARGO: USUARIO');
                    this.pages = this.paginasUsuario;
                    break;
                }
                default: {
                    console.log('CARGO: NO DEFINIDO');
                    this.pages = [{ titulo: 'Inicio', component: __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */], icono: "home" }];
                    break;
                }
            }
            console.log('USUARIO LOGUEADO : MENU HABILITADO');
            this.openPage(this.pages[0]);
            this.menu.enable(true);
        }
        else {
            console.log('USUARIO NO LOGUEADO : MENU DESHABILIADO');
            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_1__pages_login_login__["a" /* LoginPage */]);
            this.menu.enable(false);
        }
    };
    MyApp.prototype.obtieneUsuario = function () {
        var _this = this;
        this.usuarioMenu = null;
        this.perfilMenu = null;
        this.db.obtenerUsuario()
            .then(function (data) {
            _this.usuarioMenu = data[0]['nombres'];
            _this.perfilMenu = data[0]['des_cargo'];
        }).catch(function (error) {
            console.log('ERROR USUARIO BD');
            _this.usuarioMenu = 'Usuario';
            _this.perfilMenu = 'Perfil';
        });
    };
    MyApp.prototype.openPage = function (page) {
        console.log('ABRIR PAGINA ' + JSON.stringify(page));
        this.nav.setRoot(page.component);
        this.activePage = page;
    };
    MyApp.prototype.verificaPagina = function (page) {
        return page === this.activePage;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\app\app.html"*/'<ion-menu [content]="content" width="35">\n    \n  <ion-content class="menu-content fondo">\n    <!-- <ion-card text-center class="hide-card"> -->\n      <div text-center class="cabecera">\n        <br>\n        <img [src]=iconoUsuario class="custom-avatar" />\n        <h2>{{ usuarioMenu }}</h2>\n        <p>{{ perfilMenu }}</p>\n        <br>\n      </div>\n      <!-- </ion-card> -->\n      <ion-list  no-lines>\n      <br>\n      <button class="fondo"  menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)" [class.active]="verificaPagina(p)">\n        <ion-icon [name]="p.icono" item-left></ion-icon>\n        {{p.titulo}}\n      </button>\n    </ion-list> \n  </ion-content>\n</ion-menu>\n\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\ionicproyectos\magicCar\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_0__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* MenuController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuarioModel; });
var UsuarioModel = /** @class */ (function () {
    function UsuarioModel(codigo, cod_cargo, des_cargo, cod_comuna, rut, nombres, ap_paterno, ap_materno, direccion, email, fecha_nac, telefono) {
        this.codigo = codigo;
        this.cod_cargo = cod_cargo;
        this.des_cargo = des_cargo;
        this.cod_comuna = cod_comuna;
        this.rut = rut;
        this.nombres = nombres;
        this.ap_paterno = ap_paterno;
        this.ap_materno = ap_materno;
        this.direccion = direccion;
        this.email = email;
        this.fecha_nac = fecha_nac;
        this.telefono = telefono;
    }
    return UsuarioModel;
}());

//# sourceMappingURL=usuario.js.map

/***/ }),

/***/ 48:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuPrincipalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_database_database__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(147);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MenuPrincipalComponent = /** @class */ (function () {
    function MenuPrincipalComponent(platform, viewControl, nav, app, alertCtrl, db, eventos, toast) {
        this.platform = platform;
        this.viewControl = viewControl;
        this.nav = nav;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.db = db;
        this.eventos = eventos;
        this.toast = toast;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */];
        if (this.platform.is('cordova')) {
            this.esDispositivo = true;
        }
        else {
            this.esDispositivo = false;
        }
        this.items = [
            { id: 1, titulo: 'Cerrar sesión' }
        ];
    }
    MenuPrincipalComponent.prototype.clickItem = function (item) {
        console.log(item.id);
        switch (item.id) {
            case 1: {
                this.mostrarAlerta();
                break;
            }
            default: {
                break;
            }
        }
        this.viewControl.dismiss(item);
    };
    MenuPrincipalComponent.prototype.mostrarAlerta = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Cerrar sesión',
            message: '¿Está seguro que quiere cerrar la sesión',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancela');
                    }
                }, {
                    text: 'Si',
                    handler: function () {
                        _this.cerrarSesion();
                    }
                }
            ]
        });
        alert.present();
    };
    MenuPrincipalComponent.prototype.cerrarSesion = function () {
        var _this = this;
        //Si es dipositivo elimina de SQLite
        if (this.esDispositivo) {
            this.db.borrarUsuario().
                then(function () {
                console.log('USUARIO ELIMINADO ');
                localStorage.removeItem('login');
                localStorage.removeItem('cod_cargo');
                _this.eventos.publish('verifica_ruta');
            }).catch(function (error) {
                _this.mostrarToast('Error al eliminar usuario de la Base de datos');
                console.log('ERROR AL ELIMINAR CS');
            });
        }
        else {
            console.log('CIERRA SESION EN NAVEGADOR');
            localStorage.removeItem('login');
            localStorage.removeItem('cod_cargo');
            this.eventos.publish('verifica_ruta');
        }
        //     this.app.getRootNav().setRoot(LoginPage);
    };
    MenuPrincipalComponent.prototype.mostrarToast = function (mensaje) {
        var toast = this.toast.create({
            message: mensaje,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    MenuPrincipalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'menu-principal',template:/*ion-inline-start:"C:\ionicproyectos\magicCar\src\components\menu-principal\menu-principal.html"*/'<ion-list no-lines>\n  <ion-item *ngFor="let item of items" \n  (click)="clickItem(item)">\n    {{item.titulo}}\n  </ion-item>\n</ion-list> '/*ion-inline-end:"C:\ionicproyectos\magicCar\src\components\menu-principal\menu-principal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_0__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ToastController */]])
    ], MenuPrincipalComponent);
    return MenuPrincipalComponent;
}());

//# sourceMappingURL=menu-principal.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatabaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(264);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DatabaseProvider = /** @class */ (function () {
    function DatabaseProvider(storage, platform) {
        var _this = this;
        this.storage = storage;
        this.platform = platform;
        this.crea_usuario = "CREATE TABLE IF NOT EXISTS usuario " +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "codigo INTEGER, " +
            "cod_cargo INTEGER, " +
            "des_cargo TEXT, " +
            "cod_comuna INTEGER, " +
            "rut TEXT, " +
            "nombres TEXT, " +
            "ap_paterno TEXT, " +
            "ap_materno TEXT, " +
            "direccion TEXT, " +
            "email TEXT, " +
            "fecha_nac TEXT, " +
            "telefono TEXT)";
        this.crea_rec_cabecera = "CREATE TABLE IF NOT EXISTS rec_cabecera " +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "codigo INTEGER, " +
            "patente TEXT, " +
            "rut TEXT, " +
            "cod_empresa INTEGER, " +
            "fecha TEXT, " +
            "tipo TEXT, ";
        if (!this.isOpen) {
            this.storage = new __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */]();
            // Crear base de datos
            this.storage.create({
                name: "magic.db",
                location: "default"
            }).then(function (db) {
                console.log('Base de datos creada');
                _this.db = db;
                db.executeSql(_this.crea_usuario, []);
                _this.isOpen = true;
            })
                .catch(function (error) {
                console.log(error);
            });
        }
    }
    /////////////////////
    // METODOS USUARIO //
    /////////////////////
    DatabaseProvider.prototype.crearUsuario = function (usuario) {
        var _this = this;
        var sql = 'INSERT INTO usuario(codigo, cod_cargo, des_cargo, cod_comuna, rut, nombres, ap_paterno, ap_materno, direccion, email, fecha_nac, telefono) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)';
        return new Promise(function (resolve, reject) {
            _this.db.executeSql(sql, [
                usuario.codigo,
                usuario.cod_cargo,
                usuario.des_cargo,
                usuario.cod_comuna,
                usuario.rut,
                usuario.nombres,
                usuario.ap_paterno,
                usuario.ap_materno,
                usuario.direccion,
                usuario.email,
                usuario.fecha_nac,
                usuario.telefono
            ]).then(function (data) {
                resolve(data);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DatabaseProvider.prototype.obtenerUsuario = function () {
        var _this = this;
        var sql = "SELECT * FROM usuario";
        return new Promise(function (resolve, reject) {
            _this.db.executeSql(sql, null)
                .then(function (data) {
                var usuarioArr = [];
                if (data.rows.length > 0) {
                    for (var i = 0; i < data.rows.length; i++) {
                        usuarioArr.push({
                            codigo: data.rows.item(i).codigo,
                            cod_cargo: data.rows.item(i).cod_cargo,
                            des_cargo: data.rows.item(i).des_cargo,
                            cod_comuna: data.rows.item(i).codigo,
                            rut: data.rows.item(i).rut,
                            nombres: data.rows.item(i).nombres,
                            ap_paterno: data.rows.item(i).ap_paterno,
                            ap_materno: data.rows.item(i).ap_materno,
                            direccion: data.rows.item(i).direccion,
                            email: data.rows.item(i).email,
                            fecha_nac: data.rows.item(i).fecha_nac,
                            telefono: data.rows.item(i).telefono
                        });
                    }
                }
                resolve(usuarioArr);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    DatabaseProvider.prototype.borrarUsuario = function () {
        var _this = this;
        var sql = "DELETE FROM usuario";
        return new Promise(function (resolve, reject) {
            _this.db.executeSql(sql, null)
                .then(function () {
                resolve();
            })
                .catch(function (err) {
                console.log('Error Eliminar BD ' + err);
                reject(err);
            });
        });
    };
    ////////////////////////////////
    // METODOS CABECERA RECEPCION //
    ////////////////////////////////
    DatabaseProvider.prototype.crearRecCabecera = function (cabecera) {
        var _this = this;
        var sql = 'INSERT INTO rec_cabecera(codigo, patente, rut, cod_empresa, fecha, tipo, estado) VALUES (?,?,?,?,?,?,?)';
        return new Promise(function (resolve, reject) {
            _this.db.executeSql(sql, [
                cabecera.codigo,
                cabecera.patente,
                cabecera.rut,
                cabecera.cod_empresa,
                cabecera.fecha,
                cabecera.tipo,
                cabecera.estado
            ]).then(function (data) {
                resolve(data);
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    DatabaseProvider.prototype.editarRecCabecera = function (cabecera) {
    };
    DatabaseProvider.prototype.listarRecCabecera = function () {
        var _this = this;
        var sql = 'SELECT * FROM rec_cabecera ORDER BY id DESC';
        return new Promise(function (resolve, reject) {
            _this.db.executeSql(sql, [])
                .then(function (data) {
                var cebecerasArray = [];
                if (data.rows.length > 0) {
                    for (var i = 0; i < data.rows.length; i++) {
                        cebecerasArray.push({
                            id: data.rows.item(i).id,
                            codigo: data.rows.item(i).codigo,
                            patente: data.rows.item(i).patente,
                            rut: data.rows.item(i).rut,
                            cod_empresa: data.rows.item(i).cod_empresa,
                            fecha: data.rows.item(i).fecha,
                            tipo: data.rows.item(i).tipo,
                            estado: data.rows.item(i).estado
                        });
                    }
                }
                resolve(cebecerasArray);
            }, function (error) {
                reject(error);
            });
        });
    };
    DatabaseProvider.prototype.eliminarRecCabecera = function (id) {
        var _this = this;
        var sql = 'DELETE FROM rec_cabecera WHERE id =' + id;
        return new Promise(function (resolve, reject) {
            _this.db.executeSql(sql, [])
                .then(function (data) {
                console.log('Se elimina cabecera id ' + id);
                resolve(data);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    DatabaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["n" /* Platform */]])
    ], DatabaseProvider);
    return DatabaseProvider;
}());

//# sourceMappingURL=database.js.map

/***/ })

},[282]);
//# sourceMappingURL=main.js.map