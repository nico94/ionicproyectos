import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class WebserviceProvider {
  ip: string = null;
  urlCargarOT: string;
  cabeceras: any;

  constructor(
    public http: HttpClient
  ) {
    this.refrescarIP();
    this.cabeceras = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  refrescarIP() {
    // let ipServer = localStorage.getItem('ip'); // IP AL SERVER 100
    let ipserver = '192.168.0.50'; // IP DE CRISTIAN
    this.ip = ipserver;
    if (this.ip) {
      this.urlCargarOT = 'http://' + this.ip + 'ws/';
    }
  }

  listarRecepciones() {
    // let data = [
    //   {
    //     id: 1,
    //     patente: "AA2312",
    //     rut: "18.682.101-7",
    //     empresa: "Linderos",
    //     fec_rec: "11 jun 2018",
    //     tipo: "D&P",
    //     estado: 1
    //   }, {
    //     id: 2,
    //     patente: "BB1234",
    //     rut: "18.682.101-7",
    //     empresa: "Linderos",
    //     fec_rec: "9 jun 2018",
    //     tipo: "Mecánica",
    //     estado: 2
    //   }
    // ];
    // return this.data.   
  }

  cargarOT() {

  }
}