import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import 'rxjs/add/observable/interval';

@Component({
  selector: 'page-tareas-iniciar',
  templateUrl: 'tareas-iniciar.html',
})
export class TareasIniciarPage {
  tarea:any[]=[];
  timerVar;
  timerVal;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public parametros: NavParams
  ){
    this.cargaTarea();
  }

  ionViewDidLoad() {}

  cargaTarea(){
    this.tarea = this.navParams.get('tarea');   
  }
  
  iniciarTarea(){
    // this.timerVar = Observable.interval(1000).subscribe(x=>{
    //   //console.log(x);
    //   this.timerVal = x;
    // });
  }

  cerrar(){
    this.viewCtrl.dismiss();
  }
}