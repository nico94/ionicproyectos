import { UsuarioModel } from './../../modelos/usuario';
import { DatabaseProvider } from './../../providers/database/database';
import { Component } from '@angular/core';
import { NavController, NavParams, Platform, LoadingController, Events } from 'ionic-angular';
import { AutenticacionProvider } from './../../providers/autenticacion/autenticacion';
import { ToastController } from 'ionic-angular';
import { Md5 } from 'ts-md5/dist/md5';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user: any[] = [];
  usuario: string;
  password: string;
  passwordMD5: any;
  logo: string = "./assets/imgs/LogoSolo.png";
  esDispositivo: boolean = false;
  cargando: any;

  constructor(
    public toast: ToastController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AutenticacionProvider,
    public plataforma: Platform,
    public loadingCtrl: LoadingController,
    public db: DatabaseProvider,
    public eventos: Events
  ) {
    if (this.plataforma.is('cordova')) {
      this.esDispositivo = true;
    } else {
      this.esDispositivo = false;
    }
  }

  ionViewDidLoad() { }

  iniciarSesion() {
    if (this.usuario != null) {
      if (this.password != null) {
        this.mostrarCarga();
        this.passwordMD5 = Md5.hashAsciiStr(this.password);
        this.auth.iniciarSesion(this.usuario, this.passwordMD5).subscribe(
          data => {
            this.user = data['InciarSesionResult'];
            if (this.user['estado']) {
              let usuModel = new UsuarioModel(
                this.user['codigo'],
                this.user['cod_cargo'],
                this.user['des_cargo'],
                this.user['cod_comuna'],
                this.user['rut'],
                this.user['nombres'],
                this.user['ap_paterno'],
                this.user['ap_materno'],
                this.user['direccion'],
                this.user['email'],
                this.user['fecha_nac'],
                this.user['telefono']
              );

              if (this.esDispositivo) {
                this.db.crearUsuario(usuModel).then((data) => {
                  localStorage.setItem('login', this.user['codigo']);
                  localStorage.setItem('cod_cargo', this.user['cod_cargo']);
                  this.eventos.publish('verifica_ruta');
                }).catch((error) => {
                  this.mostrarToast('Error en base de datos');
                });
              } else {
                localStorage.setItem('login', this.user['codigo']);
                localStorage.setItem('cod_cargo', this.user['cod_cargo']);
                this.eventos.publish('verifica_ruta');
              }
              this.cargando.dismiss();
            } else {
              this.cargando.dismiss();
              this.mostrarToast('Usuario/contraseña incorrectos');
            }
          },
          error => {
            this.cargando.dismiss();
            this.mostrarToast('Error en servidor');
          });
      } else {
        this.mostrarToast('Ingrese contraseña');
      }
    } else {
      this.mostrarToast('Ingrese usuario');
    }
  }

  mostrarCarga() {
    this.cargando = this.loadingCtrl.create({
      content: "Iniciando sesión..."
    });
    this.cargando.present();
  }

  mostrarToast(mensaje: string) {
    let toast = this.toast.create({
      message: mensaje,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
}