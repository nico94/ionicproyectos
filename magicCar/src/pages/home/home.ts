import { DatabaseProvider } from './../../providers/database/database';
import { MenuPrincipalComponent } from './../../components/menu-principal/menu-principal';
import { Component } from '@angular/core';
import { NavController, PopoverController, Events, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  url:any;
  urlLimpia:string;

  constructor(
    public navCtrl: NavController,
    public eventos: Events,
    public popoverCtrl: PopoverController,
    public domSanitizer: DomSanitizer,
    public db:DatabaseProvider,
    public navParams: NavParams
  ) {
    eventos.publish('obtenerUsuario');
  }
  
  ionViewDidLoad() {
    this.urlLimpia="http://186.10.19.170/dashboardmap/visor.aspx?id=1&index=1";
    this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(this.urlLimpia);
  }

  mostrarMenu(evento){
    let menu = this.popoverCtrl.create(MenuPrincipalComponent);
    menu.present({
      ev: evento
    });
  }
}