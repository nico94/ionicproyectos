import { ModTareaPage } from './../mod-tarea/mod-tarea';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController } from 'ionic-angular';

@Component({
  selector: 'page-orden-trabajo-ingreso',
  templateUrl: 'orden-trabajo-ingreso.html',
})
export class OrdenTrabajoIngresoPage {
  fechaIngreso: any = new Date().toISOString();
  seleccionEstado: any;
  tareas:any[]=[];

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public alertCtrl: AlertController
  ){
    this.cargaTareas();
  } 

  ionViewDidLoad() {}

  editarTarea(tarea:any){
    let modalEdit = this.modalCtrl.create(ModTareaPage, { tarea: tarea }, { enableBackdropDismiss: false });
    modalEdit.present();
  }
  
  agregarTarea(){
    let modalAdd = this.modalCtrl.create(ModTareaPage, { tarea: null }, { enableBackdropDismiss: false });
    modalAdd.present();
  }

  // mostrarModal(tipo:number, tarea:any){
  //   let alert = null; 
    
  //   if(tipo == 1 && tarea){
  //     alert = this.alertCtrl.create({ enableBackdropDismiss: false });
  //     alert.setTitle('Editar tarea');
  //     alert.addInput({ type: 'input', placeholder: 'Descripcion', value: tarea.descripcion });
  //     alert.addInput({ type: 'number', placeholder: 'Valor', value: tarea.valor });
  //     alert.addInput({ type: 'input', placeholder: 'Responsable', value: tarea.responsable });
  //     // alert.addInput({ type: 'radio', label: 'Por iniciar', value: '1', checked: true });
  //     // alert.addInput({ type: 'radio', label: 'En proceso', value: '2' });
  //     alert.addInput({ type: 'checkbox', label: 'Blue', value: 'blue', checked: true });

  //     alert.addButton('Cancelar');
  //     alert.addButton({
  //       text: 'Editar',
  //       handler:(data:any)=>{

  //       }
  //     });

  //     //   {
  //     //   title: 'Editar tarea',
  //     //   message: 'Editando tarea',
  //     //   enableBackdropDismiss: false,
  //     //   inputs: [
  //     //     { name: 'Descipcion', placeholder: 'Descripcion', value: tarea.descripcion },
  //     //     { name: 'Valor', placeholder: 'Valor', value: tarea.valor },
  //     //     { name: 'Responsable', placeholder: 'Responsable', value: tarea.responsable},
  //     //     { name: 'Estado', placeholder: 'Estado', value: tarea.estado},
  //     //     { name: 'Programada', placeholder: 'Programada', value: tarea.mantencion },
  //     //     { type: 'checkbox', label: 'Blue', value: 'blue', checked: true }
  //     //   ],
  //     //   buttons:[
  //     //     { text: 'Cancelar',
  //     //       handler: () => {
  //     //         console.log('Presiona cancelar');
  //     //       }
  //     //     },
  //     //     { text: 'Editar',
  //     //       handler: () => {
  //     //         console.log('Aqui editar');
  //     //       }
  //     //     }
  //     //   ]
  //     // });
  //   }else{
  //     //Es nuevo
  //     alert = this.alertCtrl.create({
  //       title: 'Agregar tarea',
  //       message: 'Añadiendo tarea',
  //       enableBackdropDismiss: false,
  //       inputs: [
  //         { name: 'Descipcion', placeholder: 'Descripcion' },
  //         { name: 'Valor', placeholder: 'Valor' },
  //         { name: 'Responsable', placeholder: 'Responsable' },
  //         { name: 'Estado', placeholder: 'Estado' },
  //         { name: 'Programada', placeholder: 'Programada' }
  //       ],
  //       buttons: [
  //         {
  //           text: 'Cancel',
  //           handler: () => {
  //             console.log('Cancel clicked');
  //           }
  //         },
  //         {
  //           text: 'Agregar',
  //           handler: () => {
  //             console.log('Saved clicked');
  //           }
  //         }
  //       ]
  //     });
  //   }
  //   if(alert){
  //     alert.present();
  //   }
  // }

  eliminarTarea(tarea:any){
    let alert = this.alertCtrl.create({
      title: 'Eliminar actividad',
      message: '¿Está seguro que quiere eliminar '+tarea.descripcion+'?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancela');
          }
        }, {
          text: 'Si',
          handler: () => {
            const indice: number = this.tareas.indexOf(tarea);
            if(indice !== -1){
              this.tareas.splice(indice,1);
            }
          }
        }
      ]
    });
    alert.present();
  }

  cargaTareas(){
    this.tareas=[
      { id:1, descripcion:"Cambiar Filtro purificador de aire",valor:"$9.990", responsable:"Nicolas", estado:"Por iniciar", mantencion:"Si" },
      { id:1, descripcion:"Cambiar aceite motor",valor:"$20.990", responsable:"Nicolas", estado:"Por iniciar", mantencion:"Si" },
      { id:1, descripcion:"Lavar asientos",valor:"$1.990", responsable:"Nicolas", estado:"Por iniciar", mantencion:"Si" },
      { id:1, descripcion:"Inflar neumaticos",valor:"$0", responsable:"Nicolas", estado:"Por iniciar", mantencion:"Si" },
      { id:1, descripcion:"Pintar",valor:"$150.990", responsable:"Nicolas", estado:"Por iniciar", mantencion:"Si" }
    ];
  }
}