import { TareasIniciarPage } from './../tareas-iniciar/tareas-iniciar';
import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, ModalController } from 'ionic-angular';
import { MenuPrincipalComponent } from '../../components/menu-principal/menu-principal';

@Component({
  selector: 'page-tareas-pendientes',
  templateUrl: 'tareas-pendientes.html',
})

export class TareasPendientesPage {
  tareasPendientes:any[]=[];
  esBusqueda:boolean;
  fecha: any = new Date().toISOString();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public modalCtrl: ModalController
  ){
    this.cargarPendientes();
  }

  ionViewDidLoad() {}

  cargarPendientes(){
    this.tareasPendientes = [
      { id: 1, patente: "BBDD99", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "Mazda 3 S 2.0", prioridad: 1, estado: 1},
      { id: 2, patente: "BBDD01", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "Jeep", prioridad: 1, estado: 1},
      { id: 3, patente: "BBDD02", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "BMW RX-5", prioridad: 1, estado: 1},
      { id: 4, patente: "BBDD03", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "Nissan Terrano", prioridad: 1, estado: 1},
      { id: 5, patente: "BBDD04", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "Montero Sport", prioridad: 1, estado: 1},
      { id: 6, patente: "BBDD05", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "Mazda 3 S 2.0", prioridad: 1, estado: 2},
      { id: 7, patente: "BBDD06", descripcion: "Pulir el auto", fecha: "22/04/2018", hora: "16:10", modelo: "ToyotaTercel 1.6", prioridad: 2, estado: 1 },
      { id: 8, patente: "BBDD07", descripcion: "Pulir el auto", fecha: "22/04/2018", hora: "16:10", modelo: "Mazda 2", prioridad: 2, estado: 1 }
    ]
  }

  mostrarMenu(evento) {
    let menu = this.popoverCtrl.create(MenuPrincipalComponent);
    menu.present({ ev: evento });
  }

  cambiarBusqueda(){
    this.esBusqueda = !this.esBusqueda;
  }

  seleccionaTarea(tarea:any){
    console.log('Tarea '+tarea.descripcion);
    let modalTarea = this.modalCtrl.create(TareasIniciarPage,{ tarea: tarea },{enableBackdropDismiss: false});
    modalTarea.present();
  }
}
