import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-mod-tarea',
  templateUrl: 'mod-tarea.html',
})
export class ModTareaPage {
  titulo:string;
  seleccionResponsable:any;
  estados:any[]=[];
  fechaAsiganda:any = new Date().toISOString();
  fechaTermino:any = new Date().toISOString();

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams
  ){
    this.titulo="Editar tarea";
    this.cargaEstados();
  }

  ionViewDidLoad(){}

  cargaEstados(){
    this.estados = [
      { id: 1,nombre : "Por iniciar"},
      { id: 2, nombre: "En proceso"},
      { id: 3, nombre: "Pendiente"}
    ];
  }

  guardar(){
    this.viewCtrl.dismiss();
  }
}