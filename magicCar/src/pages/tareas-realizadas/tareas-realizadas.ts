import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, ModalController } from 'ionic-angular';
import { MenuPrincipalComponent } from '../../components/menu-principal/menu-principal';

@Component({
  selector: 'page-tareas-realizadas',
  templateUrl: 'tareas-realizadas.html',
})

export class TareasRealizadasPage {
  tareasRealizadas:any[]=[];
  esBusqueda:boolean;
  fecha: any = new Date().toISOString();
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public modalCtrl: ModalController
  ){
    this.cargaRealizadas();
  }

  ionViewDidLoad() {}

  cargaRealizadas(){
    this.tareasRealizadas = [
      { id: 1, patente: "BBDD99", descripcion: "Pulido completo", fecha: "20/04/2018", hora: "12:10", modelo: "Mazda 3 S 2.0", prioridad: 1 },
      { id: 2, patente: "BBDD99", descripcion: "Pulido completo", fecha: "22/04/2018", hora: "16:10", modelo: "ToyotaTercel 1.6", prioridad: 2 }
    ]
  }
  
  cambiarBusqueda() {
    this.esBusqueda = !this.esBusqueda;
  }

  mostrarMenuPrincipal(evento){
    let menu = this.popoverCtrl.create(MenuPrincipalComponent);
    menu.present({ ev: evento });
  }
}