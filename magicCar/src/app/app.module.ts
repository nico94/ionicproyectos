import { TareasIniciarPage } from './../pages/tareas-iniciar/tareas-iniciar';
import { ModTareaPage } from './../pages/mod-tarea/mod-tarea';
//Modulos
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { SQLite } from "@ionic-native/sqlite";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';
//Modals
import { ModRecepcionEstadoPage } from './../pages/mod-recepcion-estado/mod-recepcion-estado';
import { ModRecepcionDocumentacionPage } from './../pages/mod-recepcion-documentacion/mod-recepcion-documentacion';
//Menus
import { MenuPrincipalComponent } from './../components/menu-principal/menu-principal';
//Paginas
import { LoginPage } from './../pages/login/login';
import { HomePage } from '../pages/home/home';
import { RecepcionIngresoPage } from './../pages/recepcion-ingreso/recepcion-ingreso';
import { RecepcionPage } from './../pages/recepcion/recepcion';
import { OrdenTrabajoPage } from '../pages/orden-trabajo/orden-trabajo';
import { ReportesPage } from './../pages/reportes/reportes';
import { OrdenTrabajoIngresoPage } from '../pages/orden-trabajo-ingreso/orden-trabajo-ingreso';
import { TareasRealizadasPage } from './../pages/tareas-realizadas/tareas-realizadas';
import { TareasPendientesPage } from './../pages/tareas-pendientes/tareas-pendientes';
//Providers
import { AutenticacionProvider } from '../providers/autenticacion/autenticacion';
import { DatabaseProvider } from '../providers/database/database';
import { MenuCardRecepcionComponent } from '../components/menu-card-recepcion/menu-card-recepcion';
import { MenuCardOtComponent } from '../components/menu-card-ot/menu-card-ot';
import { WebserviceProvider } from '../providers/webservice/webservice';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    RecepcionPage,
    RecepcionIngresoPage,
    OrdenTrabajoPage,
    OrdenTrabajoIngresoPage,
    ReportesPage,
    ModRecepcionEstadoPage,
    ModRecepcionDocumentacionPage,
    ModTareaPage,
    TareasPendientesPage,
    TareasRealizadasPage,
    TareasIniciarPage,
    MenuPrincipalComponent,
    MenuCardRecepcionComponent,  
    MenuCardOtComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp,
      {
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre' ],
        monthShortNames: ['ene', 'feb', 'mar',  'abr' , 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic' ],
        dayNames: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo' ],
        dayShortNames: ['lun', 'mar', 'mie' , 'jue', 'vie', 'sab', 'dom' ],
      }),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
      MyApp,
      LoginPage,
      HomePage,
      RecepcionPage,
      RecepcionIngresoPage,
      OrdenTrabajoPage,
      OrdenTrabajoIngresoPage,
      ReportesPage,
      TareasPendientesPage,
      TareasRealizadasPage,
      TareasIniciarPage,
      ModRecepcionEstadoPage,
      ModRecepcionDocumentacionPage,
      ModTareaPage,
      MenuPrincipalComponent,
      MenuCardRecepcionComponent,
      MenuCardOtComponent
    ],
    providers: [
      StatusBar,
      SplashScreen,
      SQLite,
      {provide: ErrorHandler, useClass: IonicErrorHandler},
      AutenticacionProvider,
      DatabaseProvider,
    WebserviceProvider
    ]
  })
export class AppModule {}