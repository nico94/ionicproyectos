import { DatabaseProvider } from './../providers/database/database';
import { LoginPage } from './../pages/login/login';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//PAGINAS
import { HomePage } from '../pages/home/home';
import { ReportesPage } from './../pages/reportes/reportes';
import { OrdenTrabajoPage } from './../pages/orden-trabajo/orden-trabajo';
import { RecepcionPage } from './../pages/recepcion/recepcion';
import { TareasPendientesPage } from '../pages/tareas-pendientes/tareas-pendientes';
import { TareasRealizadasPage } from '../pages/tareas-realizadas/tareas-realizadas';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  pages: Array<{ titulo: string, component: any, icono: string }>;
  paginasAdmin: any =[
    { titulo: 'Inicio', component: HomePage, icono: "home" },
    { titulo: 'Recepción vehículos', component: RecepcionPage, icono: "calendar" },
    { titulo: 'Orden trabajo vehículos', component: OrdenTrabajoPage, icono: "document" },
    { titulo: 'Reportes', component: ReportesPage, icono: "print" }
  ];
  paginasUsuario: any = [
    { titulo: 'Inicio', component: HomePage, icono: "home" },
    { titulo: 'Tareas pendientes', component: TareasPendientesPage, icono: "alarm" },
    { titulo: 'Tareas realizadas', component: TareasRealizadasPage, icono: "done-all" }
  ];
  rootPage: any;
  activePage: any;
  sqlite:any;
  usuarioMenu:string;
  perfilMenu:string;
  iconoUsuario: string ="./assets/imgs/man.png";
  esDispositivo:boolean;

  constructor(
    public platform: Platform,
    public eventos: Events,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public db: DatabaseProvider,
    public menu: MenuController
  ) {
    this.initializeApp();
    eventos.subscribe('obtenerUsuario',()=>{
      this.obtieneUsuario();
    });

    eventos.subscribe('inicializa_app',()=>{
      this.initializeApp();
    });

    eventos.subscribe('verifica_ruta',()=>{
      // console.log('Pasa por el verifica ruta');
      this.verificaUsuarioRuta();
    });
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.verificaUsuarioRuta();
      this.statusBar.backgroundColorByHexString("#264865");
      this.splashScreen.hide(); 
      this.verificaDispositivo();
    });
  }

  verificaDispositivo(){
    if (this.platform.is('cordova')) {
      this.esDispositivo = true;
      console.log('DISPOSITIVO');
    } else {
      this.esDispositivo = false;
      console.log('NAVEGADOR');
    }        
  }

  verificaUsuarioRuta(){
    let usuario;
    let cargo;
    usuario = parseInt(localStorage.getItem('login'));
    cargo = parseInt(localStorage.getItem('cod_cargo'));
    if (usuario) {
      switch (cargo) {
        case 11: {
          // console.log('CARGO: ES ADMIN');
          this.pages = this.paginasAdmin;
          break;
        }
        case 2: {
          // console.log('CARGO: USUARIO');
          this.pages = this.paginasUsuario;
          break;
        }
        default: {
          // console.log('CARGO: NO DEFINIDO');
          this.pages = [{ titulo: 'Inicio', component: HomePage, icono: "home" }];
          break;
        }
      }

      // console.log('USUARIO LOGUEADO : MENU HABILITADO');
      this.openPage(this.pages[0]);
      this.menu.enable(true);
    } else {
      // console.log('USUARIO NO LOGUEADO : MENU DESHABILIADO');
      this.nav.setRoot(LoginPage);
      this.menu.enable(false);
    }
  }
  
  obtieneUsuario(){
    this.usuarioMenu= null;
    this.perfilMenu= null;
    this.db.obtenerUsuario()
      .then(data => {
        this.usuarioMenu = data[0]['nombres'];
        this.perfilMenu = data[0]['des_cargo'];
      }).catch(error => {
        console.log('ERROR USUARIO BD');
        this.usuarioMenu = 'Usuario';
        this.perfilMenu = 'Perfil';
      });
  }

  openPage(page) {
    // console.log('ABRIR PAGINA '+JSON.stringify(page));
    this.nav.setRoot(page.component);
    this.activePage = page;
  }
  
  verificaPagina(page): boolean{
    return page === this.activePage;
  }
}