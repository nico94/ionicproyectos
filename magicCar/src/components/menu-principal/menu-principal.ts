import { DatabaseProvider } from './../../providers/database/database';
import { Component } from '@angular/core';
import { ViewController, NavController, AlertController, App, Platform, Events, ToastController } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';

@Component({
  selector: 'menu-principal',
  templateUrl: 'menu-principal.html'
})

export class MenuPrincipalComponent {
  items: any;
  rootPage:any = LoginPage;
  esDispositivo:boolean;

  constructor(
    public platform: Platform,
    public viewControl: ViewController,
    public nav:NavController,
    public app:App,
    public alertCtrl: AlertController,
    public db:DatabaseProvider,
    public eventos: Events,
    public toast: ToastController
  ){
    if (this.platform.is('cordova')) {
      this.esDispositivo = true;      
    } else {
      this.esDispositivo = false;      
    }  

    this.items=[ 
      { id:1, titulo: 'Cerrar sesión' }
    ]
  }

  clickItem(item){  
    console.log(item.id);
    switch (item.id){
      case 1:{
        this.mostrarAlerta();
        break;
      }
      default:{
        break;
      }
    }
    this.viewControl.dismiss(item);
  }

  mostrarAlerta(){
    let alert = this.alertCtrl.create({
      title: 'Cerrar sesión',
      message: '¿Está seguro que quiere cerrar la sesión',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancela');
          }
        },{
          text: 'Si',
          handler: () => {
            this.cerrarSesion();
          }
        }
      ]
    });
    alert.present();
  }
  
  cerrarSesion(){
    //Si es dipositivo elimina de SQLite
    if(this.esDispositivo){
      this.db.borrarUsuario().
        then(()=>{
          console.log('USUARIO ELIMINADO ');
          localStorage.removeItem('login');       
          localStorage.removeItem('cod_cargo');
          this.eventos.publish('verifica_ruta');         
        }).catch((error)=>{
          this.mostrarToast('Error al eliminar usuario de la Base de datos');
          console.log('ERROR AL ELIMINAR CS');
        });
      }else{
        console.log('CIERRA SESION EN NAVEGADOR');
        localStorage.removeItem('login');       
        localStorage.removeItem('cod_cargo'); 
        this.eventos.publish('verifica_ruta');         
    }
 //     this.app.getRootNav().setRoot(LoginPage);
  }

  mostrarToast(mensaje: string) {
    let toast = this.toast.create({
      message: mensaje,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}