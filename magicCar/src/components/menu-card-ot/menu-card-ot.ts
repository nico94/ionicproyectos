import { ViewController } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'menu-card-ot',
  templateUrl: 'menu-card-ot.html'
})
export class MenuCardOtComponent {
  items:any;

  constructor(
    public viewControl: ViewController
  ){
    this.items=[
      { id:1, titulo: 'Editar'},
      { id:2, titulo: 'Imprimir'}
    ]
  }

  clickItem(item){
    switch(item.id){
      case 1:{
        break;
      }
      case 2:{
        break;
      }
    }
    this.viewControl.dismiss(item);
  }
}