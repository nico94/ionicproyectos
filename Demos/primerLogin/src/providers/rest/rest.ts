import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class RestProvider {
  apiUrl: string = "http://186.10.19.170/wsJesusPons/Servicios/Usuario.svc/rest/login";

  constructor(public http: HttpClient) {
    console.log('Hello RestProvider Provider');
  }

  iniciarSesion(){
    return new Promise(
      (resolve,reject)=>{
      
      let body =
      {
        "usuario": {
            "IdUsuario": "admin",
            "PswUsuario": "21232F297A57A5A743894A0E4A801FC3"
          }
      };

      let headers = new HttpHeaders({
       'Content-Type': 'application/json'
      });

      this.http.post(this.apiUrl, body, { headers:headers })
      .subscribe(res=>{
        resolve(res);
        console.log(res);
      },(err)=>{
        reject(err);
        console.log(err);
       // reject(err);
      });
    });

    //return this.http.post(this.apiUrl,body, { headers : headers });
  }
}