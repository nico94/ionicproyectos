import { RestProvider } from './../../providers/rest/rest';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[
    RestProvider
  ]
})
export class HomePage {
  data:any[]=[];

  constructor(public navCtrl: NavController, 
    public  restService: RestProvider
  ) {
    this.inciarSesion();
  }

  inciarSesion(){
    // this.restService.iniciarSesion(null,null).then(()=>)
  }

}