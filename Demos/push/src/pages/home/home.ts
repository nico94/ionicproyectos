import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { SQLite } from 'ionic-native';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public alertCtrl: AlertController) {
      this.openMenu();
    }

  mostrarAlerta() {
    let alert = this.alertCtrl.create({
      title: 'Hola!',
      subTitle: 'Hola mundo!',
      buttons: ['OK']
    });
    alert.present();
  }

  openMenu() {
    this.menuCtrl.open();
  }

  doRefresh(refresher){
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

}
