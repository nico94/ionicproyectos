import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { FCM } from '@ionic-native/fcm';
import { LocalNotifications } from '@ionic-native/local-notifications';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(
    platform: Platform,
    private localNotifications: LocalNotifications,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    fcm: FCM
  ) {

    platform.ready().then(() => {
      fcm.subscribeToTopic('all');
      fcm.getToken().then(token => {      
        console.log(token);
      })
      fcm.onNotification().subscribe(data => {
        if (data.wasTapped) {
          console.log("Received in background");
          this.mostrarNot(data);
        } else {
          console.log("Received in foreground");
          this.mostrarNot(data);
        };
      })
      fcm.onTokenRefresh().subscribe(token => {
        console.log(token);
      });

      statusBar.styleDefault();
      splashScreen.hide();


    });    
  }

  mostrarNot(data:any){
    this.localNotifications.schedule({
      id: 1,
      text: 'Single ILocalNotification',
      sound: 'file://sound.mp3',
      data: data
    });
  }
}