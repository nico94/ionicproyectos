# IonicProyectos

####Para agregar la plataforma de android a IONIC
ionic cordova platform add android@6.4.0

###Para quitar la plataforma
ionic cordova platform remove android


####Para correr app en liveReload y con console log
ionic cordova run android -lcs

####Para generar pagina
ionic generate page [<nombre_pagina>]

####Para generar componente
ionic generate component [<nombre_componente>]

#### Iconos
ionic cordova platform rm android
ionic cordova platform add android@latest
ionic cordova resources
ionic cordova build android

#### Generar APK firmada
ionic cordova build --release android